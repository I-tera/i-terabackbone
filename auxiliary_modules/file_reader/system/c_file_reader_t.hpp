#ifndef C_FILE_READER_T_H
#define C_FILE_READER_T_H

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "json.hpp"
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include "c_module_base_t.hpp"

/// class c_file_reader_t
/// Class for generating Pseudo random bit sequence
/// Allows the user to set the seed, and uses the polynomial X^63 + X^62 + X^60 + X^59 + 1
/// It fills arrays of different types and sizes with the PRBS

template <int T_P, class T_BE_i, class T_BE_f>
class c_file_reader_t: public virtual c_module_base_t<T_P, T_BE_i, T_BE_f>
{
public:  
  typedef UME::SIMD::SIMDVec<T_BE_i, T_P> T_E_i;
private:
  FILE *src_file = nullptr;
  c_fifo_port_wr_t<T_P, T_BE_i> *wr_port_i;
  c_fifo_port_wr_t<T_P, T_BE_f> *wr_port_f;
  //ports configuration
  s_signal_config<T_P> *my_outputs;
  string filename;
public:
  c_file_reader_t()
  {
    wr_port_i = nullptr;
    wr_port_f = nullptr;
    my_outputs = nullptr;
  }
  ~c_file_reader_t(){free_resources();}
  
  void initialize(nlohmann::json config);
  void free_resources();

  void reset(){fclose(src_file); src_file = fopen(filename.c_str(), "r");}
  void set_static(const string &static_name, int32_t *static_values){}
  bool process();

  uint32_t        get_nof_probes(){return 0;}
  int32_t         get_probe_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_probe_data(uint32_t nof_probe){throw std::runtime_error("c_file_reader_t ERROR: module has no probes");/*s_signal_config<T_P> garbage; return garbage;*/}
  void            assign_probe_port(c_fifo_port_wr_t<T_P, int32_t> *port, uint32_t nof_probe){throw std::runtime_error("c_file_reader_t ERROR: module has no probes");}

  uint32_t        get_nof_input_float_ports(){return 0;}
  uint32_t        get_nof_input_int_ports(){return 0;}
  int32_t         get_input_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_input_data(uint32_t nof_probe = 0) const {throw std::runtime_error("c_file_reader_t ERROR: module has no input port");}
  void            assign_input_port(c_fifo_port_rd_t<T_P, T_BE_i> *port, uint32_t n_port = 0){throw std::runtime_error("c_file_reader_t ERROR: module has no input port");}
  void            assign_input_port(c_fifo_port_rd_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_file_reader_t ERROR: module has no input port");}
  
  uint32_t        get_nof_output_float_ports(){if (my_outputs[0].port_type == is_int)return 0;else return 1;}
  uint32_t        get_nof_output_int_ports()  {if (my_outputs[0].port_type == is_int)return 1;else return 0;}
  int32_t         get_output_number(const string &signal_name){return 0;}
  s_signal_config<T_P> get_output_data(uint32_t nof_probe = 0) const {if (nof_probe > 0){throw std::runtime_error("c_file_reader_t ERROR: invalid output port number");}return my_outputs[nof_probe];}
  void            assign_output_port(c_fifo_port_wr_t<T_P, T_BE_i> *port, uint32_t n_port){if ((my_outputs[0].port_type == is_int) && (n_port == 0)){wr_port_i = port;}else{throw std::runtime_error("c_file_reader_t ERROR: invalid output port number or type");}}
  void            assign_output_port(c_fifo_port_wr_t<T_P, T_BE_f> *port, uint32_t n_port = 0){if ((my_outputs[0].port_type == is_float) && (n_port == 0)){wr_port_f = port;}else{throw std::runtime_error("c_file_reader_t ERROR: invalid output port number or type");}}

  
  void            review_fsm_config(s_fsm_config *fsm_config, uint32_t nof_fsms){}
  void            review_port_config(s_signal_config<T_P> *i_ports,s_signal_config<T_P> *o_ports,s_signal_config<T_P> *p_ports){}

};

template <int T_P, class T_BE_i, class T_BE_f>
void c_file_reader_t<T_P, T_BE_i, T_BE_f>::initialize(nlohmann::json config)
{
  free_resources();

  try
    {
      filename = config["FileName"].get<string>();
      src_file = fopen(filename.c_str(), "r");
    }
  catch (...)
    {
      throw std::runtime_error("c_file_reader_t ERROR: problem with input file");
    }


  my_outputs = new s_signal_config<T_P>[1];
  my_outputs[0].name                  = "o_data";
  my_outputs[0].elemental_parallelism = 1;
  my_outputs[0].log_all               = true;
  my_outputs[0].port_type             = is_int;
  my_outputs[0].value_during_reset    = 0;
  my_outputs[0].fifo                  = nullptr;
  
  try {
    string data_type = config["DataType"].get<string>();
    if (data_type.compare("int") == 0)
      my_outputs[0].port_type             = is_int;
    else if (data_type.compare("float") == 0)
      my_outputs[0].port_type             = is_float;
    else
      throw  std::runtime_error("c_file_reader_t ERROR: unsupported DataType");
  }
  catch (...)
    {
      throw std::runtime_error("c_file_reader_t ERROR: problem with field DataType");
    }
      
  c_module_base_t<T_P, T_BE_i>::get_signal_info(config, &my_outputs[0], 1, "output");

  reset();
}

template <int T_P, class T_BE_i, class T_BE_f>
void c_file_reader_t<T_P, T_BE_i, T_BE_f>::free_resources()
{
  if (src_file != nullptr) {fclose(src_file); src_file = nullptr;}
  if (my_outputs != nullptr) delete[] my_outputs; my_outputs = nullptr;
}


template <int T_P, class T_BE_i, class T_BE_f>
bool c_file_reader_t<T_P, T_BE_i, T_BE_f>::process()
{
  bool progress = false;
  T_BE_i aux_4_read_i;
  T_BE_f aux_4_read_f;
  int nof_reads = 0;

  if (my_outputs[0].port_type == is_int)
    {
      if (wr_port_i->free_space() > 0)
        {
          do
            {
              nof_reads = fscanf(src_file, " %d ", &aux_4_read_i);
              if (nof_reads == 1)
                {
                  wr_port_i->write(aux_4_read_i);
                  progress = true;
                }
            }
          while ((nof_reads == 1) && (wr_port_i->free_space() > 0));
        }
    }
  else
    {
      if (wr_port_f->free_space() > 0)
        {
          do
            {
              nof_reads = fscanf(src_file, " %f ", &aux_4_read_f);
              if (nof_reads == 1)
                {
                  wr_port_f->write(aux_4_read_f);
                  progress = true;
                }
            }
          while ((nof_reads == 1) && (wr_port_f->free_space() > 0));
        }
    }
  
  
#ifdef D_IE_VERBOSE
  if (available_space == 0)
    printf(" no_space ");
  else if (nof_reads != 1)
    printf(" empty_file ");
#endif
  return progress;
}

      

#endif
