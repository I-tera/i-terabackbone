//#define D_DEBUGMODE

#define NElements  25000
#define NMessages  300
#define D_FIFO1_DEPTH 143
#define D_SIM_LENGTH 90

#define D_P 8
#define D_BE_i uint32_t
#define D_BE_f float

#include "c_file_reader_t.hpp"
#include "c_module_base_t.hpp"
#include "c_fifo_t.hpp"
#include "c_sink_t.hpp"

#include <stdio.h>
#include "json.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <fstream>


#include <chrono>

using namespace std;


int main(void)
{

  c_fifo_t<D_P, D_BE_i> FIFO_i;
  FIFO_i.initialize(D_FIFO1_DEPTH,
                    0,
                    "int_test_signal",
                    true,
                    "output_data_int.txt",
                    10,
                    0,
                    true);
  
  c_fifo_t<D_P, D_BE_f> FIFO_f;
  FIFO_f.initialize(D_FIFO1_DEPTH,
                    0,
                    "float_test_Signal",
                    true,
                    "output_data_float.txt",
                    8,
                    4,
                    true);
  
  c_module_base_t<D_P, D_BE_i, D_BE_f> *dut_i;
  c_module_base_t<D_P, D_BE_i, D_BE_f> *dut_f;
  dut_i = new c_file_reader_t<D_P, D_BE_i, D_BE_f>;
  dut_f = new c_file_reader_t<D_P, D_BE_i, D_BE_f>;

  std::ifstream i_file("../../conf/file_reader.json");
  nlohmann::json config;
  i_file >> config;

  config["DataType"] = "int";
  config["FileName"] = "int_input.txt";
  dut_i->free_resources();
  dut_i->initialize(config);
  dut_i->assign_output_port(FIFO_i.get_wr_port());
  dut_i->reset();

  
  config["DataType"] = "float";
  config["signals"][0]["nf_bits"] = 3;
  config["FileName"] = "flt_input.txt";
  dut_f->free_resources();
  dut_f->initialize(config);
  dut_f->assign_output_port(FIFO_f.get_wr_port());
  dut_f->reset();

  bool watchdog = true;
  uint32_t sim_counter = 0;
  while (watchdog && (sim_counter < D_SIM_LENGTH))
    {
      watchdog = false;
      watchdog |= dut_i->process();      
      watchdog |= dut_f->process();

      FIFO_i.run_logger();
      FIFO_f.run_logger();
      
      sim_counter ++;
    }
  
  printf("\nTEST COMPLETED\n");
  return 0;
}
  
