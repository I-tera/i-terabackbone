#ifndef C_PRBS_64_T_H
#define C_PRBS_64_T_H

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "json.hpp"
#include "UMESimd.h"
#include "c_channel_guru.hpp"
#include "dynamic_memory.hpp"
#include "c_module_base_t.hpp"

/// class c_prbs_64_t
/// Class for generating Pseudo random bit sequence
/// Allows the user to set the seed, and uses the polynomial X^63 + X^62 + X^60 + X^59 + 1
/// It fills arrays of different types and sizes with the PRBS

template <int T_P, class T_BE_i, class T_BE_f>
class c_prbs_64_t: public virtual c_module_base_t<T_P, T_BE_i, T_BE_f>
{
public:  
  typedef UME::SIMD::SIMDVec<T_BE_i, T_P> T_E_i;
private:
  char                *afa_0;
  T_E_i               *state;
  unsigned int         state_ptr;
  T_BE_i              *original_seeds;
  bool                 all_zeros;
  c_fifo_port_wr_t<T_P, T_BE_i> *wr_port;
  //ports configuration
  s_signal_config<T_P> *my_outputs;
public:
  c_prbs_64_t()
  {
    afa_0 = nullptr;
    state = nullptr;
    state_ptr = 0;
    original_seeds = nullptr;
    all_zeros = false;
    wr_port = nullptr;
    my_outputs = nullptr;
  }
  ~c_prbs_64_t(){free_resources();}
  
  void initialize(nlohmann::json config);
  void free_resources();

  void reset(){}
  void set_static(const string &static_name, int32_t *static_values){}
  bool process();

  uint32_t        get_nof_probes(){return 0;}
  int32_t         get_probe_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_probe_data(uint32_t nof_probe){throw std::runtime_error("c_prbs_64_t ERROR: module has no probes");/*s_signal_config<T_P> garbage; return garbage;*/}
  void            assign_probe_port(c_fifo_port_wr_t<T_P, int32_t> *port, uint32_t nof_probe){throw std::runtime_error("c_prbs_64_t ERROR: module has no probes");}

  uint32_t        get_nof_input_float_ports(){return 0;}
  uint32_t        get_nof_input_int_ports(){return 0;}
  int32_t         get_input_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_input_data(uint32_t nof_probe = 0) const {throw std::runtime_error("c_prbs_64_t ERROR: module has no input port");}
  void            assign_input_port(c_fifo_port_rd_t<T_P, T_BE_i> *port, uint32_t n_port = 0){throw std::runtime_error("c_prbs_64_t ERROR: module has no input port");}
  void            assign_input_port(c_fifo_port_rd_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_prbs_64_t ERROR: module has no input port");}
  
  uint32_t        get_nof_output_float_ports(){return 0;}
  uint32_t        get_nof_output_int_ports(){return 1;}
  int32_t         get_output_number(const string &signal_name){return 0;}
  s_signal_config<T_P> get_output_data(uint32_t nof_probe = 0) const {if (nof_probe > 0){throw std::runtime_error("c_prbs_64_t ERROR: invalid output port number");}return my_outputs[nof_probe];}
  void            assign_output_port(c_fifo_port_wr_t<T_P, T_BE_i> *port, uint32_t n_port){if (n_port == 0){wr_port = port;}else{throw std::runtime_error("c_prbs_64_t ERROR: invalid output port number");}}
  void            assign_output_port(c_fifo_port_wr_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_prbs_64_t ERROR: module has no float output port");}

  
  void            review_fsm_config(s_fsm_config *fsm_config, uint32_t nof_fsms){}
  void            review_port_config(s_signal_config<T_P> *i_ports,s_signal_config<T_P> *o_ports,s_signal_config<T_P> *p_ports){}

  T_E_i  calculate_prbs();
  void set_seed(T_BE_i *i_NewSeeds);
  void print_state();
};

template <int T_P, class T_BE_i, class T_BE_f>
void c_prbs_64_t<T_P, T_BE_i, T_BE_f>::initialize(nlohmann::json config)
{
  free_resources();
  
  original_seeds = new T_BE_i[T_P];
  afa_0 = allocate_aligned_memory<T_E_i>(64, 128, state, "c_prbs_64_t.state");
  try {all_zeros = (0 != (config["AllZeros"].get<int>()));}
  catch (...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.all_zeros\n");}
  if (!all_zeros)
    {
      int auxsize;
      try {auxsize = config["Seed"].size();}
      catch (...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.Seed\n");}
      if ((config["Seed"].size() <= 1) && (T_P == 1))
	{
	  T_BE_i auxSeeds[1];
	  try{auxSeeds[0] = config["Seed"].get<T_BE_i>();}
          catch(...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.Seed\n");}
	  set_seed(&auxSeeds[0]);
	}	
      else if (config["Seed"].size() == T_P)
	{	  
	  T_BE_i auxSeeds[T_P];
	  for (int i = 0; i < T_P; i++)
	    {
	      try{auxSeeds[i] = config["Seed"][i].get<T_BE_i>();}
              catch(...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.Seed\n");}
	    }
	  set_seed(&auxSeeds[0]);
	}			     
      else if (config["Seed"].size() <= 1)
	{
	  try{srand(config["Seed"].get<uint>());}
          catch(...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.Seed\n");}
	  T_BE_i auxSeeds[T_P];
	  try{auxSeeds[0] = config["Seed"].get<T_BE_i>();}
          catch(...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.Seed\n");}
#ifdef D_DEBUGMODE
	  for (int i = 0; i < T_P; i++)
	    {
              try{auxSeeds[i] = config["Seed"].get<T_BE_i>();}
              catch(...) {throw std::runtime_error(" c_prbs_64_t ERROR: problem with field SimulationProfile.PRBS.Seed\n");}
	    }
#else
	  for (int i = 1; i < T_P; i++)
            auxSeeds[i] = (T_BE_i)rand();
#endif	    
	  set_seed(&auxSeeds[0]);
	}
      else
	{
	  throw std::runtime_error(" ERROR: c_prbs_64_t: not supported seed method");
	}
    }

  
  my_outputs = new s_signal_config<T_P>[1];
  my_outputs[0].name                  = "o_data";
  my_outputs[0].elemental_parallelism = 1;
  my_outputs[0].log_all               = true;
  my_outputs[0].port_type             = is_int;
  my_outputs[0].value_during_reset    = 0;
  my_outputs[0].fifo                  = nullptr;
  c_module_base_t<T_P, T_BE_i>::get_signal_info(config, &my_outputs[0], 1, "output");

  reset();
}

template <int T_P, class T_BE_i, class T_BE_f>
void c_prbs_64_t<T_P, T_BE_i, T_BE_f>::free_resources()
{
  if (state != nullptr)
    delete[] afa_0;
  state = nullptr;
  if (original_seeds != nullptr)
    delete[] original_seeds;
  original_seeds = nullptr;
  
  if (my_outputs != nullptr) delete[] my_outputs; my_outputs = nullptr;
}

template <int T_P, class T_BE_i, class T_BE_f>
void c_prbs_64_t<T_P, T_BE_i, T_BE_f>::set_seed(T_BE_i *i_NewSeeds)
{
  for (int i = 0; i < T_P; i++)
    original_seeds[i] = i_NewSeeds[i];

  T_E_i aux;
  aux.load(&original_seeds[0]);
  int auxThreshold = (sizeof(T_BE_i)*8 < 64) ? sizeof(T_BE_i)*8 : 64;
  for (int i = 0; i < auxThreshold; i++)
    {
      state[i] = aux>>(i) & 0x01;
    }
  for (int i = auxThreshold; i < 64; i++)
    {
      state[i] = 0;
    }
  state_ptr = 0; 
  
  for (int i = 0; i < 5000; i++)
    {
      calculate_prbs();
    }      
}

template <int T_P, class T_BE_i, class T_BE_f>
bool c_prbs_64_t<T_P, T_BE_i, T_BE_f>::process()
{
  uint32_t available_space = wr_port->free_space();
  for (uint32_t i = 0; i < available_space; i++)
    {
      state_ptr= (state_ptr - 1)%64;
      state[state_ptr] = (state[(state_ptr+63)%64] ^
                          state[(state_ptr+62)%64] ^
                          state[(state_ptr+60)%64] ^
                          state[(state_ptr+59)%64]);
      wr_port->write(state[state_ptr]);
    }
#ifdef D_IE_VERBOSE
  if (available_space == 0)
    printf(" no_space ");
#endif
  return available_space > 0;
}

      
template <int T_P, class T_BE_i, class T_BE_f>
typename c_prbs_64_t<T_P, T_BE_i, T_BE_f>::T_E_i c_prbs_64_t<T_P, T_BE_i, T_BE_f>::calculate_prbs()
{
  state_ptr= (state_ptr - 1)%64;
  state[state_ptr] = (state[(state_ptr+63)%64] ^
                      state[(state_ptr+62)%64] ^
                      state[(state_ptr+60)%64] ^
                      state[(state_ptr+59)%64]);
  return state[state_ptr];
}
	



template <int T_P, class T_BE_i, class T_BE_f>
void c_prbs_64_t<T_P, T_BE_i, T_BE_f>::print_state()
{
  printf("\n c_prbs_64_t Information");
 
  uint64_t auxXprint[T_P];
  for (int i = 0; i < T_P; i++)
    auxXprint[i] = 0;

  T_BE_i auxXextract[T_P];
  for (int j = 0; j < 64; j++)
    {
      state[(state_ptr+j)%64].store(&auxXextract[0]);
      for (int i = 0; i < T_P; i++)
	{
	  uint64_t auxXrotate = auxXextract[i];
	  auxXprint[i] |= auxXrotate<<j;
	}
    }
  
  for (int i = 0; i < T_P; i++)
    printf("\n Current State [%d] = %lu", i, auxXprint[i]);
   
}

#endif
