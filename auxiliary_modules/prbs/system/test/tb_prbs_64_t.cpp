//#define D_DEBUGMODE

#define NElements  25000
#define NMessages  300
#define D_FIFO1_DEPTH 143
#define D_SIM_LENGTH 90

#define D_P 8
#define D_BE uint32_t

#include "c_prbs_64_t.hpp"
#include "c_module_base_t.hpp"
#include "c_fifo_t.hpp"
#include "c_sink_t.hpp"

#include <stdio.h>
#include "json.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <fstream>


#include <chrono>

using namespace std;


int main(void)
{

  c_fifo_t<D_P, D_BE> FIFO1(D_FIFO1_DEPTH, 1);
  c_sink_t<D_P, D_BE> SINK1("Results/prbs_output.txt", 1, 0, false, false);
  SINK1.assign_input_port(FIFO1.get_rd_port());

  /*  c_prbs_64_t<D_P, D_BE> my_prbs;
      c_module_base_t<D_P, D_BE> *dut = &my_prbs;*/
  c_module_base_t<D_P, D_BE, float> *dut;
  dut = new c_prbs_64_t<D_P, D_BE, float>;

  
  std::ifstream i_file("TB_Input.json");
  nlohmann::json config;
  i_file >> config;

  dut->free_resources();
  dut->initialize(config);
  dut->assign_output_port(FIFO1.get_wr_port());
  dut->reset();

  bool watchdog = true;
  uint32_t sim_counter = 0;
  while (watchdog && (sim_counter < D_SIM_LENGTH))
    {
      watchdog = false;
      watchdog |= dut->process();
      watchdog |= SINK1.process();
      sim_counter ++;
    }

  printf("\nTEST COMPLETED\n");
  return 0;
}
  
