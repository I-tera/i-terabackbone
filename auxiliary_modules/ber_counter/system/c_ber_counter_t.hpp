#ifndef C_BER_COUNTER_T_H
#define C_BER_COUNTER_T_H

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "json.hpp"
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include "c_module_base_t.hpp"

//#define D_DEBUGMODE
using namespace std;

template <int T_P, class T_BE_i, class T_BE_f>
class c_ber_counter_t: public virtual c_module_base_t<T_P, T_BE_i, T_BE_f>
{
public:  
  typedef UME::SIMD::SIMDVec<T_BE_i, T_P> T_E_i;
private:
  string name;
  uint64_t bits_to_ignore;
  uint64_t ignored_bits_counter;
  c_fifo_port_rd_t<T_P, T_BE_i> *i_port_0;
  c_fifo_port_rd_t<T_P, T_BE_i> *i_port_1;
  UME::SIMD::SIMDVec<uint64_t, T_P> bit_counter;
  UME::SIMD::SIMDVec<uint64_t, T_P> bit_error_counter;
  s_signal_config<T_P> *my_inputs;
 public:
  void set_name(const string &i_name){name = i_name;}
  string get_name(){return name;}
  c_ber_counter_t() {ignored_bits_counter = 0;bits_to_ignore = 0; name = "";i_port_0 = nullptr;i_port_1 = nullptr; reset();my_inputs   =nullptr;}
  ~c_ber_counter_t() {free_resources();}

  void initialize(nlohmann::json config);
  void free_resources(){if (my_inputs != nullptr){delete[] my_inputs;my_inputs = nullptr;}}

  void reset(){ignored_bits_counter = 0;bit_counter = 0; bit_error_counter = 0;}
  void set_static(const string &static_name, int32_t *static_values){if (static_name.compare("bits2ignore") == 0) {bits_to_ignore = static_values[0];}}
  bool process();

  uint32_t             get_nof_probes(){return 0;}
  int32_t              get_probe_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_probe_data(uint32_t nof_probe){throw std::runtime_error("c_ber_counter_t ERROR: module has no probes");/*s_signal_config<T_P> garbage; return garbage;*/}
  void                 assign_probe_port(c_fifo_port_wr_t<T_P, int32_t> *port, uint32_t nof_probe){throw std::runtime_error("c_ber_counter_t ERROR: module has no probes");}

  uint32_t             get_nof_input_float_ports(){return 0;}
  uint32_t             get_nof_input_int_ports(){return 2;}
  int32_t              get_input_number(const string &signal_name);
  s_signal_config<T_P> get_input_data(uint32_t nof_probe = 0) const {if (nof_probe >= 2) {throw std::runtime_error("c_ber_counter_t ERROR: invalid input port number");} else {return my_inputs[nof_probe];}}
  void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_i> *port, uint32_t n_port = 0){if(n_port==0){i_port_0 = port;}else if (n_port==1){i_port_1=port;}else{throw std::runtime_error("c_ber_counter_t ERROR: invalid input port number");}}
  void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_ber_counter_t ERROR: module has no input float port");}
  
  uint32_t             get_nof_output_float_ports(){return 0;}
  uint32_t             get_nof_output_int_ports(){return 0;}
  int32_t              get_output_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_output_data(uint32_t nof_probe = 0) const {throw std::runtime_error("c_ber_counter_t ERROR: module has no outputs");}
  void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_i> *port, uint32_t n_port){throw std::runtime_error("c_ber_counter_t ERROR: module has no outputs");}
  void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_ber_counter_t ERROR: module has no outputs");}

  void review_fsm_config(s_fsm_config *fsm_config, uint32_t nof_fsms){}
  void review_port_config(s_signal_config<T_P> *i_ports,
                          s_signal_config<T_P> *o_ports,
                          s_signal_config<T_P> *p_ports){};

  
  UME::SIMD::SIMDVec<uint64_t, T_P> get_bit_counter_v(){return bit_counter;}
  UME::SIMD::SIMDVec<uint64_t, T_P> get_bit_error_counter_v(){return bit_error_counter;}
  uint64_t get_bit_counter(){return vector_to_scalar(bit_counter);}
  uint64_t get_bit_error_counter(){return vector_to_scalar(bit_error_counter);}
private:
  uint64_t vector_to_scalar(UME::SIMD::SIMDVec<uint64_t, T_P> vector);
};


template <int T_P, class T_BE_i, class T_BE_f>
int32_t c_ber_counter_t<T_P, T_BE_i, T_BE_f>::get_input_number(const string &signal_name)
{
  for (int32_t i = 0; i < 2; i++)
    {
      if (signal_name.compare((get_input_data(i)).name) == 0)
	return i;
    }
  return -1;
}

template <int T_P, class T_BE_i, class T_BE_f>
void c_ber_counter_t<T_P, T_BE_i, T_BE_f>::initialize(nlohmann::json config)
{
  free_resources();

  my_inputs = new s_signal_config<T_P>[2];
  my_inputs[0].name                  = "i_data_0";
  my_inputs[0].elemental_parallelism = 1;
  my_inputs[0].log_all               = true;
  my_inputs[0].fifo                  = nullptr;
  my_inputs[0].port_type             = is_int;
  my_inputs[1].name                  = "i_data_1";
  my_inputs[1].elemental_parallelism = 1;
  my_inputs[1].log_all               = true;
  my_inputs[1].fifo                  = nullptr;
  my_inputs[1].port_type             = is_int;
  c_module_base_t<T_P, T_BE_i>::get_signal_info(config, &my_inputs[0], 2, "input");
  
  reset();
}

template <int T_P, class T_BE_i, class T_BE_f>
uint64_t c_ber_counter_t<T_P, T_BE_i, T_BE_f>::vector_to_scalar(UME::SIMD::SIMDVec<uint64_t, T_P> vector)
{
  uint64_t scalar = 0;
  uint64_t auxForUnpack [T_P];
  vector.store(&auxForUnpack[0]);
  for (uint32_t i = 0; i < T_P; i++)
    scalar += auxForUnpack[i];
  return scalar;
}


template <int T_P, class T_BE_i, class T_BE_f>
bool c_ber_counter_t<T_P, T_BE_i, T_BE_f>::process()//add_noise_complex(T_E_i* real, T_E_i* imag, T_E_i sigma, uint64_t NSymbols)
{
  UME::SIMD::SIMDVec<T_BE_i, T_P> a, b;
  uint32_t n_bits = i_port_0->available_data();
  if (n_bits > i_port_1->available_data())
    n_bits = i_port_1->available_data();

  if (ignored_bits_counter + n_bits < bits_to_ignore)
    {
      ignored_bits_counter += n_bits;
    }
  else if (ignored_bits_counter >= bits_to_ignore)
    {
      for (uint32_t i = 0; i < n_bits; i++)
        {
          bit_counter = bit_counter + 1;
          bit_error_counter[!((*i_port_0)[i]==(*i_port_1)[i])] = bit_error_counter + 1;
        }
    }
  else
    {
      for (uint32_t i = 0; i < n_bits; i++)
        {
          if (ignored_bits_counter < bits_to_ignore)
            ignored_bits_counter ++;
          else
            {
              bit_counter = bit_counter + 1;
              bit_error_counter[!((*i_port_0)[i]==(*i_port_1)[i])] = bit_error_counter + 1;
            }
        }
    }
  i_port_0->move_pointer(n_bits);
  i_port_1->move_pointer(n_bits);
  
#ifdef D_IE_VERBOSE
  printf(" p0 [%6d], p1[%6d]", i_port_0->available_data(), i_port_1->available_data());
#endif
  return n_bits > 0;
}

#endif
