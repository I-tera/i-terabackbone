#ifndef C_SINK_T_H
#define C_SINK_T_H

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <stdint.h>
#include <time.h>
#include "c_module_base_t.hpp"
#include "json.hpp"
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include "c_vector_logger_t.hpp"

/// class c_sink_t
/// Class for generating Pseudo random bit sequence
/// Allows the user to set the seed, and uses the polynomial X^63 + X^62 + X^60 + X^59 + 1
/// It fills arrays of different types and sizes with the PRBS

template <int T_P, class T_BE_i, class T_BE_f>
class c_sink_t: public virtual c_module_base_t<T_P, T_BE_i, T_BE_f>
{
private:
  typedef UME::SIMD::SIMDVec<T_BE_i, T_P> T_E_i;
  c_fifo_port_rd_t<T_P, T_BE_i> *rd_port;
  s_signal_config<T_P> *my_inputs;
public:
  c_sink_t(){rd_port = nullptr; my_inputs = nullptr;}
  ~c_sink_t(){free_resources();}
  
  void initialize(nlohmann::json config);
  void free_resources(){if (my_inputs != nullptr){delete[] my_inputs;my_inputs = nullptr;}}
  
  void reset(){}
  void set_static(const string &static_name, int32_t *static_values){}
  bool process();

  uint32_t             get_nof_probes(){return 0;}
  int32_t              get_probe_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_probe_data(uint32_t nof_probe){throw std::runtime_error("c_sink_t ERROR: module has no probes");/*s_signal_config<T_P> garbage; return garbage;*/}
  void                 assign_probe_port(c_fifo_port_wr_t<T_P, int32_t> *port, uint32_t nof_probe){throw std::runtime_error("c_ber_counter_t ERROR: module has no probes");}

  uint32_t             get_nof_input_float_ports(){return 0;}
  uint32_t             get_nof_input_int_ports(){return 1;}
  int32_t              get_input_number(const string &signal_name){return 0;}
  s_signal_config<T_P> get_input_data(uint32_t nof_probe = 0) const {if (nof_probe >= 1) {throw std::runtime_error("c_sink_t ERROR: invalid input port number");} else {return my_inputs[nof_probe];}}
  void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_i> *port, uint32_t n_port = 0){if(n_port==0){rd_port = port;}else{throw std::runtime_error("c_sink_t ERROR: invalid input port number");}}
  void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_sink_t ERROR: module has no input float port");}
  
  uint32_t             get_nof_output_float_ports(){return 0;}
  uint32_t             get_nof_output_int_ports(){return 0;}
  int32_t              get_output_number(const string &signal_name){return -1;}
  s_signal_config<T_P> get_output_data(uint32_t nof_probe = 0) const {throw std::runtime_error("c_sink_t ERROR: module has no outputs");}
  void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_i> *port, uint32_t n_port){throw std::runtime_error("c_sink_t ERROR: module has no outputs");}
  void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_f> *port, uint32_t n_port = 0){throw std::runtime_error("c_sink_t ERROR: module has no outputs");}

  void review_fsm_config(s_fsm_config *fsm_config, uint32_t nof_fsms){}
  void review_port_config(s_signal_config<T_P> *i_ports,
                          s_signal_config<T_P> *o_ports,
                          s_signal_config<T_P> *p_ports){};

};


template <int T_P, class T_BE_i, class T_BE_f>
void c_sink_t<T_P, T_BE_i, T_BE_f>::initialize(nlohmann::json config)
{
  free_resources();

  my_inputs = new s_signal_config<T_P>[1];
  my_inputs[0].name                  = "i_data";
  my_inputs[0].elemental_parallelism = 1;
  my_inputs[0].log_all               = true;
  my_inputs[0].fifo                  = nullptr;
  my_inputs[0].port_type             = is_int;
  c_module_base_t<T_P, T_BE_i>::get_signal_info(config, &my_inputs[0], 1, "input");
  
  reset();
}

template <int T_P, class T_BE_i, class T_BE_f>
bool c_sink_t<T_P, T_BE_i, T_BE_f>::process()
  {
    bool res = false;
    T_E_i aux;
    while(rd_port->available_data() > 0)
      {
        res=true;
        aux = rd_port->read();
      }
    return res;
  }

#endif
