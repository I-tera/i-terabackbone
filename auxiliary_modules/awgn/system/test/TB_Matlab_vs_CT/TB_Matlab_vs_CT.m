%% IMPORTANT
%run matlab with the following command
% LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6 matlab
%%
tic
p = genpath('../../../../../');   
addpath(p)

clear
clc
SimLength = 1000;
NSims     = 100;
HistRange = -2:0.01:2;

mex CXXFLAGS='$CXXFLAGS -std=c++11 -march=native -DBST=1024' CXXOPTIMFLAGS='-O3 -DNDEBUG' LDOPTIMFLAGS='-O3'  ...
    ../../c_awgn_t_wrapper.cpp  ...
    -I"../../../../../libs/dynamic_memory" ...
    -I"../../../../../libs/umesimd" ...
    -I"../../../../../libs/json" ...
    -I"../../../../../libs/vector_logger" ...
    -I"../../../../../libs/fxp" ...
    -I"../../../../../fifo" ...
    -I"../../../../../ifc_sync_valid/" ...
    -I"../../../../../ifc_sync_valid/fsms" ...
    -I"../../../../../"


Modulations = [{'QPSK'}, {'QAM8STAR'}, {'QAM16'}];


for idx_mod = 1: length(Modulations)
%for  idx_mod = 3:3   
    myHist = 0*HistRange;
    fprintf("\n Testing AWGN for %s", char(Modulations(idx_mod)));
    
    jsonname = char(sprintf("Test_%s.json", char(Modulations(idx_mod))));
    
    AuxRes = jsondecode(fileread(jsonname));
    myRef = C_ConstellationHandler(char(Modulations(idx_mod)), ...
                                   AuxRes.SimulationProfile.SNRType, ...
                                   AuxRes.SimulationProfile.SNRStart, ...
                                   AuxRes.SimulationProfile.BaudRate, ...
                                   AuxRes.SimulationProfile.nPolarizations, ...
                                   'Rubbish');
    
    [~, idx] = max(abs(myRef.Constellation));
    c_awgn_t_wrapper('Initialize', jsonname);
    
    for simidx = 1 : NSims
        i_I = ones(1,SimLength) * real(myRef.Constellation(idx));
        i_Q = ones(1,SimLength) * imag(myRef.Constellation(idx));

        [o_I, o_Q] = c_awgn_t_wrapper('AddNoise', i_I.', i_Q.', myRef.Sigma);

        myTest = o_I + 1i*o_Q;
    %     
    %     figure
    %     plot(myTest, '.');
    %     hold on    
    %     plot(myRef.Constellation, 'ro');

        myHist = myHist + hist(real(myTest-myRef.Constellation(idx)), HistRange);
    end
    
    idealHist = normpdf(HistRange, 0, myRef.Sigma);
    idealHist = idealHist / sum(idealHist) * SimLength * NSims;
    
    fig = figure;
    semilogy(HistRange, myHist/(SimLength * NSims), 'bx')
    hold on
    semilogy(HistRange, idealHist/(SimLength * NSims), 'k-');
    axis([-2 2 1/(SimLength * NSims) 1])
    ylabel('Occurrences');
    xlabel('Amplitude');
    title(char(sprintf('AWGN evaluation for %s with %e symbols', char(Modulations(idx_mod)), SimLength * NSims)));
    grid on
    saveas(fig, char(sprintf("Test_%s.png", char(Modulations(idx_mod)))));
    
    
%     
%     error = sum(abs(myRef.Constellation.' - myTest));
%     if (error > 1e-5)
%         fprintf(":------> ERRORS \n");
%         for idx_const = 1: length(bitstream_dec)
%             fprintf("\n %4d: reference = %+f %+fi, DUT = %+f %+fi, error = %+f %+fi", ...
%                         bitstream_dec(idx_const), ...
%                         real(myRef.Constellation(idx_const)), ...
%                         imag(myRef.Constellation(idx_const)), ...
%                         real(myTest(idx_const)), ...
%                         imag(myTest(idx_const)), ...
%                         real(myRef.Constellation(idx_const) - myTest(idx_const)), ...
%                         imag(myRef.Constellation(idx_const) - myTest(idx_const)));
%            
%         end
%         fprintf("\n");
%         %disp([bitstream_dec.', myRef.Constellation.', myTest, myRef.Constellation.' - myTest]);
%     else
%         fprintf(": no errors\n ");
%     end
end
    
    
