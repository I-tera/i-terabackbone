import sys
import os
stored=os.path.dirname(os.path.abspath(__file__))
path='/'.join(stored.split("/")[:-2])
sys.path.insert(1,path)


from awgn_sync_valid import py_ie_module
import numpy as np

json_file_name   = str.encode((stored+'/../../../conf/awgn.json'))
#json_module_name = b'Mapper'

print(json_file_name)

my_dut = py_ie_module()

my_dut.initialize(json_file_name)
my_dut.send_static("sigma", np.array([1000000], dtype='int32'))
i_dict = {}
i_dict['i_I']         = np.array([1], dtype='int32')
i_dict['i_Q']         = np.array([0], dtype='int32')
i_dict['i_valid']     = np.array([1], dtype='int32')
i_dict['i_sync']      = np.array([0], dtype='int32')
i_dict['rst_async_n'] = np.array([0], dtype='int32')
o_dict = my_dut.run(i_dict)

i_dict['rst_async_n'] = np.array([1], dtype='int32')
i_dict['i_valid']     = np.array([1], dtype='int32')
i_dict['i_sync']      = np.array([1], dtype='int32')
o_dict = my_dut.run(i_dict)

i_dict['rst_async_n'] = np.array([1], dtype='int32')
i_dict['i_valid']     = np.array([1], dtype='int32')
i_dict['i_sync']      = np.array([0], dtype='int32')
o_dict = my_dut.run(i_dict)
print(o_dict)

i_dict['rst_async_n'] = np.array([1], dtype='int32')
i_dict['i_valid']     = np.array([1], dtype='int32')
i_dict['i_sync']      = np.array([0], dtype='int32')
o_dict = my_dut.run(i_dict)
print(o_dict)
i_dict['rst_async_n'] = np.array([1], dtype='int32')
i_dict['i_valid']     = np.array([1], dtype='int32')
i_dict['i_sync']      = np.array([0], dtype='int32')
o_dict = my_dut.run(i_dict)
print(o_dict)
i_dict['rst_async_n'] = np.array([1], dtype='int32')
i_dict['i_valid']     = np.array([1], dtype='int32')
i_dict['i_sync']      = np.array([0], dtype='int32')
o_dict = my_dut.run(i_dict)
print(o_dict)

