from distutils.core      import setup
from distutils.extension import Extension
from Cython.Build        import cythonize
import os


os.system("ln -s ../../../ifc_sync_valid/c_ie_module.pxd awgn_sync_valid.pxd")
os.system("ln -s ../../../ifc_sync_valid/c_ie_module.pyx awgn_sync_valid.pyx")
os.system("rm *.so")

ext_modules = Extension(
    "awgn_sync_valid",   # Module name as it would be imported in python      
    ["awgn_sync_valid.pyx"],
    language="c++",
    extra_compile_args=["-std=c++11", "-march=native" ,"-O0",
                        "-I../../../libs/json/",
                        "-I../../../libs/umesimd/",
                        "-I../../../libs/fxp/",
                        "-I../../../libs/dynamic_memory/",
                        "-I../../../libs/channel_guru/",
                        "-I../../../libs/vector_logger/",
                        "-I../../../libs/GF/",
                        "-I../../../",
                        "-I../../../fifo/",
                        "-I../../../ifc_sync_valid",
                        "-I../../../ifc_sync_valid/fsms",
                        "-I./",
                        "-DD_T_P=8",
                        "-DD_T_BE_i=uint32_t",
                        "-DD_T_BE_f=float",
                        "-DD_FILE_TO_INCLUDE=\"c_awgn_t.hpp\"",
                        "-DD_DUT_T=c_awgn_t<D_T_P, D_T_BE_i, D_T_BE_f>"],
    extra_link_args=["-std=c++11"]
)

setup(ext_modules=cythonize(ext_modules))

os.system("rm awgn_sync_valid.pxd")
os.system("rm awgn_sync_valid.pyx")
os.system("rm -r build")
os.system("rm -r awgn_sync_valid.cpp")

