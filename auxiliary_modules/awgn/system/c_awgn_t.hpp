#ifndef C_AWGN_T_H
#define C_AWGN_T_H

#include <stdalign.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "json.hpp"
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include "c_module_base_t.hpp"

//#define D_DEBUGMODE
using namespace std;

template <int T_P, class T_BE_i, class T_BE_f>
class c_awgn_t: public c_module_base_t<T_P, T_BE_i, T_BE_f>
{
public:  
  typedef UME::SIMD::SIMDVec<T_BE_i, T_P> T_E_i;
  typedef UME::SIMD::SIMDVec<T_BE_f, T_P> T_E_f;
  typedef T_BE_i T_BE_int;
  typedef T_BE_f T_BE_float;
  const static int T_Parallelism = T_P;
private:
  string name;
  uint64_t changui;
  uint64_t symbol_counter;
  c_fifo_port_rd_t<T_P, T_BE_f> *i_port_R;
  c_fifo_port_rd_t<T_P, T_BE_f> *i_port_I;
  c_fifo_port_wr_t<T_P, T_BE_f> *o_port_R;
  c_fifo_port_wr_t<T_P, T_BE_f> *o_port_I;
  T_BE_f                         sigma;
  T_BE_f                         noise_power;
  char                                           *afa_0;
  UME::SIMD::SIMDVec<uint64_t, T_P> *xoshiro;
  char *afa_1;
  UME::SIMD::SIMDVecMask<      T_P>  *mask_0101;
  uint64_t                          *original_seeds;
  inline  UME::SIMD::SIMDVec<uint64_t, T_P> rotl(const UME::SIMD::SIMDVec<uint64_t, T_P> x, int k) {return (x << k) | (x >> (64-k));}
  //ports configuration
  s_signal_config<T_P> *my_inputs;
  s_signal_config<T_P> *my_outputs;

  
  char *afa_2=nullptr;
  T_E_f                *S_pwr_acc_1e0=nullptr;
  char *afa_3=nullptr;
  T_E_f                *S_pwr_acc_1e3=nullptr;
  char *afa_4=nullptr;
  T_E_f                *S_pwr_acc_1e6=nullptr;
  char *afa_5=nullptr;
  T_E_f                *S_pwr_acc_1e9=nullptr;
  float                S_pwr_cnt_1e0;
  float                S_pwr_cnt_1e3;
  float                S_pwr_cnt_1e6;
  float                S_pwr_cnt_1e9;
  bool                 measure_power = false;
 public:
  void set_name(const string &i_name){name = i_name;}
  string get_name(){return name;}
  c_awgn_t()
  {
    name = "";
    changui = 0;
    symbol_counter = 0;
    i_port_R = nullptr;
    i_port_I = nullptr;
    o_port_R = nullptr;
    o_port_I = nullptr;
    sigma = 0;
    noise_power = 0;
    afa_0 = nullptr;
    xoshiro = nullptr;
    afa_1 = nullptr;
    mask_0101 = nullptr;
    original_seeds = nullptr;
    my_inputs = nullptr;
    my_outputs = nullptr;
    measure_power = false;
    afa_2=nullptr;
    S_pwr_acc_1e0=nullptr;
    afa_3=nullptr;
    S_pwr_acc_1e3=nullptr;
    afa_4=nullptr;
    S_pwr_acc_1e6=nullptr;
    afa_5=nullptr;
    S_pwr_acc_1e9=nullptr;
  };
  ~c_awgn_t() {free_resources();}

  void initialize(nlohmann::json config);
  void free_resources()
  {
    if (xoshiro != nullptr)          {delete[] afa_0; xoshiro = nullptr;}
    if (mask_0101 != nullptr)        {delete[] afa_1; mask_0101 = nullptr;}
    if (original_seeds!=nullptr)     {delete[] original_seeds;original_seeds=nullptr;}
    if (my_inputs        != nullptr) {delete[] my_inputs;  my_inputs   = nullptr;}
    if (my_outputs       != nullptr) {delete[] my_outputs; my_outputs  = nullptr;}
    if (S_pwr_acc_1e0 != nullptr) {delete[] afa_2; S_pwr_acc_1e0 = nullptr;}
    if (S_pwr_acc_1e3 != nullptr) {delete[] afa_3; S_pwr_acc_1e3 = nullptr;}
    if (S_pwr_acc_1e6 != nullptr) {delete[] afa_4; S_pwr_acc_1e6 = nullptr;}
    if (S_pwr_acc_1e9 != nullptr) {delete[] afa_5; S_pwr_acc_1e9 = nullptr;}
  }

  void reset(){reset_power_meter(); symbol_counter = 0;}
  bool process();

  uint32_t             get_nof_probes(){return 0;}
  int32_t              get_probe_number(const string &signal_name){return -1;};
  s_signal_config<T_P> get_probe_data(uint32_t nof_probe){throw std::runtime_error("c_awgn_t ERROR: module has no probes");}
  void                 assign_probe_port(c_fifo_port_wr_t<T_P, int32_t> *port, uint32_t nof_probe){throw std::runtime_error("c_awgn_t ERROR: module has no probes");}

  uint32_t             get_nof_input_float_ports(){return 2;}
  uint32_t             get_nof_input_int_ports(){return 0;}
  int32_t              get_input_number(const string &signal_name);
  s_signal_config<T_P> get_input_data(uint32_t nof_probe)const {if (nof_probe > 2){throw std::runtime_error("c_awgn_t ERROR: number of input port exceeds input port count");} return my_inputs[nof_probe];}
  void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_i> *port, uint32_t n_port = 0){throw std::runtime_error("c_awgn_t ERROR: this module doesn't have int input ports");}
  void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_f> *port, uint32_t n_port){if (n_port==0){i_port_R = port;}else if (n_port==1) {i_port_I = port;} else{throw std::runtime_error("c_awgn_t ERROR: input float port number too big");}}
  
  uint32_t             get_nof_output_float_ports(){return 2;}
  uint32_t             get_nof_output_int_ports(){return 0;}
  int32_t              get_output_number(const string &signal_name);
  s_signal_config<T_P> get_output_data(uint32_t nof_probe = 0)const {if (nof_probe > 2){throw std::runtime_error("c_awgn_t ERROR: port index too big");}return my_outputs[nof_probe];}
  void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_i> *port, uint32_t n_port = 0){throw std::runtime_error("c_awgn_t ERROR: this module doesn't have int output ports");}
  void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_f> *port, uint32_t n_port){if (n_port==0){o_port_R =  port;}else if (n_port ==1){o_port_I = port;} else{throw std::runtime_error("c_awgn_t ERROR: output float port number too big");}}
  
  void                 set_sigma(T_BE_f i_sigma){sigma = i_sigma;}
  
  void set_static(const string &static_name, int32_t *static_values)
  {
    if      (static_name.compare("changui") == 0) {changui = static_values[0];}
    else if (static_name.compare("sigma"  ) == 0) {set_sigma(float(static_values[0])/1000000.0);}
  }
  void                 review_fsm_config(s_fsm_config *fsm_config, uint32_t nof_fsms){}
  void                 review_port_config(s_signal_config<T_P> *i_ports,s_signal_config<T_P> *o_ports,s_signal_config<T_P> *p_ports){}
private:
  void set_seed(uint64_t *i_NewSeeds);
  void run_xoshiro(uint64_t nRuns);
  void jump_xoshiro(UME::SIMD::SIMDVecMask<T_P> UpdMask);
  UME::SIMD::SIMDVec<float, T_P>    new_xoshiro_float_0_1();
  UME::SIMD::SIMDVec<float, T_P>    vfastlog2(UME::SIMD::SIMDVec<float, T_P> x);
  UME::SIMD::SIMDVec<float, T_P>    vfastlog(UME::SIMD::SIMDVec<float, T_P> x);
public:
  void reset_power_meter() {
    *S_pwr_acc_1e0 = 0;
    *S_pwr_acc_1e3 = 0;
    *S_pwr_acc_1e6 = 0;
    *S_pwr_acc_1e9 = 0;
    S_pwr_cnt_1e0 = 0;
    S_pwr_cnt_1e3 = 0;
    S_pwr_cnt_1e6 = 0;
    S_pwr_cnt_1e9 = 0;}
  T_BE_f  get_noise_power_meter_v(){return noise_power;}
  T_BE_f  get_noise_power_meter();
  T_E_f   get_signal_power_meter_v(){return ((*S_pwr_acc_1e9) + (*S_pwr_acc_1e6) + (*S_pwr_acc_1e3) + (*S_pwr_acc_1e0) ) / (((S_pwr_cnt_1e9 * 1000 + S_pwr_cnt_1e6)*1000 + S_pwr_cnt_1e3)*1000+S_pwr_cnt_1e0);}
  T_BE_f  get_signal_power_meter()
  {
    T_BE_f meter = 0.0;
    T_BE_f auxForUnpack [T_P];
    T_E_f aux = get_signal_power_meter_v();
    aux.store(&auxForUnpack[0]);
    for (uint32_t i = 0; i < T_P; i++)
      meter += auxForUnpack[i]/T_P;
    return meter;
  }
  T_BE_f  get_SNR(){return 10*log10(get_signal_power_meter()/get_noise_power_meter());}
    
  void activate_power_meter()  {measure_power = true;}
  void deactivate_power_meter(){measure_power = false;}
  void print(UME::SIMD::SIMDVec<uint32_t, T_P> var2print);
  void print(UME::SIMD::SIMDVec<uint64_t, T_P> var2print);
  void print(UME::SIMD::SIMDVec<float, T_P> var2print);
};


template <int T_P, class T_BE_i, class T_BE_f>
int32_t c_awgn_t<T_P, T_BE_i, T_BE_f>::get_input_number(const string &signal_name)
{
  for (uint32_t i = 0; i < get_nof_input_float_ports() + get_nof_input_int_ports(); i++)
    {
      if (signal_name.compare((get_input_data(i)).name) == 0)
	return i;
    }
  return -1;
}


template <int T_P, class T_BE_i, class T_BE_f>
int32_t c_awgn_t<T_P, T_BE_i, T_BE_f>::get_output_number(const string &signal_name)
{
  for (uint32_t i = 0; i < get_nof_output_float_ports() + get_nof_output_int_ports(); i++)
    {
      if (signal_name.compare((get_output_data(i)).name) == 0)
	return i;
    }
  return -1;
}


template <int T_P, class T_BE_i, class T_BE_f>
T_BE_f  c_awgn_t<T_P, T_BE_i, T_BE_f>::get_noise_power_meter()  
{
  T_BE_f meter = 0.0;
  T_BE_f auxForUnpack [T_P];
  T_E_f aux = get_noise_power_meter_v();
  aux.store(&auxForUnpack[0]);
  for (uint32_t i = 0; i < T_P; i++)
    meter += auxForUnpack[i]/T_P;
  return meter;
}

template <int T_P, class T_BE_i, class T_BE_f>
void  c_awgn_t<T_P, T_BE_i, T_BE_f>::initialize(nlohmann::json config)
{
  free_resources();

  symbol_counter = 0;
  my_inputs = new s_signal_config<T_P>[2];
  my_inputs[0].name                  = "i_I";
  my_inputs[0].elemental_parallelism = 1;
  my_inputs[0].log_all               = true;
  my_inputs[0].fifo                  = nullptr;
  my_inputs[0].port_type             = is_float;
  my_inputs[1].name                  = "i_Q";
  my_inputs[1].elemental_parallelism = 1;
  my_inputs[1].log_all               = true;
  my_inputs[1].fifo                  = nullptr;
  my_inputs[1].port_type             = is_float;
  c_module_base_t<T_P, T_BE_i>::get_signal_info(config, &my_inputs[0], 2, "input");


  my_outputs = new s_signal_config<T_P>[2];
  my_outputs[0].name                  = "o_I";
  my_outputs[0].elemental_parallelism = 1;
  my_outputs[0].log_all               = true;
  my_outputs[0].fifo                  = nullptr;
  my_outputs[0].port_type             = is_float;
  my_outputs[0].value_during_reset    = 0;
  my_outputs[1].name                  = "o_Q";
  my_outputs[1].elemental_parallelism = 1;
  my_outputs[1].log_all               = true;
  my_outputs[1].fifo                  = nullptr;
  my_outputs[1].port_type             = is_float;
  my_outputs[1].value_during_reset    = 0;
  c_module_base_t<T_P, T_BE_i>::get_signal_info(config, &my_outputs[0], 2, "output");

  
  afa_2 = allocate_aligned_memory<T_E_f>(1, 128, S_pwr_acc_1e0, "c_awgn_t.S_pwr_acc_1e0");
  afa_3 = allocate_aligned_memory<T_E_f>(1, 128, S_pwr_acc_1e3, "c_awgn_t.S_pwr_acc_1e3");
  afa_4 = allocate_aligned_memory<T_E_f>(1, 128, S_pwr_acc_1e6, "c_awgn_t.S_pwr_acc_1e6");
  afa_5 = allocate_aligned_memory<T_E_f>(1, 128, S_pwr_acc_1e9, "c_awgn_t.S_pwr_acc_1e9");
  reset_power_meter();
    
  original_seeds = new uint64_t[T_P];
  afa_0 = allocate_aligned_memory<UME::SIMD::SIMDVec<uint64_t, T_P>>(4, 128, xoshiro, "c_awgn_t.xoshiro");
  afa_1 = allocate_aligned_memory<UME::SIMD::SIMDVecMask<      T_P>>(1, 128, mask_0101, "c_awgn_t.mask_0101");
  
  uint32_t aux_mask_initializer[T_P];
  for (int i = 0; i < T_P; i++)
    aux_mask_initializer[i] = i%2;
 alignas(128) UME::SIMD::SIMDVec<uint32_t, T_P> AuxMask;
  AuxMask.load(&aux_mask_initializer[0]);
 
  *mask_0101 = (AuxMask == 1);
  
  //Check existence of config.Seed
  if (config.count("Seed") != 1) {throw std::runtime_error("c_awgn_t ERROR: problem with json field seed\n");}


  //check if there is one seed and parallelism is 1
  if ((config["Seed"].size() <= 1) && (T_P == 1))
    {
      uint64_t auxSeeds[1];

      try{auxSeeds[0] = config["Seed"].get<uint64_t>();}
      catch (...) {throw std::runtime_error("c_awgn_t ERROR: problem with json field seed\n\n");}

      set_seed(&auxSeeds[0]);
    }
  //check if there is one initialization variable per lane
  else if (int(config["Seed"].size()) == T_P)
    {	  
      uint64_t auxSeeds[T_P];

      for (int i = 0; i < T_P; i++)
	{
	  try{auxSeeds[i] = config["Seed"][i].get<uint64_t>();}
          catch (...) {throw std::runtime_error("c_awgn_t ERROR: problem with json field seed\n\n");}
          
	}

      set_seed(&auxSeeds[0]);
    }
  //check if there is one seed but many lanes
  else if (config["Seed"].size() <= 1)
    {
      uint64_t auxSeeds[T_P];

      try {auxSeeds[0] = config["Seed"].get<uint64_t>();}
      catch (...) {throw std::runtime_error("c_awgn_t ERROR: problem with json field seed\n\n");}

      for (int i = 0; i < T_P; i++)
	{
	  try {auxSeeds[i] = config["Seed"].get<uint64_t>();}
          catch (...) {throw std::runtime_error("c_awgn_t ERROR: problem with json field seed\n\n");}
	}
      set_seed(&auxSeeds[0]);
      
      
#ifndef D_DEBUGMODE
      //if D_DEBuGMODE is defined, all seeds are the same, and noise is exactly the same for all lanes
      bool AuxMask[T_P];
      for (int i= 0; i < T_P; i++)
        AuxMask[i] = true;
      
      for (int i = 0; i < T_P-1; i++)
        {
          AuxMask[i] = false;
          UME::SIMD::SIMDVecMask<T_P> m0(&AuxMask[0]);
          jump_xoshiro(m0);
        }
#endif
    }
  else
    {
      throw std::runtime_error("c_awgn_t ERROR: not supported seed method");
    }
  run_xoshiro(5000);

}
  
  
template <int T_P, class T_BE_i, class T_BE_f>
void c_awgn_t<T_P, T_BE_i, T_BE_f>::set_seed(uint64_t *i_NewSeeds)
{
  for (int i = 0; i < T_P; i++)
    original_seeds[i] = i_NewSeeds[i];

  xoshiro[0].load(&original_seeds[0]);
  xoshiro[1] = 0;
  xoshiro[2] = 0;
  xoshiro[3] = 0;
}

template <int T_P, class T_BE_i, class T_BE_f>
void c_awgn_t<T_P, T_BE_i, T_BE_f>::run_xoshiro(uint64_t nRuns)
{
  UME::SIMD::SIMDVec<uint64_t, T_P> t;
  for (uint64_t i = 0; i < nRuns; i++)
    {
      t = xoshiro[1] << 17;
      xoshiro[2] ^= xoshiro[0];
      xoshiro[3] ^= xoshiro[1];
      xoshiro[1] ^= xoshiro[2];
      xoshiro[0] ^= xoshiro[3];
      xoshiro[2] ^= t;
      xoshiro[3] = rotl(xoshiro[3], 45);
    }
}

template <int T_P, class T_BE_i, class T_BE_f>
void c_awgn_t<T_P, T_BE_i, T_BE_f>::jump_xoshiro(UME::SIMD::SIMDVecMask<T_P> UpdMask)
{
  uint64_t JUMP[] = { 0x180ec6d33cfd0aba, 0xd5a61266f0c9392c, 0xa9582618e03fc9aa, 0x39abdc4529b1661c };
  uint64_t Mask = 1;

 alignas(128) UME::SIMD::SIMDVec<uint64_t, T_P> old_xoshiro[4];
  old_xoshiro[0] = xoshiro[0];
  old_xoshiro[1] = xoshiro[1];
  old_xoshiro[2] = xoshiro[2];
  old_xoshiro[3] = xoshiro[3];
  
 alignas(128) UME::SIMD::SIMDVec<uint64_t, T_P> aux_xoshiro[4];
  aux_xoshiro[0] = 0;
  aux_xoshiro[1] = 0;
  aux_xoshiro[2] = 0;
  aux_xoshiro[3] = 0;

  for (int i = 0; i < 4; i++)
    {
      for (int b = 0; b < 64; b++)
        {
          if (JUMP[i] & (Mask << b))
            {
              aux_xoshiro[0] ^= xoshiro[0];
              aux_xoshiro[1] ^= xoshiro[1];
              aux_xoshiro[2] ^= xoshiro[2];
              aux_xoshiro[3] ^= xoshiro[3];
            }
          run_xoshiro(1);          
        }
    }
  
  xoshiro[0] = old_xoshiro[0];
  xoshiro[1] = old_xoshiro[1];
  xoshiro[2] = old_xoshiro[2];
  xoshiro[3] = old_xoshiro[3];
  
  xoshiro[0][UpdMask] = aux_xoshiro[0];
  xoshiro[1][UpdMask] = aux_xoshiro[1];
  xoshiro[2][UpdMask] = aux_xoshiro[2];
  xoshiro[3][UpdMask] = aux_xoshiro[3];
}

template <int T_P, class T_BE_i, class T_BE_f>
void  c_awgn_t<T_P, T_BE_i, T_BE_f>::print(UME::SIMD::SIMDVec<float, T_P> var2print)
{
  float    AuxXprint[T_P];
  var2print.store(&AuxXprint[0]);
  printf("< %8.4f", AuxXprint[0]); 
  for (int i = 1; i<T_P; i++)
    printf(", %8.4f", AuxXprint[i]);
  printf(">");
}

template <int T_P, class T_BE_i, class T_BE_f>
void  c_awgn_t<T_P, T_BE_i, T_BE_f>::print(UME::SIMD::SIMDVec<uint32_t, T_P> var2print)
{
  uint32_t    AuxXprint[T_P];
  var2print.store(&AuxXprint[0]);
  printf("< %8X", AuxXprint[0]); 
  for (int i = 1; i<T_P; i++)
    printf(", %8X", AuxXprint[i]);
  printf(">");
}


template <int T_P, class T_BE_i, class T_BE_f>
void  c_awgn_t<T_P, T_BE_i, T_BE_f>::print(UME::SIMD::SIMDVec<uint64_t, T_P> var2print)
{
  uint64_t    AuxXprint[T_P];
  var2print.store(&AuxXprint[0]);
  printf("< %8lX", AuxXprint[0]); 
  for (int i = 1; i<T_P; i++)
    printf(", %8lX", AuxXprint[i]);
  printf(">");
}

template <int T_P, class T_BE_i, class T_BE_f>
inline   UME::SIMD::SIMDVec<float, T_P> c_awgn_t<T_P, T_BE_i, T_BE_f>::new_xoshiro_float_0_1()
{
  alignas(128) UME::SIMD::SIMDVec<uint64_t, T_P> AuxRnd_both_u64;
  struct alignas(128) Pack2v
  {
    UME::SIMD::SIMDVec<uint32_t, T_P> a;
    UME::SIMD::SIMDVec<uint32_t, T_P> b;
  };
  Pack2v *AuxRnd_both_u32 = reinterpret_cast<Pack2v*>(&AuxRnd_both_u64);


  //Extraction of new value
  AuxRnd_both_u64 = rotl(xoshiro[1] * 5, 7) * 9;
  //Xoshiro preparation for next value
  UME::SIMD::SIMDVec<uint64_t, T_P> t;
  t = xoshiro[1] << 17;
  xoshiro[2] ^= xoshiro[0];
  xoshiro[3] ^= xoshiro[1];
  xoshiro[1] ^= xoshiro[2];
  xoshiro[0] ^= xoshiro[3];
  xoshiro[2] ^= t;
  xoshiro[3] = rotl(xoshiro[3], 45);
  //Extract vector of <uint32_t, T_P>
  alignas(128) UME::SIMD::SIMDVec<uint32_t, T_P> AuxRnd_u32_t;
  AuxRnd_u32_t[!(*mask_0101)] = AuxRnd_both_u32->a;
  AuxRnd_both_u64 = AuxRnd_both_u64 << 32;
  AuxRnd_u32_t[(*mask_0101)] = AuxRnd_both_u32->b;

  /*printf("\n U32_t : ");
    print(AuxRnd_u32_t);*/
  
  //Extract vector of <float, T_P> in range 0:1
  UME::SIMD::SIMDVec<float, T_P> Result_f = 1.0f;
  UME::SIMD::SIMDVec<uint32_t, T_P> *Result_u = reinterpret_cast<UME::SIMD::SIMDVec<uint32_t, T_P>*>(&Result_f);

  
  
  UME::SIMD::SIMDVec<uint32_t, T_P> AuxMask(0x7FFFFF);
  *Result_u = *Result_u | (AuxMask & AuxRnd_u32_t);
  Result_f = Result_f -0.99999994f;
  
  /*printf("\n Result_f : ");
    print(Result_f);*/
    
  return Result_f;
}

template <int T_P, class T_BE_i, class T_BE_f>
inline UME::SIMD::SIMDVec<float, T_P> c_awgn_t<T_P, T_BE_i, T_BE_f>::vfastlog2(UME::SIMD::SIMDVec<float, T_P> x)
{
  UME::SIMD::SIMDVec<float   , T_P>  vx_f = x;
  const UME::SIMD::SIMDVec<uint32_t, T_P>* vx_i = reinterpret_cast<UME::SIMD::SIMDVec<uint32_t, T_P>*>(&vx_f);
  UME::SIMD::SIMDVec<uint32_t, T_P> mx_i = (*vx_i & 0x007FFFFF) | 0x3F000000;
  const UME::SIMD::SIMDVec<float   , T_P>* mx_f = reinterpret_cast<UME::SIMD::SIMDVec<float, T_P>*>(&mx_i);
   UME::SIMD::SIMDVec<float   , T_P> y = (const UME::SIMD::SIMDVec<float   , T_P>)(*vx_i);
  y *= (1.1920928955078125e-7f);

  const UME::SIMD::SIMDVec<float   , T_P> c_124_22551499 = (124.22551499f);
  const UME::SIMD::SIMDVec<float   , T_P> c_1_498030302  = (  1.498030302f);
  const UME::SIMD::SIMDVec<float   , T_P> c_1_725877999  = (  1.72587999f);
  const UME::SIMD::SIMDVec<float   , T_P> c_0_3520087068 = (  0.3520887068f);

  return y - c_124_22551499
           - c_1_498030302 * *mx_f 
           - c_1_725877999 / (c_0_3520087068 + *mx_f);
}

template <int T_P, class T_BE_i, class T_BE_f>
inline UME::SIMD::SIMDVec<float, T_P> c_awgn_t<T_P, T_BE_i, T_BE_f>::vfastlog(UME::SIMD::SIMDVec<float, T_P> x)
{
  const UME::SIMD::SIMDVec<float   , T_P> c_0_69314718 = (0.69314718f);
  return c_0_69314718 * vfastlog2(x);
}
  

template <int T_P, class T_BE_i, class T_BE_f>
bool c_awgn_t<T_P, T_BE_i, T_BE_f>::process()//add_noise_complex(T_E_f* real, T_E_f* imag, T_E_f sigma, uint64_t NSymbols)
{  
  uint32_t n_symbols = i_port_R->available_data();
  n_symbols = std::min(n_symbols, i_port_I->available_data());
  n_symbols = std::min(n_symbols, o_port_R->free_space());
  n_symbols = std::min(n_symbols, o_port_I->free_space());
  
  noise_power = (T_BE_f)(2.0)*sigma*sigma;
  T_E_f four = (T_BE_f)4.0f;
  T_E_f corr_sigma = sigma / four.sqrt();
  const UME::SIMD::SIMDVec<float, T_P> aux_2pi = 2.0*3.14159265359;
  UME::SIMD::SIMDVec<float, T_P> aux_1;
  UME::SIMD::SIMDVec<float, T_P> aux_2;
  UME::SIMD::SIMDVec<float, T_P> aux_a;
  UME::SIMD::SIMDVec<float, T_P> aux_b;
  T_E_f aux_noise_R;
  T_E_f aux_noise_I;

  if (symbol_counter + n_symbols < changui) //bypass, don't add noise
    {
      for (unsigned long i = 0; i < n_symbols; i++)
        {
          (*o_port_R)[i] = (*i_port_R)[i];
          (*o_port_I)[i] = (*i_port_I)[i];
        }
      i_port_R->move_pointer(n_symbols);
      i_port_I->move_pointer(n_symbols);
      o_port_R->move_pointer(n_symbols);
      o_port_I->move_pointer(n_symbols);
      symbol_counter += n_symbols;
    }
  else if (symbol_counter >= changui) //changui completed, add noise to every sample
    {
      if (measure_power)
        {
          for (uint32_t i = 0; i < n_symbols; i++)
            {
              *S_pwr_acc_1e0 += (*i_port_R)[i]*(*i_port_R)[i] + (*i_port_I)[i]*(*i_port_I)[i];
              S_pwr_cnt_1e0 += 1;
              if (S_pwr_cnt_1e0 == 1000)
                {
                  *S_pwr_acc_1e3 += *S_pwr_acc_1e0;
                  S_pwr_cnt_1e3 ++;
                  *S_pwr_acc_1e0 = 0;
                  S_pwr_cnt_1e0 = 0;
                  if (S_pwr_cnt_1e3 == 1000)
                    {
                      *S_pwr_acc_1e6 += *S_pwr_acc_1e3;
                      S_pwr_cnt_1e6 ++;
                      *S_pwr_acc_1e3 = 0;
                      S_pwr_cnt_1e3 = 0;
                      if (S_pwr_cnt_1e6 == 1000)
                        {
                          *S_pwr_acc_1e9 += *S_pwr_acc_1e6;
                          S_pwr_cnt_1e9 ++;
                          *S_pwr_acc_1e6 = 0;
                          S_pwr_cnt_1e6 = 0;
                        }
                    }
                }
            }
        }
  
      for (unsigned long i = 0; i < n_symbols; i++)
        {
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R = (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I = (T_E_f)(aux_b*aux_2.sin());
      
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R += (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I += (T_E_f)(aux_b*aux_2.sin());
      
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R += (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I += (T_E_f)(aux_b*aux_2.sin());
      
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R += (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I += (T_E_f)(aux_b*aux_2.sin());
      
          (*o_port_R)[i] = (*i_port_R)[i] + aux_noise_R;
          (*o_port_I)[i] = (*i_port_I)[i] + aux_noise_I;
        }
      i_port_R->move_pointer(n_symbols);
      i_port_I->move_pointer(n_symbols);
      o_port_R->move_pointer(n_symbols);
      o_port_I->move_pointer(n_symbols);
    }
  else
    {
      if (measure_power)
        {
          for (uint32_t i = changui - symbol_counter; i < n_symbols; i++)
            {
              *S_pwr_acc_1e0 += (*i_port_R)[i]*(*i_port_R)[i] + (*i_port_I)[i]*(*i_port_I)[i];
              S_pwr_cnt_1e0 += 1;
              if (S_pwr_cnt_1e0 == 1000)
                {
                  *S_pwr_acc_1e3 += *S_pwr_acc_1e0;
                  S_pwr_cnt_1e3 ++;
                  *S_pwr_acc_1e0 = 0;
                  S_pwr_cnt_1e0 = 0;
                  if (S_pwr_cnt_1e3 == 1000)
                    {
                      *S_pwr_acc_1e6 += *S_pwr_acc_1e3;
                      S_pwr_cnt_1e6 ++;
                      *S_pwr_acc_1e3 = 0;
                      S_pwr_cnt_1e3 = 0;
                      if (S_pwr_cnt_1e6 == 1000)
                        {
                          *S_pwr_acc_1e9 += *S_pwr_acc_1e6;
                          S_pwr_cnt_1e9 ++;
                          *S_pwr_acc_1e6 = 0;
                          S_pwr_cnt_1e6 = 0;
                        }
                    }
                }
            }
        }

      for (unsigned long i = 0; i < changui - symbol_counter; i++)
        {
          (*o_port_R)[i] = (*i_port_R)[i];
          (*o_port_I)[i] = (*i_port_I)[i];
        }
      for (unsigned long i = changui - symbol_counter; i < n_symbols; i++)
        {
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R = (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I = (T_E_f)(aux_b*aux_2.sin());
      
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R += (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I += (T_E_f)(aux_b*aux_2.sin());
      
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R += (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I += (T_E_f)(aux_b*aux_2.sin());
      
          aux_1 = new_xoshiro_float_0_1();
          aux_2 = aux_2pi * new_xoshiro_float_0_1();
          aux_a = -2.0f * vfastlog(aux_1);
          aux_b = corr_sigma * aux_a.sqrt();
          aux_noise_R += (T_E_f)(aux_b*aux_2.cos());
          aux_noise_I += (T_E_f)(aux_b*aux_2.sin());
      
          (*o_port_R)[i] = (*i_port_R)[i] + aux_noise_R;
          (*o_port_I)[i] = (*i_port_I)[i] + aux_noise_I;
        }
      i_port_R->move_pointer(n_symbols);
      i_port_I->move_pointer(n_symbols);
      o_port_R->move_pointer(n_symbols);
      o_port_I->move_pointer(n_symbols);
      symbol_counter += n_symbols;
    }

  
#ifdef D_IE_VERBOSE
  if (!((i_port_R->available_data() > 0) &&
        (i_port_I->available_data() > 0)))
    printf(" no_data ");
  if (!(	 (o_port_R->free_space() > 0) &&
                 (o_port_I->free_space()  > 0)))
    printf(" no_space ");
#endif
  return n_symbols > 0;
}

#endif
