#include "c_awgn_t.hpp"
#include "json.hpp"
#include "c_fifo_t.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <fstream>

#include <math.h>
#include <matrix.h>
#include <mex.h>

#include <vector>
#include <map>
#include <algorithm>
#include <memory>
#include <string.h>
#include <sstream>


#define D_FIFO_INR_DEPTH 114
#define D_FIFO_INI_DEPTH 117
#define D_FIFO_OUTR_DEPTH 69
#define D_FIFO_OUTI_DEPTH 45

using namespace std;

////////////////////////////////////////////////////////////////
// Template types //////////////////////////////////////////////
////////////////////////////////////////////////////////////////
#define T_P   8
#define T_BE float
#define T_E  UME::SIMD::SIMDVec<T_BE, T_P>

/////////////////////////////////////////////////////////////////
// Persistent variables /////////////////////////////////////////
/////////////////////////////////////////////////////////////////
c_fifo_port_wr_t<T_P, T_BE> *input_R;
c_fifo_port_wr_t<T_P, T_BE> *input_I;
c_fifo_t<T_P, T_BE> FIFO_in_R(D_FIFO_INR_DEPTH, 1);
c_fifo_t<T_P, T_BE> FIFO_in_I(D_FIFO_INI_DEPTH, 1);
c_awgn_t<T_P, uint32_t, T_BE> myAWGN;
c_fifo_t<T_P, T_BE> FIFO_out_R(D_FIFO_OUTR_DEPTH, 1);
c_fifo_t<T_P, T_BE> FIFO_out_I(D_FIFO_OUTI_DEPTH, 1);
c_fifo_port_rd_t<T_P, T_BE> *output_R;
c_fifo_port_rd_t<T_P, T_BE> *output_I;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  const mwSize *dims;
  const mwSize *dims1;
  const mwSize *dims2;
  const mwSize *dims3;
  const mwSize *dims4;
  const mwSize *dims5;
  /////////////////////////////////////////////////////////////////////
  // Displaying call summary //////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////
  /*
    mexPrintf("\n======================================================");
    mexPrintf("\n== Function call summary==============================");
    mexPrintf("\n======================================================");
    mexPrintf("\n Called with %d arguments", nrhs);
    mexPrintf("\n Returning %d values", nlhs);
    for (int i = 0; i < nrhs; i++)
    {
    dims = mxGetDimensions(prhs[i]);
    mexPrintf("\n Argument[%d] has dimensions %d x %d", i, (int)dims[0], (int)dims[1]);
    }
    /* */
  /////////////////////////////////////////////////////////////////////
  // command validation ///////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////
  dims = mxGetDimensions(prhs[0]);
  if ( mxIsChar(prhs[0]) != 1)
    mexErrMsgTxt("\n ERROR: Command must be a string");
  if (((int)dims[0] != 1))
    mexErrMsgTxt("\n ERROR: Command must be of size 1xN");
  char *i_Command = mxArrayToString(prhs[0]);
  //mexPrintf("\n Command accepted (%s)", i_Command);
    
  if (strcmp(i_Command, "Initialize") == 0)
    {
      input_R = FIFO_in_R.get_wr_port();
      input_I = FIFO_in_I.get_wr_port();
      myAWGN.assign_input_port(FIFO_in_R.get_rd_port(),0);
      myAWGN.assign_input_port(FIFO_in_I.get_rd_port(),1);
      
      myAWGN.assign_output_port(FIFO_out_R.get_wr_port(),0);
      myAWGN.assign_output_port(FIFO_out_I.get_wr_port(),1);
      output_R = FIFO_out_R.get_rd_port();
      output_I = FIFO_out_I.get_rd_port();
      
      //mexPrintf("\n Comando = Initialize");
      //Parameter dimension check
      if (nrhs != 2)
        mexErrMsgTxt("\n ERROR: Initialize function must receive 1 parameters");
      if (mxIsChar(prhs[1]) != 1)
        mexErrMsgTxt("\n ERROR: Initialize function must receive a string as parameter");
      dims = mxGetDimensions(prhs[1]);
      if (((int)dims[0] != 1))
        mexErrMsgTxt("\n EERROR: Initialize function must receive a string of size 1xN");
      char *i_JsonFilename = mxArrayToString(prhs[1]);
      string JsonFilename(i_JsonFilename);

      std::ifstream i_file(JsonFilename);
      nlohmann::json config;
      i_file >> config;
      nlohmann::json SimProfile = config["SimulationProfile"];
      string auxSimParam_mFolderName = SimProfile["BasePath"].get<string>() + "/" + SimProfile["ResultFolderName"].get<string>();
      nlohmann::json Params  = SimProfile["AWGN"];
      Params["BasePath"]   = SimProfile["BasePath"];
      Params["Modulation"] = SimProfile["Modulation"];
      Params["AllZeros"]   = SimProfile["AllZeros"];
      Params["LogFolder"]  = auxSimParam_mFolderName + "/AWGN_Vectors";
  
      myAWGN.initialize(Params);
    }
  else if (strcmp(i_Command, "AddNoise") == 0)
    {
      FIFO_in_R.reset();
      FIFO_in_I.reset();
      FIFO_out_R.reset();
      FIFO_out_I.reset();
      
      //mexPrintf("\n Comando = Decode");
      //Parameter dimension check
      if (nrhs != 4)
        mexErrMsgTxt("\n ERROR: AddNoise function must receive 3 parameters");
      dims1 =  mxGetDimensions(prhs[1]); //input symbol stream
      dims4 =  mxGetDimensions(prhs[2]); //input symbol stream
      dims5 =  mxGetDimensions(prhs[3]); //Sigma
      if (dims1[0] != dims4[0])
        mexErrMsgTxt("\n ERROR: AddNoise must receive two vectors of the same length");
          
      unsigned int nsymb = dims1[0];
      
      if (nlhs != 2)
        mexErrMsgTxt("\n ERROR: AddNoise function must return 2 parameters");
      //Creation of output variables
      plhs[0] = mxCreateDoubleMatrix(nsymb, 1, mxREAL);
      plhs[1] = mxCreateDoubleMatrix(nsymb, 1, mxREAL);
      //pointers to matlab variables
      double *i_I_mat, *i_Q_mat, *i_Sigma_mat, *o_I_mat, *o_Q_mat;
      i_I_mat = mxGetPr(prhs[1]);
      i_Q_mat = mxGetPr(prhs[2]);
      i_Sigma_mat   = mxGetPr(prhs[3]);
      o_I_mat = mxGetPr(plhs[0]);
      o_Q_mat = mxGetPr(plhs[1]);

      T_BE  Sigma_i = *i_Sigma_mat;
      myAWGN.set_sigma(Sigma_i);
      
      bool watchdog = true;
      uint32_t input_stream_R_ptr = 0;
      uint32_t input_stream_I_ptr = 0;
      uint32_t output_stream_R_ptr = 0;
      uint32_t output_stream_I_ptr = 0;
      while (watchdog)
	{
	  watchdog = false;
	  uint32_t i_space = input_R->free_space();
	  for (uint32_t i = 0; (i < i_space) && (input_stream_R_ptr < nsymb); i++)
	    input_R->write((T_BE)i_I_mat[input_stream_R_ptr++]);
	  i_space = input_I->free_space();
	  for (uint32_t i = 0; (i < i_space) && (input_stream_I_ptr < nsymb); i++)
	    input_I->write((T_BE)i_Q_mat[input_stream_I_ptr++]);

	  watchdog = watchdog || myAWGN.process();

	  T_E aux_for_download;
	  T_BE AuxXUnpack[T_P];
	  uint32_t o_data = output_R->available_data();
	  watchdog = watchdog || (o_data > 0);
	  for (int i= 0; i < o_data; i++)
	    {
	      aux_for_download = output_R->read();
	      aux_for_download.store(&AuxXUnpack[0]);
	      o_I_mat[output_stream_R_ptr++] = 1.0*AuxXUnpack[0];
	    }
	  o_data = output_I->available_data();
	  watchdog = watchdog || (o_data > 0);
	  for (int i= 0; i < o_data; i++)
	    {
	      aux_for_download = output_I->read();
	      aux_for_download.store(&AuxXUnpack[0]);
	      o_Q_mat[output_stream_I_ptr++] = 1.0*AuxXUnpack[0];
	    }
	}
   
    }
  else
    mexErrMsgTxt("\n ERROR: Comando no reconocido");
  //mexPrintf("\n");
    
}

