import random
import sys
import json
sys.path.insert(1, '../../../open_backbone/ifc_sync_valid/verification/VectorHandler')
import VectorHandler
import os
import numpy as np
class bcolors:
    HEADER='\033[95m'
    OKBLUE='\033[94m'
    OKGREEN='\033[92m'
    WARNING='\033[93m'
    FAIL='\033[91m'
    ENDC='\033[0m'
    BOLD='\033[1m'
    UNDERLINE='\033[4m'

class IFCHandler(VectorHandler.VectorHandler):

    #def __init__(self, SYNC_PERIOD, module_dut,case_json_file,json_module_name, vectors_path):
    def __init__(self, module_dut,case_json_file,json_module_name, vectors_path):
        #self.SYNC_PERIOD      = SYNC_PERIOD
        self.module_dut       = module_dut
        self.case_json_file   = case_json_file
        self.json_module_name = json_module_name
        self.vectors_path      = vectors_path
        with open(self.case_json_file, 'r') as f:
            self.test_conf = json.load(f)


    def cycles_with_n_valids(self, ENOUGH_VALIDS, pvalid, min_cycles):
        return int(ENOUGH_VALIDS*1.17/(pvalid * min_cycles) + 1) * min_cycles

    def check_enough_valids(self, valid_vector, min):
        n_valids = valid_vector.count(1)
        if (n_valids < min):
            print("Not enough valids(%s)"%n_valids)
            return False
        else:
            print("Valids for reference vectors: %s"%n_valids)
            return True


    def sync_load (self, reset_vector, valid_vector, param_dict ,param = 'P_SYNC_IDEAL'):
        sync_vector = []
        count_valid_cycles = 0

        for valid,reset in zip(valid_vector,reset_vector):
            if(reset == 0):
                sync_vector.append(random.randint(0, 1))
            else:
                if(valid == 0):
                    if (param == 'P_SYNC_POWER'):
                        sync_vector.append(0)
                    else:
                        sync_vector.append(random.randint(0, 1))
                else:
                    if ((param == 'P_SYNC_IDEAL') or (param == 'P_SYNC_POWER')):
                        if(count_valid_cycles == 0):
                            sync_vector.append(1)
                        #elif(count_valid_cycles % self.SYNC_PERIOD == 0):
                        #    sync_vector.append(int(random.uniform(0,1) < param_dict['prob']))
                        else:
                            sync_vector.append(0)
                    elif param == 'P_SYNC_RND':
                        sync_vector.append(int(random.uniform(0,1) < param_dict['prob']))
                    elif param == 'P_SYNC_INACTIVE':
                        sync_vector.append(0)

                    count_valid_cycles += 1
        return sync_vector

    #def valid_load (self, L, param_dict, param):
    def valid_load (self, reset_vector, param_dict, param):

        valid_vector = []

        for reset in reset_vector:
            if(reset == 0):
                valid_vector.append (int(random.uniform(0,1) < 0.5))
            else:
                valid_vector.append (int(random.uniform(0,1) < param_dict['prob']))


        return valid_vector


    def rst_load (self, L, param_dict, param = 'P_RESET_INIT'):

        INIT_RESET_PERIOD = 10
        rst_vector = []

        for i in range(0,L):
            if(param == 'P_RESET_INIT'):
                if(i< INIT_RESET_PERIOD):
                    rst_vector.append(0)
                else:
                    rst_vector.append(1)
            elif(param == 'P_RESET_RND'):
                if(i == L-1):
                    rst_vector.append(0)
                else:
                    #becouse it s active low i compare with >
                    rst_vector.append(int(random.uniform(0,1) > param_dict['prob']))
            elif(param == 'P_RESET_INACTIVE'):
                rst_vector.append(1)
            elif(param == 'P_RESET_ACTIVE'):
                rst_vector.append(0)

        return rst_vector


    def run_ifc_emulator(self, i_dict):

        o_dict       = {}
        for input_data in zip(*i_dict.values()):
            input_dict = {signal_name: np.array([signal], dtype='int32') for signal_name, signal in zip(i_dict.keys(),input_data)}
            try:
                output_dict = self.module_dut.run(input_dict)
            except ValueError:
                print("\n")
                print("-"*60)
                print("-"*60)
                print("-"*60)
                print("\n--------Error running pywrapper with this dict:")
                for key,val in input_dict.items():
                    print("_"*80,"\n",key,":")
                    try:
                        print("\n"+bcolors.WARNING +"NS: "+str(len(val[0]))+ bcolors.ENDC)
                    except:
                        pass
                    print("\n"+bcolors.WARNING +str(val)+ bcolors.ENDC)

                sys.exit()
            for out_signal_name, out_signal in output_dict.items():
                o_dict.setdefault(out_signal_name,[]).append(out_signal)
        return o_dict


    def data_log ( self, data_dict):
        for signal_name, signal  in data_dict.items():
            for json_signal in self.test_conf["SimulationProfile"][self.json_module_name]["signals"]:
                if (signal_name == json_signal["name"]):
                    if not ((json_signal["type"] == "probe") and (json_signal["match"] == 0)):
                        self.write_file(self.vectors_path + signal_name + '_sim.txt', signal)

    def rtl_signal_read ( self):
        o_dict={}
        
        for signal in self.test_conf["SimulationProfile"][self.json_module_name]["signals"]:
            if "log" in signal.keys() and signal["log"]==1:                
                o_dict[signal["name"]] = np.array(self.read_file(self.vectors_path  + signal["name"] +'_rtl.txt')).astype(np.int)
        return o_dict
    def check_results(self, window = ('start','end'), check_ref_vectors=False,suffix='_rtl'):





        internal_signal_container  = {}
        output_signal_container    = {}
        output_signal_ref_container= {}
        o_valid_signal_container   = {}



        for signal in self.test_conf["SimulationProfile"][self.json_module_name]["signals"]:
            #print( "signal", signal)
            if "match" in signal.keys() and signal["match"]==1:
                #print( "matching signal", signal)

                if(signal["name"]== "o_valid"):
                    o_valid_signal_container.update({'rtl': self.read_file(self.vectors_path  + signal["name"] +'%s.txt'%(suffix) ),'sim': self.read_file(self.vectors_path  + signal["name"] +'_sim.txt' )})
                elif(signal["type"]== "probe"):
                    print( "internal signal", signal)
                    continue #uncomment here if you want not to check probe signals
                    internal_signal_container.update({signal["name"]:{'rtl': self.read_file(self.vectors_path  + signal["name"] +'%s.txt'%(suffix) ),'sim': self.read_file(self.vectors_path  + signal["name"] +'_sim.txt' )}})
                elif(signal["type"]== "output"):
                    output_signal_container.update({signal["name"]:{'rtl': self.read_file(self.vectors_path  + signal["name"] +'%s.txt'%(suffix) ),'sim': self.read_file(self.vectors_path  + signal["name"] +'_sim.txt' )}})

                    if check_ref_vectors == True and (signal["name"] != "o_sync" and signal["name"] != "o_valid") :
                        output_signal_ref_container.update({signal["name"]:{'ref': self.read_file(self.vectors_path  + signal["name"] +'_sim_ref.txt' )[::-1]}})




        if(window[0] == 'start'):
            start = 0
        else:
            start = window[0]

        if(window[1] == 'end'):
            end = len(o_valid_signal_container['rtl'])
        else:
            end = window[1]

        print("Checking result in window",start,end)


        valid_count        = 0
        data_assert_count  = 0
        valid_assert_count = 0

        if(check_ref_vectors == False):
            #Internal signals not checked when checking ref vectors
            for internal_signal_name,internal_signal in internal_signal_container.items():
                print ("Matching internal signal "+internal_signal_name)
                for signal_rtl, signal_sim in zip(internal_signal['rtl'],internal_signal['sim']):

                    if(valid_count >= start and valid_count <= end-1):
                        assert signal_rtl == signal_sim
                        if(signal_rtl != signal_sim):
                            print("internal signal not matching")
                            print(internal_signal_name,signal_rtl[0:10],signal_sim[0:10])
                        data_assert_count = data_assert_count +1
                    valid_count = valid_count +1
                print("internal signal checked during :",data_assert_count,"cycles" )
                valid_count        = 0
                data_assert_count  = 0

            valid_count        = 0
            data_assert_count  = 0
            valid_assert_count = 0


        verbose = True
        o_sync_signal_container = output_signal_container['o_sync']
        for output_signal_name,output_signal in output_signal_container.items():
            if check_ref_vectors == False:
                print ("Matching output signal "+output_signal_name+ "\n")
                line =0
                waiting_for_first_valid = False
                for signal_rtl, signal_sim , valid_rtl, valid_sim, sync_rtl in zip(output_signal['rtl'],output_signal['sim'],o_valid_signal_container['rtl'], o_valid_signal_container['sim'] ,o_sync_signal_container['rtl']):

                    if(valid_count >= start and valid_count <= end-1):
                        if(valid_rtl != valid_sim ):
                            print('valid not matching')
                            print(valid_rtl,valid_sim )

                        valid_assert_count = valid_assert_count +1
                        assert valid_rtl == valid_sim
                        #print('valid matching')
                        if(valid_rtl == ['1'] or True):

                            if waiting_for_first_valid:
                                if verbose:
                                    print("...")
                                if valid_rtl == ['1'] :
                                    if verbose:
                                        print("First valid after reset!")
                                    if sync_rtl == ['1']:
                                        if verbose:
                                            print("Sync found. Everything OK")
                                        waiting_for_first_valid = False
                                        assert signal_rtl == signal_sim
                                    else:
                                        if verbose:
                                            print("Data not matching")
                                        assert False == True
                            else:
                                if signal_rtl != signal_sim:
                                    if verbose:
                                        print('data not matching,waiting for the next valid...',signal_rtl[0:10],signal_sim[0:10],line)
                                    waiting_for_first_valid = True
                                else:
                                    assert signal_rtl == signal_sim
                                    waiting_for_first_valid = False


                                #   print("OK")
                            #print('data matching')
                            data_assert_count = data_assert_count +1
                            line +=1
                    valid_count = valid_count + 1
                print("\toutput valid  checked during :",valid_assert_count,"cycles" )
                print("\toutput signal "+output_signal_name+ " checked during :",data_assert_count,"cycles\n" )
                assert valid_assert_count>=0
                valid_count        = 0
                valid_assert_count = 0
                data_assert_count  = 0
            elif(output_signal_name != "o_sync" and output_signal_name != "o_valid" ):
                print ("Matching signal "+output_signal_name+ " against reference vectors\n")
                line=1
                for signal_rtl , valid_rtl in zip(output_signal['rtl'], o_valid_signal_container['rtl'] ):
                    #print("valid count: %d",valid_count,start,end)
                    if(valid_count >= start and valid_count <= end-1):
                        if(valid_rtl == ['1']):
                            signal_ref = output_signal_ref_container[output_signal_name]["ref"].pop()
                            #print("reading line %d ..."%line)
                            line += 1
                            assert signal_rtl == signal_ref #HINT
                            #print('data matching')
                            data_assert_count = data_assert_count +1
                            #print ("valid_count",valid_count)
                            if(signal_rtl != signal_ref):
                                print('data %s not matching'%(output_signal_name))
                                print(signal_rtl[0:15],signal_ref[0:15])
                                #input("continue....")
                    valid_count = valid_count + 1
                print("\toutput signal "+output_signal_name+ " checked during :",data_assert_count,"cycles\n" )
                if not (data_assert_count>0):
                    print("\tWARNING: output signal %s no checked( assert count is %s. Did the RTL run?)\n"% (output_signal_name,data_assert_count))
                    assert False


                valid_count        = 0
                data_assert_count  = 0

    def remove_old_data_files(self, running_path):
        #removing old vectors file
        if (os.path.exists(self.vectors_path)):
            os.chdir(self.vectors_path)
            os.system( 'find . -type f ! -name '+"'*_ref*'"+' -a ! -path'+" './power/*'"+' -a ! -path'+" './vectors/*'"+' -delete')
            os.chdir(running_path)

    def get_default_i_dict(self):
        try:
            return self.default_i_dict.copy()
        except:
            print("To use get_default_i_dict(), define  self.default_i_dict in your module_test class.")
            sys.exit()


def python_wrapper_compile(python_wrapper_path,actual_path):
    os.chdir(python_wrapper_path)
    cmd = 'python setup.py build_ext --inplace'
    os.system(cmd)
    os.chdir(actual_path)
