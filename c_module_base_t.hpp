#ifndef C_MODULE_BASE_T_H
#define C_MODULE_BASE_T_H

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_fifo_t.hpp"
#include "json.hpp"
#include "UMESimd.h"
#include "c_fsm_base.hpp"

using namespace std;

enum e_port_type
  {
    is_int,
    is_float};

template <int T_P>
struct s_signal_config
{
  string                        name;                    //value that links simulator and json
  int32_t                       elemental_parallelism;   //owned by simulator
  bool                          log_all;                 //owned by simulator
  int32_t                       value_during_reset;      //owned by simulator
  int32_t                       latency;                 //owned by json
  int32_t                       parallelism;             //owned by json
  int32_t                       nb;                      //owned by json
  int32_t                       nbf;                     //owned by json
  bool                          is_signed;               //owned by json
  e_port_type                   port_type;              //owned by simulator
  int32_t                       fifo_number;
  int32_t                       fifo_expansion_factor = 1;
  double                        filling_factor = 1.0;
  c_fifo_port_wr_t<T_P, int32_t> *fifo;                  //operational variable owned by simulator
};


template <int T_P, class T_BE_i=int32_t, class T_BE_f=float>
class c_module_base_t
{
private:
  //to be overwritten: base type of inputs
  typedef T_BE_i                          T_BE_int;
  //to be overwritten: base type of outputs
  typedef T_BE_f                          T_BE_float;
  //to be overwritten: parallelism
  const static int T_Parallelism = T_P;
public:
  virtual ~c_module_base_t(){};
  
  /// Function that initializes the module according the json parameters
  /// \param config: a nlohmann::json object with the required parameters
  virtual void initialize(nlohmann::json config) = 0;

  /// Function that releases all dynamically allocated resources
  virtual void free_resources() = 0;

  /// Function that puts the module state back to reset (equivalent to rst signal)
  virtual void reset() = 0;
  
  /// Function that processes data as long as there is enough data at the input
  /// ports and space on the output ports
  /// \return: true if some data was processed, false otherwise
  virtual bool process() = 0;
 
  /// Function that returns the number of probe signals of the module
  /// \return: the number of probes
  virtual uint32_t             get_nof_probes() = 0;

  /// Function that gives the internal probe number that matches the given name
  /// \param signal_name: the name of the probe whose number is requested
  /// \return: the probe number
  virtual int32_t              get_probe_number(const string &signal_name) = 0;
  
  /// Function that returns a struct with all the information of the probe
  /// \param nof_probe: the probe number
  /// \return: a struct with probe information about parallelism, latency, etc
  virtual s_signal_config<T_P> get_probe_data(uint32_t nof_probe) = 0;
  
  /// Function that assigns a fifo_wr_port to the given probe port index
  /// \param port: a pointer to a fifo write port
  /// \param nof_probe: the probe number
  virtual void                 assign_probe_port(c_fifo_port_wr_t<T_P, int32_t> *port, uint32_t nof_probe) = 0;


  



  
  /// Function that returns the number of input float ports of the module
  /// \return: the number of input ports of the module
  virtual uint32_t             get_nof_input_float_ports()  = 0;
  /// Function that returns the number of input int ports of the module
  /// \return: the number of input ports of the module
  virtual uint32_t             get_nof_input_int_ports()  = 0;

  /// Function that gives the internal input number that matches the given name
  /// \param signal_name: the name of the input port whose number is requested
  /// \return: the input port number
  virtual int32_t              get_input_number(const string &signal_name) = 0;

  /// Function that returns a struct with all the information of the input port
  /// \param nof_port: the port number
  /// \return: a struct with port information about parallelism, etc.
  virtual s_signal_config<T_P> get_input_data(uint32_t nof_port = 0) const = 0;
  
  /// Function that assigns a fifo_rd_port to the given input int port index
  /// \param port: a pointer to a fifo read port
  /// \param n_port: the input port number to be connected
  virtual void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_i> *port, uint32_t n_port = 0) = 0;

  /// Function that assigns a fifo_rd_port to the given input float port index
  /// \param port: a pointer to a fifo read port
  /// \param n_port: the input port number to be connected
  virtual void                 assign_input_port(c_fifo_port_rd_t<T_P, T_BE_f> *port, uint32_t n_port = 0) = 0;




  
  /// Function that returns the number of output float ports of the module
  /// \return: the number of output ports of the module
  virtual uint32_t             get_nof_output_float_ports() = 0;
  /// Function that returns the number of output int ports of the module
  /// \return: the number of output ports of the module
  virtual uint32_t             get_nof_output_int_ports() = 0;

  /// Function that gives the internal output number that matches the given name
  /// \param signal_name: the name of the output port whose number is requested
  /// \return: the output port number
  virtual int32_t              get_output_number(const string &signal_name) = 0;

  /// Function that returns a struct with all the information of the output port
  /// \param nof_port: the port number
  /// \return: a struct with port information about parallelism, latency, etc.
  virtual s_signal_config<T_P> get_output_data(uint32_t nof_port = 0) const = 0;

  /// Function that assigns a fifo_rd_port to the given output port index
  /// \param port: a pointer to a fifo write port
  /// \param n_port: the output port number to be connected
  virtual void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_i> *port, uint32_t n_port = 0) = 0;

  /// Function that assigns a fifo_rd_port to the given output port index
  /// \param port: a pointer to a fifo write port
  /// \param n_port: the output port number to be connected
  virtual void                 assign_output_port(c_fifo_port_wr_t<T_P, T_BE_f> *port, uint32_t n_port = 0) = 0;

  
  /// Function that parses a json structure and fills a s_signal_config array
  /// with all the relevant signal information
  /// \param config: the json object where the signal information is present within an array called "signals"
  /// \param signals_config: a pointer the initial element of an array of structures of the type s_signal_config, where the signal data shall be filled
  ///         at least the signal names must be present in the elements of the array, because this is used for interpreting the json object
  /// \param n_signals: the number of elements in the signal array
  /// \param expected_type: the type of signal expected for the given names. Can be "input", "output" or "probe"
  void get_signal_info(nlohmann::json        config,
		       s_signal_config<T_P> *signal_configs,
		       uint32_t              n_signals,
		       string                expected_type);

  /// Function that sets an i_static for a module
  /// \param static_name: the i_static signal name, as declared in the JSON
  /// \param static_values: an array with the i_static values. The length must be according to the
  ///        parallelism declared in json.
  virtual void set_static(const string &static_name, int32_t *static_values) = 0;
  
  virtual void review_fsm_config(s_fsm_config *fsm_config, uint32_t nof_fsms) = 0;
  virtual void review_port_config(s_signal_config<T_P> *i_ports,
                                  s_signal_config<T_P> *o_ports,
                                  s_signal_config<T_P> *p_ports) = 0;
};

template <int T_P, class T_BE_i, class T_BE_o>
void c_module_base_t<T_P, T_BE_i, T_BE_o>::get_signal_info(nlohmann::json config,
							   s_signal_config<T_P> *signal_configs,
							   uint32_t         n_signals,
							   string expected_type)
{
  for (uint32_t s = 0; s < n_signals; s++)
    {
      int index = -1;
      for (uint32_t s_config = 0; s_config < config["signals"].size(); s_config++)
	{
	  if (signal_configs[s].name.compare(config["signals"][s_config]["name"].get<string>()) == 0)
	    index = s_config;
	}
      if (index < 0)
	{
	  throw std::runtime_error(("c_module_base_t ERROR: signal " + expected_type + "[" + std::to_string(s) + "] " + signal_configs[s].name + " not found in json").c_str());
	}
      if (expected_type.compare(config["signals"][index]["type"].get<string>()) != 0)
	throw std::runtime_error("c_module_base_t ERROR: expected different type");
      signal_configs[s].nb          = config["signals"][index]["n_bits"].get<int>();
      signal_configs[s].nbf         = config["signals"][index]["nf_bits"].get<int>();
      signal_configs[s].latency     = config["signals"][index]["latency"].get<int>();
      signal_configs[s].parallelism = config["signals"][index]["parallelism"].get<int>();
      signal_configs[s].is_signed   = config["signals"][index]["signed"].get<int>() == 1;

      if (signal_configs[s].parallelism % signal_configs[s].elemental_parallelism != 0)
	throw std::runtime_error(("c_module_base_t ERROR: signal " + expected_type + "[" + std::to_string(s) + "] " + signal_configs[s].name + " incompatible input paralelism (json = " + std::to_string(signal_configs[s].parallelism) + ", model elemental parallelism = " + std::to_string(signal_configs[s].elemental_parallelism) + ")").c_str());
      
    }
}
#endif
