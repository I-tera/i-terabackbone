//#define D_DEBUGMODE

#include "../c_fifo_t.hpp"


#define D_SIM_LENGTH 1000000
#define D_MAX_WR_BLOCK 180


#define D_P 8
#define D_BE uint32_t

#define D_FIFO1_DEPTH 814
#define D_FIFO2_DEPTH 315
#define D_FIFO3_DEPTH 198
#define D_FIFO4_DEPTH 561
#define D_FIFO5_DEPTH 333

//#define D_DEBUG

#include <stdio.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <fstream>


#include <chrono>


/*TEST description
                                             ]
            -------------------------------- ]
            |                                ]
source --FIFO1       ------FIFO3------------ ]
            |        |                       ]sink
            -------FIFO2                     ]
                     |                       ]
                     ------FIFO4---FIFO5---- ]
*/
using namespace std;


int main(void)
{
  printf("\n\n TEST STARTED \n");
  printf("\n                                             ]");
  printf("\n            -------------------------------> ]");
  printf("\n            |                                ]");
  printf("\nsource ->FIFO1       ----->FIFO3-----------> ]");
  printf("\n            |        |                       ]sink");
  printf("\n            ------>FIFO2                     ]");
  printf("\n                     |                       ]");
  printf("\n                     ----->FIFO4-->FIFO5---> ]");
  srand(time(NULL));
  
  c_fifo_t<        D_P, D_BE> FIFO1(D_FIFO1_DEPTH, 2);
  c_fifo_port_wr_t<D_P, D_BE> *FIFO1_wr;
  c_fifo_port_rd_t<D_P, D_BE> *FIFO1_rd[2];
  FIFO1_wr    = FIFO1.get_wr_port();
  FIFO1_rd[0] = FIFO1.get_rd_port(0);
  FIFO1_rd[1] = FIFO1.get_rd_port(1);
  
  c_fifo_t<        D_P, D_BE> FIFO2(D_FIFO2_DEPTH, 2);
  c_fifo_port_wr_t<D_P, D_BE> *FIFO2_wr;
  c_fifo_port_rd_t<D_P, D_BE> *FIFO2_rd[2];
  FIFO2_wr    = FIFO2.get_wr_port();
  FIFO2_rd[0] = FIFO2.get_rd_port(0);
  FIFO2_rd[1] = FIFO2.get_rd_port(1);
  
  c_fifo_t<        D_P, D_BE> FIFO3(D_FIFO3_DEPTH, 1);
  c_fifo_port_wr_t<D_P, D_BE> *FIFO3_wr;
  c_fifo_port_rd_t<D_P, D_BE> *FIFO3_rd;
  FIFO3_wr = FIFO3.get_wr_port();
  FIFO3_rd = FIFO3.get_rd_port(0);
  
  c_fifo_t<        D_P, D_BE> FIFO4(D_FIFO4_DEPTH, 1);
  c_fifo_port_wr_t<D_P, D_BE> *FIFO4_wr;
  c_fifo_port_rd_t<D_P, D_BE> *FIFO4_rd;
  FIFO4_wr = FIFO4.get_wr_port();
  FIFO4_rd = FIFO4.get_rd_port(0);
  
  c_fifo_t<        D_P, D_BE> FIFO5(D_FIFO5_DEPTH, 1);
  c_fifo_port_wr_t<D_P, D_BE> *FIFO5_wr;
  c_fifo_port_rd_t<D_P, D_BE> *FIFO5_rd;
  FIFO5_wr = FIFO5.get_wr_port();
  FIFO5_rd = FIFO5.get_rd_port(0);

  uint32_t block_length;
  D_BE data_2_write = 1;
  D_BE data_2_read = 1;
  uint64_t rejected_count = 0;
  uint64_t element_count = 0;
  uint64_t error_count = 0;
  for (uint32_t j = 0; j < D_SIM_LENGTH; j++)
    {
#ifdef D_DEBUG
      printf("\n------------------------------------------------------------");
#endif
      //source -> FIFO1
      block_length = rand() % D_MAX_WR_BLOCK;
#ifdef D_DEBUG
      printf("\n movement (%4d): source        -> FIFO1(%4d)", block_length, FIFO1_wr->free_space());
#endif
      if (block_length <= FIFO1_wr->free_space())
	{
	  for (uint32_t i = 0; i < block_length; i++)
	    FIFO1_wr[0][i] = data_2_write++;
	  FIFO1_wr->move_pointer(block_length);
#ifdef D_DEBUG
	  printf(" ok ");
#endif
	}
      else
	{
      rejected_count += block_length;
#ifdef D_DEBUG
      printf(" <- rejected");
#endif
	}

      //FIFO1 -> FIFO2
      block_length = rand() % D_MAX_WR_BLOCK;
#ifdef D_DEBUG
      printf("\n movement (%4d): FIFO1_1(%4d) -> FIFO2(%4d)",
	     block_length,
	     FIFO1_rd[1]->available_data(),
	     FIFO2_wr->free_space());
#endif
      if ((block_length <= FIFO2_wr->free_space()) &&
	  (block_length <= FIFO1_rd[1]->available_data()))
	{
	  for (uint32_t i = 0; i < block_length; i++)
	    FIFO2_wr[0][i] = (*FIFO1_rd[1])[i];
	  FIFO2_wr->move_pointer(block_length);
	  FIFO1_rd[1]->move_pointer(block_length);
#ifdef D_DEBUG
	  printf(" ok ");
#endif
	}
#ifdef D_DEBUG
      else
	printf(" <- rejected");
#endif

      
      //FIFO2 -> FIFO3
      block_length = rand() % D_MAX_WR_BLOCK;
#ifdef D_DEBUG
      printf("\n movement (%4d): FIFO2_0(%4d) -> FIFO3(%4d)",
	     block_length,
	     FIFO2_rd[0]->available_data(),
	     FIFO3_wr->free_space());
#endif
      if ((block_length <= FIFO3_wr->free_space()) &&
	  (block_length <= FIFO2_rd[0]->available_data()))
	{
	  for (uint32_t i = 0; i < block_length; i++)
	    FIFO3_wr[0][i] = (*FIFO2_rd[0])[i];
	  FIFO3_wr->move_pointer(block_length);
	  FIFO2_rd[0]->move_pointer(block_length);
#ifdef D_DEBUG
	  printf(" ok ");
#endif
	}
#ifdef D_DEBUG
      else
	printf(" <- rejected");
#endif

      
      //FIFO2 -> FIFO4
      block_length = rand() % D_MAX_WR_BLOCK;
#ifdef D_DEBUG
      printf("\n movement (%4d): FIFO2_1(%4d) -> FIFO4(%4d)",
	     block_length,
	     FIFO2_rd[1]->available_data(),
	     FIFO4_wr->free_space());
#endif
      if ((block_length <= FIFO4_wr->free_space()) &&
	  (block_length <= FIFO2_rd[1]->available_data()))
	{
	  for (uint32_t i = 0; i < block_length; i++)
	    FIFO4_wr[0][i] = (*FIFO2_rd[1])[i];
	  FIFO4_wr->move_pointer(block_length);
	  FIFO2_rd[1]->move_pointer(block_length);
#ifdef D_DEBUG
	  printf(" ok ");
#endif
	}
#ifdef D_DEBUG
      else
	printf(" <- rejected");
#endif

      
      //FIFO4 -> FIFO5
      block_length = rand() % D_MAX_WR_BLOCK;
#ifdef D_DEBUG
      printf("\n movement (%4d): FIFO4_0(%4d) -> FIFO5(%4d)",
	     block_length,
	     FIFO4_rd->available_data(),
	     FIFO5_wr->free_space());
#endif
      if ((block_length <= FIFO5_wr->free_space()) &&
	  (block_length <= FIFO4_rd->available_data()))
	{
	  for (uint32_t i = 0; i < block_length; i++)
	    FIFO5_wr[0][i] = (*FIFO4_rd)[i];
	  FIFO5_wr->move_pointer(block_length);
	  FIFO4_rd->move_pointer(block_length);
#ifdef D_DEBUG
	  printf(" ok ");
#endif
	}
#ifdef D_DEBUG
      else
	printf(" <- rejected");
#endif

      uint32_t max_2_read = FIFO1_rd[0]->available_data();
      if (max_2_read > FIFO3_rd->available_data())
	max_2_read = FIFO3_rd->available_data();
      if (max_2_read > FIFO5_rd->available_data())
	max_2_read = FIFO5_rd->available_data();
#ifdef D_DEBUG
      printf("\n check   (%4d): FIFO1_0(%4d), FIFO3(%4d), FIFO5(%4d)",
	     max_2_read,
	     FIFO1_rd[0]->available_data(),
	     FIFO3_rd->available_data(),
	     FIFO5_rd->available_data());
#endif
      D_BE probe_1_unpacked[D_P];
      D_BE probe_3_unpacked[D_P];
      D_BE probe_5_unpacked[D_P];
      for (uint32_t i = 0; i < max_2_read; i++)
	{
	  (*FIFO1_rd[0])[i].store(&probe_1_unpacked[0]);
	  (*FIFO3_rd)[i].store(&probe_3_unpacked[0]);
	  (*FIFO5_rd)[i].store(&probe_5_unpacked[0]);
	  for (uint32_t k = 0; k < D_P; k++)
	    {
	      if ((probe_1_unpacked[k] != probe_3_unpacked[k]) ||
		  (probe_3_unpacked[k] != probe_5_unpacked[k]) ||
		  (probe_5_unpacked[k] != data_2_read))
		error_count += 1;
	      element_count+= 1;
	    }
	  data_2_read++;
	}
      FIFO1_rd[0]->move_pointer(max_2_read);
      FIFO3_rd->move_pointer(max_2_read);
      FIFO5_rd->move_pointer(max_2_read);
    }
    printf("\n==========================================================");
    printf("\n\n TEST SUMMARY \n");
    if ((error_count == 0) && (element_count > 0))
      printf("\n Test result: PASSED");
    else
      printf("\n Test result: FAILED");
    printf("\n Transmitted element count = %lu", element_count/D_P);
    printf("\n Error count               = %lu", error_count/D_P);
    printf("\n Rejected count            = %lu", rejected_count);
    printf("\n");
    return 0;
}
      
