#ifndef C_FIFO_T_H
#define C_FIFO_T_H

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include "json.hpp"
#include <cmath>
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include "c_vector_logger_t.hpp"

using namespace std;

template <int T_P, class T_BE>
class c_fifo_t;

template <int T_P, class T_BE>
class c_fifo_port_wr_t
{
  friend class c_fifo_t<T_P, T_BE>;
private:
  typedef UME::SIMD::SIMDVec<T_BE, T_P> T_E;
  c_fifo_t<T_P, T_BE> *my_fifo;
  T_E *buffer;
  uint32_t pointer;
  uint32_t size;
public:
  c_fifo_port_wr_t()
  {
    my_fifo = nullptr;
    size    = 0;
    buffer  = nullptr;
    pointer = 0;
  }
  void initialize(c_fifo_t<T_P, T_BE> *p_fifo)
  {
    my_fifo = p_fifo;
    size    = p_fifo->size;
    buffer  = p_fifo->buffer;
    pointer = 0;
  }
  void reset_pointer(){pointer = 0;}
  inline void move_pointer(uint32_t shift)
  {
    my_fifo->activity_detected = true;
    if (free_space() >= shift)
      pointer = (pointer+shift) %size;
    else
      throw std::runtime_error(("c_fifo_t ERROR: write pointer surpassed read pointer [" + my_fifo->name + "]\n").c_str());
  }
  inline uint32_t get_pointer() const {return pointer;}
  inline uint32_t free_space() const { return my_fifo->get_free_space();}
  inline T_E &operator[](uint32_t index) {return buffer[(pointer+index)%size];}
  inline void write(T_E new_data){buffer[pointer]=new_data; pointer = (pointer + 1)%size;my_fifo->activity_detected = true;}
  inline void write(T_BE new_data){buffer[pointer]=new_data; pointer = (pointer + 1)%size;my_fifo->activity_detected = true;}
  void display() {my_fifo->display();}
};


template <int T_P, class T_BE>
class c_fifo_port_rd_t
{
  friend class c_fifo_t<T_P, T_BE>;
private:
  typedef UME::SIMD::SIMDVec<T_BE, T_P> T_E;
  c_fifo_t<T_P, T_BE> *my_fifo;
  T_E *buffer;
  uint32_t pointer;
  uint32_t size;
public:
  c_fifo_port_rd_t()
  {
    my_fifo = nullptr;
    size    = 0;
    buffer  = nullptr;
    pointer = 0;
  }
  void initialize(c_fifo_t<T_P, T_BE> *p_fifo)
  {
    my_fifo = p_fifo;
    size    = my_fifo->size;
    buffer  = my_fifo->buffer;
    pointer = 0;
  }
  void reset_pointer(){pointer = 0;}
  uint32_t available_data() const {return (size + my_fifo->get_wr_pointer() - pointer)%size;}
  uint32_t get_pointer() const {return pointer;}
  void move_pointer(uint32_t shift)
  {
    my_fifo->activity_detected = true;
    if (available_data() >= shift)
      pointer = (pointer+shift) %size;
    else
      throw std::runtime_error(("c_fifo_t ERROR: write pointer surpassed read pointer [" + my_fifo->name + "]\n").c_str());
  }
  T_E operator[](uint32_t index) const {return buffer[(pointer+index)%size];}
  T_E read(){uint32_t old_pointer = pointer; pointer = (pointer + 1)%size;my_fifo->activity_detected = true; return buffer[old_pointer];}
  void display() {my_fifo->display();}
};

template <int T_P, class T_BE>
class c_fifo_t
{
  friend class c_fifo_port_wr_t<T_P, T_BE>;
  friend class c_fifo_port_rd_t<T_P, T_BE>;
private:
  typedef UME::SIMD::SIMDVec<T_BE, T_P> T_E;
  string name;
  uint32_t size;
  T_E *buffer = nullptr;
  c_fifo_port_wr_t<T_P, T_BE> *write_port = nullptr;
  c_fifo_port_rd_t<T_P, T_BE> *read_ports = nullptr;
  uint32_t nof_accessible_read_ports;
  uint32_t nof_read_ports;
  char* auxForAlloc_0 = nullptr;
  bool activity_detected = false;
  c_vector_logger_t<T_E> my_logger;
  bool log;
public:
  string get_name() const {return name;}
  void resize_buffer(uint32_t i_size)
  {
    if (buffer != nullptr)    {delete[] auxForAlloc_0; buffer = nullptr;}
    size = i_size;
    auxForAlloc_0 = allocate_aligned_memory<T_E>(size, 128, buffer, "c_fifo_t.buffer");
    for (uint32_t i= 0; i < size; i++)
      buffer[i] = 0;
    write_port[0].initialize(this);
    for (uint32_t i = 0; i < nof_read_ports; i++)
      {
	read_ports[i].initialize(this);
      }
  }
  c_fifo_t():name("no_name") {
    log = false;
    size = 0;
    buffer = nullptr;
    write_port = nullptr;
    read_ports = nullptr;
    nof_accessible_read_ports = 0;
    nof_read_ports = 0;
    auxForAlloc_0 = nullptr;
    activity_detected = false;
    /*name = "no_name";*/}
  c_fifo_t(uint32_t i_size, uint32_t i_nof_accessible_read_ports, const string &i_name = "no_name"):name(i_name){
    log = false;
    size = 0;
    buffer = nullptr;
    write_port = nullptr;
    read_ports = nullptr;
    nof_accessible_read_ports = 0;
    nof_read_ports = 0;
    auxForAlloc_0 = nullptr;
    activity_detected = false;
    initialize(i_size, i_nof_accessible_read_ports, i_name);
    /*name = i_name;*/}
  void initialize(uint32_t i_size,
                  uint32_t i_nof_accessible_read_ports,
                  const string &i_name = "no_name",
                  bool i_log = false,
                  string i_log_file_name = "garbage.txt",
                  int i_nb = 1,
                  int i_nbf = 1,
                  bool i_signed = false)
  {
    log = i_log;
    free_resources();
    name = i_name;
    size = i_size;
    nof_accessible_read_ports = i_nof_accessible_read_ports;
    nof_read_ports = (log)? nof_accessible_read_ports+1:nof_accessible_read_ports; 
    auxForAlloc_0 = allocate_aligned_memory<T_E>(size, 128, buffer, "c_fifo_t.buffer");
    for (uint32_t i= 0; i < size; i++)
      buffer[i] = 0;
    write_port = new c_fifo_port_wr_t<T_P, T_BE>;
    write_port[0].initialize(this);
    read_ports = new c_fifo_port_rd_t<T_P, T_BE> [nof_read_ports];
    for (uint32_t i = 0; i < nof_read_ports; i++)
      {
	read_ports[i].initialize(this);
      }
    activity_detected = false;
    if (log)
      {
        my_logger.initialize(i_log_file_name, nullptr, "",1, i_nb, i_nbf, i_signed, false, false);
      }
  }
  ~c_fifo_t(){free_resources();}
  void free_resources()
  {
    if (buffer != nullptr)    {delete[] auxForAlloc_0; buffer = nullptr;}
    if (write_port != nullptr){delete      write_port; write_port = nullptr;}
    if (read_ports != nullptr){delete[]    read_ports; read_ports = nullptr;}
  }
  c_fifo_port_wr_t<T_P, T_BE>* get_wr_port(){return &write_port[0];}
  uint32_t get_nof_rd_ports() const {return nof_accessible_read_ports;}
  c_fifo_port_rd_t<T_P, T_BE>* get_rd_port(int index=0){return &read_ports[index];}
  void reset()
  {
    write_port->reset_pointer();
    for (uint32_t i = 0; i < nof_read_ports; i++)
      read_ports[i].reset_pointer();
    activity_detected = false;
  }
  void reset_activity_meter(){activity_detected = false;}
  bool get_activity_detected() const {return activity_detected;}
  uint32_t get_wr_pointer() const {return write_port[0].get_pointer();}
  uint32_t get_free_space()
  {
    uint32_t free_space = size;
    for (uint32_t i = 0; i < nof_read_ports; i++)
      {
	if (free_space > ((read_ports[i].pointer + size - 1 - write_port[0].pointer))%size)
	  free_space =   ((read_ports[i].pointer + size - 1 - write_port[0].pointer))%size;
      }
    return free_space;
  }
  void run_logger()
  {
    if (log)
      {
        while (read_ports[nof_read_ports-1].available_data() > 0)
          {
            T_E aux = read_ports[nof_read_ports-1].read();
            my_logger.add_log_entry(&aux);
          }
      }
  }
  void display()
  {
    printf("[size: %6u; ", size);
    printf("free: %6u; ", get_free_space());
    T_BE aux_for_unpack[T_P*3];
    aux_for_unpack[0] = 0;
    aux_for_unpack[T_P] = 0;
    aux_for_unpack[2*T_P] = 0;
    for (uint32_t i = 0; i < nof_read_ports; i++)
      {
	                read_ports[i][0].store(&aux_for_unpack[    0]);
        if (size > 1)	read_ports[i][1].store(&aux_for_unpack[  T_P]);
        if (size > 2)	read_ports[i][2].store(&aux_for_unpack[2*T_P]);					 
	//printf("data[%2u]: %6u [%d %d %d], ", i, read_ports[i].available_data(), (int32_t)aux_for_unpack[0], (int32_t)aux_for_unpack[T_P], (int32_t)aux_for_unpack[2*T_P]);
        printf("data[%2u", i);
        printf("]: %6u", read_ports[i].available_data());
                      printf(" [%d", (int32_t)aux_for_unpack[    0]);
        if (size > 1) printf(" %d",  (int32_t)aux_for_unpack[  T_P]);
        if (size > 2) printf(" %d",  (int32_t)aux_for_unpack[2*T_P]);
        printf("], ");
	printf("]");
      }
  }
  void display_mem_content()
  {
    for (int i = 0; i < size; i++)
      {
	printf("\n [%6d] = %6d ", i, buffer[i]);
	if (write_port[0].pointer == i)
	  printf(" <-WR ");
	else
	  printf("      ");
	for (int j = 0; j < nof_read_ports; j++)
	  {
	    if (read_ports[j].pointer == i)
	      printf(" <-RD_%d ", j);
	    else
	      printf("         ");
	  }
      }
    printf("\n free space = %d", get_free_space());
    for (int j = 0; j < nof_read_ports; j++)
      printf("\n available_data[%d] = %u", j, read_ports[j].available_data());
    printf("\n==============================================");
  }
    
};
      
  




#endif
