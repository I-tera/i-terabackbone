import numpy as np
#cimport numpy as np
from libcpp.string cimport string
from libcpp cimport bool
import pdb

cdef class py_ie_module:
     cdef c_interface_emulator_t_4python ie_module # Cpp instance
     cdef int i_nof_ports
     cdef int o_nof_ports
     cdef int p_nof_ports
     cdef int i_total_length
     cdef int o_total_length
     cdef int p_total_length

     def get_nof_i_ports(self):
         return self.i_nof_ports
     def get_nof_o_ports(self):
         return self.o_nof_ports
     def get_nof_p_ports(self):
         return self.p_nof_ports
     

     def __cinit__(self):
         i_nof_ports = 0
         o_nof_ports = 0
         p_nof_ports = 0
         i_total_length = 1
         o_total_length = 1
         p_total_length = 1

     def initialize(self, json_file_name, json_module_name = b'one_file'):
         self.ie_module = c_interface_emulator_t_4python()
         cdef string c_json_file_name = json_file_name
         cdef string c_json_module_name = json_module_name
         cdef bool c_skip_first_latency = False
         if (json_module_name == b'one_file'):
              self.ie_module.initialize(c_json_file_name, c_skip_first_latency)
         else:
              self.ie_module.initialize(c_json_file_name, c_json_module_name, c_skip_first_latency)
         self.i_nof_ports = self.ie_module.get_nof_i_ports()
         self.o_nof_ports = self.ie_module.get_nof_o_ports()
         self.p_nof_ports = self.ie_module.get_nof_p_ports()
         cdef int port_number = 0
         self.i_total_length = 0
         for x in range (0,self.i_nof_ports):
             port_number = x
             self.i_total_length += self.ie_module.get_i_port_parallelism(port_number)
         self.o_total_length = 0
         for x in range (0,self.o_nof_ports):
             port_number = x
             self.o_total_length += self.ie_module.get_o_port_parallelism(port_number)
         self.p_total_length = 0
         for x in range (0,self.p_nof_ports):
             port_number = x
             self.p_total_length += self.ie_module.get_p_port_parallelism(port_number)
         if (self.i_total_length == 0):
             self.i_total_length = 1
         if (self.o_total_length == 0):
             self.o_total_length = 1
         if (self.p_total_length == 0):
             self.p_total_length = 1

     def send_static(self, key, val):
         cdef string c_i_static_key = key.encode()
         container = np.empty(val.size, dtype='int32')
         container = val
         cdef int[::1] c_i_static_val = container
         self.ie_module.set_static(c_i_static_key, &c_i_static_val[0])
         

     def run(self, i_dict):
        #print('i: ', self.i_nof_ports, ' total: ', self.i_total_length)
        #print('o: ', self.o_nof_ports, ' total: ', self.o_total_length)
        #print('p: ', self.p_nof_ports, ' total: ', self.p_total_length)

        for my_key, my_val in i_dict.items():
            if ("i_static_" in my_key):
              # print('key = ', my_key, ' val = ', my_val)
                self.send_static(my_key, my_val)
              # print('done')

        #load inputs
        i_data = np.empty(self.i_total_length, dtype='int32')
        start_idx = 0
        cdef int port_number = 0
        for x in range (0,self.i_nof_ports):
            port_number = x
            port_name = self.ie_module.get_i_port_name(port_number)
            port_paral = self.ie_module.get_i_port_parallelism(port_number)
            key = str(port_name)
            key = key[2:-1]
            #print('i_port number ', x, '[', port_paral, ',', port_name, ',', key)
            i_data[start_idx:start_idx+port_paral] = i_dict[key]
            start_idx += port_paral
        cdef int[::1] c_i_data = i_data            
        cdef int      c_i_valid = i_dict['i_valid'][0]
        cdef int      c_i_sync = i_dict['i_sync'][0]
        cdef int      c_rst_async_n = i_dict['rst_async_n'][0]
        
        o_data = np.empty(self.o_total_length, dtype='int32')
        cdef int[::1] c_o_data = o_data
        cdef int      c_o_valid = 0
        cdef int      c_o_sync = 0
        p_data = np.empty(self.p_total_length, dtype='int32')
        cdef int[::1]   c_p_data = p_data

        #print('calling clock')
        self.ie_module.clock(&c_i_data[0], c_i_valid, c_i_sync, c_rst_async_n,
                             &c_o_data[0], c_o_valid, c_o_sync, &c_p_data[0])
        #print('back from clock')

        o_dict = {}
        o_dict['o_valid'] = c_o_valid
        o_dict['o_sync']  = c_o_sync
        start_idx = 0
        for x in range (0,self.o_nof_ports):
            port_number = x
            port_name = self.ie_module.get_o_port_name(port_number)
            port_paral = self.ie_module.get_o_port_parallelism(port_number)
            key = str(port_name)
            key = key[2:-1]
            #print('o_port_number ', x, '[', port_paral, ',', port_name, ',', key)
            o_dict[key] = o_data[start_idx:start_idx+port_paral]
            start_idx += port_paral
        start_idx = 0
        for x in range (0,self.p_nof_ports):
            port_number = x
            port_name = self.ie_module.get_p_port_name(port_number)
            port_paral = self.ie_module.get_p_port_parallelism(port_number)
            key = str(port_name)
            key = key[2:-1]
            #print('p_port_number ', x, '[', port_paral, ',', port_name, ',', key)
            o_dict[key] = p_data[start_idx:start_idx+port_paral]
            start_idx += port_paral
            
        return o_dict
         