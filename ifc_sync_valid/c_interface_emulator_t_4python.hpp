//STUFF TO BE DEFINED EXTERNALLY
//#define D_T_P    8
//#define D_T_BE_i uint32_t
//#define D_T_BE_f float
//#define D_FILE_TO_INCLUDE "module_file"
//#define D_DUT_T <class_definition>


////////////////////////////////////////////////////////////
#define D_T_E_f  UME::SIMD::SIMDVec<D_T_BE_f, D_T_P>
#define D_T_E_i  UME::SIMD::SIMDVec<D_T_BE_i, D_T_P>
#include D_FILE_TO_INCLUDE
#include "c_interface_emulator_t.hpp"
#include "json.hpp"


#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>
#include <memory>
#include <string.h>
#include <sstream>


class c_interface_emulator_t_4python
{
private:
  c_interface_emulator_t<D_DUT_T> my_dut_ie;
  int32_t **i_data;
  int32_t **o_data;
  int32_t **p_data;
public:
  c_interface_emulator_t_4python()
  {
    i_data = nullptr;
    o_data = nullptr;
    p_data = nullptr;
  }
  void initialize(string json_file_name, string json_module_name, bool skip_first_latency = false)
  {
    FILE *p_file;
    p_file = fopen("module_simulator.log", "w");
    fclose(p_file);

    try
      { 
        free_resources();
        std::ifstream i_file(json_file_name);
        nlohmann::json config;
        i_file >> config;


        //printf("\n\n\n\n\n CHECKPOINT 0");
        nlohmann::json sim_profile;
        
        if (config.find("SimulationProfile") != config.end())
          sim_profile = config["SimulationProfile"];
        else
          sim_profile = config;
        
        //printf("\n\n\n\n\n CHECKPOINT 1");fflush(stdout);
        nlohmann::json params = sim_profile;
        std::size_t current, previous = 0;
        current = json_module_name.find('.');
        string aux;

        
        //std::cout << "\n\n\n";
        //std::cout << params.dump();
        //fflush(stdout);
        while (current != std::string::npos)
          {
            aux = json_module_name.substr(previous, current-previous);
            params = params[aux];
            //std::cout << "\n\n\n" << aux << "\n";
            //std::cout << params.dump();
            //fflush(stdout);
            previous = current+1;
            current = json_module_name.find('.', previous);
          }
        aux = json_module_name.substr(previous, current-previous);
        params = params[aux];

        //std::cout << "\n\n\n" << aux << "\n";
        //std::cout << params.dump();
        //fflush(stdout);
        
        try
          {
            params["BasePath"]   = sim_profile["BasePath"];
            params["Modulation"] = sim_profile["Broadcast"]["Modulation"];
            params["AllZeros"]   = sim_profile["Broadcast"]["AllZeros"];
            params["LogFolder"]  = "garbage";
        }
        catch(...)
            {
            }
  
        my_dut_ie.initialize(params, skip_first_latency);
  
        //assembly of i_data structures
        i_data = new int32_t*[my_dut_ie.get_nof_i_ports()];
        o_data = new int32_t*[my_dut_ie.get_nof_o_ports()];
        p_data = new int32_t*[my_dut_ie.get_nof_p_ports()];
      }
    catch (std::runtime_error& e)
      {
        FILE *p_file;
        p_file = fopen("module_simulator.log", "w");
        fprintf(p_file, "%s", e.what());
        fclose(p_file);
        throw;
      }
  }
  void initialize(string json_file_name, bool skip_first_latency = false)
  {
    FILE *p_file;
    p_file = fopen("module_simulator.log", "w");
    fclose(p_file);
    try
      {
        free_resources();
        //printf("\n json name = %s", json_file_name.c_str());fflush(stdout);
        std::ifstream i_file(json_file_name);
        nlohmann::json config;
#ifndef IDEA_JSON
        i_file >> config;
#else
        config = nlohmann::json::parse(i_file);
#endif
        // printf("\n n type = %s", config["Type"].get<string>().c_str());fflush(stdout);
  
        my_dut_ie.initialize(config, skip_first_latency);
  
        //assembly of i_data structures
        i_data = new int32_t*[my_dut_ie.get_nof_i_ports()];
        o_data = new int32_t*[my_dut_ie.get_nof_o_ports()];
        p_data = new int32_t*[my_dut_ie.get_nof_p_ports()];
      }
    catch (std::runtime_error& e)
      {
        FILE *p_file;
        p_file = fopen("module_simulator.log", "w");
        fprintf(p_file, "%s", e.what());
        fclose(p_file);
        throw;
      }
  }
    
  
  void set_static(string static_name, int32_t *static_values){my_dut_ie.set_static(static_name, static_values);}
  
  ~c_interface_emulator_t_4python(){free_resources();}
  void free_resources()
  {
    try
      {
        if (i_data != nullptr)
          delete[] i_data;
        i_data = nullptr;
        if (o_data != nullptr)
          delete[] o_data;
        o_data = nullptr;
        if (p_data != nullptr)
          delete[] p_data;
        p_data = nullptr;
        my_dut_ie.free_resources();
      }
    catch (std::runtime_error& e)
      {
        FILE *p_file;
        p_file = fopen("module_simulator.log", "w");
        fprintf(p_file, "%s", e.what());
        fclose(p_file);
        throw;
      }
  }
  
  void clock(int32_t *i_data, int32_t i_valid, int32_t i_sync, int32_t i_rst,
             int32_t *o_data, int32_t &o_valid, int32_t &o_sync,
             int32_t *p_data)
  {
    try
      {
        //printf("\n c_interface_emulator_t_4python checkpoint 0\n");fflush(stdout);
        //generation of the arrays of arrays based on the piled arrays
        int32_t piled_data_position = 0;
        for (int i = 0; i < my_dut_ie.get_nof_i_ports(); i++)
          {
            this->i_data[i] = &i_data[piled_data_position];
            piled_data_position += my_dut_ie.get_i_port_parallelism(i);
            /*for (int j = 0; j < my_dut_ie.get_i_port_parallelism(i); j++)
              {
              printf("\n i_data[%d][%d] = %d", i, j, this->i_data[i][j]);
              fflush(stdout);
              }//*/
          }
    
        //printf("\n c_interface_emulator_t_4python checkpoint 1\n");fflush(stdout);
        piled_data_position = 0;
        for (int i = 0; i < my_dut_ie.get_nof_o_ports(); i++)
          {
            this->o_data[i] = &o_data[piled_data_position];
            piled_data_position += my_dut_ie.get_o_port_parallelism(i);
            /*for (int j = 0; j < my_dut_ie.get_o_port_parallelism(i); j++)
              {
              printf("\n o_data[%d][%d] = %d", i, j, this->o_data[i][j]);
              fflush(stdout);
              }//*/
          }
    
        //printf("\n c_interface_emulator_t_4python checkpoint 2\n");fflush(stdout);
        piled_data_position = 0;
        for (int i = 0; i < my_dut_ie.get_nof_p_ports(); i++)
          {
            this->p_data[i] = &p_data[piled_data_position];
            piled_data_position += my_dut_ie.get_p_port_parallelism(i);
            /*for (int j = 0; j < my_dut_ie.get_p_port_parallelism(i); j++)
              {
              printf("\n p_data[%d][%d] = %d", i, j, this->p_data[i][j]);
              fflush(stdout);
              }//*/
          }

        //printf("\n c_interface_emulator_t_4python checkpoint 3\n");fflush(stdout);
        my_dut_ie.clock(this->i_data,
                        i_valid,
                        i_sync,
                        i_rst,
                        this->o_data,
                        o_valid,
                        o_sync,
                        this->p_data);
      }
    catch (std::runtime_error& e)
      {
        FILE *p_file;
        p_file = fopen("module_simulator.log", "w");
        fprintf(p_file, "%s", e.what());
        fclose(p_file);
        throw;
      }
    
    //printf("\n c_interface_emulator_t_4python checkpoint 4\n");fflush(stdout);
  }
  int32_t get_i_port_value_min(int port)  const {return my_dut_ie.get_i_port_value_min(port);}
  int32_t get_o_port_value_min(int port)  const {return my_dut_ie.get_o_port_value_min(port);}
  int32_t get_p_port_value_min(int port)  const {return my_dut_ie.get_p_port_value_min(port);}
  int32_t get_i_port_value_range(int port)const {return my_dut_ie.get_i_port_value_range(port);}
  int32_t get_o_port_value_range(int port)const {return my_dut_ie.get_o_port_value_range(port);}
  int32_t get_p_port_value_range(int port)const {return my_dut_ie.get_p_port_value_range(port);}
  int32_t get_nof_i_ports()               const {return my_dut_ie.get_nof_i_ports();}
  int32_t get_nof_o_ports()               const {return my_dut_ie.get_nof_o_ports();}
  int32_t get_nof_p_ports()               const {return my_dut_ie.get_nof_p_ports();}
  int32_t get_i_port_parallelism(int port)const {return my_dut_ie.get_i_port_parallelism(port);}
  int32_t get_o_port_parallelism(int port)const {return my_dut_ie.get_o_port_parallelism(port);}
  int32_t get_p_port_parallelism(int port)const {return my_dut_ie.get_p_port_parallelism(port);}
  string  get_i_port_name(int port)       const {return my_dut_ie.get_i_port_name(port);}
  string  get_o_port_name(int port)       const {return my_dut_ie.get_o_port_name(port);}
  string  get_p_port_name(int port)       const {return my_dut_ie.get_p_port_name(port);}

  /**********************************************************************************************/
  /***************************  Method overloading for DPI implementation ***********************/
  /**********************************************************************************************/

  void clock(int32_t **i_data, int32_t i_valid, int32_t i_sync,
	     int32_t i_rst_n, int32_t **o_data, int32_t *o_valid,
	     int32_t *o_sync, int32_t **p_data)
  {
    try
      {

        int32_t aux_o_valid;
        int32_t aux_o_sync;

        my_dut_ie.clock(i_data,
			i_valid,
			i_sync,
			i_rst_n,
                        o_data,
                        aux_o_valid,
                        aux_o_sync,
                        p_data);

	*o_valid = aux_o_valid;
	*o_sync = aux_o_sync;

      }
    catch (std::runtime_error& e)
      {
        FILE *p_file;
        p_file = fopen("module_simulator.log", "w");
        fprintf(p_file, "%s", e.what());
        fclose(p_file);
        throw;
      }
  }
    
};
