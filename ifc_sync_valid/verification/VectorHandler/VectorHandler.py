import sys
class VectorHandler:
    def write_file(self,file_, data):
        buffer = ''
        with open(file_, 'w') as f:
            for parallelism in data:
                if type(parallelism) == int:
                    buffer += "%s\n"%parallelism
                    #f.write("%s\n" % parallelism)
                else:
                    for sample in parallelism:
                        buffer += "%s "%sample
                        #f.write("%s " % sample)
                    buffer += "\n"
                    #f.write("\n")
            f.write(buffer)

    def read_file(self,file_):
        try:
            obj_f= open(file_, "r")
        except FileNotFoundError:
            print("\n\nFile < %s > not found. Did the RTL simulation run?\n\n"%(file_))
            sys.exit()

        obj = obj_f.readlines()
        obj = [" ".join(line.split()) for line in obj]
        res = []
        for line in obj:
            res.append([s if (s != 'x' and s !='X') else '0' for s in line.split(' ')])
        obj_f.close()
        return res
