//responsibilities for the invoker
//#include "<module_source_file>"
//name by which the json substructure is to be found in the json file base->"SimulationProfile"->D_JSON_NAME
//#define D_JSON_NAME     "<module_name_within_json"
//type of the module to be wrapped. Needs to be child of c_module_base_t
//#define D_DUT_T         <class_definition>




#include "c_interface_emulator_t.hpp"
#include "json.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <fstream>

#include <math.h>
#include <matrix.h>
#include <mex.h>

#include <vector>
#include <map>
#include <algorithm>
#include <memory>
#include <string.h>
#include <sstream>


using namespace std;


/////////////////////////////////////////////////////////////////
// Persistent variables /////////////////////////////////////////
/////////////////////////////////////////////////////////////////
c_interface_emulator_t<D_DUT_T> my_dut_ie;



void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  const mwSize *dims;
  const mwSize *dims1;
  const mwSize *dims2;
  const mwSize *dims3;
  const mwSize *dims4;
  const mwSize *dims5;
  /////////////////////////////////////////////////////////////////////
  // Displaying call summary //////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////
  /*
    mexPrintf("\n======================================================");
    mexPrintf("\n== Function call summary==============================");
    mexPrintf("\n======================================================");
    mexPrintf("\n Called with %d arguments", nrhs);
    mexPrintf("\n Returning %d values", nlhs);
    for (int i = 0; i < nrhs; i++)
    {
    dims = mxGetDimensions(prhs[i]);
    mexPrintf("\n Argument[%d] has dimensions %d x %d", i, (int)dims[0], (int)dims[1]);
    }
    /* */
  /////////////////////////////////////////////////////////////////////
  // command validation ///////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////
  dims = mxGetDimensions(prhs[0]);
  if ( mxIsChar(prhs[0]) != 1)
    mexErrMsgTxt("\n ERROR: Command must be a string");
  if (((int)dims[0] != 1))
    mexErrMsgTxt("\n ERROR: Command must be of size 1xN");
  char *i_Command = mxArrayToString(prhs[0]);
  //mexPrintf("\n Command accepted (%s)", i_Command);
    
  if (strcmp(i_Command, "Initialize") == 0)
    {
      
      //mexPrintf("\n Comando = Initialize");
      //Parameter dimension check
      if (nrhs != 2)
        mexErrMsgTxt("\n ERROR: Initialize function must receive 1 parameters");
      if (mxIsChar(prhs[1]) != 1)
        mexErrMsgTxt("\n ERROR: Initialize function must receive a string as parameter");
      dims = mxGetDimensions(prhs[1]);
      if (((int)dims[0] != 1))
        mexErrMsgTxt("\n EERROR: Initialize function must receive a string of size 1xN");
      char *i_JsonFilename = mxArrayToString(prhs[1]);
      string JsonFilename(i_JsonFilename);

      std::ifstream i_file(JsonFilename);
      nlohmann::json config;
      i_file >> config;
      nlohmann::json SimProfile = config["SimulationProfile"];
      string auxSimParam_mFolderName = SimProfile["BasePath"].get<string>() + "/" + SimProfile["ResultFolderName"].get<string>();
      nlohmann::json Params  = SimProfile[D_JSON_NAME];
      Params["BasePath"]   = SimProfile["BasePath"];
      Params["modulation"] = SimProfile["Broadcast"]["modulation"];
      Params["all_zeros"]   = SimProfile["Broadcast"]["all_zeros"];
      Params["LogFolder"]  = auxSimParam_mFolderName + "/" + D_JSON_NAME + "_vectors";

      my_dut_ie.initialize(Params);
    }
  else if (strcmp(i_Command, "SetStatic") == 0)
    {
      if (nrhs != 3)
        mexErrMsgTxt("\n ERROR: SetStatic function must receive 2 parameters");
      if (mxIsChar(prhs[1]) != 1)
        mexErrMsgTxt("\n ERROR: SetStatic.param1 must be a string");
      dims = mxGetDimensions(prhs[1]);
      if (((int)dims[0] != 1))
        mexErrMsgTxt("\n ERROR: SetStatic.param1 must be a string of size 1xN");
      char *i_static_name = mxArrayToString(prhs[1]);
      string static_name(i_static_name);

      
      if (((int)dims[0] != 1))
        mexErrMsgTxt("\n ERROR: SetStatic.param2 must be an array of size 1xN");
      dims = mxGetDimensions(prhs[2]);
      int32_t *static_value = new int32_t[(int)dims[1]];
      double  *i_mat = mxGetPr(prhs[2]);
      for (int p = 0; p < (int)dims[1]; p++)
        {
          static_value[p] = int32_t(i_mat[p]);
        }

      
      my_dut_ie.set_static(static_name,
                           static_value);
      
    }     
  else if (strcmp(i_Command, "Clock") == 0)
    {      
      //mexPrintf("\n Comando = Decode");
      //Input dimension check
      //printf("\n expected 1+3+%d inputs, received %d", my_dut_ie.get_nof_i_ports(), nrhs);fflush(stdout);
      if (nrhs != 1+3+my_dut_ie.get_nof_i_ports())
	{ 
	  mexErrMsgTxt("\n ERROR: Incompatible number of parameters");
	}

      dims1 = mxGetDimensions(prhs[1]);
      if (dims1[0] != 1)
	mexErrMsgTxt("\n ERROR: second parameter must be of size 1 (rst_n)");
      dims1 = mxGetDimensions(prhs[2]);
      if (dims1[0] != 1)
	mexErrMsgTxt("\n ERROR: second parameter must be of size 1 (i_valid)");
      dims1 = mxGetDimensions(prhs[3]);
      if (dims1[0] != 1)
	mexErrMsgTxt("\n ERROR: second parameter must be of size 1 (i_sync)");
      
      for (int i = 0; i < my_dut_ie.get_nof_i_ports(); i++)
	{
	  dims1 =  mxGetDimensions(prhs[1+3+i]); //input symbol stream
	  if (dims1[0] != my_dut_ie.get_i_port_parallelism(i))
	    {
	      char msg[100];
	      sprintf(msg, "\n ERROR: incompatible input parallelism (exp %d, rec %lu)", my_dut_ie.get_i_port_parallelism(i), dims1[0]);
	      mexErrMsgTxt(msg);
	    }
	}

      if (nlhs != my_dut_ie.get_nof_o_ports() + my_dut_ie.get_nof_p_ports() + 2)
	{
	  mexErrMsgTxt("\n ERROR: Incompatible number of return variables");
	}

      
      //Creation of output variables
      plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL); //o_valid
      plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL); //o_sync
      for (int i = 0; i < my_dut_ie.get_nof_o_ports(); i++)
	plhs[2+i] = mxCreateDoubleMatrix(my_dut_ie.get_o_port_parallelism(i), 1, mxREAL);
      for (int i = 0; i < my_dut_ie.get_nof_p_ports(); i++)
	plhs[2+i+my_dut_ie.get_nof_o_ports()] = mxCreateDoubleMatrix(my_dut_ie.get_p_port_parallelism(i), 1, mxREAL);

      //creation of matlab pointers
      double  *i_mat_rst_n = mxGetPr(prhs[1]);
      double  *i_mat_valid = mxGetPr(prhs[2]);
      double  *i_mat_sync  = mxGetPr(prhs[3]);
      double **i_mat_data  = new double*[my_dut_ie.get_nof_i_ports()];
      for (int i = 0; i < my_dut_ie.get_nof_i_ports(); i++)
	i_mat_data[i] = mxGetPr(prhs[4+i]);

      double  *o_mat_valid = mxGetPr(plhs[0]);
      double  *o_mat_sync  = mxGetPr(plhs[1]);
      double **o_mat_data  = new double*[my_dut_ie.get_nof_o_ports()];
      for (int i = 0; i < my_dut_ie.get_nof_o_ports(); i++)
	o_mat_data[i] = mxGetPr(plhs[2+i]);
      double **o_mat_p_data = new double*[my_dut_ie.get_nof_p_ports()];
      for (int i = 0; i < my_dut_ie.get_nof_p_ports(); i++)
	o_mat_p_data[i] = mxGetPr(plhs[2+i+my_dut_ie.get_nof_o_ports()]);

      //creation of actual data
      int32_t **i_data  = new int32_t*[my_dut_ie.get_nof_i_ports()];
      for (int i = 0; i < my_dut_ie.get_nof_i_ports(); i++)
	i_data[i] = new int32_t[ my_dut_ie.get_i_port_parallelism(i)];
      int32_t   i_valid;
      int32_t   i_sync;
      int32_t   i_rst;
      int32_t **o_data = new int32_t*[my_dut_ie.get_nof_o_ports()];
      for (int i = 0; i < my_dut_ie.get_nof_o_ports(); i++)
	o_data[i] = new int32_t[my_dut_ie.get_o_port_parallelism(i)];
      int32_t   o_valid;
      int32_t   o_sync;
      int32_t **p_probes = new int32_t*[my_dut_ie.get_nof_p_ports()];
      for (int i = 0; i < my_dut_ie.get_nof_p_ports(); i++)
	p_probes[i] = new int32_t[my_dut_ie.get_p_port_parallelism(i)];

      //load of input data      
      i_valid = int32_t(*i_mat_valid);
      i_sync  = int32_t(*i_mat_sync);
      i_rst   = int32_t(*i_mat_rst_n);
      for (int i = 0; i < my_dut_ie.get_nof_i_ports(); i++)
	{
	  for (int p = 0; p < my_dut_ie.get_i_port_parallelism(i); p++)
	    i_data[i][p] = int32_t(i_mat_data[i][p]);
	}

      //operation
      my_dut_ie.clock(i_data,
		      i_valid,
		      i_sync,
		      i_rst,
		      o_data,
		      o_valid,
		      o_sync,
		      p_probes);

      //download of output data
      *o_mat_valid = (double)o_valid;
      *o_mat_sync  = (double)o_sync;
      for (int i = 0; i < my_dut_ie.get_nof_o_ports(); i++)
	for (int p = 0; p < my_dut_ie.get_o_port_parallelism(i); p++)
	  o_mat_data[i][p] = (double)o_data[i][p];
      for (int i = 0; i < my_dut_ie.get_nof_p_ports(); i++)
	for (int p = 0; p < my_dut_ie.get_p_port_parallelism(i); p++)
	  o_mat_p_data[i][p] = (double)p_probes[i][p];



      //creation of matlab pointers
      delete [] i_mat_data;
      delete [] o_mat_data;
      delete [] o_mat_p_data;
      
      //creation of actual data
      for (int i = 0; i < my_dut_ie.get_nof_i_ports(); i++)
	delete[] i_data[i];
      delete [] i_data;
      for (int i = 0; i < my_dut_ie.get_nof_o_ports(); i++)
	delete[] o_data[i];
      delete[] o_data;
      for (int i = 0; i < my_dut_ie.get_nof_p_ports(); i++)
	delete[] p_probes[i];
      delete[] p_probes;
      
    }
  else
    mexErrMsgTxt("\n ERROR: Comando no reconocido");
  //mexPrintf("\n");
    
}
