# Make cpp class RETIMER, and some of its internal methods
from libcpp.string cimport string
#cimport numpy as np
from libcpp cimport bool

cdef extern from "c_interface_emulator_t_4python.hpp":
     cdef cppclass c_interface_emulator_t_4python:
          c_interface_emulator_t_4python()          
          void initialize(string json_file_name, string json_module_name, bool skip_first_latency)
          void initialize(string json_file_name, bool skip_first_latency)
          void set_static(string static_name, int *static_values)
          void clock(int *i_data, int i_valid, int i_sync, int i_rst,
                     int *o_data, int &o_valid, int &o_sync, int *p_data)
          int get_nof_i_ports()
          int get_nof_o_ports()
          int get_nof_p_ports()
          int get_i_port_parallelism(int port)
          int get_o_port_parallelism(int port)
          int get_p_port_parallelism(int port)
          string get_i_port_name(int port)
          string get_o_port_name(int port)
          string get_p_port_name(int port);
