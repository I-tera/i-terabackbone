#ifndef C_INTERFACE_EMULATOR_T_H
#define C_INTERFACE_EMULATOR_T_H


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "json.hpp"
#include "UMESimd.h"
#include "c_fxp_t.hpp"
#include "c_fsm_base.hpp"
#include "c_fsm_gated_latency.hpp"
#include "c_fsm_latency.hpp"
#include "c_fsm_par_conv.hpp"
#include "c_fsm_synchronizer.hpp"

//#define D_IE_VERBOSE

enum e_interface_emulator_state
{
  reset,
  working
};


  
using namespace std;


/// Class that builds a virtual interface around a module that ignores it
/// The interface is composed of the following signals:
///   i_rst_n: synchronous reset signal, active low
///   i_valid: validates the state of all other signals except i_rst_n.
///            If i_valid is low, all input signals shall be ignored;
///            otherwise they shall be used
///   i_sync: signal that synchronizes "blocks" of data, whatever they are.
///            The system stays idle until the first one arrives and starts
///            processing data. It may be active only every a predefined
///            number of valid cycles; if active in a forbidden moment, the
///            module resets
///   i_data: as many input ports as necessary. All of them use the same
///            single i_valid and i_sync
///   o_valid: the same as i_valid, but towards the output
///   o_sync:  a copy of i_sync, synchronized with the data
///   o_data: as many output ports as necessary. All of them use the same
///            single o_valid and o_sync
/// template class T_WRAPPED: the class that implements the module to be wrapped
template <class T_WRAPPED>
class c_interface_emulator_t
{
public:
  const static int T_Parallelism = T_WRAPPED::T_Parallelism;
private:
  // wrapped module
  T_WRAPPED    my_real_dut;
  c_module_base_t<T_WRAPPED::T_Parallelism, typename T_WRAPPED::T_BE_int, typename T_WRAPPED::T_BE_float> *my_dut;

  //FSM stuff
  uint32_t      output_extra_latency;
  uint32_t      i2dut_latency;
  uint32_t      nof_fsms;
  s_fsm_config *fsm_config;
  c_fsm_base  **my_fsms;
  uint32_t     internal_i_valid;
  uint32_t     internal_i_sync;
  uint32_t     internal_i_soft_rst;
  uint32_t    *internal_o_valid;
  uint32_t    *internal_o_sync;
  uint32_t    *internal_o_soft_rst;
  uint32_t    *internal_pre_o_valid;
  uint32_t     frozen_zero;
  uint32_t    *datapath_enable;
  uint32_t    *datapath_soft_rst;



  // INPUT PORTS STUFF
  float    i_parallelism_multiplier;
  uint32_t                                                         nof_i_ports;
  s_signal_config<T_Parallelism>                                  *i_ports_config;
  int32_t                                                       ***i_data_copy;
  
  uint32_t                                                         nof_i_int_ports;
  c_fifo_t        <T_Parallelism, typename T_WRAPPED::T_BE_int>   *i_int_fifos;
  c_fifo_port_wr_t<T_Parallelism, typename T_WRAPPED::T_BE_int>  **i_int_ports_wr;
  
  uint32_t                                                         nof_i_float_ports;
  c_fifo_t        <T_Parallelism, typename T_WRAPPED::T_BE_float> *i_float_fifos;  
  c_fifo_port_wr_t<T_Parallelism, typename T_WRAPPED::T_BE_float>**i_float_ports_wr;

  // OUTPUT PORT STUFF
  float    o_parallelism_multiplier;
  int32_t                                                          output_latency;
  uint32_t                                                         nof_o_ports;
  s_signal_config<T_Parallelism>                                  *o_ports_config;
  int32_t                                                        **o_data_copy;
  
  uint32_t                                                         nof_o_int_ports;
  c_fifo_t        <T_Parallelism, typename T_WRAPPED::T_BE_int>   *o_int_fifos;
  c_fifo_port_rd_t<T_Parallelism, typename T_WRAPPED::T_BE_int>  **o_int_ports;
  
  uint32_t                                                         nof_o_float_ports;
  c_fifo_t        <T_Parallelism, typename T_WRAPPED::T_BE_float> *o_float_fifos;
  c_fifo_port_rd_t<T_Parallelism, typename T_WRAPPED::T_BE_float>**o_float_ports;

  // INTERNAL PORT STUFF
  float    p_parallelism_multiplier;
  uint32_t                                                         nof_p_ports;
  s_signal_config<T_Parallelism>                                  *p_ports_config;
  c_fifo_t        <T_Parallelism, int32_t>                        *p_fifos;
  c_fifo_port_rd_t<T_Parallelism, int32_t>                       **p_ports;
  c_fifo_port_wr_t<T_Parallelism, int32_t>                       **p_ports_wr_recirc;
  c_fifo_port_rd_t<T_Parallelism, int32_t>                       **p_ports_rd_recirc;

  e_interface_emulator_state state;
  uint64_t                   sync_cycle_counter;
  uint64_t                   sync_period;
public:
  /// Constructor: calls the function void initialize(nlohmann::json config)
  /// Default constructor: performs nothing
  c_interface_emulator_t()
  {
    my_dut                   = &my_real_dut;
    output_extra_latency     = 0;
    i2dut_latency            = 0;
    nof_fsms                 = 0;
    fsm_config               = nullptr;
    my_fsms                  = nullptr;
    internal_i_valid         = 0;
    internal_i_sync          = 0;
    internal_i_soft_rst      = 0;
    internal_o_valid         = nullptr;
    internal_o_sync          = nullptr;
    internal_o_soft_rst      = nullptr;
    internal_pre_o_valid     = nullptr;
    frozen_zero              = 0;
    datapath_enable          = nullptr;
    datapath_soft_rst        = nullptr;
    i_parallelism_multiplier = 1;
    nof_i_ports              = 0;
    i_ports_config           = nullptr;
    i_data_copy              = nullptr;
    nof_i_int_ports          = 0;
    i_int_fifos              = nullptr;
    i_int_ports_wr           = nullptr;
    nof_i_float_ports        = 0;
    i_float_fifos            = nullptr;  
    i_float_ports_wr         = nullptr;
    o_parallelism_multiplier = 1;
    output_latency           = -1;
    nof_o_ports              = 0;
    o_ports_config           = nullptr;
    o_data_copy              = nullptr;
    nof_o_int_ports          = 0;
    o_int_fifos              = nullptr;
    o_int_ports              = nullptr;
    nof_o_float_ports        = 0;
    o_float_fifos            = nullptr;
    o_float_ports            = nullptr;
    p_parallelism_multiplier = 1;
    nof_p_ports              = 0;
    p_ports_config           = nullptr;
    p_fifos                  = nullptr;
    p_ports                  = nullptr;
    p_ports_wr_recirc        = nullptr;
    p_ports_rd_recirc        = nullptr;
    state                    = reset;
    sync_cycle_counter       = 0;
    sync_period              = 0;
  }
  
  /// Destructor: frees all resources
  ~c_interface_emulator_t(){    free_resources();  }

  int32_t get_i_port_value_min(int port) const {return i_ports_config[port].is_signed ? (int32_t)0 - (std::int32_t(1)<<(i_ports_config[port].nb-1)) : 0;}
  int32_t get_o_port_value_min(int port) const {return o_ports_config[port].is_signed ? (int32_t)0 - (std::int32_t(1)<<(o_ports_config[port].nb-1)) : 0;}
  int32_t get_p_port_value_min(int port) const {return p_ports_config[port].is_signed ? (int32_t)0 - (std::int32_t(1)<<(p_ports_config[port].nb-1)) : 0;}
  
  int32_t get_i_port_value_range(int port) const {return (std::int32_t(1)<<(i_ports_config[port].nb)) -1;}
  int32_t get_o_port_value_range(int port) const {return (std::int32_t(1)<<(o_ports_config[port].nb)) -1;}
  int32_t get_p_port_value_range(int port) const {return (std::int32_t(1)<<(p_ports_config[port].nb)) -1;}



  
  /// Function that returns the number of input ports of the module
  /// \return: the number of input ports of the module
  int32_t get_nof_i_ports() const {return nof_i_ports;}
  
  /// Function that returns the number of output ports of the module
  /// \return: the number of output ports of the module
  int32_t get_nof_o_ports() const {return nof_o_ports;}
  
  /// Function that returns the number of probe ports of the module.
  /// Probe ports are not actually ports of the module, but provide access
  /// to internal signals for the purpose of development and verification
  /// \return: the number of probe ports of the module
  int32_t get_nof_p_ports() const {return nof_p_ports;}

  /// Function that returns the parallelism of the requested input port
  /// \param port: the port number
  /// \return: the parallelism of the requested port
  int32_t get_i_port_parallelism(int port) const {return i_ports_config[port].parallelism;}

  /// Function that returns the parallelism of the requested output port
  /// \param port: the port number
  /// \return: the parallelism of the requested port
  int32_t get_o_port_parallelism(int port) const {return o_ports_config[port].parallelism;}

  /// Function that returns the parallelism of the requested probe port
  /// \param port: the port number
  /// \return: the parallelism of the requested port
  int32_t get_p_port_parallelism(int port) const {return p_ports_config[port].parallelism;}

  
  /// Function that returns the name of the requested input port
  /// \param port: the port number
  /// \return: the name of the requested port
  string get_i_port_name(int port) const {return i_ports_config[port].name;}

  /// Function that returns the name of the requested output port
  /// \param port: the port number
  /// \return: the name of the requested port
  string get_o_port_name(int port) const {return o_ports_config[port].name;}

  /// Function that returns the name of the requested probe port
  /// \param port: the port number
  /// \return: the name of the requested port
  string get_p_port_name(int port) const {return p_ports_config[port].name;}
  
  /// Function that builds the whole structure for wrapping the object
  /// \param config: a json object with the following structure:
  ///     {
  ///    variables necessary for the wrapped module itself
  ///    "sync_period": 168,
  ///    "signals": 
  ///      [
  ///       {"name": "i_data",                   "parallelism": 444,  "signed": 0, "n_bits": 1, "nf_bits": 0, "type": "input",  "latency": 0, "log": 0},
  ///       {"name": "o_data",                   "parallelism": 512,  "signed": 0, "n_bits": 1, "nf_bits": 0, "type": "output", "latency": 9, "log": 0},
  ///       {"name": "p_internal_one",           "parallelism": 956,  "signed": 0, "n_bits": 1, "nf_bits": 0, "type": "probe",  "latency": 0, "log": 0}
  ///       ]
  ///      }
  void initialize(nlohmann::json config, bool skip_first_latency = false);
  
  /// Destructor: frees all resources
  void free_resources();

  /// Function that models one clock cycle in the life of the wrapped module,
  /// including the control interface
  /// \param i_data: an array of pointers. Each pointer points to the first
  ///        element of an array where the data of each input port shall be present.
  ///        each array shall have the correct number of elements as can be obtained by
  ///        get_i_port_parallelism. Data is interpreted as equivalent integer,
  ///        and is internally transformed according to signed, nb and nbf described in
  ///        the initialization json.
  ///        The order of the input ports respects the order in the "signals" array in json
  /// \param i_valid: 0 or 1, according to the interface definition
  /// \param i_sync: 0 or 1, according to the interface definition
  /// \param i_rst_n: 0 or 1, according to the interface definition
  /// \param o_data: an array of pointers. Each pointer points to the first
  ///        element of an array where the output data of each port shall be stored.
  ///        Each array shall have the correct number of elements as can be obtained by
  ///        get_o_port_parallelism. Data is stored as equivalent integer.
  ///        The order of the output ports respects the order in the "signals" array in json
  /// \param o_valid: integer by reference where the signal o_valid shall be stored
  /// \param o_sync: integer by reference where the signal o_sync shall be stored
  /// \paran p_data: an array of pointers. Each pointer points to the first
  ///        element of an array where the probe data of each probe port shall be stored.
  ///        Each array shall have the correct number of elements as can be obtained by
  ///        get_p_port_parallelism. Data is stored as equivalent integer.
  ///        The order of the probe ports respects the order in the "signals" array in json
  void clock(int32_t **i_data,
	     int32_t   i_valid,
	     int32_t   i_sync,
	     int32_t   i_rst_n,
	     int32_t **o_data,
	     int32_t  &o_valid,
	     int32_t  &o_sync,
	     int32_t **p_data);

  /// Function that sets a static signal with an array of values
  /// \param static_name: the name of the i_static signal as declared in JSON
  /// \param static_values: an array of values in equivalent integer int32_t with the values for the i_static signal.
  ///        the length of the array must match the parallelism declared in python
  void set_static(string static_name, int32_t *static_values);

private:
  void recirculate_p_data(int nof_p_port);
  void discard_p_data_recirculation(int nof_p_port);

  void update_o_ports(int32_t **o_data);
  void update_p_ports(int32_t **p_data);
  bool load_i_data(int32_t **i_data);
};

template <class T_WRAPPED>
void  c_interface_emulator_t<T_WRAPPED>::initialize(nlohmann::json config, bool skip_first_latency)
{
#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 0");fflush(stdout);
#endif
  state = reset;

  free_resources();

#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 1");fflush(stdout);
#endif
  my_real_dut.initialize(config);

#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 2");fflush(stdout);
#endif
  bool stop_counting_i2dut_latency = false;
  i2dut_latency = 0;
  output_extra_latency = 0;
  nof_fsms = config["fsms"].size();
  fsm_config = new s_fsm_config[nof_fsms];
  my_fsms   = new c_fsm_base*[nof_fsms];
  for (uint32_t i = 0; i < nof_fsms; i++)
    my_fsms[i] = nullptr;
  for (uint32_t i = 0; i < nof_fsms; i++)
    {
      fsm_config[i].name          = config["fsms"][i]["name"].get<string>();
      fsm_config[i].parall_in     = config["fsms"][i]["par_in"].get<uint32_t>();
      fsm_config[i].parall_out    = config["fsms"][i]["par_out"].get<uint32_t>();
      fsm_config[i].latency       = config["fsms"][i]["latency"].get<uint32_t>();
      if ((i == 0) && (skip_first_latency))
        {
          if (fsm_config[i].latency == 0)
             std::runtime_error("\n c_interface_emulator_t ERROR: Not possible to skip first latency, as this is already 0");
          fsm_config[i].latency = fsm_config[i].latency - 1;
        }
      
      fsm_config[i].resync_period = config["fsms"][i]["resync_period"].get<uint32_t>();
      string type = config["fsms"][i]["type"].get<string>();
      if      (type.compare("resync")               == 0)
	{
	  fsm_config[i].type = resync;
	  my_fsms[i] = new c_fsm_synchronizer;
	  datapath_enable   = my_fsms[i]->get_o_valid();
	  datapath_soft_rst = my_fsms[i]->get_o_soft_rst();
	  if (!stop_counting_i2dut_latency)
	    i2dut_latency += fsm_config[i].latency;
	  stop_counting_i2dut_latency = true;
	} 
      else if (type.compare("latency")               == 0)
	{
	  if (!stop_counting_i2dut_latency)
	    i2dut_latency += fsm_config[i].latency;
	  output_extra_latency += fsm_config[i].latency;
	  fsm_config[i].type = latency;
	  my_fsms[i] = new c_fsm_latency;
	}
      else if (type.compare("gated_latency")         == 0)
	{
	  if (!stop_counting_i2dut_latency)
	    i2dut_latency += fsm_config[i].latency;
	  fsm_config[i].type = gated_latency;
	  my_fsms[i] = new c_fsm_gated_latency;
	}
      else if (type.compare("par_conv")    == 0)
	{
	  if (!stop_counting_i2dut_latency)
	    i2dut_latency += fsm_config[i].latency;
	  fsm_config[i].type = par_conv;
	  my_fsms[i] = new c_fsm_par_conv;
	}
      else
	{
	  throw std::runtime_error("\n c_interface_emulator_t ERROR: not recognized fsm type");
	}
      
      if (i == 0)
	{
	  my_fsms[i]->connect_i_sync(&internal_i_sync);
	  my_fsms[i]->connect_i_valid(&internal_i_valid);
	  my_fsms[i]->connect_i_soft_rst(&internal_i_soft_rst);
	}
      else
	{
	  my_fsms[i]->connect_i_sync(my_fsms[i-1]->get_o_sync());
	  my_fsms[i]->connect_i_valid(my_fsms[i-1]->get_o_valid());
	  my_fsms[i]->connect_i_soft_rst(my_fsms[i-1]->get_o_soft_rst());
	}
    }
  internal_o_valid    = my_fsms[nof_fsms-1  ]->get_o_valid();
  internal_o_sync     = my_fsms[nof_fsms-1  ]->get_o_sync();
  internal_o_soft_rst = my_fsms[nof_fsms-1  ]->get_o_soft_rst();
  internal_pre_o_valid= my_fsms[nof_fsms-2  ]->get_o_valid();

#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 3");fflush(stdout);
#endif

  my_real_dut.review_fsm_config(&fsm_config[0], nof_fsms);
  
  for (uint32_t i = 0; i < nof_fsms; i++)
    {
      my_fsms[i]->initialize(fsm_config[i]);
    }
  
#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 4");fflush(stdout);
#endif
  //creation of input, output and probe fifos and ports
  nof_i_ports = 0;
  nof_i_int_ports = 0;
  nof_i_float_ports = 0;
  nof_o_int_ports = 0;
  nof_o_float_ports = 0;
  nof_o_ports = 0;
  nof_p_ports = 0;
  for (uint32_t signal_idx = 0; signal_idx < config["signals"].size(); signal_idx++)
    {
      
      if (((config["signals"][signal_idx]["name"].get<string>()).compare("i_valid") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("i_sync") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("rst_async_n") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("o_valid") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("o_sync") == 0))
        { }
      else if ((config["signals"][signal_idx]["name"].get<string>()).substr(0,9).compare("i_static_") == 0)
        { }
      else if ((config["signals"][signal_idx]["name"].get<string>()).find("__i_static_") != string::npos)
        { }     
      else if ((config["signals"][signal_idx]["type"].get<string>()).compare( "input") == 0)
        {
          
          int dut_port_number = my_real_dut.get_input_number(config["signals"][signal_idx]["name"].get<string>());
          s_signal_config<T_Parallelism> aux_config = my_real_dut.get_input_data(dut_port_number);
          if      (aux_config.port_type == is_int  ) {nof_i_int_ports++;  }
          else if (aux_config.port_type == is_float) {nof_i_float_ports++; }
          nof_i_ports ++;
        }
      else if ((config["signals"][signal_idx]["type"].get<string>()).compare( "output") == 0)
        {
          
          int dut_port_number = my_real_dut.get_output_number(config["signals"][signal_idx]["name"].get<string>());
          s_signal_config<T_Parallelism> aux_config = my_real_dut.get_output_data(dut_port_number);
          if      (aux_config.port_type == is_int  ) {nof_o_int_ports++; }
          else if (aux_config.port_type == is_float) {nof_o_float_ports++; }      
          nof_o_ports ++;
        }
      else if ((config["signals"][signal_idx]["type"].get<string>()).compare( "probe") == 0)
        {
          
          nof_p_ports ++;
        }
      
    }

  if (nof_i_int_ports   > 0) i_int_fifos        = new c_fifo_t<        T_Parallelism, typename T_WRAPPED::T_BE_int>    [nof_i_int_ports];
  if (nof_i_int_ports   > 0) i_int_ports_wr     = new c_fifo_port_wr_t<T_Parallelism, typename T_WRAPPED::T_BE_int>*   [nof_i_int_ports];
  if (nof_i_float_ports > 0) i_float_fifos      = new c_fifo_t<        T_Parallelism, typename T_WRAPPED::T_BE_float>  [nof_i_float_ports];
  if (nof_i_float_ports > 0) i_float_ports_wr   = new c_fifo_port_wr_t<T_Parallelism, typename T_WRAPPED::T_BE_float>* [nof_i_float_ports];
  if ((nof_i_int_ports+nof_i_float_ports) > 0) i_ports_config     = new s_signal_config< T_Parallelism>                                  [nof_i_int_ports+nof_i_float_ports];
  
  if (nof_o_int_ports   > 0) o_int_fifos        = new c_fifo_t<        T_Parallelism, typename T_WRAPPED::T_BE_int>    [nof_o_int_ports];
  if (nof_o_int_ports   > 0) o_int_ports        = new c_fifo_port_rd_t<T_Parallelism, typename T_WRAPPED::T_BE_int>*   [nof_o_int_ports];
  if (nof_o_float_ports > 0) o_float_fifos      = new c_fifo_t<        T_Parallelism, typename T_WRAPPED::T_BE_float>  [nof_o_float_ports];
  if (nof_o_float_ports > 0) o_float_ports      = new c_fifo_port_rd_t<T_Parallelism, typename T_WRAPPED::T_BE_float>* [nof_o_float_ports];
  if ((nof_o_int_ports+nof_o_float_ports) > 0) o_ports_config     = new s_signal_config< T_Parallelism>                                  [nof_o_int_ports+nof_o_float_ports];

#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 5");fflush(stdout);
#endif
  o_data_copy = new int32_t*[nof_o_int_ports+nof_o_float_ports];
  i_data_copy = new int32_t**[i2dut_latency];
  for (uint32_t ii = 0; ii < i2dut_latency; ii ++)
    i_data_copy[ii] = new int32_t*[nof_i_int_ports+nof_i_float_ports];
  
  if (nof_p_ports > 0) p_fifos           = new c_fifo_t<        T_Parallelism, int32_t> [nof_p_ports];
  if (nof_p_ports > 0) p_ports_wr_recirc = new c_fifo_port_wr_t<T_Parallelism, int32_t>*[nof_p_ports];
  if (nof_p_ports > 0) p_ports_rd_recirc = new c_fifo_port_rd_t<T_Parallelism, int32_t>*[nof_p_ports];
  if (nof_p_ports > 0) p_ports           = new c_fifo_port_rd_t<T_Parallelism, int32_t>*[nof_p_ports];
  if (nof_p_ports > 0) p_ports_config    = new s_signal_config< T_Parallelism         > [nof_p_ports];

#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 6");fflush(stdout);
#endif
  
  //creation and connection of fifos
  i_parallelism_multiplier = -1.0;
  nof_i_ports = 0;
  nof_i_int_ports = 0;
  nof_i_float_ports = 0;

  o_parallelism_multiplier = -1.0;
  nof_o_ports = 0;
  nof_o_int_ports = 0;
  nof_o_float_ports = 0;

  p_parallelism_multiplier = -1.0;
  nof_p_ports = 0;

  for (uint32_t signal_idx = 0; signal_idx < config["signals"].size(); signal_idx++)
    {
      
#ifdef D_IE_VERBOSE
      printf("\n SIGNAL[%u]", signal_idx);fflush(stdout);
      printf("name = %s, parallelism=%d, signed=%d, n_bits=%d, nf_bits=%d, type=%s, latency=%d, match=%d, log=%d",
             (config["signals"][signal_idx]["name"].get<string>()).c_str(),
             config["signals"][signal_idx]["parallelism"].get<int>(),
             config["signals"][signal_idx]["signed"].get<int>(),
             config["signals"][signal_idx]["n_bits"].get<int>(),
             config["signals"][signal_idx]["nf_bits"].get<int>(),
             (config["signals"][signal_idx]["type"].get<string>()).c_str(),
             config["signals"][signal_idx]["latency"].get<int>(),
             config["signals"][signal_idx]["match"].get<int>(),
             config["signals"][signal_idx]["log"].get<int>());
#endif
      
      if (((config["signals"][signal_idx]["name"].get<string>()).compare("i_valid") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("i_sync") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("rst_async_n") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("o_valid") == 0) ||
          ((config["signals"][signal_idx]["name"].get<string>()).compare("o_sync") == 0))
        {
#ifdef D_IE_VERBOSE
          printf("\n SIGNAL[%u] = %s belongs to interface", signal_idx, (config["signals"][signal_idx]["name"].get<string>()).c_str());fflush(stdout);
#endif
        }
       else if ((config["signals"][signal_idx]["name"].get<string>()).substr(0,9).compare("i_static_") == 0)
        {
#ifdef D_IE_VERBOSE
          printf("\n SIGNAL[%u] = %s is static", signal_idx, (config["signals"][signal_idx]["name"].get<string>()).c_str());fflush(stdout);
#endif
        }
       else if ((config["signals"][signal_idx]["name"].get<string>()).find("__i_static_") != string::npos)
         {
#ifdef D_IE_VERBOSE
          printf("\n SIGNAL[%u] = %s is sub static", signal_idx, (config["signals"][signal_idx]["name"].get<string>()).c_str());fflush(stdout);
#endif
         }     
      else if ((config["signals"][signal_idx]["type"].get<string>()).compare( "input") == 0)
        {
#ifdef D_IE_VERBOSE
          printf("\n SIGNAL[%u] = %s is input", signal_idx, (config["signals"][signal_idx]["name"].get<string>()).c_str());fflush(stdout);
#endif
          int dut_port_number = my_real_dut.get_input_number(config["signals"][signal_idx]["name"].get<string>());
          i_ports_config[nof_i_ports] = my_real_dut.get_input_data(dut_port_number);
          if (i_ports_config[nof_i_ports].port_type == is_int)
            {
              i_int_fifos[nof_i_int_ports].initialize(i_ports_config[nof_i_ports].fifo_expansion_factor * std::max(i_ports_config[nof_i_ports].elemental_parallelism, i_ports_config[nof_i_ports].parallelism)*(i_ports_config[nof_i_ports].latency+2)+2,
                                                      1,
                                                      i_ports_config[nof_i_ports].name);
              i_int_ports_wr[nof_i_int_ports] = i_int_fifos[nof_i_int_ports].get_wr_port();
              i_ports_config[nof_i_ports].fifo_number = nof_i_int_ports;
              my_real_dut.assign_input_port(i_int_fifos[nof_i_int_ports].get_rd_port(), dut_port_number);
              nof_i_int_ports++;

	  
            }
          else if (i_ports_config[nof_i_ports].port_type == is_float)
            {
              i_float_fifos[nof_i_float_ports].initialize(i_ports_config[nof_i_ports].fifo_expansion_factor * std::max(i_ports_config[nof_i_ports].elemental_parallelism, i_ports_config[nof_i_ports].parallelism)*(i_ports_config[nof_i_ports].latency+2)+2,
                                                          1,
                                                          i_ports_config[nof_i_ports].name);
              i_float_ports_wr[nof_i_float_ports] = i_float_fifos[nof_i_float_ports].get_wr_port();
              i_ports_config[nof_i_ports].fifo_number = nof_i_float_ports;
              my_real_dut.assign_input_port(i_float_fifos[nof_i_float_ports].get_rd_port(), dut_port_number);
              nof_i_float_ports++;
            }

          if (!(((float)i_ports_config[nof_i_ports].parallelism / (float)i_ports_config[nof_i_ports].elemental_parallelism == i_parallelism_multiplier) ||
                (i_parallelism_multiplier == -1.0) || 
                ((i_ports_config[nof_i_ports].parallelism == i_ports_config[nof_i_ports].elemental_parallelism) &&
                 (i_ports_config[nof_i_ports].log_all == false))))
            throw std::runtime_error("interface_emulator ERROR: incompatible input paralelism (2)");

          for (uint32_t ii = 0; ii < i2dut_latency; ii++)
            {
              i_data_copy[ii][nof_i_ports] = new int32_t[i_ports_config[nof_i_ports].parallelism];
              for (int32_t j = 0; j < i_ports_config[nof_i_ports].parallelism; j++)
                i_data_copy[ii][nof_i_ports][j] = i_ports_config[nof_i_ports].value_during_reset;
            }
      
          if (i_ports_config[nof_i_ports].log_all)
            i_parallelism_multiplier = float(i_ports_config[nof_i_ports].parallelism) / float(i_ports_config[nof_i_ports].elemental_parallelism);
          nof_i_ports ++;
        }
      else if ((config["signals"][signal_idx]["type"].get<string>()).compare( "output") == 0)
        {
#ifdef D_IE_VERBOSE
          printf("\n SIGNAL[%u] = %s is output", signal_idx, (config["signals"][signal_idx]["name"].get<string>()).c_str());fflush(stdout);
#endif

          int dut_port_number = my_real_dut.get_output_number(config["signals"][signal_idx]["name"].get<string>());
          o_ports_config[nof_o_ports] = my_real_dut.get_output_data(dut_port_number);
          if ((output_latency >= 0) && (o_ports_config[nof_o_ports].latency != output_latency))
            throw std::runtime_error("interface_emulator ERROR: incompatible output latencies");

          output_latency = o_ports_config[nof_o_ports].latency;

          if (o_ports_config[nof_o_ports].port_type == is_int)
            {
              o_int_fifos[nof_o_int_ports].initialize(o_ports_config[nof_o_ports].fifo_expansion_factor * std::max(o_ports_config[nof_o_ports].parallelism, o_ports_config[nof_o_ports].elemental_parallelism)*(output_extra_latency+1)+2,
                                                      1,
                                                      o_ports_config[nof_o_ports].name);
              o_int_ports[nof_o_int_ports]           = o_int_fifos[nof_o_int_ports].get_rd_port(0);
              o_ports_config[nof_o_ports].fifo_number = nof_o_int_ports;
              my_real_dut.assign_output_port(o_int_fifos[nof_o_int_ports].get_wr_port(), dut_port_number);
              nof_o_int_ports++;
            }
          else if (o_ports_config[nof_o_ports].port_type == is_float)
            {
              o_float_fifos[nof_o_float_ports].initialize(o_ports_config[nof_o_ports].fifo_expansion_factor * std::max(o_ports_config[nof_o_ports].parallelism, o_ports_config[nof_o_ports].elemental_parallelism)*(output_extra_latency+1)+2,
                                                          1,
                                                          o_ports_config[nof_o_ports].name);
              o_float_ports[nof_o_float_ports]           = o_float_fifos[nof_o_float_ports].get_rd_port(0);
              o_ports_config[nof_o_ports].fifo_number = nof_o_float_ports;
              my_real_dut.assign_output_port(o_float_fifos[nof_o_float_ports].get_wr_port(), dut_port_number);
              nof_o_float_ports++;
            }

          if (!(((float)o_ports_config[nof_o_ports].parallelism / (float)o_ports_config[nof_o_ports].elemental_parallelism == o_parallelism_multiplier) ||
                (o_parallelism_multiplier == -1.0) || 
                ((o_ports_config[nof_o_ports].parallelism == o_ports_config[nof_o_ports].elemental_parallelism) &&
                 (o_ports_config[nof_o_ports].log_all == false))))
            throw std::runtime_error("interface_emulator ERROR: incompatible output paralelism (2)");
      

          if (o_ports_config[nof_o_ports].log_all)
            o_parallelism_multiplier = (float)o_ports_config[nof_o_ports].parallelism / (float)o_ports_config[nof_o_ports].elemental_parallelism;
      
          o_data_copy[nof_o_ports] = new int32_t[o_ports_config[nof_o_ports].parallelism];
          for (int32_t j = 0; j < o_ports_config[nof_o_ports].parallelism; j++)
            o_data_copy[nof_o_ports][j] = o_ports_config[nof_o_ports].value_during_reset;
      
          nof_o_ports ++;
        }
      else if ((config["signals"][signal_idx]["type"].get<string>()).compare( "probe") == 0)
        {
#ifdef D_IE_VERBOSE
          printf("\n SIGNAL[%u] = %s is internal", signal_idx, (config["signals"][signal_idx]["name"].get<string>()).c_str());fflush(stdout);
#endif
          
          int dut_port_number = my_real_dut.get_probe_number(config["signals"][signal_idx]["name"].get<string>());

          p_ports_config[nof_p_ports] = my_real_dut.get_probe_data(dut_port_number);


          uint32_t elemental_size = p_ports_config[nof_p_ports].parallelism;
          if (!p_ports_config[nof_p_ports].log_all)
            elemental_size = elemental_size * p_parallelism_multiplier;
          p_fifos[nof_p_ports].initialize(p_ports_config[nof_p_ports].fifo_expansion_factor * (elemental_size*(p_ports_config[nof_p_ports].latency+2-i2dut_latency)+2),
                                          2,
                                          p_ports_config[nof_p_ports].name);
          p_ports[nof_p_ports]           = p_fifos[nof_p_ports].get_rd_port(0);
          p_ports_rd_recirc[nof_p_ports] = p_fifos[nof_p_ports].get_rd_port(1);
          p_ports_wr_recirc[nof_p_ports] = p_fifos[nof_p_ports].get_wr_port( );

          p_fifos[nof_p_ports].reset();//clear probe fifos
      
          //printf("\n prefilling port %s with %d inputs", p_ports_config[nof_p_ports].name.c_str(), elemental_size*(p_ports_config[nof_p_ports].latency));fflush(stdout);
          for (uint32_t j = 0; j < elemental_size*(p_ports_config[nof_p_ports].latency+1-i2dut_latency); j++) //prefill p fifo
            p_ports_wr_recirc[nof_p_ports]->write(p_ports_config[nof_p_ports].value_during_reset);

          p_ports_rd_recirc[nof_p_ports]->move_pointer(elemental_size*(p_ports_config[nof_p_ports].latency-i2dut_latency));
          p_ports[nof_p_ports]->move_pointer(elemental_size);


          if (!(((float)p_ports_config[nof_p_ports].parallelism / (float)p_ports_config[nof_p_ports].elemental_parallelism == p_parallelism_multiplier) ||
                (p_parallelism_multiplier == -1.0) || 
                ((p_ports_config[nof_p_ports].parallelism == p_ports_config[nof_p_ports].elemental_parallelism) &&
                 (p_ports_config[nof_p_ports].log_all == false))))
            throw std::runtime_error("interface_emulator ERROR: incompatible probe paralelism (2)");

          if (p_ports_config[nof_p_ports].log_all)
            p_parallelism_multiplier = (float)p_ports_config[nof_p_ports].parallelism / (float)p_ports_config[nof_p_ports].elemental_parallelism;
          my_real_dut.assign_probe_port(p_fifos[nof_p_ports].get_wr_port(), dut_port_number);
          nof_p_ports ++;

        }
      else
        {
          throw std::runtime_error("\n c_interface_emulator_t ERROR: unrecognized signal type\n");
        }
    }
        
  
#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 9");fflush(stdout);
#endif
  for (uint32_t i=0; i < nof_i_int_ports   ; i++){   i_int_fifos[i].reset();} //clear input fifos
  for (uint32_t i=0; i < nof_i_float_ports ; i++){   i_float_fifos[i].reset();} //clear input fifos
  for (uint32_t i=0; i < nof_o_int_ports  ; i++){   o_int_fifos[i].reset();} //clear output fifos
  for (uint32_t i=0; i < nof_o_float_ports; i++){ o_float_fifos[i].reset();} //clear output fifos
  
#ifdef D_IE_VERBOSE
  printf("\nc_interface_emulator_t::initialize checkpoint 10");fflush(stdout);
#endif
  
}

template <class T_WRAPPED>
void  c_interface_emulator_t<T_WRAPPED>::set_static(string static_name, int32_t *static_values)
{
  my_real_dut.set_static(static_name, static_values);
  my_real_dut.review_fsm_config(&fsm_config[0], nof_fsms);
  my_real_dut.review_port_config(&i_ports_config[0], &o_ports_config[0], &p_ports_config[0]);
  
  for (uint32_t i = 0; i < nof_fsms; i++)
    {
      my_fsms[i]->reconfigure(fsm_config[i]);
    }
}

template <class T_WRAPPED>
void  c_interface_emulator_t<T_WRAPPED>::free_resources()
{
  if (o_data_copy != nullptr)
    {
      for (uint32_t i = 0; i < nof_o_int_ports    + nof_o_float_ports; i++)
	{
	  if (o_data_copy[i] != nullptr)
	    delete[] o_data_copy[i];
	  o_data_copy[i] = nullptr;
	}
      delete[] o_data_copy;
      o_data_copy = nullptr;
    }
  
  if (i_data_copy != nullptr)
    {
      for (uint32_t ii = 0; ii < i2dut_latency; ii++)
	{
	  for (uint32_t i = 0; i < nof_i_int_ports + nof_i_float_ports; i++)
	    {
	      if (i_data_copy[ii][i] != nullptr)
		delete[] i_data_copy[ii][i];
	      i_data_copy[ii][i] = nullptr;
	    }
	  delete[] i_data_copy[ii];
	}
      delete[] i_data_copy;
      i_data_copy = nullptr;
    }
    
  if (fsm_config != nullptr)
    delete[] fsm_config;
  fsm_config = nullptr;
  if (my_fsms != nullptr)
    {
      for (uint32_t i = 0; i < nof_fsms; i++)
	{
	  if (my_fsms[i] != nullptr)
	    {
	      my_fsms[i]->free_resources();
              delete my_fsms[i];
	      //delete my_fsms[i];
	      my_fsms[i] = nullptr;
	    }
	}
      delete[] my_fsms;
      my_fsms = nullptr;
    }

  
  uint32_t i;
  if (i_int_ports_wr != nullptr)
    {delete[] i_int_ports_wr; i_int_ports_wr = nullptr;}
  if (i_int_fifos != nullptr)
    {for (i=0; i < nof_i_int_ports; i++){i_int_fifos[i].free_resources();}
      delete[] i_int_fifos; i_int_fifos = nullptr;}
  if (i_float_ports_wr != nullptr)
    {delete[] i_float_ports_wr; i_float_ports_wr = nullptr;}
  if (i_float_fifos != nullptr)
    {for (i=0; i < nof_i_float_ports; i++){i_float_fifos[i].free_resources();}
      delete[] i_float_fifos; i_float_fifos = nullptr;}
  if (i_ports_config != nullptr)
    {delete[] i_ports_config; i_ports_config = nullptr;}
  nof_i_ports = 0;
  nof_i_int_ports = 0;
  nof_i_float_ports = 0;
  
  if (o_int_ports != nullptr)
    {delete[] o_int_ports; o_int_ports = nullptr;}
  if (o_int_fifos != nullptr)
    {for (i=0; i< nof_o_int_ports; i++){o_int_fifos[i].free_resources();}
      delete[] o_int_fifos; o_int_fifos = nullptr;}
  if (o_float_ports != nullptr)
    {delete[] o_float_ports; o_float_ports = nullptr;}
  if (o_float_fifos != nullptr)
    {for (i=0; i< nof_o_float_ports; i++){o_float_fifos[i].free_resources();}
      delete[] o_float_fifos; o_float_fifos = nullptr;}
  if (o_ports_config != nullptr)
    {delete[] o_ports_config; o_ports_config = nullptr;}
  nof_o_ports = 0;
  nof_o_int_ports = 0;
  nof_o_float_ports = 0;
  
  if (p_ports_wr_recirc != nullptr)
    {delete[] p_ports_wr_recirc; p_ports_wr_recirc = nullptr;}
  if (p_ports_rd_recirc != nullptr)
    {delete[] p_ports_rd_recirc; p_ports_rd_recirc = nullptr;}
  if (p_ports != nullptr)
    {delete[] p_ports; p_ports = nullptr;}
  if (p_fifos != nullptr)
    {for (i=0; i<nof_p_ports; i++){p_fifos[i].free_resources();}
      delete[] p_fifos; p_fifos = nullptr;}
  if (p_ports_config != nullptr)
    {delete[] p_ports_config; p_ports_config = nullptr;}
  nof_p_ports = 0;

  
  my_real_dut.free_resources();
}

template <class T_WRAPPED>
void c_interface_emulator_t<T_WRAPPED>::clock(int32_t **i_data,
					      int32_t   i_valid,
					      int32_t   i_sync,
					      int32_t   i_rst_n,
					      int32_t **o_data,
					      int32_t  &o_valid,
					      int32_t  &o_sync,
					      int32_t **p_data)
{
  bool o_datapath_enable_toggling = false;
    
#ifdef D_IE_VERBOSE
  printf("\n\n\n\n\n NEW CLOCK ");
  printf("\n i_rst_n = %d; i_valid = %d; i_sync = %d; ", i_rst_n, i_valid, i_sync);
  fflush(stdout);
#endif
  //////////////////////////////////////////////////
  // FSM EMULATION /////////////////////////////////
  //////////////////////////////////////////////////
  internal_i_valid = i_valid;
  internal_i_sync  = i_sync;
  if (i_rst_n == 0)
    {
      o_datapath_enable_toggling = ((*internal_pre_o_valid) == 1) && ((*internal_o_soft_rst) == 0);
        
      for (uint32_t i = 0; i < nof_fsms; i++)
	my_fsms[i]->reset();
      o_valid = 0;
      o_sync  = 0;
    }
  else
    {
      for (uint32_t i = 0; i < nof_fsms; i++)
	my_fsms[i]->clock();
      o_valid = *internal_o_valid;
      o_sync  = *internal_o_sync;
    }
#ifdef D_IE_VERBOSE
  for (uint32_t i = 0; i < nof_fsms; i++)
    my_fsms[i]->display();
#endif
  
  /////////////////////////////////////////////////
  // DATAPATH EMULATION ///////////////////////////
  /////////////////////////////////////////////////
#ifdef D_IE_VERBOSE
  printf("\n DATAPATH EMULATION");fflush(stdout);
#endif
  if ((i_rst_n == 0) || (*datapath_soft_rst == 1))
    {
#ifdef D_IE_VERBOSE
      printf("\n     RESET");fflush(stdout);
#endif
      for (uint32_t i=0; i < nof_i_int_ports   ; i++){   i_int_fifos[i].reset();} //clear input fifos
      for (uint32_t i=0; i < nof_i_float_ports ; i++){ i_float_fifos[i].reset();} //clear input fifos
      my_real_dut.reset();                                         //reset dut
    }
  
  if ((o_valid == 1) && ((*datapath_soft_rst == 1) || (i_rst_n == 0))) //solving case for o_valid high during rst
    {
      update_o_ports(o_data);
      for (uint32_t i=0; i < nof_o_ports; i++)
	for (int p=0; p < o_ports_config[i].parallelism; p++)
	  o_data_copy[i][p] = o_data[i][p];
    }
  else if (o_datapath_enable_toggling)
    {
      update_o_ports(o_data);
      for (uint32_t i=0; i < nof_o_ports; i++)
	for (int p=0; p < o_ports_config[i].parallelism; p++)
	  o_data_copy[i][p] = o_data[i][p];
    }
      
  
  if ((i_rst_n == 0) || (*datapath_soft_rst == 1))
    {
      for (uint32_t i=0; i < nof_o_int_ports  ; i++){   o_int_fifos[i].reset();} //clear output fifos
      for (uint32_t i=0; i < nof_o_float_ports; i++){ o_float_fifos[i].reset();} //clear output fifos
      //output calculation
      for (uint32_t i=0; i < nof_o_ports; i++)
	for (int p=0; p < o_ports_config[i].parallelism; p++)
	  o_data[i][p] = o_data_copy[i][p];
    }
                                             
  
#ifdef D_IE_VERBOSE
  printf("\n\n===========================\n INITIAL");
  for (uint32_t i=0; i < nof_i_int_ports; i++)
    {printf("\n i_int_port[%2u]  : ", i); i_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_i_float_ports; i++)
    {printf("\n i_float_port[%2u]: ", i); i_float_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_int_ports; i++)
    {printf("\n o_int_port[%2u]:   ", i); o_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_float_ports; i++)
    {printf("\n o_float_port[%2u]: ", i); o_float_fifos[i].display();}
  //for (uint32_t i=0; i < nof_p_ports; i++)
  //  {printf("\n p_port[%2u]: ", i); p_fifos[i].display();}
  fflush(stdout);
#endif

  
  for (uint32_t i=0; i < nof_p_ports ; i++)
    p_fifos[i].reset_activity_meter();
#ifdef D_IE_VERBOSE
  printf("\n Checkpoint 3");fflush(stdout);
#endif
  if (*datapath_enable == 1)
    { 
#ifdef D_IE_VERBOSE
      printf("\n Checkpoint 3.1");fflush(stdout);
#endif
	
      //input update
#ifdef D_IE_VERBOSE
      printf("\n filling input fifos...");fflush(stdout);
#endif
      bool enough_input = load_i_data(i_data_copy[i2dut_latency-1]);

#ifdef D_IE_VERBOSE
      printf("\n Checkpoint 3.2");fflush(stdout);
#endif

      
#ifdef D_IE_VERBOSE
  printf("\n\n===========================\n INPUTS LOADED");
  for (uint32_t i=0; i < nof_i_int_ports; i++)
    {printf("\n i_int_port[%2u]  : ", i); i_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_i_float_ports; i++)
    {printf("\n i_float_port[%2u]: ", i); i_float_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_int_ports; i++)
    {printf("\n o_int_port[%2u]:   ", i); o_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_float_ports; i++)
    {printf("\n o_float_port[%2u]: ", i); o_float_fifos[i].display();}
  //for (uint32_t i=0; i < nof_p_ports; i++)
  //  {printf("\n p_port[%2u]: ", i); p_fifos[i].display();}
  fflush(stdout);
#endif
      
      //printf("done");fflush(stdout);
#ifdef D_IE_VERBOSE
      printf("\n processing data...");fflush(stdout);
#endif
      if (enough_input)
	if (! my_real_dut.process())
	  throw std::runtime_error("c_interface_emulator_t ERROR: invalid clock");
#ifdef D_IE_VERBOSE
      printf("\n Checkpoint 3.3");fflush(stdout);
#endif
    }

#ifdef D_IE_VERBOSE
  printf("\n Checkpoint 4");fflush(stdout);
#endif
  for (uint32_t i=0; i < nof_i_ports; i++)
    for (int p=0; p < i_ports_config[i].parallelism; p++)
      {
	for (int ii = i2dut_latency-1; ii > 0; ii--)
	  {
	    i_data_copy[ii][i][p] = i_data_copy[ii-1][i][p];
	  }
	i_data_copy[0][i][p] = i_data[i][p];
      }

#ifdef D_IE_VERBOSE
  printf("\n Checkpoint 5");fflush(stdout);
#endif
      
#ifdef D_IE_VERBOSE
  printf("\n\n===========================\n BEFORE P UPDATE");
  for (uint32_t i=0; i < nof_i_int_ports; i++)
    {printf("\n i_int_port[%2u]  : ", i); i_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_i_float_ports; i++)
    {printf("\n i_float_port[%2u]: ", i); i_float_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_int_ports; i++)
    {printf("\n o_int_port[%2u]:   ", i); o_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_float_ports; i++)
    {printf("\n o_float_port[%2u]: ", i); o_float_fifos[i].display();}
  //for (uint32_t i=0; i < nof_p_ports; i++)
  //  {printf("\n p_port[%2u]: ", i); p_fifos[i].display();}
  fflush(stdout);
#endif
  //printf("\n nof_p_ports = %d", nof_p_ports);
  for (uint32_t i=0; i < nof_p_ports ; i++)
    {
      if (p_fifos[i].get_activity_detected())
	{
	  
	  //printf("\n discard = %d", i);fflush(stdout);
	  discard_p_data_recirculation(i);
	  //printf(" done");fflush(stdout);
	}
      else
	{
	  //printf("\n recirculate = %d", i); fflush(stdout);
	  recirculate_p_data(i);
	  //printf(" done");fflush(stdout);
	}
    } //clear probe fifos
  
#ifdef D_IE_VERBOSE
  printf("\n\n===========================\n AFTER P UPDATE");
  for (uint32_t i=0; i < nof_i_int_ports; i++)
    {printf("\n i_int_port[%2u]  : ", i); i_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_i_float_ports; i++)
    {printf("\n i_float_port[%2u]: ", i); i_float_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_int_ports; i++)
    {printf("\n o_int_port[%2u]:   ", i); o_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_float_ports; i++)
    {printf("\n o_float_port[%2u]: ", i); o_float_fifos[i].display();}
  //for (uint32_t i=0; i < nof_p_ports; i++)
  //  {printf("\n p_port[%2u]: ", i); p_fifos[i].display();}
  fflush(stdout);
#endif


#ifdef D_IE_VERBOSE
  printf("\n Checkpoint 6");fflush(stdout);
#endif
  //fflush(stdout);
  //printf("\n checkpoint 6");
  //output calculation

  if ((o_valid == 1) && (*datapath_soft_rst == 0) && (i_rst_n == 1))
    {
      update_o_ports(o_data);
      for (uint32_t i=0; i < nof_o_ports; i++)
	for (int p=0; p < o_ports_config[i].parallelism; p++)
	  o_data_copy[i][p] = o_data[i][p];
    }
  else
    {
      for (uint32_t i=0; i < nof_o_ports; i++)
	for (int p=0; p < o_ports_config[i].parallelism; p++)
	  o_data[i][p] = o_data_copy[i][p];
    }
  
#ifdef D_IE_VERBOSE
  printf("\n Checkpoint 7");fflush(stdout);
#endif
  update_p_ports(p_data);

#ifdef D_IE_VERBOSE
  printf("\n Checkpoint 8");fflush(stdout);
#endif
  
#ifdef D_IE_VERBOSE
  printf("\n\n===========================\n UPDATED O and P");
  for (uint32_t i=0; i < nof_i_int_ports; i++)
    {printf("\n i_int_port[%2u]  : ", i); i_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_i_float_ports; i++)
    {printf("\n i_float_port[%2u]: ", i); i_float_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_int_ports; i++)
    {printf("\n o_int_port[%2u]:   ", i); o_int_fifos[i].display();}
  for (uint32_t i=0; i < nof_o_float_ports; i++)
    {printf("\n o_float_port[%2u]: ", i); o_float_fifos[i].display();}
  //for (uint32_t i=0; i < nof_p_ports; i++)
  //  {printf("\n p_port[%2u]: ", i); p_fifos[i].display();}
  fflush(stdout);
#endif
  
}


template <class T_WRAPPED>
void c_interface_emulator_t<T_WRAPPED>::recirculate_p_data(int nof_p_port)
{
  int i = nof_p_port;
  int aux_jump = 1;
  if (!p_ports_config[i].log_all)
    aux_jump = p_parallelism_multiplier;
  for (int j = 0; j < aux_jump*p_ports_config[i].parallelism; j++)
    p_ports_wr_recirc[i]->write(p_ports_rd_recirc[i]->read());
}


template <class T_WRAPPED>
void c_interface_emulator_t<T_WRAPPED>::discard_p_data_recirculation(int nof_p_port)
{
  int i = nof_p_port;
  int aux_jump = 1;
  if (!p_ports_config[i].log_all)
    aux_jump = p_parallelism_multiplier;
  (*p_ports_rd_recirc[i]).move_pointer(aux_jump*p_ports_config[i].parallelism);
}


template <class T_WRAPPED>
void c_interface_emulator_t<T_WRAPPED>::update_o_ports(int32_t **o_data)
{  
  for (uint32_t i=0; i < nof_o_ports; i++)
    {
      int aux_jump = 1;
      if (!o_ports_config[i].log_all)
	aux_jump = o_parallelism_multiplier;
      int32_t aux_for_unpack[T_Parallelism];

      if (o_ports_config[i].port_type == is_int)
	{
	  if ((*o_int_ports[o_ports_config[i].fifo_number]).available_data() >= uint32_t(float(o_ports_config[i].parallelism * aux_jump) * o_ports_config[i].filling_factor))
	    { 
	      typename T_WRAPPED::T_BE_int aux_for_unpack_2[T_Parallelism];
              int p = 0;
	      for (       ; p < int(float(o_ports_config[i].parallelism)* o_ports_config[i].filling_factor); p++)
		{
		  (o_int_ports[o_ports_config[i].fifo_number]->read()).store(&aux_for_unpack_2[0]);
		  o_data[i][p] = (int32_t)aux_for_unpack_2[0];
		}
              for (       ; p < o_ports_config[i].parallelism; p++)
                o_data[i][p] = 0;
              o_int_ports[o_ports_config[i].fifo_number]->move_pointer(o_ports_config[i].parallelism*(aux_jump-1)* o_ports_config[i].filling_factor);
            }
	  else
	    {
	      throw std::runtime_error("c_interface_emulator_t ERROR: o valid high without new data at output");
	    }
	}
      else if (o_ports_config[i].port_type == is_float)
	{
	  c_fxp_t <T_Parallelism, UME::SIMD::SIMDVec<typename T_WRAPPED::T_BE_float, T_Parallelism>> aux_fxp(o_ports_config[i].nb,
													     o_ports_config[i].nbf,
													     o_ports_config[i].is_signed,
													     false);
	  if ((*o_float_ports[o_ports_config[i].fifo_number]).available_data() >= o_ports_config[i].parallelism*aux_jump* o_ports_config[i].filling_factor)
	    {
	      	      
	      UME::SIMD::SIMDVec<typename T_WRAPPED::T_BE_float, T_Parallelism> aux_f;
	      UME::SIMD::SIMDVec<int32_t, T_Parallelism> aux;
	      typename T_WRAPPED::T_BE_float aux_4_check[T_Parallelism];
              int p = 0;
	      for (       ; p < o_ports_config[i].parallelism* o_ports_config[i].filling_factor; p++)
		{
		  aux_f = o_float_ports[o_ports_config[i].fifo_number]->read();
		  aux_f.store(&aux_4_check[0]);
		  aux = aux_fxp.float_2_int_eq(aux_f);
		  aux.store(&aux_for_unpack[0]);
		  if (aux_for_unpack[0] / pow(2,o_ports_config[i].nbf)  != aux_4_check[0])
		    throw std::runtime_error(("c_interface_emulator_t ERROR: loss of resolution to int eq [" + o_ports_config[i].name + "]: "  
                                              + std::to_string(aux_4_check[0]) + " -> "
					      + std::to_string(aux_for_unpack[0] / pow(2,o_ports_config[i].nbf))).c_str());
		  o_data[i][p] = aux_for_unpack[0];
		  o_data_copy[i][p] = o_data[i][p];
		}
              for (       ; p < o_ports_config[i].parallelism; p++)
                o_data[i][p] = 0;
	      o_float_ports[o_ports_config[i].fifo_number]->move_pointer(o_ports_config[i].parallelism*(aux_jump-1)* o_ports_config[i].filling_factor);
	    }
	  else
	    {
	      throw std::runtime_error("c_interface_emulator_t ERROR: o valid high without new data at the output");
	    }
	}      
    }
}

template <class T_WRAPPED>
void c_interface_emulator_t<T_WRAPPED>::update_p_ports(int32_t **p_data)
{
  for (uint32_t i=0; i < nof_p_ports; i++)
    {
      int aux_jump = 1;
      if (!p_ports_config[i].log_all)
	aux_jump = p_parallelism_multiplier;
      int32_t aux_for_unpack[T_Parallelism];

      for (int p=0; p < p_ports_config[i].parallelism; p++)
	{
	  (p_ports[i]->read()).store(&aux_for_unpack[0]);
	  p_data[i][p] = aux_for_unpack[0];
	}
      p_ports[i]->move_pointer(p_ports_config[i].parallelism*(aux_jump-1));
    }
}

template <class T_WRAPPED>
bool c_interface_emulator_t<T_WRAPPED>::load_i_data(int32_t **i_data)
{
  bool enough_input = true;
  for (uint32_t i=0; i < nof_i_ports; i++)
    {
      if (i_ports_config[i].port_type == is_float)
	{
	  c_fxp_t <T_Parallelism, UME::SIMD::SIMDVec<typename T_WRAPPED::T_BE_float, T_Parallelism>> aux_fxp(i_ports_config[i].nb,
													     i_ports_config[i].nbf,
													     i_ports_config[i].is_signed,
													     false);
	  for (int p=0; p < i_ports_config[i].parallelism * i_ports_config[i].filling_factor; p++)
	    i_float_ports_wr[i_ports_config[i].fifo_number]->write(aux_fxp.int_eq_2_float(i_data[i][p]));
	  enough_input = enough_input && (((i_float_fifos[i_ports_config[i].fifo_number].get_rd_port(0))->available_data()) >=
					  (uint32_t)(i_ports_config[i_ports_config[i].fifo_number].elemental_parallelism));
	}
      else if (i_ports_config[i].port_type == is_int)
	{
	  for (int p=0; p < i_ports_config[i].parallelism * i_ports_config[i].filling_factor; p++)
	    i_int_ports_wr[i_ports_config[i].fifo_number]->write(UME::SIMD::SIMDVec<typename T_WRAPPED::T_BE_int, T_Parallelism>(i_data[i][p]));
	  enough_input = enough_input && (((i_int_fifos[i_ports_config[i].fifo_number].get_rd_port(0))->available_data()) >=
					  (uint32_t)(i_ports_config[i_ports_config[i].fifo_number].elemental_parallelism));
	}
    }
  return enough_input;
}
#endif

