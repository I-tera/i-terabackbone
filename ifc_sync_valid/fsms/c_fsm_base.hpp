#ifndef C_FSM_BASE_HPP
#define C_FSM_BASE_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>

using namespace std;

enum e_fsm_type
  {
    resync,
    latency,
    gated_latency,
    par_conv
  };
    
struct s_fsm_config
{
  string     name;
  e_fsm_type type;
  uint32_t   parall_in;
  uint32_t   parall_out;
  uint32_t   latency;
  uint32_t   resync_period;
};

class c_fsm_base
{
public:
  uint32_t   *i_sync;
  uint32_t   *i_valid;
  uint32_t   *i_soft_rst;
  uint32_t    o_sync;
  uint32_t    o_valid;
  uint32_t    o_soft_rst;

  c_fsm_base(){o_sync = 0; o_valid = 0; o_soft_rst = 0;i_sync = nullptr; i_valid = nullptr; i_soft_rst = nullptr;}
  virtual ~c_fsm_base(){};
  virtual void initialize(const s_fsm_config &config) = 0;
  virtual void reconfigure(const s_fsm_config &config) = 0;
  virtual void free_resources() = 0;
  virtual void clock() = 0;
  virtual void reset() = 0;
  virtual void display() = 0;

  void connect_i_sync(uint32_t *port)     {i_sync = port;}
  void connect_i_valid(uint32_t *port)    {i_valid = port;}
  void connect_i_soft_rst(uint32_t *port) {i_soft_rst = port;}

  uint32_t* get_o_sync()    {return &o_sync;}
  uint32_t* get_o_valid()   {return &o_valid;}
  uint32_t* get_o_soft_rst(){return &o_soft_rst;}
};

  

#endif
