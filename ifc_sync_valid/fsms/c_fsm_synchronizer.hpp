#ifndef C_FSM_SYNCHRONIZER_HPP
#define C_FSM_SYNCHRONIZER_HPP

#include "c_fsm_base.hpp"

class c_fsm_synchronizer: public virtual c_fsm_base
{
private:
  int32_t resync_period;
  int32_t latency;
  int32_t sync_counter;
  uint32_t *valid_delayline;
  uint32_t *sync_delayline;
  
public:
  c_fsm_synchronizer()
  {
    resync_period = 0;
    latency = 0;
    sync_counter = -1;
    valid_delayline = nullptr;
    sync_delayline = nullptr;
  }
  void initialize(const s_fsm_config &config)
  {
    free_resources();
    resync_period = config.resync_period;
    latency = config.latency;
    valid_delayline = new uint32_t[latency+1];
    sync_delayline = new uint32_t[latency+1];
    reset();
  }
  void reconfigure(const s_fsm_config &config)
  {
    resync_period = config.resync_period;
    if (resync_period == 0)
      resync_period = 1;
  }
  void free_resources()
  {
    if (valid_delayline != nullptr){delete[] valid_delayline; valid_delayline = nullptr;}
    if (sync_delayline  != nullptr){delete[] sync_delayline;  sync_delayline  = nullptr;}
  }
  void reset()
  {
    for (int32_t i = latency; i >= 0; i--)
      {
	valid_delayline[i] = 0;
	sync_delayline[i] = 0;
      }
    o_valid = 0;
    o_sync = 0;
    o_soft_rst = 0;
    sync_counter = -1;
  }
  void clock()
  {    
    if (sync_counter == -1) //waiting for resync
      {
	if ((*i_valid == 1) && (*i_sync == 1))
	  {
	    for (int32_t i = latency; i > 0; i--)
	      {
		valid_delayline[i] = 0;
		sync_delayline[i] = 0;
	      }
	    valid_delayline[0] = 1;
	    sync_delayline[0]  = 1;
	    o_soft_rst = 0;
	    sync_counter = 1;
	  }
	else
	  {
	    for (int32_t i = latency; i > 0; i--)
	      {
		valid_delayline[i] = 0;
		sync_delayline[i] = 0;
	      }
	    valid_delayline[0] = 0;
	    sync_delayline[0]  = 0;
	    o_soft_rst = 1;
	    sync_counter = -1;
	  }
      }
    /*else if (sync_counter == 0) //resync expected
      {
	for (int32_t i = latency; i > 0; i--)
	  {
	    valid_delayline[i] = valid_delayline[i-1];
	    sync_delayline[i] = sync_delayline[i-1];
	  }
	valid_delayline[0] = *i_valid;
	sync_delayline[0]  = *i_sync;
	o_soft_rst = *i_soft_rst;
	sync_counter += *i_valid;
	sync_counter = sync_counter % resync_period;
        }*/
    else
      {
	if ((*i_sync == 1) && (*i_valid == 1))
	  {
	    for (int32_t i = latency; i > 0; i--)
	      {
		valid_delayline[i] = 0;
		sync_delayline[i] = 0;
	      }
	    valid_delayline[0] = *i_valid;
	    sync_delayline[0]  = *i_sync;
	    o_soft_rst = 1;
	    sync_counter = 1;
	  }
	else
	  {
	    for (int32_t i = latency; i > 0; i--)
	      {
		valid_delayline[i] = valid_delayline[i-1];
		sync_delayline[i] = sync_delayline[i-1];
	      }
	    valid_delayline[0] = *i_valid;
	    sync_delayline[0]  = *i_sync;
	    o_soft_rst = *i_soft_rst;
	    sync_counter += *i_valid;
	    sync_counter = sync_counter % resync_period;
	  }
      }
    o_valid = valid_delayline[latency];
    o_sync = sync_delayline[latency];
  }
  void display()
  {
    printf("\n==== \n c_fsm_synchronizer state");
    printf("\n count %d / %d", sync_counter, resync_period);
    printf("\n valid: %u -> ", *i_valid);
    printf("[");
    for (int32_t i = 0; i <= latency; i++)
      printf("%u,", valid_delayline[i]);
    printf("\b]");
    printf(" -> %u", o_valid);
    
    printf("\n sync : %u -> ", *i_sync);
    printf("[");
    for (int32_t i = 0; i <= latency; i++)
      printf("%u,", sync_delayline[i]);
    printf("\b]");
    printf(" -> %u", o_sync);

    printf("\n s_rst: %u -> %u", *i_soft_rst, o_soft_rst);
  }
    
};
#endif
