module fsm_pconv_4_to_2_3_4#(
   parameter QPSK_ID    = 'd0,
   parameter QAM8_ID    = 'd1,
   parameter QAM16_ID   = 'd2,
   parameter NB_COUNTER = 'd3,
   parameter NB_MOD_SEL = 'd2
   )
   (
   input                   clk                ,
   input                   i_valid            ,
   input                   i_valid_counter    ,
   input                   i_sync             ,
   input                   i_rst              ,
   input                   rst_async_n        ,
   input [NB_MOD_SEL-1:0]  i_static_modulation,
   output                  o_valid            ,
   output                  o_dp_pconv_valid   ,
   output                  o_sync             ,
   output                  o_rst              ,
   output [NB_COUNTER-1:0] o_counter
   );

   localparam STATE_RESET               = 1'b0;
   localparam STATE_WORKING             = 1'b1;
   localparam QPSK_MAX_COUNT_VAL        = 'd1;
   localparam QAM8_MAX_COUNT_VAL        = 'd3;
   localparam QAM16_MAX_COUNT_VAL       = 'd0;
   localparam NB_VALID_COUNTER          = 'd2;
   localparam NB_REST_PER_CLOCK         = NB_VALID_COUNTER+1;
   localparam QPSK_REST_PER_CLOCK       = 'd2;
   localparam QAM8_REST_PER_CLOCK       = 'd3;
   localparam QAM16_REST_PER_CLOCK      = 'd0;
   localparam IN_PER_CLOCKS             = 'd4;

   wire                         qam16_o_valid     ;
   wire                         qam8_o_valid      ;
   wire                         qpsk_o_valid      ;
   wire                         modulations_valid ;
   wire                         modulations_o_sync;
   wire                         out_counter_valid ;
   //wire [NB_COUNTER-1:0]        w_count_max_value ;
   wire [NB_REST_PER_CLOCK-1:0] rest_per_clock    ;
   wire [NB_REST_PER_CLOCK-1:0] inputs_per_clock  ;

   reg                           w_modulations_valid ;
   //reg                           sync_input_ff       ;
   reg                           current_state_ff    ;
   reg                           w_next_state        ;
   reg                           w_count_en          ;
   //reg  [NB_COUNTER-1:0]         out_counter_ff      ;
   reg  [NB_VALID_COUNTER+1-1:0] av_word_ff          ;
   reg                           modulations_valid_ff;
   reg                           sync_output_ff      ;

   assign o_rst     = i_rst               ;
   assign o_valid   = modulations_valid_ff/*w_modulations_valid*/ ;
   assign o_dp_pconv_valid   = w_modulations_valid;

   assign o_counter = av_word_ff;//out_counter_ff      ;
   assign o_sync    = sync_output_ff/*modulations_o_sync*/  ;

//   always_ff @ (posedge clk, negedge rst_async_n) begin:sync_register
//      if(!rst_async_n)begin
//         sync_input_ff <= 1'b0;
//      end
//      else begin
//         if(i_rst)begin
//            sync_input_ff <= 1'b0;
//         end
//         else begin
//            sync_input_ff <= i_sync;
//         end
//      end
//   end



   assign modulations_o_sync = i_sync;//(i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])?(i_sync): ((i_sync||sync_input_ff)&&(out_counter_ff=={NB_COUNTER{1'b0}}));

   assign modulations_valid = (i_static_modulation == QPSK_ID[NB_MOD_SEL-1:0])?  (qpsk_o_valid ):
   (i_static_modulation == QAM8_ID[NB_MOD_SEL-1:0])?  (qam8_o_valid ):
   (i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])? (qam16_o_valid ): 1'b0;

   //assign w_count_max_value = (i_static_modulation == QPSK_ID[NB_MOD_SEL-1:0])?  (QPSK_MAX_COUNT_VAL[NB_COUNTER-1:0] ):
   //(i_static_modulation == QAM8_ID[NB_MOD_SEL-1:0])?  (QAM8_MAX_COUNT_VAL[NB_COUNTER-1:0] ):
   //(i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])? (QAM16_MAX_COUNT_VAL[NB_COUNTER-1:0] ): QAM16_MAX_COUNT_VAL[NB_COUNTER-1:0];


   //fsm w_next_state logic
   always_comb begin:w_next_state_logic
      case(current_state_ff)
         STATE_RESET:
            begin
               if(i_rst)begin
                  w_next_state = STATE_RESET;
                  w_modulations_valid = 1'b0;
                  w_count_en = 1'b0;
               end
               else begin
                  w_next_state = STATE_WORKING;
                  w_modulations_valid = modulations_valid;
                  w_count_en = 1'b1;
               end
            end
         STATE_WORKING:
            begin
               if(i_rst)begin
                  w_next_state = STATE_RESET;
                  w_modulations_valid = 1'b0;
                  w_count_en = 1'b0;
               end
               else begin
                  w_next_state = STATE_WORKING;
                  w_modulations_valid = modulations_valid;
                  w_count_en = 1'b1;
               end
            end
      endcase
   end

   assign rest_per_clock = (i_static_modulation == QPSK_ID[NB_MOD_SEL-1:0] )? (QPSK_REST_PER_CLOCK[NB_REST_PER_CLOCK-1:0] ):
   (i_static_modulation == QAM8_ID[NB_MOD_SEL-1:0] )? (QAM8_REST_PER_CLOCK[NB_REST_PER_CLOCK-1:0] ):
   (i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])? (QAM16_REST_PER_CLOCK[NB_REST_PER_CLOCK-1:0]):QAM16_REST_PER_CLOCK[NB_REST_PER_CLOCK-1:0];

   assign inputs_per_clock = IN_PER_CLOCKS[NB_REST_PER_CLOCK-1:0];

   always_ff @ ( posedge clk, negedge rst_async_n ) begin:available_words_blk
      if(!rst_async_n)begin
         av_word_ff <= {(NB_VALID_COUNTER+1){1'b0}};
      end
      else begin
         if (i_rst)begin
            av_word_ff <= {(NB_VALID_COUNTER+1){1'b0}};
         end
         else begin
            if(w_count_en) begin
               if(i_valid)begin
                  av_word_ff <= inputs_per_clock - rest_per_clock + av_word_ff;//OVERFLOW ALREADY CONSIDERED
               end
               else begin
                  if(av_word_ff<rest_per_clock)begin
                     av_word_ff <= av_word_ff;
                  end
                  else begin
                     av_word_ff <= av_word_ff - rest_per_clock;
                  end
               end
            end
         end
      end
   end

   assign out_counter_valid = /*i_valid_counter*/i_valid | (av_word_ff>=rest_per_clock);


   //always_ff @ ( posedge clk,  negedge rst_async_n ) begin:out_counter_ff_proc
   //   if(!rst_async_n)begin
   //      out_counter_ff <= {NB_COUNTER{1'b0}};
   //   end
   //   else begin:out_counter_ff_sync_proc
   //      if (i_rst)begin
   //         out_counter_ff <= {NB_COUNTER{1'b0}};
   //      end
   //      else begin
   //         if(out_counter_valid && w_count_en) begin
   //            if(out_counter_ff == w_count_max_value)begin
   //               out_counter_ff <= {NB_COUNTER{1'b0}};
   //            end
   //            else begin
   //               out_counter_ff <= out_counter_ff + {{NB_COUNTER-1{1'b0}},1'b1};//OVERFLOW ALREADY CONSIDERED,PADDING CONSIDERED
   //            end
   //         end
   //      end
   //   end
   //end

   assign qam16_o_valid = i_valid;
   assign qam8_o_valid  = out_counter_valid;
   assign qpsk_o_valid  = out_counter_valid;

   always_ff @(posedge clk, negedge rst_async_n)begin:current_state
      if(!rst_async_n)begin
         current_state_ff <= STATE_RESET;
      end
      else begin:current_state_ff_sync_proc
         if(i_rst)begin
            current_state_ff <= STATE_RESET;
         end
         else begin
            if(i_valid)begin
               current_state_ff <= w_next_state;
            end
         end
      end
   end



   always_ff @ (posedge clk, negedge rst_async_n) begin:valid_register
      if(!rst_async_n)begin
      modulations_valid_ff <= 1'b0;
      sync_output_ff       <= 1'b0;
      end
      else begin
         if(i_rst) begin
            modulations_valid_ff   <= 1'b0;
            sync_output_ff         <= 1'b0;
         end
         else begin
            modulations_valid_ff <= w_modulations_valid;
            sync_output_ff       <= modulations_o_sync;
         end
      end
   end

endmodule
