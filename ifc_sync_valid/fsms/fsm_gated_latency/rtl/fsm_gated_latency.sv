module fsm_gated_latency
  #(
    parameter LATENCY           = 4
    )
   (
    input                clk,
    output [LATENCY-1:0] o_ctrl,
    output               o_valid,
    output               o_sync,
    output               o_rst,
    input                rst_async_n,
    input                i_valid,
    input                i_rst,
    input                i_sync
    );
   

   //enable fifo
   reg [LATENCY-1:0] valid_fifo_ff;
   always_ff @(posedge clk, negedge rst_async_n) begin : valid_fifo_block
        if(!rst_async_n) begin
           valid_fifo_ff <= {LATENCY{1'b0}};
        end
        else begin
             if(i_rst) begin
               valid_fifo_ff <= {LATENCY{1'b0}};
             end
             else begin
                if(i_valid) begin
                  valid_fifo_ff <= {valid_fifo_ff[LATENCY-2 -: LATENCY-1],i_valid};
                end
             end
        end // else: !if(~rst_async_n)
   end
   
   //sync fifo
   reg [LATENCY-1:0] sync_fifo_ff;
   always_ff @(posedge clk, negedge rst_async_n) begin : sync_fifo_block
        if(!rst_async_n) begin
          sync_fifo_ff <= {LATENCY{1'b0}};
        end
        else begin
             if(i_rst) begin
                sync_fifo_ff <= {LATENCY{1'b0}};
             end
             else begin
                  if(i_valid) begin
                     sync_fifo_ff <= {sync_fifo_ff[LATENCY-2 -: LATENCY-1],i_sync};
                  end
             end
        end // else: !if(~rst_async_n)
   end
   
   assign o_sync         = sync_fifo_ff[LATENCY-1] & i_valid  ;
   assign o_ctrl         = valid_fifo_ff & {LATENCY{i_valid}};
   assign o_valid        = valid_fifo_ff[LATENCY-1] & i_valid;
   assign o_rst          = i_rst                            ;


endmodule
