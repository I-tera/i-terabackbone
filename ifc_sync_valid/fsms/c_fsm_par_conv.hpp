#ifndef C_FSM_PAR_CONV_HPP
#define C_FSM_PAR_CONV_HPP

#include "c_fsm_base.hpp"

class c_fsm_par_conv: public virtual c_fsm_base
{
private:
  uint32_t *sync_fifo;
  uint32_t wr_pointer;
  uint32_t rd_pointer;
  uint32_t input_parallelism;
  uint32_t output_parallelism;
  uint32_t fifo_size;
  uint32_t available_data() const
  {
    return (wr_pointer + fifo_size - rd_pointer) % (fifo_size);
  }
public:
  c_fsm_par_conv()
  {
    sync_fifo = nullptr;
    wr_pointer = 0;
    rd_pointer = 0;
    input_parallelism = 1;
    output_parallelism = 1;
    fifo_size = 2;
  }
  void initialize(const s_fsm_config &config)
  {
    free_resources();
    input_parallelism = config.parall_in;
    output_parallelism = config.parall_out;
    fifo_size = input_parallelism * output_parallelism+1;
    sync_fifo = new uint32_t[fifo_size];
    rd_pointer = 0;
    wr_pointer = 0;
    reset();
  }
  void reconfigure(const s_fsm_config &config)
  {
    if (fifo_size < config.parall_in * config.parall_out + 1)
      {
        initialize(config);
      }
    else
      {
        input_parallelism = config.parall_in;
        output_parallelism = config.parall_out;
      }
  }
  void free_resources()
  {
    if (sync_fifo != nullptr){delete[] sync_fifo; sync_fifo = nullptr;}
  }
  void reset()
  {
    for (uint32_t i = 0; i < fifo_size; i++)
      sync_fifo[i] = 0;
    rd_pointer = 0;
    wr_pointer = 0;
    o_valid = 0;
    o_sync = 0;
    o_soft_rst = 0;
  }
  void clock()
  {
    if (*i_soft_rst == 1)
      {
	reset();
      }
    else
      {
	if (*i_valid == 1)
	  {
	    if (fifo_size - available_data() <= input_parallelism)
	      throw std::runtime_error("c_fsm_par_conv ERROR: fifo overflow");
	    sync_fifo[wr_pointer] = *i_sync;
	    wr_pointer = (wr_pointer + 1) % (fifo_size);
	    for (uint32_t i = 1; i < input_parallelism; i++)
	      {
		sync_fifo[wr_pointer] = 0;
		wr_pointer = (wr_pointer + 1) % (fifo_size);
	      }
	  }

	if (available_data() >= output_parallelism)
	  {
	    o_valid = 1;
	    o_sync = sync_fifo[rd_pointer];
	    rd_pointer = (rd_pointer + 1) % (fifo_size);
	    for (uint32_t i = 1; i < output_parallelism; i++)
	      {
		if (sync_fifo[rd_pointer] == 1)
		  throw std::runtime_error("c_fsm_par_conv ERROR: sync in a not leading position");
		rd_pointer = (rd_pointer + 1) % (fifo_size);
	      }
	  }
	else
	  {
	    o_valid = 0;
	    o_sync = 0;
	  }
      }
    o_soft_rst = *i_soft_rst;
  }
  void display()
  {
    printf("\n==== \n c_fsm_par_conv state");
    printf("\n valid: %u -> ", *i_valid);
    printf(" -> %u", o_valid);
    printf("\n sync : %u -> ", *i_sync);
    printf(" -> %u", o_sync);
    printf("\n s_rst: %u -> %u", *i_soft_rst, o_soft_rst);
    printf("\n available_data: %u", available_data());
  }
    
};
#endif
