module fsm_pconv_2_3_4_to_4#(
   parameter QPSK_ID    = 0,
   parameter QAM8_ID    = 1,
   parameter QAM16_ID   = 2,
   parameter NB_COUNTER = 2,
   parameter NB_MOD_SEL = 2
   )
   (
      input clk,
      input i_valid,
      input i_sync,
      input i_rst,
      input i_valid_counter,
      input  rst_async_n,
      input [NB_MOD_SEL-1:0]i_static_modulation,
      output o_valid,
      output o_dp_pconv_valid,
      output o_sync,
      output o_rst,
      output [NB_COUNTER-1:0] o_counter
      );

      localparam QPSK_MAX_COUNT_VAL  = 1;
      localparam QAM8_MAX_COUNT_VAL  = 3;
      localparam QAM16_MAX_COUNT_VAL = 0;

      wire                  qam16_o_valid       ;
      wire                  qam8_o_valid        ;
      wire                  qpsk_o_valid        ;
      wire                  modulations_valid   ;
      wire                  modulations_o_sync  ;
      wire                  w_modulations_valid ;
      wire [NB_COUNTER-1:0] w_count_max_value   ;

      //reg  w_modulations_valid;

      reg                   sync_input_ff       ;
      reg                   sync_output_ff      ;
      reg  [NB_COUNTER-1:0] counter_ff          ;
      reg                   modulations_valid_ff;

      assign o_valid            = modulations_valid_ff;
      assign o_dp_pconv_valid   = w_modulations_valid ;
      assign o_counter          = counter_ff          ;
      assign o_sync             = sync_output_ff      /*modulations_o_sync*/;
      assign o_rst              = i_rst               ;




      always_ff @ (posedge clk, negedge rst_async_n) begin:valid_register
         if(!rst_async_n)begin
            modulations_valid_ff <= 1'b0;
            sync_output_ff       <= 1'b0;
         end
         else begin
            if(i_rst) begin
               modulations_valid_ff   <= 1'b0;
               sync_output_ff         <= 1'b0;
            end
            else begin
               modulations_valid_ff <= w_modulations_valid;
               sync_output_ff       <= modulations_o_sync;
            end
         end
      end


      assign modulations_o_sync = (i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])?(i_sync): (sync_input_ff /*&& w_modulations_valid*/);




      assign modulations_valid = (i_static_modulation == QPSK_ID[NB_MOD_SEL-1:0])?  (qpsk_o_valid ):
      (i_static_modulation == QAM8_ID[NB_MOD_SEL-1:0])?  (qam8_o_valid ):
      (i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])? (qam16_o_valid):1'b0;




      assign w_count_max_value = (i_static_modulation == QPSK_ID[NB_MOD_SEL-1:0])?  (QPSK_MAX_COUNT_VAL[NB_COUNTER-1:0] ):
      (i_static_modulation == QAM8_ID[NB_MOD_SEL-1:0])?  (QAM8_MAX_COUNT_VAL[NB_COUNTER-1:0] ):
      (i_static_modulation == QAM16_ID[NB_MOD_SEL-1:0])? (QAM16_MAX_COUNT_VAL[NB_COUNTER-1:0] ): QAM16_MAX_COUNT_VAL[NB_COUNTER-1:0];
      assign w_modulations_valid = modulations_valid;



      always_ff @ ( posedge clk, negedge rst_async_n ) begin:counter_register
         if(!rst_async_n)begin
            counter_ff <= {NB_COUNTER{1'b0}};
         end
         else begin
            if (i_rst)begin
               counter_ff <= {NB_COUNTER{1'b0}};
            end
            else begin
               if(i_valid_counter) begin:counter_ff_working
                  if(counter_ff == w_count_max_value)begin
                     counter_ff <= {NB_COUNTER{1'b0}};
                  end
                  else begin
                     counter_ff <= counter_ff + 1'b1;
                  end
               end
            end
         end
      end

   assign qam16_o_valid = i_valid;
   assign qam8_o_valid  = |counter_ff && i_valid;
   assign qpsk_o_valid  = counter_ff[0]&&i_valid;

   always_ff @ (posedge clk, negedge rst_async_n) begin:sync_register
      if(!rst_async_n)begin
         sync_input_ff   <= 1'b0;
      end
      else begin
         if(i_rst) begin
            sync_input_ff   <= 1'b0;
         end
         else begin
            if (i_valid) begin
               sync_input_ff   <= i_sync;
            end
         end
      end
   end
endmodule
