/**
 * @Author: Matias Ezequiel Grosso <MatiasGrosso>
 * @Date:   2019-11-22T12:57:48-03:00
 * @Last modified by:   i-tera
 * @Last modified time: 2019-12-02T14:37:00-03:00
 */



module resynchronizer#(
   parameter PARALELLISM_DATA ='d444
   )
   (
   input                          clk         ,
   output                         o_rst    ,
   output                         o_valid     ,
   output                         o_ctrl ,
   output                         o_sync   ,
   output [PARALELLISM_DATA-1:0]  o_data      ,
   input                          rst_async_n ,
   input [PARALELLISM_DATA-1:0]   i_data      ,
   input                          i_valid     ,
   input                          i_sync
   );
   wire                       fsm_resync_ctrl      ;
   wire                       fsm_resync_rst       ;
   wire                       fsm_resync_valid     ;
   wire                       fsm_resync_sync      ;
   reg [PARALELLISM_DATA-1:0] data_dff             ;

   always_ff @(posedge clk)begin:input_register_proc
      if(fsm_resync_ctrl)begin:data_ff_reset
         data_dff <= i_data;
      end
      else begin:data_dff_update
         data_dff <= data_dff;
      end
   end

   fsm_resync uu_fsm_resync(
   .clk        (clk              ),
   .o_rst   (fsm_resync_rst      ),
   .o_valid    (fsm_resync_valid ),
   .o_ctrl(fsm_resync_ctrl       ),
   .o_sync  (fsm_resync_sync     ),
   .rst_async_n(rst_async_n      ),
   .i_valid    (i_valid          ),
   .i_sync     (i_sync                     )
   );

   assign o_rst   = fsm_resync_rst   ;
   assign o_valid = fsm_resync_valid ;
   assign o_sync  = fsm_resync_sync  ;
   assign o_data  = data_dff         ;
   assign o_ctrl  = fsm_resync_ctrl  ;
endmodule
