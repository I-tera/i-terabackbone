/**
 * @Author: Matias Ezequiel Grosso <MatiasGrosso>
 * @Date:   2019-11-22T12:52:57-03:00
 * @Last modified by:   i-tera
 * @Last modified time: 2019-12-02T14:38:01-03:00
 */



module fsm_resync(
   input  clk,
   output o_rst,
   output o_valid,
   output o_sync,
   output o_ctrl,
   input  rst_async_n,
   input  i_valid,
   input  i_sync
   );
   wire  rst         ;
   wire  valid      ;
   wire  sync        ;
   reg   start_state_ff ;
   reg   valid_ff   ;
   reg   sync_ff     ;

   always_ff @(posedge clk, negedge rst_async_n)begin:start_state_proc
         if(~rst_async_n)begin:reset_state
            start_state_ff <= 1'b0;
         end
         else begin:state_set
            if(~(start_state_ff))begin:state_update
               start_state_ff <= i_valid & i_sync;
            end
            else begin: state_no_update
               start_state_ff <= start_state_ff;
            end
         end
   end
   assign rst     = start_state_ff? i_valid & i_sync: 1'b1 ;//& (valid_counter != ((i_valid_counter_max_value-1))):1'b1;
   assign valid  = start_state_ff? i_valid:i_valid & i_sync;
   assign sync    = i_valid & i_sync;

   always_ff @(posedge clk, negedge rst_async_n)begin:valid_ff_proc
      if(~rst_async_n)begin:valid_ff_reset
         valid_ff <= 1'b0;
      end
      else begin:valid_ff_update
         valid_ff <= valid;
      end
   end

   always_ff @(posedge clk, negedge rst_async_n)begin:sync_ff_proc
      if(~rst_async_n) begin:sync_ff_reset
         sync_ff <= 1'b0;
      end
      else begin:sync_ff_update
         sync_ff <= sync;
      end
   end

   assign o_rst       = rst;
   assign o_valid     = valid_ff;
   assign o_sync      = sync_ff;
   assign o_ctrl      = valid;

endmodule

   //valid_counter
   // reg [NB_VALID_COUNTER-1:0] valid_counter;

   // always @(posedge clk, negedge rst_async_n)
   //   begin:valid_counter_reg
   //      if(~rst_async_n)
   //       valid_counter <= {NB_VALID_COUNTER{1'b0}};
   //      else
   //        begin
   //            if(rst)
   //           valid_counter <= {NB_VALID_COUNTER{1'b0}};
   //            else
   //              begin
   //              //if(valid)
   //              if(i_valid)
   //                begin
   //                  if(valid_counter == (i_valid_counter_max_value-1))
   //                     valid_counter <= {NB_VALID_COUNTER{1'b0}};
   //                  else
   //                   valid_counter <= valid_counter + 1'b1;
   //               end
   //              else
   //                valid_counter <= valid_counter;
   //              end
   //         end // else: !if(~rst_async_n)
   //   end
