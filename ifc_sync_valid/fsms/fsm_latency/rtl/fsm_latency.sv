module fsm_latency 
  #(
    parameter LATENCY           = 4
    )
   (
    input                clk,
    output [LATENCY-1:0] o_ctrl,
    output               o_valid,
    output               o_sync,
    output               o_rst,
    input                rst_async_n,
    input                i_valid,
    input                i_rst,
    input                i_sync
    );

   generate
      if(LATENCY  == 'd1) begin: single_lat_blk
           //enable fifo
         reg valid_fifo_ff;
         always_ff @(posedge clk, negedge rst_async_n) begin : valid_fifo_block
            if(~rst_async_n) begin
              valid_fifo_ff <=1'b0;
            end
            else begin
               if(i_rst) begin
                 valid_fifo_ff <=1'b0;
               end
               else begin
                  valid_fifo_ff <= i_valid;      
               end
            end // else: !if(~rst_async_n)
         end
           
         //sync fifo
         reg sync_fifo_ff;
         always_ff @(posedge clk, negedge rst_async_n) begin : sync_fifo_block
            if(~rst_async_n) begin
              sync_fifo_ff <= 1'b0;
            end
            else begin
               if(i_rst) begin
                  sync_fifo_ff <=1'b0;
               end
               else begin
                  sync_fifo_ff <= i_sync;       
               end
            end // else: !if(~rst_async_n)
         end // block: sync_fifo_block
         assign o_sync         = sync_fifo_ff;
         assign o_ctrl         = valid_fifo_ff;
         assign o_valid        = valid_fifo_ff;
           
      end // block: single_lat_blk
      else begin : mult_lat_blk
         //enable fifo
         reg [LATENCY-1:0] valid_fifo_ff;
         always_ff @(posedge clk, negedge rst_async_n) begin : valid_fifo_block
            if(~rst_async_n) begin
              valid_fifo_ff <= {LATENCY{1'b0}};
            end
            else begin
               if(i_rst) begin
                 valid_fifo_ff <= {LATENCY{1'b0}};
               end
               else begin
                  valid_fifo_ff <= {valid_fifo_ff[LATENCY-2 -: LATENCY-1],i_valid};       
               end
            end // else: !if(~rst_async_n)
         end
           
         //sync fifo
         reg [LATENCY-1:0] sync_fifo_ff;
         always_ff @(posedge clk, negedge rst_async_n) begin : sync_fifo_block
                if(~rst_async_n) begin 
                  sync_fifo_ff <= {LATENCY{1'b0}};
                end
                else begin
                   if(i_rst) begin
                      sync_fifo_ff <= {LATENCY{1'b0}};
                   end
                     else begin
                        sync_fifo_ff <= {sync_fifo_ff[LATENCY-2 -: LATENCY-1],i_sync};       
                     end
                end // else: !if(~rst_async_n)
         end // block: sync_fifo_block
         assign o_sync         = sync_fifo_ff[LATENCY-1];
         assign o_ctrl         = valid_fifo_ff;
         assign o_valid        = valid_fifo_ff[LATENCY-1];
           
      end // block: mult_lat_blk
   endgenerate
   
   assign o_rst          = i_rst;
   
endmodule
