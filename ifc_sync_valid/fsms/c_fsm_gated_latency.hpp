#ifndef C_FSM_GATED_LATENCY_HPP
#define C_FSM_GATED_LATENCY_HPP

#include "c_fsm_base.hpp"

class c_fsm_gated_latency: public virtual c_fsm_base
{
private:
  uint32_t latency;
  uint32_t *valid_delayline;
  uint32_t *sync_delayline;
  
public:
  c_fsm_gated_latency()
  {
    latency = 0;
    valid_delayline = nullptr;
    sync_delayline = nullptr;
  }
  void initialize(const s_fsm_config &config)
  {
    free_resources();
    latency = config.latency;
    valid_delayline = new uint32_t[latency+1];
    sync_delayline = new uint32_t[latency+1];
    reset();
  }
  void reconfigure(const s_fsm_config &config){}
  void free_resources()
  {
    if (valid_delayline != nullptr){delete[] valid_delayline; valid_delayline = nullptr;}
    if (sync_delayline  != nullptr){delete[] sync_delayline;  sync_delayline  = nullptr;}
  }
  void reset()
  {
    for (int32_t i = latency; i >= 0; i--)
      {
	valid_delayline[i] = 0;
	sync_delayline[i] = 0;
      }
    o_valid = 0;
    o_sync = 0;
  }
  void clock()
  {
    o_valid = valid_delayline[latency] * (*i_valid);
    o_sync = sync_delayline[latency] * (*i_valid);
    o_soft_rst = *i_soft_rst;
    
    valid_delayline[0] = *i_valid;
    sync_delayline[0]  = *i_sync;
    if (*i_soft_rst == 1)
      {
	reset();
      }
    else
      {
	if (*i_valid == 1)
	  {
	    for (int32_t i = latency; i > 0; i--)
	      {
		valid_delayline[i] = valid_delayline[i-1];
		sync_delayline[i] = sync_delayline[i-1];
	      }
	  }
      }
    o_soft_rst = *i_soft_rst;
  }
  void display()
  {
    printf("\n==== \n c_fsm_gated_latency state");
    printf("\n valid: %u -> ", *i_valid);
    printf("[");
    for (uint32_t i = 1; i <= latency; i++)
      printf("%u,", valid_delayline[i]);
    printf("\b]");
    printf(" -> %u", o_valid);
    
    printf("\n sync : %u -> ", *i_sync);
    printf("[");
    for (uint32_t i = 1; i <= latency; i++)
      printf("%u,", sync_delayline[i]);
    printf("\b]");
    printf(" -> %u", o_sync);

    printf("\n s_rst: %u -> %u", *i_soft_rst, o_soft_rst);
  }
    
};
#endif
