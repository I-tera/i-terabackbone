%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HOJA T\'ECNICA DE LAS MAQUINAS DE ESTADOS PARA LA INTERFACE SYNC-VALID%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{paper}

\usepackage[english]{babel}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{times}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{fancyhdr}
\usepackage{anysize}
\usepackage{verbatim}
\usepackage{array}
\usepackage{tabularx}
\usepackage{float}
\bibliographystyle{plain}
\usepackage{titletoc, tocloft}
\setlength{\cftsubsecindent}{1cm}
\setlength{\cftsubsubsecindent}{2cm}


\restylefloat{table}


\title{I-TERA S.A.S.\\
	   \vspace{0.5cm}
  	   Finite State Machine Catalog for Sync-Valid Interface}
\author{Facundo Picco}
\subtitle{Revision 0.1}



\begin{document}

\maketitle

Revision History
\begin{table}[h]
\begin{center}
 \begin{tabular}{|m{2cm}|m{2cm}|m{3cm}|m{5cm}|} 
 \hline
 Revision & Date & Author & Modifications \\ [0.5ex] 
 \hline
 0.1 & 04/12/2019 & Facundo Picco & Initial draft \\ 
 \hline
\end{tabular}
\end{center}
\end{table}

\clearpage
\tableofcontents
\listoffigures
\listoftables

\clearpage
\section{Introduction}

\subsection{Purpose of this document}
The purpose of this document is to describe the micro architecture of the finite state machines available for the sync-valid-interface (see \cite{SyncValid}). These finite state machines can be reused by any module implementing the interface, and they can be connected in series to control complex blocks.
This document has been written to be a reference for:
\begin{itemize}
	\item Systems designer
	\item RTL designer
	\item Test engineer
	\item Back-end engineer
	\item Block users
\end{itemize}


\subsection{Acronyms and Abbreviations}
In Table \ref{Tab:acronyms}, there are acronyms and abbreviations that were used in this document.
\begin{table}[h]
\caption{Acronyms and Abbreviation Terms}
\begin{center}
 \begin{tabular}{|m{2cm}|m{9cm}|} 
 \hline
 Term & Meaning \\ [0.5ex] 
 \hline
 I/O & Input/Output \\ 
 \hline
 QAM & Quadrature amplitude modulation \\
 \hline
 QPSK & Quadrature phase-shift keying \\
 \hline
 RTL & Register transfer level \\
 \hline
 FSM & Finite state machine \\
 \hline
\end{tabular}
 \label{Tab:acronyms}
\end{center}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\section{Generalities}
In this section, the generalities of the FSMs will be described
\subsection{High-Level Functional Description}
All finite state machines described in this document respect the same interface. This allows them to be connected in series without any extra effort by connecting their input and output signals as following.
\begin{itemize}
  \item fsm1.o\_valid to fsm2.i\_valid
  \item fsm1.o\_sync to fsm2.i\_valid
  \item fsm1.o\_rst to fsm2.i\_rst
\end{itemize}
Besides the interface signals, each finite state machine has a specific o\_ctrl port which contains the signals that should control the behavior of the data path logic.
The Figure ~\ref{bbd} shows a black box diagram of this generic FSM.

\begin{figure}[!ht]
  \vspace{0.0cm}
  \centering
  \includegraphics[scale=1.0]{figures/bdd.png}
  \caption{Black box diagram of generic FSM}
  \label{bbd}
\end{figure}

\subsubsection{Features}
\begin{itemize}
  \item Implements full valid-sync interface
  \item Allows modular construction of complete finite state machines of blocks that implement this specific interface
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\section{fsm\_resync}
\subsection{Functional description}
On start up, the fsm\_resync will block all i\_valid signals until the first i\_sync is received. From that moment on, the module will be transparent and will keep on checking if i\_sync signals arrives, meaning that the block needs a re-synchronization. As long as no active i\_sync is received, the module is transparent and bypasses i\_valid directly.\\
On the event on an active i\_sync received, then a loss of synchronization is detected. The module then activates the internal soft reset o\_rst to bring back the full module and downstream modules to their original state, and prepare the data path and control to treat the incoming data as the very first set of data. The o\_rst signal is delivered one clock earlier than o\_valid, o\_sync (and externally to this module, o\_data). This allows all modules to have one clock cycle for resetting modules and prepare for treating the leading data portion marked by the new i\_sync as the very first portion of data, without waste.
\subsection{Michroarchitecture}
Fig ~\ref{fsmlatency_ibd} shows the internal block diagram of the fsm\_resync module

\begin{figure}[!ht]
  \vspace{0.0cm}
  \centering
  \includegraphics[scale=0.4]{figures/fsm_resync.png}
  \caption{fsm\_resync internal block diagram}
  \label{fsmresync_ibd}
\end{figure}

\subsection{Interfaces}
Table ~\ref{Tab:fsmresync_interface} shows the interface of the fsm\_resync module
\begin{table}[!ht]
\caption{fsm\_resync interface}
\begin{center}
 \begin{tabular}{|p{2.5cm}|p{0.8cm}|p{1cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{4.5cm}|} 
 \hline
 Port name     & I/O & NBW & NI & NS & S & Description \\ \hline
 clk           & C   & -   & -  & -  & - & Input clock with duty cycle of 50\%  \\  \hline
 rst\_async\_n & R   & -   & -  & -  & - & Reset, asynchronous, active low \\ \hline
 i\_valid      & In  & 1   & -  & -  & - & See \cite{SyncValid} \\ \hline
 i\_sync       & In  & 1   & -  & -  & - & See \cite{SyncValid} \\ \hline
 i\_rst        & In  & 1   & -  & -  & - & Internal reset for preparation for re-synchronization, provided by upstream module \\ \hline
 o\_valid      & Out & 1   & -  & -  & - & See \cite{SyncValid} \\ \hline
 o\_sync       & Out & 1   & -  & -  & - & See \cite{SyncValid} \\ \hline
 o\_rst        & Out & 1   & -  & -  & - & Internal reset for preparation for re synchronization, provided to downstream module and subordinate logic\\ \hline
 o\_ctrl       & Out & 1   & -  & -  & - & It is an anticipated version of the o\_valid dedicated to enable toggling of the data path input register, which shall be kept frozen every time i\_valid is low and until first i\_sync arrives to minimize power dissipation.\\ \hline
\end{tabular}
 \label{Tab:fsmresync_interface}
\end{center}
\end{table}

\subsection{Timing Diagrams}
Pending

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\section{fsm\_latency}
\subsection{Functional description}
The finite state machine fsm\_latency implements a simple shift register of both i\_valid and i\_sync so that they are aligned with the progress of the data in the data path. It also implements i\_rst interface to reset all internal shift registers, and propagates it directly to downstream modules.
The o\_ctrl signal is a bus in which each wire corresponds to a different delay in the shift register. There are as many wires in the bus as latency in the module. The number of latency is configurable via a parameter.
\subsection{Michroarchitecture}
Fig ~\ref{fsmlatency_ibd} shows the internal block diagram of the fsm\_latency module
\begin{figure}[!ht]
  \vspace{0.0cm}
  \centering
  \includegraphics[scale=0.4]{figures/fsm_latency.png}
  \caption{fsm\_latency internal block diagram}
  \label{fsmlatency_ibd}
\end{figure}
\subsection{Interfaces}
Table ~\ref{Tab:fsmlatency_interface} shows the interface of the fsm\_resync module
\begin{table}[!ht]
\caption{fsm\_latency interface}
\begin{center}
 \begin{tabular}{|m{2cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{6cm}|} 
 \hline
 Port name & I/O & NBW & NI & NS & S & Description \\ [0.5ex]  \hline
 clk & C & & & & & Input clock with duty cycle of 50\% \\ \hline
 rst\_async\_n & R & & & & & Reset, asynchronous, active low \\ \hline
 i\_valid & In & 1 & & & & See \cite{SyncValid} \\ \hline
 i\_sync & In & 1 & & & & See \cite{SyncValid} \\ \hline
 i\_rst & In & 1 & & & & Internal reset for preparation for re synchronization, provided by upstream module \\ \hline
 o\_valid & Out & 1 & & & & See \cite{SyncValid} \\ \hline
 o\_sync & Out & 1 & & & & See \cite{SyncValid} \\ \hline
 o\_rst & Out & 1 & & & & Internal reset for preparation for re synchronization, provided to downstream module and subordinate logic\\ \hline
 o\_ctrl & Out & 1 & & & & Is a bus of wires that are the outputs of all the registers of the i\_valid shift register.\\ \hline
\end{tabular}
 \label{Tab:fsmlatency_interface}
\end{center}
\end{table}
\subsection{Timing Diagrams}
Pending

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage
\section{fsm\_gated\_latency}
\subsection{Functional description}
The finite state machine fsm\_gated\_latency implements a simple shift register of both i\_valid and i\_sync so that they are aligned with the progress of the data in the data path. In this case the shift registers are only allowed to shift when there is an active i\_valid at the input. It also implements i\_rst interface to reset all internal shift registers, and propagates it directly to downstream modules.
The o\_ctrl signal is a bus in which each wire corresponds to a different delay in the valid shift register, enabled by i\_valid. There are as many wires in the bus as latency in the module. The number of latency is configurable via a parameter.
\subsection{Michroarchitecture}
Fig ~\ref{fsmgatedlatency_ibd} shows the internal block diagram of the fsm\_latency module
\begin{figure}[!ht]
  \vspace{0.0cm}
  \centering
  \includegraphics[scale=0.4]{figures/fsm_gated_latency.png}
  \caption{fsm\_gated\_latency internal block diagram}
  \label{fsmgatedlatency_ibd}
\end{figure}
\subsection{Interfaces}
Table ~\ref{Tab:fsmgatedlatency_interface} shows the interface of the fsm\_resync module
\begin{table}[!ht]
\caption{fsm\_gated\_latency interface}
\begin{center}
 \begin{tabular}{|m{2cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{7cm}|} 
 \hline
 Port name & I/O & NBW & NI & NS & S & Description \\ [0.5ex] 
 \hline
 clk & C & & & & & Input clock with duty cycle of 50\% \\
 \hline
 rst\_async\_n & R & & & & & Reset, asynchronous, active low \\
 \hline
 i\_valid & In & 1 & & & & See \cite{SyncValid} \\
 \hline
 i\_sync & In & 1 & & & & See \cite{SyncValid} \\
 \hline
 i\_rst & In & 1 & & & & Internal reset for preparation for re synchronization, provided by upstream module \\
 \hline
 o\_valid & Out & 1 & & & & See \cite{SyncValid} \\
 \hline
 o\_sync & Out & 1 & & & & See \cite{SyncValid} \\
 \hline
 o\_rst & Out & 1 & & & & Internal reset for preparation for re synchronization, provided to downstream module and subordinate logic\\
 \hline
 o\_ctrl & Out & 1 & & & & Is a bus of wires that are the outputs of all the registers of the i\_valid shift register enabled by i\_valid.\\
 \hline
\end{tabular}
 \label{Tab:fsmgatedlatency_interface}
\end{center}
\end{table}
\subsection{Timing Diagrams}
Pending

\clearpage
\section{fsm\_pconv\_2\_3\_4\_to\_4}
\subsection{Functional description}
The finite state machine fsm\_pconv\_2\_3\_4\_to\_4, which is designed to drive a configurable parallelism converter of ratio 2:4(1:2), 3:4 or 4:4(1:1). It has essentially a little finite state machine with a module-3 counter. The maximum value reached by the counter is selected depending on the value on i\_pconv\_selection signal. The counter is disabled and enabled by the FSM. The count is sent out to be used by a data path parallelism converter. Furthermore, the count is used with the i\_valid input to generate the o\_valid signal for each parallelism conversion. \\
The o\_sync signal is connected to the i\_sync input when the 4-4 conversion is selected; otherwise it is connected to a registered version of i\_valid gated by the o\_valid signal.\\
The valid rate at the output is different than that at the input. For 2-4 the output valid rate is 1/2 of the input valid rate; for 3-4 it is 3/4; and for 4-4 it does not change.
\subsection{Michroarchitecture}
Fig ~\ref{fsmpconv234to4_ibd} shows the internal block diagram of the fsm\_pconv\_2\_3\_4\_to\_4 module
\begin{figure}[!ht]
  \vspace{0.0cm}
  \centering
  \includegraphics[scale=0.8]{figures/fsm_pconv_2_3_4_to_4.png}
  \caption{fsm\_pconv\_2\_3\_4\_to\_4 internal block diagram}
  \label{fsmpconv234to4_ibd}
\end{figure}
\subsection{Interfaces}
Table ~\ref{Tab:fsmpconv234to4_interface} shows the interface of the fsm\_pconv\_2\_3\_4\_to\_4 module
\begin{table}[!ht]
\caption{fsm\_pconv\_2\_3\_4\_to\_4 interface}
\begin{center}
 \begin{tabular}{|m{2cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{6cm}|} 
 \hline
 Port name & I/O & NBW & NI & NS & S & Description \\ [0.5ex] 
 \hline
 clk & C & & & & & Input clock with duty cycle of 50\% \\
 \hline
 rst\_async\_n & R & & & & & Reset, asynchronous, active low \\
 \hline
 i\_valid & In & 1 & & & & See \cite{SyncValid} \\
 \hline
 i\_sync & In & 1 & & & & See \cite{SyncValid} \\
 \hline
 i\_rst & In & 1 & & & & Internal reset for preparation for re synchronization, provided by upstream module \\
 \hline
 o\_valid & Out & 1 & & & & See \cite{SyncValid} \\
 \hline
 o\_sync & Out & 1 & & & & See \cite{SyncValid} \\
 \hline
 o\_rst & Out & 1 & & & & Internal reset for preparation for re synchronization, provided to downstream module and subordinate logic\\
 \hline
 o\_ctrl & Out & 2 & & & & The value of the internal count for driving the data path parallelism converter.\\
 \hline
 i\_pconv\_selection & In & 2 & & & & Configures which parallelism conversion is performed: 0 for 2-4; 1 for 3-4; 2 for 4-4\\
 \hline
\end{tabular}
 \label{Tab:fsmpconv234to4_interface}
\end{center}
\end{table}
\subsection{Timing Diagrams}
Pending


\clearpage
\section{fsm\_pconv\_4\_to\_2\_3\_4}
\subsection{Functional description}
The finite state machine fsm\_pconv\_4\_to\_2\_3\_4 is designed to drive a configurable parallelism converter of ratio 4:2(2:1), 4:3 or 4:4(1:1). It is basically conformed by two counters: the available bits counter and the control counter. The first one is in charge of computing the available bits with every valid clock depending on the selected parallelism conversion ratio. If there are enough bits to fill the output parallelism, the control counter increments. The maximum value of the counter is configured depending on the selected parallelism conversion ratio. In the control counter block, the o\_counter, o\_valid and o\_sync output signals are also calculated.\\
The fsm\_pconv\_4\_to\_2\_3\_4 does not have buffer capacity and is very strict regarding the i\_valid sequences it can receive, which depend on the parallelism conversion ratio.
\begin{itemize}
  \item 4-2: after every high i\_valid there must follow one low i\_valid
  \item 4-3: after every third high i\_valid (counting from the last high i\_sync) there must follow one low i\_valid.
  \item 4-4: no restrictions
\end{itemize}
The valid rate at the output is different than that at the input. For 4-2 the output valid rate is 2*the input valid rate; for 4-3 it is 4/3*the input valid rate; and for 4-4 it does not change.
\subsection{Michroarchitecture}
Fig ~\ref{fsmpconv4to234_ibd} shows the internal block diagram of the fsm\_pconv\_4\_to\_2\_3\_4 module
\begin{figure}[!ht]
  \vspace{0.0cm}
  \centering
  \includegraphics[scale=0.6]{figures/fsm_pconv_4_to_2_3_4.png}
  \caption{fsm\_pconv\_4\_to\_2\_3\_4 internal block diagram}
  \label{fsmpconv4to234_ibd}
\end{figure}
\subsection{Interfaces}
Table ~\ref{Tab:fsmpconv4to234_interface} shows the interface of the fsm\_pconv\_4\_to\_2\_3\_4 module
\begin{table}[!ht]
\caption{fsm\_pconv\_4\_to\_2\_3\_4 interface}
\begin{center}
 \begin{tabular}{|m{2.5cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{0.8cm}|m{6cm}|} 
 \hline
 Port name & I/O & NBW & NI & NS & S & Description \\ [0.5ex]  \hline
 clk & C & & & & & Input clock with duty cycle of 50\% \\ \hline
 rst\_async\_n & R & & & & & Reset, asynchronous, active low \\ \hline
 i\_valid & In & 1 & & & & See \cite{SyncValid} \\ \hline
 i\_sync & In & 1 & & & & See \cite{SyncValid} \\ \hline
 i\_rst & In & 1 & & & & Internal reset for preparation for re synchronization, provided by upstream module \\ \hline
 o\_valid & Out & 1 & & & & See \cite{SyncValid} \\ \hline
 o\_sync & Out & 1 & & & & See \cite{SyncValid} \\ \hline
 o\_rst & Out & 1 & & & & Internal reset for preparation for re synchronization, provided to downstream module and subordinate logic \\ \hline
 o\_ctrl & Out & 2 & & & & The value of the internal count for driving the data path parallelism converter.\\  \hline
 i\_pconv\_sel & In & 2 & & & & Configures which parallelism conversion is performed: 0 for 4-2; 1 for 4-3; 2 for 4-4 \\  \hline
\end{tabular}
 \label{Tab:fsmpconv4to234_interface}
\end{center}
\end{table}
\subsection{Timing Diagrams}
Pending


\clearpage
\bibliography{refs}   %% the .bib file without extension


\end{document}


