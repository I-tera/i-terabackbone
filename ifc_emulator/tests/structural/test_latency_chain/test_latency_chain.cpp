#define D_NCLOCKS 100
#define D_VALID_PPM 500000
#define D_SYNC_PPM   10000
#define D_RST_PPM     1000

#include "c_fsm_chain.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include <fstream>


int main (void)
{
  nlohmann::json config;
  string json_filename = "chain_config.json";
  std::ifstream i_file(json_filename);
  i_file >> config;
  
  c_fsm_chain chain;
  chain.initialize(config);

  //creation and connection of driving signals
  uint32_t i_valid = 0;
  uint32_t i_sync = 0;
  uint32_t i_soft_rst = 0;
  c_ifc_sync_valid master;
  master.initialize_master(&i_sync, &i_valid, &i_soft_rst);
  c_ifc_sync_valid slave_7;
  slave_7.initialize_slave();
  c_ifc_sync_valid slave_3_4;
  slave_3_4.initialize_slave();

  //connection
  chain["lat7"].connect("sv_slave", master);
  chain["lat7"].connect("sv_master", slave_7);
  chain["lat3"].connect("sv_slave", master);
  chain["lat4"].connect("sv_master", slave_3_4);

  //checks

  const uint32_t *ptr_lat_7_valid_3;
  const uint32_t *ptr_lat_7_valid_4;
  
  ptr_lat_7_valid_3 = chain.get_signal_pointer("lat7.valid_dl.array.3");
  ptr_lat_7_valid_4 = &(chain["lat7"]["valid_dl"]["array"][4]);

  for (uint32_t i = 1; i < 10; i++)
    {
      printf("\n###################CLOCK %d#####################", i);
      i_valid = i;
      i_sync = i;
      i_soft_rst = 0;
      chain.update_wires();
      chain.clock(0);
      chain.update_wires();
      chain.display();
      printf("\n ptr_lat_7_valid_3 = %d", *ptr_lat_7_valid_3);
      printf("\n ptr_lat_7_valid_4 = %d", *ptr_lat_7_valid_4);
      //*ptr_lat_7_valid_3 = 40;
    }
  i_soft_rst = 1;
  printf("\n###################soft reset#####################");
  chain.update_wires();
  chain.clock(0);
  chain.update_wires();
  chain.display();
  
  printf("\n\n TEST PASSED");
    
      
}
