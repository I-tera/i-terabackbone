#define D_NCLOCKS 100
#define D_VALID_PPM 500000
#define D_SYNC_PPM   10000
#define D_RST_PPM     1000

#include "c_fsm_latency_sync_valid.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>

void upd_clk_upd_print( std::map<std::string, c_fsm_base*> fsms)
{
  for (auto it = fsms.begin(); it != fsms.end(); ++it)
    it->second->update_wires();
  
  for (auto it = fsms.begin(); it != fsms.end(); ++it)
    it->second->clock(0);

  for (auto it = fsms.begin(); it != fsms.end(); ++it)
    it->second->update_wires();

  
  for (auto it = fsms.begin(); it != fsms.end(); ++it)
    {
      printf("\n\n==================================== %s", (it->first).c_str());
      it->second->display();
    }
}

int main (void)
{
  std::map<std::string, c_fsm_base*> fsms;

  
  c_fsm_latency_sync_valid my_lat_7;
  nlohmann::json config_my_lat_7;
  config_my_lat_7["latency"] = 7;
  config_my_lat_7["clk_unique"] = 0;
  my_lat_7.initialize(config_my_lat_7);

  c_fsm_latency_sync_valid my_lat_3;
  nlohmann::json config_my_lat_3;
  config_my_lat_3["latency"] = 3;
  config_my_lat_3["clk_unique"] = 0;
  my_lat_3.initialize(config_my_lat_3);
  
  c_fsm_latency_sync_valid my_lat_4;
  nlohmann::json config_my_lat_4;
  config_my_lat_4["latency"] = 4;
  config_my_lat_4["clk_unique"] = 0;
  my_lat_4.initialize(config_my_lat_4);

  fsms["fsm_lat_7"] = &my_lat_7;
  fsms["fsm_lat_3"] = &my_lat_3;
  fsms["fsm_lat_4"] = &my_lat_4;

  //creation and connection of driving signals
  uint32_t i_valid = 0;
  uint32_t i_sync = 0;
  uint32_t i_soft_rst = 0;
  c_ifc_sync_valid master;
  master.initialize_master(&i_sync, &i_valid, &i_soft_rst);
  c_ifc_sync_valid slave_7;
  slave_7.initialize_slave();
  c_ifc_sync_valid slave_3_4;
  slave_3_4.initialize_slave();

  //connection
  my_lat_7.connect("sv_slave", master);
  my_lat_7.connect("sv_master", slave_7);
  my_lat_3.connect("sv_slave", master);
  my_lat_3.connect("sv_master", my_lat_4["sv_slave"]);
  my_lat_4.connect("sv_master", slave_3_4);

  //checks
  my_lat_7.check_open();
  my_lat_4.check_open();
  my_lat_3.check_open();

  const uint32_t *ptr_lat_7_valid_3;
  const uint32_t *ptr_lat_7_valid_4;
  
  ptr_lat_7_valid_3 = my_lat_7.get_signal_pointer("valid_dl.array.3");
  ptr_lat_7_valid_4 = &(my_lat_7["valid_dl"]["array"][4]);

  for (uint32_t i = 1; i < 10; i++)
    {
      printf("\n###################CLOCK %d#####################", i);
      i_valid = i;
      i_sync = i;
      i_soft_rst = 0;
      upd_clk_upd_print(fsms);
      printf("\n ptr_lat_7_valid_3 = %d", *ptr_lat_7_valid_3);
      printf("\n ptr_lat_7_valid_4 = %d", *ptr_lat_7_valid_4);
      //*ptr_lat_7_valid_3 = 40;
    }
  i_soft_rst = 1;
  printf("\n###################soft reset#####################");
  upd_clk_upd_print(fsms);
  
  printf("\n\n TEST PASSED");
    
      
}
