import os

print("Recompiling ...")
os.system("g++ test_latency.cpp -g -O3 -march=native -std=c++11" +
          " -I ../ " +
          " -I ../../../../libs/json/" +
          " -I ../../../src/fsms" +
          " -I ../../../src/ifaces" +
          " -I ../../../src/structural")
print("     done")
print("Running valgrind ...")
os.system("valgrind -v --leak-check=yes  ./a.out");
os.system("rm *.log");
os.system("rm a.out");
