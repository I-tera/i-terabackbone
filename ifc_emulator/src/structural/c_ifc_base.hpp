#ifndef C_IFC_BASE_HPP
#define C_IFC_BASE_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include <map>
#include "c_ifc_signal.hpp"

using namespace std;



enum e_ifc_directions{master, slave, undefined};
class c_ifc_base
{
  //variables
private:
  std::map<std::string, c_ifc_signal> master_i_signals;
  std::map<std::string, c_ifc_signal> master_o_signals;
protected:
  e_ifc_directions direction = undefined;
  std::string unique_id = "";

  //methods
protected:
  void set_direction(e_ifc_directions i_direction){assert(i_direction == undefined); direction = i_direction;}
  void add_master_i_signal(const std::string &signal_name, uint32_t length) {master_i_signals[signal_name].create_input(length);}
  void add_master_o_signal(const std::string &signal_name, uint32_t* source, uint32_t length) {master_o_signals[signal_name].create_output(source, length);}
public:
  void display() 
  {
    printf("\nINTERFACE INFORMATION");
    printf("\n unique_id: %s, direction: ", unique_id.c_str());
    if (direction == master) printf("master");
    if (direction == slave) printf("slave");
    if (direction == undefined) printf("undefined");
    printf("\n master_i_signals:");
    for (auto it = master_i_signals.begin(); it != master_i_signals.end(); ++it)
      {
        printf("\n     %s", it->first.c_str());
        it->second.display();
      }
    printf("\n master_o_signals:");
    for (auto it = master_o_signals.begin(); it != master_o_signals.end(); ++it)
      {
        printf("\n     %s", it->first.c_str());
        it->second.display();
      }
    fflush(stdout);
  }
  bool is_master() const {return direction == master;}
  bool is_slave() const {return direction == slave;}
  const std::string get_name() const {return unique_id;}
  e_ifc_directions get_direction() const {return direction;}
  void check_open() const;
  const uint32_t* get_signal_pointer(std::string signal_name, uint32_t index = 0)
  {
    assert((master_i_signals.find(signal_name) != master_i_signals.end())||
           (master_o_signals.find(signal_name) != master_o_signals.end()));
    if (master_i_signals.find(signal_name) != master_i_signals.end())
      {
        return &(master_i_signals[signal_name][index]);
      }
    else //(master_o_signals.find(signal_name) != master_o_signals.end())
      {
        return &(master_o_signals[signal_name][index]);
      }
  }
  
  c_ifc_signal& operator[](std::string signal)
  {
    assert((master_i_signals.find(signal) != master_i_signals.end()) ||
           (master_o_signals.find(signal) != master_o_signals.end()));
    if (master_i_signals.find(signal) != master_i_signals.end())
      {
        return (master_i_signals[signal]);
      }
    else //(master_o_signals.find(signal) != master_o_signals.end())
      {
        return (master_o_signals[signal]);
      }
  }
  void connect(c_ifc_base& the_other);

};

void c_ifc_base::connect(c_ifc_base& the_other)
{
  //check on equal interface unique ID
  assert(unique_id.compare(the_other.unique_id) == 0);
  //check on defined and compatible directions
  assert(the_other.direction != undefined);
  assert(direction != the_other.direction);

  //connection of all signals
  if (direction == master)
    {
      for (auto it = master_i_signals.begin();
           it != master_i_signals.end(); ++it)
        {
          it->second.connect(the_other.master_i_signals[it->first]);
        }
      for (auto it = the_other.master_o_signals.begin();
           it != the_other.master_o_signals.end(); ++it)
        {
          it->second.connect(master_o_signals[it->first]);
        }
    }
  else
    {
      for (auto it = master_o_signals.begin();
           it != master_o_signals.end(); ++it)
        {
          it->second.connect(the_other.master_o_signals[it->first]);
        }
      for (auto it = the_other.master_i_signals.begin();
           it != the_other.master_i_signals.end(); ++it)
        {
          it->second.connect(master_i_signals[it->first]);
        }
    }
}

void c_ifc_base::check_open() const
{
  for (auto it = master_i_signals.begin(); it != master_i_signals.end(); ++it)
    {
      if (it->second.is_open())
        {
          throw std::runtime_error(("\n c_ifc_base::check_open ERROR: unconnected port " +
                                    unique_id + "." + it->first).c_str());
        }
    }
}
  
    

#endif
