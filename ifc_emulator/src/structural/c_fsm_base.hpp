#ifndef C_FSM_BASE_HPP
#define C_FSM_BASE_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_ifc_base.hpp"
#include "json.hpp"

using namespace std;

struct s_comb_rel{
  std::string source;
  std::string sink;
};

class c_fsm_base
{
private:
  std::map<std::string, c_ifc_base*> master_interfaces; ///< map of master interfaces, with their names
  std::map<std::string, c_ifc_base*> slave_interfaces;  ///< map of slave interfaces, with their names 
  std::vector<s_comb_rel>            comb_path;         ///< vector listing all combinational paths in the form src_interface, sink_interface
  std::map<std::string, uint32_t>     clocks;           ///< map with clock names and their integer aliases.
public:
  ////////////////////////////////////////////////////////////////////////////
  /// Function that returns true if the clock index corresponds to the given name
  /// \param name: the clock name to be checked
  /// \param idx: the clock index to be checked
  /// \return: true if the clock name matches the clock index, false otherwise
  ////////////////////////////////////////////////////////////////////////////
  bool is_clock(const std::string name, uint32_t idx)
  {
    //check that the provided clock name exists
    assert(clocks.find(name) != clocks.end());
    return clocks[name] == idx;
  }

  ////////////////////////////////////////////////////////////////////////////
  /// Function that returns the vector of combinational paths
  /// \return: the combinational paths
  ////////////////////////////////////////////////////////////////////////////
  const std::vector<s_comb_rel> get_comb_path() const
  {
    return comb_path;
  }

  ////////////////////////////////////////////////////////////////////////////
  /// Destructor
  ////////////////////////////////////////////////////////////////////////////
  virtual ~c_fsm_base(){}
protected:
  ////////////////////////////////////////////////////////////////////////////
  /// Function that adds a combinational path to the vector
  /// \param source: a string with the name of the source interface
  /// \param sink: a string with the name of the sync interface
  ////////////////////////////////////////////////////////////////////////////
  void append_comb_path(const std::string &source, const std::string &sink)
  {
    //check that the source interface exists
    assert((master_interfaces.find(source) != master_interfaces.end()) ||
           (slave_interfaces.find(source) != slave_interfaces.end()));
    //check that the sink interface exists
    assert((master_interfaces.find(sink) != master_interfaces.end()) ||
           (slave_interfaces.find(sink) != slave_interfaces.end()));
    //append the new combinational path
    s_comb_rel a;
    a.source = source;
    a.sink = sink;
    comb_path.push_back(a);
  }

  ////////////////////////////////////////////////////////////////////////////
  /// Function that appends a new master or slave interface
  /// \param name: the name of the new interface
  /// \param sink: a string with the name of the sync interface
  ////////////////////////////////////////////////////////////////////////////
  void append_interface(const std::string &name, c_ifc_base* ifc)
  {
    //check that the interface name is not already in use
    assert((master_interfaces.find(name) == master_interfaces.end()) &&
           (slave_interfaces.find(name) == slave_interfaces.end()));
    //append the interface to the master or the slave maps depending on its direction
    if (ifc->is_master())
      master_interfaces[name] = ifc;
    else if (ifc->is_slave())
      slave_interfaces[name] = ifc;
    else
      throw std::runtime_error(("\n c_fsm_base::append_interface ERROR, trying to append an undefined interface " + name).c_str());
  }
  
  ////////////////////////////////////////////////////////////////////////////
  /// Function that sets a clock name with its corresponding index
  /// \param clock_name: a string the name of the new clock
  /// \param clock_index: the index of the new clock (uint32_t)
  ////////////////////////////////////////////////////////////////////////////
  void set_clock_number(std::string clock_name, uint32_t clock_index){
    clocks[clock_name] = clock_index;}

public:
  ////////////////////////////////////////////////////////////////////////////
  /// Virtual function that initializes the FSM
  /// \param config: a json object
  ////////////////////////////////////////////////////////////////////////////
  virtual void initialize(nlohmann::json config) = 0;

  ////////////////////////////////////////////////////////////////////////////
  /// Virtual function that performs all the combinational updates. It shall
  /// model each and every combinational component of the FSM.
  /// Basic rules:
  ///     It shall NOT update any variables representing registers
  ///     Its inputs (variables used to make calculations) shall only be
  ///         variables that represent internal registers or FSM input wires.
  ///         The consequence (and a suggested sanity check) is that it makes
  ///         no difference to run it one or multiple times in a row as long
  ///         as no clock() was called and no input was changed
  ////////////////////////////////////////////////////////////////////////////
  virtual void update_wires() = 0;

  
  ////////////////////////////////////////////////////////////////////////////
  /// Virtual function that performs all the sequential updates, this is,
  /// the update of the variables that represent registers.
  /// Basic rules:
  ///     It shall update each variable representing registers linked to the
  ///         given clock_index
  ///     Its inputs (variables used to make calculations) shall only be
  ///         variables that represent internal wires or FSM input wires.
  ///         The consequence (and a suggested sanity check) is that it makes
  ///         no difference to run it one or multiple times in a row as long
  ///         as no update_wires() was called and no input was changed
  ///  \param clock_index: the index of the clock that drives the update
  ////////////////////////////////////////////////////////////////////////////
  virtual void clock(uint32_t clock_index) = 0;

  ////////////////////////////////////////////////////////////////////////////
  /// Virtual function that displays the internal state of the FSM, for
  /// debug purposes
  ////////////////////////////////////////////////////////////////////////////
  virtual void display() = 0;

  
  ////////////////////////////////////////////////////////////////////////////
  /// Function that connects a local interface to a remote interface
  /// \param local_ifc_name: a string with the name of the interface belonging
  ///     to the FSM
  /// \param remote_ifc: a pointer to a remote interface
  ////////////////////////////////////////////////////////////////////////////
  void connect(std::string local_ifc_name, c_ifc_base &remote_ifc){
    if (master_interfaces.find(local_ifc_name) != master_interfaces.end())
      {
        master_interfaces[local_ifc_name]->connect(remote_ifc);
      }
    else if (slave_interfaces.find(local_ifc_name) != slave_interfaces.end())
      {
        slave_interfaces[local_ifc_name]->connect(remote_ifc);
      }
    else
      {
        throw std::runtime_error(("\n c_fsm_base::connect ERROR, non existing local interface " + local_ifc_name).c_str());
      }
  }
  
  ////////////////////////////////////////////////////////////////////////////
  /// Function that checks that each inputs of each interface (master or slave)
  /// is connected.
  ////////////////////////////////////////////////////////////////////////////
  void check_open() const {
    for (auto it = slave_interfaces.begin(); it != slave_interfaces.end(); ++it) it->second->check_open();
    for (auto it = master_interfaces.begin(); it != master_interfaces.end(); ++it) it->second->check_open();
  }
  const uint32_t* get_signal_pointer(std::string path);
private:
  c_ifc_base& operator[](std::string ifc)
  {
    if (master_interfaces.find(ifc) != master_interfaces.end())
      {
        return *(master_interfaces[ifc]);
      }
    else if (slave_interfaces.find(ifc) != slave_interfaces.end())
      {
        return *(slave_interfaces[ifc]);
      }
    else
      {
        throw std::runtime_error(("\n c_fsm_base::operator[] ERROR: non existing interface " + ifc).c_str());
      }
  }
};

const  uint32_t* c_fsm_base::get_signal_pointer(std::string path)
{
  std::string delimiter = ".";
  size_t pos = 0;
  std::string ifc_name;
  std::string signal_name;
  uint32_t index;
  if ((pos = path.find(delimiter)) != std::string::npos)
    {
      ifc_name = path.substr(0,pos);
      path.erase(0, pos+delimiter.length());
    }
  else
    {
      throw std::runtime_error(("\n c_fsm_base::get_signal_pointer ERROR: no . in string " + path).c_str());
    }
  if ((pos = path.find(delimiter)) != std::string::npos)
    {
      signal_name = path.substr(0,pos);
      path.erase(0, pos+delimiter.length());
      index = std::stoi(path);
    }
  else
    {
      signal_name = path;
      index = 0;
    }
    
  if (master_interfaces.find(ifc_name) != master_interfaces.end())
    {
      return master_interfaces[ifc_name]->get_signal_pointer(signal_name, index);
    }
  else if (slave_interfaces.find(ifc_name) != slave_interfaces.end())
    {
      return slave_interfaces[ifc_name]->get_signal_pointer(signal_name, index);
    }
  else
    {
      throw std::runtime_error(("\n c_fsm_base::get_signal_pointer ERROR: invalid interface name " + path).c_str());
    }
    
}

#endif
