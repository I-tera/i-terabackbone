#ifndef C_FSM_CHAIN_HPP
#define C_FSM_CHAIN_HPP

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_fsm_base.hpp"
#include "json.hpp"

#include "c_fsm_factory.hpp"

using namespace std;

  
class c_fsm_chain
  {
  private:
    struct c_static_path{
      std::string fsm_name = "";
      std::string static_name = "";
    };
    struct c_static_signal
    {
      std::vector<c_static_path> connections;
      uint32_t *value = nullptr;
      uint32_t size = 0;
      void display()
      {
        printf("\n        size: %d",size);
        printf("\n        value: [%d", value[0]);
        for (uint32_t i = 1; i < size; i++)
          printf(", %d", value[i]);
        printf("]");
        for (auto it = connections.begin(); it != connections.end(); ++it)
        {
          printf("\n        connections: %s.%s", it->fsm_name.c_str(), it->static_name.c_str());
        }
      }
               
          
    };
    c_fsm_factory my_factory;
    std::map<std::string, c_fsm_base*> fsms;
    std::map<std::string, c_static_signal> statics;
    std::vector<c_fsm_base*> upd_wires_call_order;
    void split_in_dot(std::string full, std::string &first, std::string &last)
    {
      std::string delimiter = ".";
      size_t pos = 0;
      if ((pos = full.find(delimiter)) != std::string::npos)
        {
          first = full.substr(0,pos);
          full.erase(0,pos+delimiter.length());
          last = full;
        }
    }
  public:
    c_fsm_chain(){}
    void free_resources()
    { 
      for (auto it = statics.begin(); it != statics.end(); ++it)
        {
          if (it->second.value != nullptr)
            delete [] it->second.value;
          it->second.value = nullptr;
        }
    }
    ~c_fsm_chain(){free_resources();}
    void initialize(nlohmann::json config)
    {
      //making instances
      for (uint32_t i = 0; i < config["fsms"]["instances"].size(); i++)
        {
          fsms[config["fsms"]["instances"][i]["name"]] = my_factory.make_fsm(config["fsms"]["instances"][i]);
        }

      //connecting instances
      for (uint32_t i = 0; i < config["fsms"]["connections"].size(); i++)
        {
          std::string master_fsm_name;
          std::string master_ifc_name;
          split_in_dot(config["fsms"]["connections"][i]["master"], master_fsm_name, master_ifc_name);
          std::string slave_fsm_name;
          std::string slave_ifc_name;
          split_in_dot(config["fsms"]["connections"][i]["slave"], slave_fsm_name, slave_ifc_name);

          printf("\n\n connecting %s.%s to %s.%s", master_fsm_name.c_str(), master_ifc_name.c_str(), slave_fsm_name.c_str(), slave_ifc_name.c_str());
          fsms[master_fsm_name]->connect(master_ifc_name, (*fsms[slave_fsm_name])[slave_ifc_name]);
        }
      for (auto it = fsms.begin(); it != fsms.end(); ++it)
        it->second->check_open();

      //calculating update_wires order
      for (uint32_t i = 0; i < 10; i++)
        {
          for (auto it = fsms.begin(); it != fsms.end(); ++it)
            upd_wires_call_order.push_back(it->second);
        }

      //creating statics
      for (uint32_t i = 0; i < config["fsms"]["statics"].size(); i++)
        {
          c_static_signal aux4load;
          aux4load.size = config["fsms"]["statics"][i]["size"].get<uint32_t>();
          aux4load.value = new uint32_t[aux4load.size];
          for (uint32_t k = 0; k < aux4load.size; k++)
            aux4load.value[k] = config["fsms"]["statics"][i]["default"][k].get<uint32_t>();
          for (uint32_t k = 0; k < config["fsms"]["statics"][i]["connections"].size(); k++)
            {
              c_static_path connected_static;
              split_in_dot(config["fsms"]["statics"][i]["connections"][k], connected_static.fsm_name, connected_static.static_name);
              aux4load.connections.push_back(connected_static);
            }
          statics[config["fsms"]["statics"][i]["name"].get<std::string>()] = aux4load;
        }
      
    }
    void update_wires(){
      for (auto it = upd_wires_call_order.begin(); it != upd_wires_call_order.end(); ++it)
        (**(it)).update_wires();
    }
    void clock(uint32_t clock_index){
      for (auto it = fsms.begin(); it != fsms.end(); ++it)
        it->second->clock(clock_index);}
    c_fsm_base& operator[](std::string fsm_name)
    {
      assert(fsms.find(fsm_name) != fsms.end());
      return *fsms[fsm_name];
    }
    const uint32_t* get_signal_pointer(std::string path)
    {
      std::string delimiter = ".";
      size_t pos = 0;
      std::string fsm_name;
      if ((pos = path.find(delimiter)) != std::string::npos)
        {
          fsm_name = path.substr(0,pos);
          path.erase(0, pos+delimiter.length());
          return fsms[fsm_name]->get_signal_pointer(path);
        }
      else
        {
          throw std::runtime_error(("\n c_fsm_chain::get_signal_pointer ERROR: no . in string " + path).c_str());
        }
    }
    void display()
    {
      for (auto it = fsms.begin(); it != fsms.end(); ++it)
        {
          printf("\n\n FSM DETAILS: %s", (it->first).c_str());
          it->second->display();
        }
      printf("\n STATICS DETAILS");
      for (auto it = statics.begin(); it != statics.end(); ++it)
        {
          printf("\n    %s", (it->first).c_str());
          it->second.display();
        }
    }
};

#endif
