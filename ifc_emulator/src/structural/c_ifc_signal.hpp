#ifndef C_IFC_SIGNAL_HPP
#define C_IFC_SIGNAL_HPP

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include <map>

using namespace std;

class c_ifc_base;

class c_ifc_signal
{
  friend class c_ifc_base;
private:
  uint32_t nof_elements = 1;
  uint32_t *signal = nullptr;
public:
  c_ifc_signal(){nof_elements = 0; signal = nullptr;}
  ~c_ifc_signal(){destroy();}
  const inline uint32_t &operator[](uint32_t index)  {assert (index < nof_elements);return signal[index];}
  void display(){
    printf("\n SIGNAL INFORMATION");
    printf("\n %4u elements: ", nof_elements);
    if (signal == nullptr)
      {
        printf(" nullptr");
      }
    else
      {
        for (uint32_t i = 0; i < nof_elements; i++)
          printf(" %u", signal[i]);
      }
    fflush(stdout);
  }
private:
  void create_output(uint32_t* source, uint32_t i_nof_elements = 1)  {destroy();nof_elements = i_nof_elements;signal = source;}
  void create_input(uint32_t i_nof_elements = 1)  {destroy();nof_elements = i_nof_elements;signal = nullptr;}
  void destroy()  {signal = nullptr;nof_elements = 0;}
  void connect(c_ifc_signal &original)  {assert(nof_elements == original.nof_elements);signal = original.signal;}
  bool is_open() const   {return signal==nullptr;}
};


#endif
