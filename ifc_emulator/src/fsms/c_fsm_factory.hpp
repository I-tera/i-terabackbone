#ifndef C_FSM_FACTORY_HPP
#define C_FSM_FACTORY_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_fsm_base.hpp"
#include "json.hpp"

#include "c_fsm_latency_sync_valid.hpp"
#include "c_fsm_bypass_sync_valid.hpp"


using namespace std;

class c_fsm_factory
  {
  private:
    std::vector<c_fsm_base*> fsm_list;
  public:
    c_fsm_factory(){}
    ~c_fsm_factory(){free_resources();}
    void free_resources()
    {
      for (uint32_t d = 0; d < fsm_list.size(); d++)
        {
          if (fsm_list[d] != nullptr)
            delete fsm_list[d];
          fsm_list[d] = nullptr;
        }
    } 
    c_fsm_base* make_fsm(nlohmann::json config)
    {
      std::string type;
      try{type = config["type"].get<string>();}
      catch (...){throw std::runtime_error("c_fsm_factory ERROR: problem with field type\n");}

      fsm_list.push_back(nullptr);
      if      (type.compare("latency_sync_valid") == 0) {fsm_list.back() = new c_fsm_latency_sync_valid;}
      else if (type.compare("bypass_sync_valid")  == 0) {fsm_list.back() = new c_fsm_bypass_sync_valid;}
      else {throw std::runtime_error("c_fsm_factory ERROR: not supported fsm type");}

      fsm_list.back()->initialize(config);
      return fsm_list.back();
    } 
};

#endif
