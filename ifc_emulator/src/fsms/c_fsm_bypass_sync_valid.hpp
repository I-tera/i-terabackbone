#ifndef C_FSM_BYPASS_SYNC_VALID_HPP
#define C_FSM_BYPASS_SYNC_VALID_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_fsm_base.hpp"
#include "c_ifc_sync_valid.hpp"
#include "c_ifc_array.hpp"
#include "json.hpp"

using namespace std;

class c_fsm_bypass_sync_valid: public virtual c_fsm_base
  {
  private:
    //interfaces
    c_ifc_sync_valid slave;
    c_ifc_sync_valid master;
    //wires
    uint32_t valid_w;
    uint32_t sync_w;
    uint32_t soft_rst_w;
  public:
    c_fsm_bypass_sync_valid()
    {
      soft_rst_w = 0;
      valid_w = 0;
      sync_w = 0;
    }
    ~c_fsm_bypass_sync_valid()
    {
    }
    void display()
    {
      printf("\n FSM BYPASS (ifc sync_valid) DISPLAY");
      //slave.display();
      //master.display();
      
      printf("\n valid    (%d) ->", slave["valid"][0]);
      printf(" -> %d", master["valid"][0]);
      printf("\n sync     (%d) ->", slave["sync"][0]);
      printf(" -> %d", master["sync"][0]);
      printf("\n soft_rst (%d) ->", slave["soft_rst"][0]);
      printf(" -> %d", master["soft_rst"][0]);
      fflush(stdout);
    }
    void initialize(nlohmann::json config)
    {
      soft_rst_w = 0;
      valid_w = 0;
      sync_w = 0;
      
      
      master.initialize_master(&sync_w, &valid_w, &soft_rst_w);
      this->append_interface("sv_master", &master);
      
      slave.initialize_slave();
      this->append_interface("sv_slave", &slave);
            
      this->append_comb_path("sv_slave", "sv_master");
    }
    void update_wires()
    {
      soft_rst_w = slave["soft_rst"][0];
      valid_w = slave["valid"][0];
      sync_w = slave["sync"][0];
    }
    void clock(uint32_t clock_index)
    {
    }
    void async_reset(uint32_t clock_index)
    {
    }
};

#endif
