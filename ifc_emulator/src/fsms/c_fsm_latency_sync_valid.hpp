#ifndef C_FSM_LATENCY_SYNC_VALID_HPP
#define C_FSM_LATENCY_SYNC_VALID_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_fsm_base.hpp"
#include "c_ifc_sync_valid.hpp"
#include "c_ifc_array.hpp"
#include "json.hpp"

using namespace std;

class c_fsm_latency_sync_valid: public virtual c_fsm_base
  {
  private:
    //interfaces
    c_ifc_sync_valid slave;
    c_ifc_sync_valid master;
    c_ifc_array valid_delayline;
    c_ifc_array sync_delayline;
    //wires
    uint32_t *vdl_w;
    uint32_t *sdl_w;
    uint32_t soft_rst_w;
    //registers for "unique" clock
    uint32_t *vdl_r;
    uint32_t *sdl_r;
    //configuration variables
    uint32_t latency;
  public:
    c_fsm_latency_sync_valid()
    {
      latency = 0;
      soft_rst_w = 0;
      vdl_w = nullptr;
      sdl_w = nullptr;
      vdl_r = nullptr;
      sdl_r = nullptr;
    }
    ~c_fsm_latency_sync_valid()
    {
      if (vdl_w != nullptr) {delete[] vdl_w;} vdl_w = nullptr;
      if (sdl_w != nullptr) {delete[] sdl_w;} sdl_w = nullptr;
      if (vdl_r != nullptr) {delete[] vdl_r;} vdl_r = nullptr;
      if (sdl_r != nullptr) {delete[] sdl_r;} sdl_r = nullptr;
    }
    void display()
    {
      printf("\n FSM LATENCY (ifc sync_valid) DISPLAY");
      //slave.display();
      //master.display();
      
      printf("\n valid    (%d) ->", slave["valid"][0]);
      for (uint32_t i = 0; i < latency; i++)
        printf("%d[%d], ", vdl_w[i], vdl_r[i]);
      printf(" -> %d", master["valid"][0]);
      printf("\n sync     (%d) ->", slave["sync"][0]);
      for (uint32_t i = 0; i < latency; i++)
        printf("%d[%d], ", sdl_w[i], sdl_r[i]);
      printf(" -> %d", master["sync"][0]);
      printf("\n soft_rst (%d) ->", slave["soft_rst"][0]);
      for (uint32_t i = 0; i < latency; i++)
        printf("------");
      printf(" -> %d", master["soft_rst"][0]);
      fflush(stdout);
    }
    void initialize(nlohmann::json config)
    {
      latency = config["latency"].get<uint32_t>();
      vdl_r = new uint32_t[latency];
      sdl_r = new uint32_t[latency];
      vdl_w = new uint32_t[latency];
      sdl_w = new uint32_t[latency];
      soft_rst_w = 0;
      for (uint32_t i = 0; i < latency; i ++)
        {
          vdl_r[i] = 0;
          sdl_r[i] = 0;
          vdl_w[i] = 0;
          sdl_w[i] = 0;
        }
      
      valid_delayline.initialize_master(vdl_r, latency);
      this->append_interface("valid_dl", &valid_delayline);
      
      sync_delayline.initialize_master(sdl_r, latency);
      this->append_interface("sync_dl", &sync_delayline);
      
      master.initialize_master(&sdl_r[latency-1], &vdl_r[latency-1], &soft_rst_w);
      this->append_interface("sv_master", &master);
      
      slave.initialize_slave();
      this->append_interface("sv_slave", &slave);
            
      this->set_clock_number("unique", config["clk_unique"].get<int32_t>());
      this->append_comb_path("sv_slave", "sv_master");
    }
    void update_wires()
    {
      soft_rst_w = slave["soft_rst"][0];
      for (int32_t i = latency-1; i > 0; i--)
        {
          vdl_w[i] = vdl_r[i-1];
          sdl_w[i] = sdl_r[i-1];
        }
      vdl_w[0] = slave["valid"][0];
      sdl_w[0] = slave["sync"][0];
    }
    void clock(uint32_t clock_index)
    {
      if (this->is_clock("unique", clock_index))
        {
          if (soft_rst_w == 1)
            {
              vdl_r[latency-1] = vdl_w[latency-1];
              sdl_r[latency-1] = sdl_w[latency-1];
              for (uint32_t i = 0; i < latency-1; i ++)
                {
                  vdl_r[i] = 0;
                  sdl_r[i] = 0;
                }
            }
          else
            {
              for (uint32_t i = 0; i < latency; i ++)
                {
                  vdl_r[i] = vdl_w[i];
                  sdl_r[i] = sdl_w[i];
                }
            }
        }
    }
    void async_reset(uint32_t clock_index)
    {
      if (this->is_clock("unique", clock_index))
        {
          for (uint32_t i = 0; i < latency; i ++)
            {
              vdl_r[i] = 0;
              sdl_r[i] = 0;
            }
        }
    }
};

#endif
