#ifndef C_IFC_SYNC_VALID_HPP
#define C_IFC_SYNC_VALID_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_ifc_base.hpp"

using namespace std;

class c_ifc_sync_valid: public virtual c_ifc_base
{
public:
  c_ifc_sync_valid()
  {
    this->unique_id = "sync_valid";
    this->direction = undefined;
  }
  void initialize_master(uint32_t *src_sync, uint32_t* src_valid, uint32_t* src_rst)
  {
    this->add_master_o_signal("sync", src_sync, 1);
    this->add_master_o_signal("valid", src_valid, 1);
    this->add_master_o_signal("soft_rst", src_rst, 1);
    this->direction = master;
  }
  void initialize_slave()
  {
    this->add_master_o_signal("sync", nullptr, 1);
    this->add_master_o_signal("valid", nullptr, 1);
    this->add_master_o_signal("soft_rst", nullptr, 1);
    this->direction = slave;
  }
    
};

#endif
