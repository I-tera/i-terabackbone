#ifndef C_IFC_ARRAY_HPP
#define C_IFC_ARRAY_HPP


#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h> 
#include <cmath>
#include "c_ifc_base.hpp"

using namespace std;

class c_ifc_array: public virtual c_ifc_base
{
public:
  c_ifc_array()
  {
    this->unique_id = "array";
    this->direction = undefined;
  }
  void initialize_master(uint32_t *src, uint32_t nof_elements)
  {
    this->add_master_o_signal("array", src, nof_elements);
    this->direction = master;
  }
  void initialize_slave(uint32_t nof_elements)
  {
    this->add_master_o_signal("array", nullptr, nof_elements);
    this->direction = slave;
  }
};

#endif
