#ifndef C_OPERATOR_GF_2eN_POLY_T_H
#define C_OPERATOR_GF_2eN_POLY_T_H

#include "c_vector_logger_t.hpp"
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h>
#include "C_GF_2eN_T.hpp"



template <class T_E, class T_BE, uint32_t T_P>
class C_operator_GF_2eN_poly_T
{
private:
  T_BE     irr_poly;
  uint32_t GF_order;
public:
  //constructor and destructor
  C_operator_GF_2eN_poly_T(){}
  C_operator_GF_2eN_poly_T(uint32_t i_GF_order){initialize(i_GF_order);}
  ~C_operator_GF_2eN_poly_T(){}
  void initialize(uint32_t i_GF_order);
  void Destroy(){}
  //GF2eN elementary operations
  inline T_E  GF2eN_ApB(   T_E  A, T_E  B) {return A ^B;}
  inline T_E  GF2eN_ApB(   T_E  A, T_BE B) {return A ^B;}
  inline T_E  GF2eN_ApB(   T_BE A, T_E  B) {return A ^B;}
  inline T_BE GF2eN_ApB(   T_BE A, T_BE B) {return A ^B;}
  inline T_E  GF2eN_AmB(   T_E  A, T_E  B) {return A ^B;}
  inline T_E  GF2eN_AmB(   T_E  A, T_BE B) {return A ^B;}
  inline T_E  GF2eN_AmB(   T_BE A, T_E  B) {return A ^B;}
  inline T_BE GF2eN_AmB(   T_BE A, T_BE B) {return A ^B;}
  inline T_E  GF2eN_invert(   T_E  A);
  inline T_BE GF2eN_invert(   T_BE A);
  inline T_E  GF2eN_multiply( T_E  A, T_E  B);
  inline T_E  GF2eN_multiply( T_E  A, T_BE B);
  inline T_BE GF2eN_multiply( T_BE A, T_BE B);
  inline T_E  GF2eN_divide(   T_E  A, T_E  B){return GF2eN_multiply(A, GF2eN_invert(B));}
  inline T_E  GF2eN_divide(   T_E  A, T_BE B){return GF2eN_multiply(A, GF2eN_invert(B));}
  inline T_BE GF2eN_divide(   T_BE A, T_BE B){return GF2eN_multiply(A, GF2eN_invert(B));}
  //GF2eN polynomial operations
  inline void ApB(T_E *A, uint32_t A_size, T_E *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void AmB(T_E *A, uint32_t A_size, T_E *B, uint32_t B_size, T_E *R, uint32_t R_size) {ApB(A, A_size, B, B_size, R, R_size);}
  inline void AtB(T_E  *A, uint32_t A_size,  T_E *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void AtB(T_BE *A, uint32_t A_size,  T_E *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void AtB(T_E  *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void AtB(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size);
  inline void ArB(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void ArB(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size);
  inline void ArB(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void ArB_GF2(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void ArB_GF2(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size);
  inline void Ap_ArB(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void Ap_ArB_GF2(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size);
  inline void Ap_ArB_GF2(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size);
  //GF2eN mixed operations
  inline void eval_A_B(T_E *A, uint32_t A_size, T_BE B, T_E &R);
  inline void eval_A_B_GF2(T_E *A, uint32_t A_size, T_BE B, T_E &R, bool msb_first = false);
  //comparison and characterization
  UME::SIMD::SIMDVecMask<T_P> comp_AB(T_E *A, uint32_t A_size, T_E *B, uint32_t B_size);
  UME::SIMD::SIMDVecMask<T_P> is_zero(T_E *A, uint32_t A_size);
  T_E get_order(T_E *A, uint32_t A_size){T_E order = 0; for (int32_t i=0; i<A_size; i++){order[!(A[i] == (T_BE)0)] = i;} return order;}
  T_BE get_order(T_BE *A, uint32_t A_size){T_BE order = 0; for (int32_t i=0; i<A_size; i++){if (A[i] != 0){order = i;}} return order;}
  //display
  void display(T_E *A, uint32_t A_size, bool display_indet = false);
  void display(T_BE *A, uint32_t A_size, bool display_indet = false);
};


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::initialize(uint32_t i_GF_order)
{
  GF_order = i_GF_order;
  switch (GF_order)
    {
    case 2:
      irr_poly = 0x0007;
      break;
    case 3:
      irr_poly = 0x000B;
      break;
    case 4:
      irr_poly = 0x0013;
      break;
    case 5:
      irr_poly = 0x0025;
      break;
    case 6:
      irr_poly = 0x0043;
      break;
    case 7:
      irr_poly = 0x0089;
      break;
    case 8:
      irr_poly = 0x011D;
      break;
    case 9:
      irr_poly = 0x0211;
      break;
    case 10:
      irr_poly = 0x0409;
      break;
    case 11:
      irr_poly = 0x0805;
      break;
    case 12:
      irr_poly = 0x1053;
      break;
    case 13:
      irr_poly = 0x201B;
      break;
    case 14:
      irr_poly = 0x4443;
      break;
    case 15:
      irr_poly = 0x8003;
      break;
    case 16:
      irr_poly = 0x1100B;
      break;
    case 17:
      irr_poly = 0x20009;
      break;
    case 18:
      irr_poly = 0x40081;
      break;
    case 19:
      irr_poly = 0x80027;
      break;
    case 20:
      irr_poly = 0x100009;
      break;
    case 21:
      irr_poly = 0x200005;
      break;
    case 22:
      irr_poly = 0x400003;
      break;
    case 23:
      irr_poly = 0x800021;
      break;
    case 24:
      irr_poly = 0x1000087;
      break;
    default:
      irr_poly = 0;
      GF_order = 0;
      throw std::runtime_error("C_operator_GF_2eN_poly_T ERROR: Current implementation supports only GF(2^2) to GF(2^24) \n");
      break;
    }
}

template <class T_E, class T_BE, uint32_t T_P>
T_E C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::GF2eN_invert(T_E A)
{
  T_E R = 1;
  for (uint32_t i = 0; i < GF_order -1 ; i++)
    {
      A = GF2eN_multiply(A, A);
      R = GF2eN_multiply(R, A);
    }
  return R;
}

template <class T_E, class T_BE, uint32_t T_P>
T_BE C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::GF2eN_invert(T_BE A)
{
  T_BE R = 1;
  for (uint32_t i = 0; i < GF_order -1 ; i++)
    {
      A = GF2eN_multiply(A, A);
      R = GF2eN_multiply(R, A);
    }
  return R;
}

template <class T_E, class T_BE, uint32_t T_P>
T_E C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::GF2eN_multiply(T_E A, T_E B)
{
  T_E R = 0;
  T_BE mask_lsb = 1;
  for (uint32_t i = 0; i < GF_order; i++)
    {
      R = R ^ ((A.rsh(i) & mask_lsb) * (B));
      B = B.lsh(1) ^ (irr_poly * (mask_lsb & B.rsh(GF_order-1)));
    }
  return R;
}


template <class T_E, class T_BE, uint32_t T_P>
T_E C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::GF2eN_multiply(T_E A, T_BE B)
{
  T_E R = 0;
  T_BE mask_lsb = 1;
  for (uint32_t i = 0; i < GF_order; i++)
    {
      R = R ^ ((A.rsh(i) & mask_lsb) * (B));
      B = B<<1 ^ (irr_poly * (mask_lsb & B>>(GF_order-1)));
    }
  return R;
}

template <class T_E, class T_BE, uint32_t T_P>
T_BE C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::GF2eN_multiply(T_BE A, T_BE B)
{
  T_BE R = 0;
  T_BE mask_lsb = 1;
  for (uint32_t i = 0; i < GF_order; i++)
    {
      R = R ^ ((A>>(i) & mask_lsb) * (B));
      B = B<<1 ^ (irr_poly * (mask_lsb & B>>(GF_order-1)));
    }
  return R;
}






template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::ApB(T_E *A, uint32_t A_size, T_E *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
    uint32_t i;
    for (i = 0; (i < A_size) && (i < B_size) && (i < R_size); i++)
      R[i] = A[i] ^ B[i];
    for (; (i < A_size) && (i < R_size); i++)
      R[i] = A[i];
    for (; (i < B_size) && (i < R_size); i++)
      R[i] = B[i];
    for (; i < R_size; i++)
      R[i] = 0;
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::AtB(T_E *A, uint32_t A_size, T_E *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  for (int32_t i = 0; i < R_size; i++)
    {
      R[i] = 0;
      int32_t j = 0;
      if (i >= B_size)
        j = i - (B_size-1);
      for (; (j <= i) && (j < A_size); j++)
	{
          R[i] = R[i] ^ GF2eN_multiply(A[j], B[i-j]);
	}
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::AtB(T_BE *A, uint32_t A_size, T_E *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  for (int32_t i = 0; i < R_size; i++)
    {
      R[i] = 0;
      int32_t j = 0;
      if (i >= B_size)
        j = i - (B_size-1);
      for (; (j <= i) && (j < A_size); j++)
	{
          R[i] = R[i] ^ GF2eN_multiply(A[j], B[i-j]);
	}
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::AtB(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  for (int32_t i = 0; i < R_size; i++)
    {
      R[i] = 0;
      int32_t j = 0;
      if (i >= B_size)
        j = i - (B_size-1);
      for (; (j <= i) && (j < A_size); j++)
	{
          R[i] = R[i] ^ GF2eN_multiply(A[j], B[i-j]);
	}
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::AtB(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size)
{
  for (int32_t i = 0; i < R_size; i++)
    {
      R[i] = 0;
      int32_t j = 0;
      if (i >= B_size)
        j = i - (B_size-1);
      for (; (j <= i) && (j < A_size); j++)
	{
          R[i] = R[i] ^ GF2eN_multiply(A[j], B[i-j]);
	}
    }
}

template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::ArB(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  if (R_size < B_size - 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: result doesn't fit in result variable\n");
  if (B[B_size - 1] == 0)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: MSB element of denominator is zero \n");

  int32_t i, j;
  
  
  if (A_size < B_size)
    {
      for (i = 0; i < A_size; i ++)
        R[i] = A[i];
      for (; i < R_size; i++)
        R[i] = 0;
    }
  else if (B_size == 1)
    { 
      for (i=0; i < R_size; i++)
        R[i] = 0;
    }
  else
    {
      for (i = 0; i < B_size-1; i ++)
        R[i] = A[A_size-B_size+i+1];
      for (; i < R_size; i++)
        R[i] = 0;
      
      T_E factor;
      for (i = A_size-1; i >= B_size-1; i--)
        {
          factor = GF2eN_divide(R[B_size-2], B[B_size-1]);
          for (j = B_size-2; j > 0; j--)
            {
              R[j] = R[j-1] ^ GF2eN_multiply(factor, B[j]);
            }
          R[0] = A[i-B_size+1] ^ GF2eN_multiply(factor, B[0]);
        }
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::ArB(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size)
{
  if (R_size < B_size - 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: result doesn't fit in result variable\n");
  if (B[B_size - 1] == 0)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: MSB element of denominator is zero \n");

  int32_t i, j;
  
  
  if (A_size < B_size)
    {
      for (i = 0; i < A_size; i ++)
        R[i] = A[i];
      for (; i < R_size; i++)
        R[i] = 0;
    }
  else if (B_size == 1)
    { 
      for (i=0; i < R_size; i++)
        R[i] = 0;
    }
  else
    {
      for (i = 0; i < B_size-1; i ++)
        R[i] = A[A_size-B_size+i+1];
      for (; i < R_size; i++)
        R[i] = 0;
      
      T_E factor;
      for (i = A_size-1; i >= B_size-1; i--)
        {
          factor = GF2eN_divide(R[B_size-2], B[B_size-1]);
          for (j = B_size-2; j > 0; j--)
            {
              R[j] = R[j-1] ^ GF2eN_multiply(factor, B[j]);
            }
          R[0] = A[i-B_size+1] ^ GF2eN_multiply(factor, B[0]);
        }
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::ArB(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  if (R_size < B_size - 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: result doesn't fit in result variable\n");
  if (B[B_size - 1] == 0)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: MSB element of denominator is zero \n");

  int32_t i, j;
  
  
  if (A_size < B_size)
    {
      for (i = 0; i < A_size; i ++)
        R[i] = A[i];
      for (; i < R_size; i++)
        R[i] = 0;
    }
  else if (B_size == 1)
    { 
      for (i=0; i < R_size; i++)
        R[i] = 0;
    }
  else
    {
      for (i = 0; i < B_size-1; i ++)
        R[i] = A[A_size-B_size+i+1];
      for (; i < R_size; i++)
        R[i] = 0;
      
      T_E factor;
      for (i = A_size-1; i >= B_size-1; i--)
        {
          factor = GF2eN_divide(R[B_size-2], B[B_size-1]);
          for (j = B_size-2; j > 0; j--)
            {
              R[j] = R[j-1] ^ GF2eN_multiply(factor, B[j]);
            }
          R[0] = A[i-B_size+1] ^ GF2eN_multiply(factor, B[0]);
        }
    }
}

template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::ArB_GF2(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  if (R_size < B_size - 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: result doesn't fit in result variable\n");
  if (B[B_size - 1] != 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: MSB element of denominator is zero \n");

  int32_t i, j;
  
  for (i = 0; i < B_size-1; i ++)
    R[i] = A[A_size-B_size+i+1];
  for (; i < R_size; i++)
    R[i] = 0;

  T_E factor;
  for (i = A_size-1; i >= B_size-1; i--)
    {
      factor = R[B_size-2];
      for (j = B_size-2; j > 0; j--)
	{
	  R[j] = R[j-1] ^ (factor * B[j]);
	}
      R[0] = A[i-B_size+1] ^ (factor * B[0]);
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::ArB_GF2(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size)
{
  if (R_size < B_size - 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: result doesn't fit in result variable\n");
  if (B[B_size - 1] != 1)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.ArB: MSB element of denominator is zero \n");

  uint32_t i, j;
  
  for (i = 0; i < B_size-1; i ++)
    R[i] = A[A_size-B_size+i+1];
  for (; i < R_size; i++)
    R[i] = 0;

  T_BE factor;
  for (i = A_size-1; i >= B_size-1; i--)
    {
      factor = R[B_size-2];
      for (j = B_size-2; j > 0; j--)
	{
	  R[j] = R[j-1] ^ (factor * B[j]);
	}
      R[0] = A[i-B_size+1] ^ (factor * B[0]);
    }
}

template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::Ap_ArB(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  if (R_size < A_size)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.Ap_ArB: result doesn't fit in result variable\n");
  if (B[B_size - 1] == 0)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.Ap_ArB: MSB element of denominator is zero \n");

  ArB(A, A_size, B, B_size, R, R_size);

  uint32_t i;
  for (i = B_size-1; i< A_size; i++)
    R[i] = A[i];
  for (; i < R_size; i++)
    R[i] = 0;
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::Ap_ArB_GF2(T_E *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_E *R, uint32_t R_size)
{
  if (R_size < A_size)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.Ap_ArB: result doesn't fit in result variable\n");

  ArB_GF2(A, A_size, B, B_size, R, R_size);

  uint32_t i;
  for (i = B_size-1; i< A_size; i++)
    R[i] = A[i];
  for (; i < R_size; i++)
    R[i] = 0;
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::Ap_ArB_GF2(T_BE *A, uint32_t A_size, T_BE *B, uint32_t B_size, T_BE *R, uint32_t R_size)
{
  if (R_size < A_size)
    throw std::runtime_error("ERROR C_operator_GF_2eN_poly_T.Ap_ArB: result doesn't fit in result variable\n");

  ArB_GF2(A, A_size, B, B_size, R, R_size);

  uint32_t i;
  for (i = B_size-1; i< A_size; i++)
    R[i] = A[i];
  for (; i < R_size; i++)
    R[i] = 0;
}

template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::eval_A_B(T_E *A, uint32_t A_size, T_BE B, T_E &R)
{
  R = 0;
  T_BE indet = 1;
  for (uint32_t i = 0; i < A_size; i ++)
    {
      R = R ^ GF2eN_multiply(A[i], indet);
      indet = GF2eN_multiply(indet, B);
    }
}

template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::eval_A_B_GF2(T_E *A, uint32_t A_size, T_BE B, T_E &R, bool msb_first)
{
  R = 0;
  T_BE indet = 1;
  if (!msb_first)
    {
      for (uint32_t i = 0; i < A_size; i ++)
        {
          R = R ^ (A[i] * indet);
          indet = GF2eN_multiply(indet, B);
        }
    }
  else
    {
      for (uint32_t i = 0; i < A_size; i ++)
        {
          R = R ^ (A[A_size-i-1] * indet);
          indet = GF2eN_multiply(indet, B);
        }
    }
      
}

template <class T_E, class T_BE, uint32_t T_P>
UME::SIMD::SIMDVecMask<T_P> C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::comp_AB(T_E *A, uint32_t A_size, T_E *B, uint32_t B_size)
{
  UME::SIMD::SIMDVecMask<T_P> equals = true;
  uint32_t i = 0;
  for (i=0; (i<A_size) && (i< B_size); i++)
    equals = equals && (A[i] == B[i]);
  for (;i<A_size; i++)
    equals = equals && (A[i] == 0);
  for (;i<B_size; i++)
    equals = equals && (B[i] == 0);
  return equals;
}

template <class T_E, class T_BE, uint32_t T_P>
UME::SIMD::SIMDVecMask<T_P> C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::is_zero(T_E *A, uint32_t A_size)
{
  UME::SIMD::SIMDVecMask<T_P> is_zero = true;
  for (uint32_t i = 0;i<A_size; i++)
    is_zero = is_zero && (A[i] == 0);
  return is_zero;
}

template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::display(T_E *A, uint32_t A_size, bool display_indet)
{
  c_vector_logger_t<T_E> Logger_input;
  T_E aux;
  Logger_input.Initialize("screen",   nullptr, "screen_logger" , 1, 20, 0, false, false, false);
  
  Logger_input.Add_Log_Entry(&A[0]);
  //printf("\b");
  for (uint32_t i = 1; i < A_size; i++)
    {
      if (display_indet) printf(" + ");
      Logger_input.Add_Log_Entry(&A[i]);
      if (display_indet) printf("*x^%d", i);
    }
}


template <class T_E, class T_BE, uint32_t T_P>
void C_operator_GF_2eN_poly_T<T_E, T_BE, T_P>::display(T_BE *A, uint32_t A_size, bool display_indet)
{
  c_vector_logger_t<T_BE> Logger_input;
  T_E aux;
  Logger_input.Initialize("screen",   nullptr, "screen_logger" , 1, 20, 0, false, false, false);
  
  Logger_input.Add_Log_Entry(&A[0]);
  //printf("\b");
  for (uint32_t i = 1; i < A_size; i++)
    {
      if (display_indet) printf(" + ");
      Logger_input.Add_Log_Entry(&A[i]);
      if (display_indet) printf("*x^%d", i);
    }
}

  
#endif
