#ifndef C_GF2_MATRIX_H
#define C_GF2_MATRIX_H
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h>
#include <stdint.h>

using namespace std;

class C_GF2_Matrix
{
private:
  uint32_t nRows;
  uint32_t nColumns;
  uint32_t nRows_MCols;
  uint32_t nColumns_MRows;
  uint32_t *Matrix;
  uint64_t     *Matrix_Rows;
  uint64_t     *Matrix_Cols;
  bool FreeOnDestroyer;
  bool FreeOnDestroyer_MRows;
  bool FreeOnDestroyer_MCols;
public:
  //Constructor and destructor
  C_GF2_Matrix();
  C_GF2_Matrix(uint32_t i_Rows, uint32_t i_Cols);
  C_GF2_Matrix(const C_GF2_Matrix &M);
  ~C_GF2_Matrix(){FreeResources();}
  void FreeResources();
  void FreeResources_MatrixFull();
  void FreeResources_MatrixRows();
  void FreeResources_MatrixCols();
  //Load and save to file
  bool LoadFromFile_MatrixFull(string Filename);
  bool LoadFromFile_MatrixRows(string Filename);
  bool LoadFromFile_MatrixCols(string Filename);
  bool SaveToFile(string Filename);
  //Create matrix from array or matrix
  bool load_matrix_from_matlab_array(uint32_t i_Rows, uint32_t i_Columns, double* i_Matrix);
  bool store_matrix_in_matlab_array(double* o_Matrix) const;

  bool Wrap(uint32_t i_Rows,
	    uint32_t i_Cols,
	    uint32_t *i_Matrix);
  bool WrapSubmatrix(uint32_t i_RowStart,
		     uint32_t i_RowEnd,
		     uint32_t i_ColStart,
		     uint32_t i_ColEnd,
		     const C_GF2_Matrix &Orig);
  C_GF2_Matrix ExtractSubmatrix(uint32_t Rstart,
				uint32_t Rend,
				uint32_t Cstart,
				uint32_t Cend) const;
  //Extract fields
  uint32_t Rows() const {return nRows;}
  uint32_t Columns() const {return nColumns;}
  uint32_t element(uint32_t Row,
		       uint32_t Column) const;
  void set_element(uint32_t Row,
		   uint32_t Column,
		   uint32_t value);
  uint32_t* MatrixPointer(){return &Matrix[0];}
  //Row manipulation
  bool SwapRows(uint32_t R1,
		uint32_t R2);
  bool SwapRows_MR(uint32_t R1,
                   uint32_t R2);
  bool SwapCols(uint32_t C1,
		uint32_t C2);
  bool AddRows(uint32_t R1,
	       uint32_t R2);
  bool AddRows_MR(uint32_t R1,
                  uint32_t R2);
  bool Invert();
  bool Invert_MR();
  int Rank();
  void Zero();
  bool Systematize();
  //Operator overload
  C_GF2_Matrix operator * (const C_GF2_Matrix &B);
  bool Product(const C_GF2_Matrix &A,
	       const C_GF2_Matrix &B);
  bool Product_MC_M(const C_GF2_Matrix &A,
		    const C_GF2_Matrix &B);
  const C_GF2_Matrix &operator = (const C_GF2_Matrix& M);
  bool CloneContents(const C_GF2_Matrix &Orig);
  //Display
  void Display();
  void Display(uint32_t Rstart, uint32_t Rend, uint32_t Cstart, uint32_t Cend);

  void Sync_Matrix2MatrixCols();
  void Sync_MatrixCols2Matrix();
  void Sync_Matrix2MatrixRows();
  void Sync_MatrixRows2Matrix();
};

#endif
