#ifndef C_GF_2eN_T_H
#define C_GF_2eN_T_H

#include "c_vector_logger_t.hpp"
#include "UMESimd.h"
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h>


template <class T_E, class T_BE, uint32_t T_P>
class C_GF_2eN_T
{
private:
  T_E             element = 0;
  static T_BE     irr_poly;
  static uint32_t order;
  static bool     enabled;
public:
  C_GF_2eN_T(uint32_t i_order){change_GF_order(i_order);}
  void change_GF_order(uint32_t i_order);
  C_GF_2eN_T(const C_GF_2eN_T& i_tocopy){element = i_tocopy.element;}
  C_GF_2eN_T(const T_E& i_tocopy){element = i_tocopy;}
  C_GF_2eN_T(){/*if (!enabled) {throw std::runtime_error("GF_2eN ERROR: Instance created without initialized field \n");}*/}
  inline T_BE get_poly(){return irr_poly;}
  inline T_E get_element(){return element;}
  inline uint32_t get_order(){return order;}
  inline bool get_enabled(){return enabled;}  
  inline C_GF_2eN_T<T_E, T_BE, T_P>  invert();
  inline C_GF_2eN_T  operator +  (const C_GF_2eN_T<T_E, T_BE, T_P> &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c; c.element = this->element ^ b.element; return c;}
  inline C_GF_2eN_T  operator +  (const T_BE                       &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c; c.element = this->element ^ b; return c;}
  inline C_GF_2eN_T  operator +  (const T_E                        &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c; c.element = this->element ^ b; return c;}
  inline C_GF_2eN_T  operator -  (const C_GF_2eN_T<T_E, T_BE, T_P> &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c; c.element = this->element ^ b.element; return c;}
  inline C_GF_2eN_T  operator -  (const T_BE                       &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c; c.element = this->element ^ b; return c;}
  inline C_GF_2eN_T  operator -  (                                   )  {C_GF_2eN_T<T_E, T_BE, T_P> c; c.element = this->element ^ 0; return c;}
  inline C_GF_2eN_T  operator *  (const C_GF_2eN_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_T  operator *  (const T_BE                       &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c, d = b; c = (*this) * b; return c;}
  inline C_GF_2eN_T  operator *  (const T_E                        &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c, d = b; c = (*this) * b; return c;}
  inline C_GF_2eN_T  operator /  (const C_GF_2eN_T<T_E, T_BE, T_P> &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c, d = b; c = (d.invert()) * (*this); return c;};
  inline C_GF_2eN_T  operator /  (const T_BE                       &b)  {C_GF_2eN_T<T_E, T_BE, T_P> c, d = b; c = (d.invert()) * (*this); return c;};
  inline C_GF_2eN_T& operator =  (const C_GF_2eN_T<T_E, T_BE, T_P> &b)  {element = b.element; return *this;}
  inline C_GF_2eN_T& operator =  (const T_BE                       &b)  {element = b; return *this;}
  inline C_GF_2eN_T& operator =  (const T_E                        &b)  {element = b; return *this;}
  inline UME::SIMD::SIMDVecMask<T_P> operator == (const C_GF_2eN_T<T_E, T_BE, T_P> &b)  {return (this->element == b.element);}
  inline UME::SIMD::SIMDVecMask<T_P> operator == (const T_BE                       &b)  {return (this->element == b);}
  void display();
};


template <class T_E, class T_BE, uint32_t T_P>
bool C_GF_2eN_T<T_E, T_BE, T_P>::enabled = false;

template <class T_E, class T_BE, uint32_t T_P>
T_BE C_GF_2eN_T<T_E, T_BE, T_P>::irr_poly = 0;

template <class T_E, class T_BE, uint32_t T_P>
uint32_t C_GF_2eN_T<T_E, T_BE, T_P>::order = 0;


  
template <class T_E, class T_BE, uint32_t T_P>
void C_GF_2eN_T<T_E, T_BE, T_P>::change_GF_order(uint32_t i_order)
{
  
  if (i_order+1 > sizeof(T_BE)*8-1)
    throw std::runtime_error("GF_2eN ERROR: Data type is not big enough for the requested GF2^n order \n");
  
  order = i_order;
  
  enabled = true;
  switch (order)
    {
    case 2:
      irr_poly = 0x0007;
      break;
    case 3:
      irr_poly = 0x000B;
      break;
    case 4:
      irr_poly = 0x0013;
      break;
    case 5:
      irr_poly = 0x0025;
      break;
    case 6:
      irr_poly = 0x0043;
      break;
    case 7:
      irr_poly = 0x0089;
      break;
    case 8:
      irr_poly = 0x011D;
      break;
    case 9:
      irr_poly = 0x0211;
      break;
    case 10:
      irr_poly = 0x0409;
      break;
    case 11:
      irr_poly = 0x0805;
      break;
    case 12:
      irr_poly = 0x1053;
      break;
    case 13:
      irr_poly = 0x201B;
      break;
    case 14:
      irr_poly = 0x4443;
      break;
    case 15:
      irr_poly = 0x8003;
      break;
    case 16:
      irr_poly = 0x1100B;
      break;
    case 17:
      irr_poly = 0x20009;
      break;
    case 18:
      irr_poly = 0x40081;
      break;
    case 19:
      irr_poly = 0x80027;
      break;
    case 20:
      irr_poly = 0x100009;
      break;
    case 21:
      irr_poly = 0x200005;
      break;
    case 22:
      irr_poly = 0x400003;
      break;
    case 23:
      irr_poly = 0x800021;
      break;
    case 24:
      irr_poly = 0x1000087;
      break;
    default:
      irr_poly = 0;
      enabled = false;
      order = 0;
      break;
    }
  if (!enabled)
    throw std::runtime_error("GF_2eN ERROR: Current implementation supports only GF(2^2) to GF(2^24) \n");
}


template <class T_E, class T_BE, uint32_t T_P>
 C_GF_2eN_T<T_E, T_BE, T_P> C_GF_2eN_T<T_E, T_BE, T_P>::invert()
{
  C_GF_2eN_T res;
  res.element = 1;
  C_GF_2eN_T aux;
  aux.element = element;

  for (uint32_t i = 0; i < order-1; i++)
    {
      aux = aux * aux;
      res = res * aux;
    }
  return res;
}


template <class T_E, class T_BE, uint32_t T_P>
C_GF_2eN_T<T_E, T_BE, T_P>  C_GF_2eN_T<T_E, T_BE, T_P>::operator *(const C_GF_2eN_T<T_E, T_BE, T_P> &b)
{											 
  UME::SIMD::SIMDVecMask<T_P> m0;
  C_GF_2eN_T<T_E, T_BE, T_P> c;
  c= 0;
  T_BE mask_moving = 1<<(order-1);
  T_BE mask_modulo = 1<<(order);

  for (uint32_t i = 0; i<order;i++)
    {
      m0 = ((b.element & mask_moving) != 0);
      c.element = c.element.lsh(1);
      c.element[m0] = c.element ^ element;
      m0 = (c.element & mask_modulo) != 0;
      c.element[m0] = c.element ^ irr_poly;
      mask_moving = mask_moving >> 1;
    }
   return c;
}

template <class T_E, class T_BE, uint32_t T_P>
void C_GF_2eN_T<T_E, T_BE, T_P>::display()
{
  c_vector_logger_t<T_E> Logger_input;
  Logger_input.initialize("screen",   nullptr, "input_stream" , 1, 20, 0, false, false, false);
  Logger_input.add_log_entry(&element);
}
#endif
