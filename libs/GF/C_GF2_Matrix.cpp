#ifndef C_GF2_MATRIX_CPP
#define C_GF2_MATRIX_CPP

#include "C_GF2_Matrix.h"


C_GF2_Matrix::C_GF2_Matrix()
{
  nRows = 0;
  nColumns = 0; 
  nRows_MCols = 0;
  nColumns_MRows = 0;
  Matrix = nullptr;
  Matrix_Rows = nullptr;
  Matrix_Cols = nullptr;
  FreeOnDestroyer_MRows = false;
  FreeOnDestroyer_MCols = false;
  FreeOnDestroyer = false;
}

C_GF2_Matrix::C_GF2_Matrix(uint32_t i_Rows,
			   uint32_t i_Cols)
{
  nRows = 0;
  nColumns = 0; 
  nRows_MCols = 0;
  nColumns_MRows = 0;
  FreeOnDestroyer = false;
  FreeOnDestroyer_MRows = false;
  FreeOnDestroyer_MCols = false;
  Matrix = nullptr;
  Matrix_Rows = nullptr;
  Matrix_Cols = nullptr;
  
  FreeResources();
  nRows = i_Rows;
  nColumns = i_Cols;
  Matrix = new uint32_t[nRows * nColumns];

  //nColumns_MRows = ceil(float(nColumns)/64.0f);
  //Matrix_Rows = new uint64_t[nRows * nColumns_MRows];


  FreeOnDestroyer = true;
  FreeOnDestroyer_MRows = false;
  FreeOnDestroyer_MCols = false;
}

C_GF2_Matrix::C_GF2_Matrix(const C_GF2_Matrix &M)
{
  nRows = 0;
  nColumns = 0; 
  nRows_MCols = 0;
  nColumns_MRows = 0;
  FreeOnDestroyer = false;
  FreeOnDestroyer_MRows = false;
  FreeOnDestroyer_MCols = false;
  Matrix = nullptr;
  Matrix_Rows = nullptr;
  Matrix_Cols = nullptr;
  
  FreeResources();
  nRows = M.nRows;
  nColumns = M.nColumns;
  nRows_MCols = M.nRows_MCols;
  nColumns_MRows = M.nColumns_MRows;

  FreeOnDestroyer = M.FreeOnDestroyer;
  FreeOnDestroyer_MRows = M.FreeOnDestroyer_MRows;
  FreeOnDestroyer_MCols = M.FreeOnDestroyer_MCols;

  if (FreeOnDestroyer)
    {
      Matrix = new uint32_t[(nRows) * (nColumns)];
      for (uint32_t i = 0; i < nRows; i++)
        for (uint32_t j = 0; j < nColumns; j++)
          Matrix[i*nColumns+j] = M.Matrix[i*nColumns+j];
    }
  if (FreeOnDestroyer_MRows)
    {
      Matrix_Rows = new uint64_t[nColumns_MRows * nRows];
      for (uint32_t i = 0; i < nRows; i++)
        for (uint32_t j = 0; j < nColumns_MRows; j++)
          Matrix_Rows[i*nColumns_MRows+j] = M.Matrix_Rows[i*nColumns_MRows+j];
    }
  if (FreeOnDestroyer_MCols)
    {
      Matrix_Cols = new uint64_t[nRows_MCols * nColumns];
      for (uint32_t i = 0; i < nRows_MCols; i++)
        for (uint32_t j = 0; j < nColumns; j++)
          Matrix_Cols[i*nColumns+j] = M.Matrix_Cols[i*nColumns+j];
    }
}

void C_GF2_Matrix::FreeResources()
{
  if (/*(Matrix!=nullptr)&&*/(FreeOnDestroyer))
    {
      delete[] Matrix;
      Matrix = nullptr;
    }
  if (/*(Matrix_Rows!=nullptr)&&*/(FreeOnDestroyer_MRows))
    {
      delete[] Matrix_Rows;
      Matrix_Rows = nullptr;
    }
  if (/*(Matrix_Cols!=nullptr)&&*/(FreeOnDestroyer_MCols))
    {
      delete[] Matrix_Cols;
      Matrix_Cols = nullptr;
    }
  FreeOnDestroyer = false;
  FreeOnDestroyer_MRows = false;
  FreeOnDestroyer_MCols = false;
}

void C_GF2_Matrix::FreeResources_MatrixFull()
{
  if (/*(Matrix!=nullptr)&&*/(FreeOnDestroyer))
    {
      delete Matrix;
      Matrix = nullptr;
    }
  FreeOnDestroyer = false;
}


void C_GF2_Matrix::FreeResources_MatrixRows()
{
  if (/*(Matrix_Rows!=nullptr)&&*/(FreeOnDestroyer_MRows))
    {
      delete Matrix_Rows;
      Matrix_Rows = nullptr;
    }
  FreeOnDestroyer_MRows = false;
}


void C_GF2_Matrix::FreeResources_MatrixCols()
{
  if (/*(Matrix_Cols!=nullptr)&&*/(FreeOnDestroyer_MCols))
    {
      delete Matrix_Cols;
      Matrix_Cols = nullptr;
    }
  FreeOnDestroyer_MCols = false;
}

bool C_GF2_Matrix::LoadFromFile_MatrixFull(string Filename)
{
  FreeResources();
    
  FILE *p_file = fopen(Filename.c_str(), "r");

  if (p_file == NULL)
    return false;
  
  if (fscanf(p_file, "%u", &nRows) != 1)
    {fclose(p_file);return false;}
  if (fscanf(p_file, "%u", &nColumns) != 1)
    {fclose(p_file);return false;}
  Matrix = new uint32_t[(nRows) * (nColumns)];
  FreeOnDestroyer = true;

  int Row, Col;
  for (uint32_t i = 0; i < nRows; i++)
    for (uint32_t j = 0; j < nColumns; j++)
      Matrix[i*nColumns+j] = 0;
  while (fscanf(p_file, "%d,%d", &Row, &Col) == 2)
    {
      Matrix[(Row-1)*(nColumns)+Col-1] = 1;
    }
  fclose(p_file);
  return true;
}


bool C_GF2_Matrix::LoadFromFile_MatrixRows(string Filename)
{
  FreeResources();
    
  FILE *p_file = fopen(Filename.c_str(), "r");

  if (p_file == NULL)
    return false;
  
  if (fscanf(p_file, "%u", &nRows) != 1)
    {fclose(p_file);return false;}
  if (fscanf(p_file, "%u", &nColumns) != 1)
    {fclose(p_file);return false;}

  //creation of variables
  nColumns_MRows = ceil(float(nColumns)/64.0f);
  Matrix_Rows = new uint64_t[nColumns_MRows * nRows];
  FreeOnDestroyer_MRows = true;
  //initialization of matrix with all zeros as preparation for loading
  for (uint32_t r = 0; r<nRows; r++)
    {
      for (uint32_t c = 0; c<nColumns_MRows; c++)
	{
	  Matrix_Rows[r*nColumns_MRows+c] = 0;
	}
    }
  int Row, Col;
  while (fscanf(p_file, "%d,%d", &Row, &Col) == 2)
    {
      uint64_t auxMask;
      Row = Row-1;
      Col = Col-1;
      auxMask = 1;
      for (int i = 0; i < (Col%64); i++)
        {
          auxMask = auxMask*2;
        }
      Matrix_Rows[Row*nColumns_MRows+(Col/64)] += auxMask;
    }
  fclose(p_file);
  return true;
}

bool C_GF2_Matrix::LoadFromFile_MatrixCols(string Filename)
{
  FreeResources();
    
  FILE *p_file = fopen(Filename.c_str(), "r");

  if (p_file == NULL)
    return false;
  
  if (fscanf(p_file, "%u", &nRows) != 1)
    {fclose(p_file);return false;}
  if (fscanf(p_file, "%u", &nColumns) != 1)
    {fclose(p_file);return false;}

  //creation of variables
      nRows_MCols = ceil(float(nRows)/64.0f);
      Matrix_Cols = new uint64_t[nRows_MCols * nColumns];
      FreeOnDestroyer_MCols = true;
  //initialization of matrix with all zeros as preparation for loading
  for (uint32_t r = 0; r<nRows_MCols; r++)
    {
      for (uint32_t c = 0; c<nColumns; c++)
	{
	  Matrix_Cols[r*nColumns+c] = 0;
	}
    }
  int Row, Col;
  while (fscanf(p_file, "%d,%d", &Row, &Col) == 2)
    {
      uint64_t auxMask;
      Row = Row-1;
      Col = Col-1;
      auxMask = 1;
      for (int i = 0; i < (Row%64); i++)
        {
          auxMask = auxMask*2;
        }
      Matrix_Rows[(Row/64)*nColumns+Col] += auxMask;
    }
  fclose(p_file);
  return true;
}


bool C_GF2_Matrix::SaveToFile(string Filename)
{
  FILE *p_file = fopen(Filename.c_str(), "w");

  fprintf(p_file, "%u", nRows);
  fprintf(p_file, "\n%u", nColumns);
  for (uint32_t r = 0; r < nRows; r++)
    {
      for (uint32_t c = 0; c < nColumns; c++)
	{
	  if (Matrix[r*nColumns+c] == 1)
	    {
	      fprintf(p_file, "\n%u,%u", r+1, c+1);
	    }
	}
    }
  fclose(p_file);
  return true;
}

bool C_GF2_Matrix::load_matrix_from_matlab_array(uint32_t i_Rows, uint32_t i_Columns, double* i_Matrix)
{
  FreeResources();
  nRows = i_Rows;
  nColumns = i_Columns;
  Matrix = new uint32_t[nRows * nColumns];
  FreeOnDestroyer = true;

  for (uint32_t i = 0; i < nRows; i++)
    for (uint32_t j = 0; j < nColumns; j++)
      Matrix[i*nColumns+j] = (uint32_t)i_Matrix[j*nRows + i];
  
  return true;
}


bool C_GF2_Matrix::store_matrix_in_matlab_array(double* o_Matrix) const
{
  for (uint32_t i = 0; i < nRows; i++)
    for (uint32_t j = 0; j < nColumns; j++)
      o_Matrix[j*nRows+i] = (double)Matrix[i*nColumns+j];
  return true;
}

bool C_GF2_Matrix::Wrap(uint32_t i_Rows,
			uint32_t i_Cols,
			uint32_t *i_Matrix)
{  
  FreeResources();

  nRows = i_Rows;
  nColumns = i_Cols;
  Matrix =i_Matrix;
  return true;
}

bool C_GF2_Matrix::WrapSubmatrix(uint32_t i_RowStart,
					 uint32_t i_RowEnd,
					 uint32_t i_ColStart,
					 uint32_t i_ColEnd,
					 const C_GF2_Matrix &Orig)
{  
  FreeResources();
  if (((i_ColStart == 0) && (i_ColEnd == Orig.nColumns-1) && (i_RowStart <= i_RowEnd) && (i_RowEnd < Orig.nRows)) ||
      ((Orig.nRows == 1) && (i_RowStart == 0) && (i_RowEnd == 0) && (i_ColStart <= i_ColEnd) && (i_ColEnd < Orig.nColumns)))
    {
      nColumns = i_ColEnd - i_ColStart + 1;
      nRows = i_RowEnd - i_RowStart + 1;
      Matrix = &Orig.Matrix[i_RowStart*Orig.nColumns+i_ColStart];
      return true;
    }
  else
    {
      throw 5;
    }
}
 
C_GF2_Matrix C_GF2_Matrix::ExtractSubmatrix(uint32_t Rstart, uint32_t Rend, uint32_t Cstart, uint32_t Cend) const
{
  if ((Rstart > Rend) || (Rend >= nRows))
    throw 1;
  if ((Cstart > Cend) || (Cend >= nColumns))
    throw 2;
  C_GF2_Matrix temp(Rend-Rstart+1, Cend-Cstart+1);
  for (uint32_t r = 0; r< temp.nRows; r++)
    {
      for (uint32_t c = 0; c<temp.nColumns; c++)
	{
	  temp.Matrix[r*temp.nColumns+c] = Matrix[(r+Rstart)*nColumns+c+Cstart];
	}
    }
  return temp;
}


uint32_t C_GF2_Matrix::element(uint32_t Row,
                                   uint32_t Column) const
{
  if (FreeOnDestroyer)
    {
      return Matrix[Row*nColumns+Column];
    }
  else if (FreeOnDestroyer_MRows)
    {
      uint64_t auxMask = 1;
      for (uint i = 0; i < (Column%64); i++)
        {
          auxMask = auxMask*2;
        }
      uint32_t ans = 0;
      if ((Matrix_Rows[Row*nColumns_MRows+(Column/64)] & auxMask) > 0)
        ans = 1;
      return ans;
    }
  else if (FreeOnDestroyer_MCols)
    {
      uint64_t auxMask = 1;
      for (uint i = 0; i < (Row%64); i++)
        {
          auxMask = auxMask*2;
        }
      uint32_t ans = 0;
      if ((Matrix_Rows[(Row/64)*nColumns+Column] & auxMask) > 0)
        ans = 1;
      return ans;
    }
  else
    {
      throw 6;
    }

}


void C_GF2_Matrix::set_element(uint32_t Row,
			       uint32_t Column,
			       uint32_t value)
{
  if (value > 1)
    throw 911;
  
  if (FreeOnDestroyer)
    {
      Matrix[Row*nColumns+Column] = value;
    }
  else if (FreeOnDestroyer_MRows)
    {
      uint64_t auxMask = 1;
      for (uint i = 0; i < (Column%64); i++)
        {
          auxMask = auxMask*2;
        }
      uint64_t auxMask_n = ~auxMask;
      Matrix_Rows[Row*nColumns_MRows+(Column/64)] =
	(Matrix_Rows[Row*nColumns_MRows+(Column/64)] & auxMask_n) |
	(value * auxMask);
    }
  else if (FreeOnDestroyer_MCols)
    {
      uint64_t auxMask = 1;
      for (uint i = 0; i < (Row%64); i++)
        {
          auxMask = auxMask*2;
        }
      uint64_t auxMask_n = ~auxMask;
      Matrix_Rows[(Row/64)*nColumns+Column] =
	(Matrix_Rows[(Row/64)*nColumns+Column] & auxMask_n) |
	value * auxMask;
    }
  else
    {
      throw 6;
    }

}



bool C_GF2_Matrix::SwapRows(uint32_t R1, uint32_t R2)
{
  for (uint32_t i = 0; i < nColumns; i++)
    {
      uint32_t aux;
      aux = Matrix[R1*nColumns+i];
      Matrix[R1*nColumns+i] = Matrix[R2*nColumns+i];
      Matrix[R2*nColumns+i] = aux;
    }
  return true;
}

bool C_GF2_Matrix::SwapRows_MR(uint32_t R1, uint32_t R2)
{
  for (uint32_t i = 0; i < nColumns_MRows; i++)
    {
      uint64_t aux;
      aux = Matrix_Rows[R1*nColumns_MRows+i];
      Matrix_Rows[R1*nColumns_MRows+i] = Matrix_Rows[R2*nColumns_MRows+i];
      Matrix_Rows[R2*nColumns_MRows+i] = aux;
    }
  return true;
}

bool C_GF2_Matrix::SwapCols(uint32_t C1, uint32_t C2)
{
  for (uint32_t i = 0; i < nRows; i++)
    {
      uint32_t aux;
      aux = Matrix[i*nColumns+C1];
      Matrix[i*nColumns+C1] = Matrix[i*nColumns+C2];
      Matrix[i*nColumns+C2] = aux;
    }
  return true;
}

bool C_GF2_Matrix::AddRows(uint32_t R1, uint32_t R2)
{
  for (uint32_t i = 0; i < nColumns; i++)
    {
      Matrix[R1*nColumns+i] ^= Matrix[R2*nColumns+i];
    }
  return true;
}

bool C_GF2_Matrix::AddRows_MR(uint32_t R1, uint32_t R2)
{
  for (uint32_t i = 0; i < nColumns_MRows; i++)
    {
      Matrix_Rows[R1*nColumns_MRows+i] ^= Matrix_Rows[R2*nColumns_MRows+i];
    }
  return true;
}



bool C_GF2_Matrix::Invert()
{
  C_GF2_Matrix Aux(*this);
  for (uint32_t r = 0; r<nRows; r++)
    {
      for (uint32_t c = 0; c<nColumns; c++)
	{
	  Matrix[r*nColumns+c] = (r == c) ? 1 : 0;
	}
    }

  uint32_t r2;
  for (uint32_t r1 = 0; r1 < nRows; r1++)
    {
      printf("\n Inverting row %u out of %u", r1, nRows);
      if (Aux.Matrix[r1*nColumns+r1] == 0)
	{
	  r2 = r1+1;
	  while ((Aux.Matrix[r2*nColumns+r1] == 0) && (r2 < nRows))
	    r2++;
	  if (r2 == nRows)
	    throw 98;//not invertsible matrix
	  Aux.SwapRows(r1,r2);
	  SwapRows(r1, r2);
	}
      for (r2 = 0; r2 < nRows; r2++)
	{
	  if ((r1 != r2) && (Aux.Matrix[r2*nColumns+r1] == 1))
	    {
	      Aux.AddRows(r2, r1);
	      AddRows(r2,r1);
	    }
	}
    }
  return true;
}



bool C_GF2_Matrix::Invert_MR()
{
  //copy for gaussian ellimination
  C_GF2_Matrix Aux(*this);
  //identity matrix creation
  for (uint32_t r = 0; r<nRows; r++)
    {
      for (uint32_t c = 0; c<nColumns_MRows; c++)
	{
	  Matrix_Rows[r*nColumns_MRows+c] = 0;
	}
    }
  uint64_t auxMask = 1;
  for (uint32_t r = 0; r<nRows; r++) 
    {
      if (r%64 == 0)
	auxMask = 1;
      else
	auxMask = auxMask*2;
      Matrix_Rows[r*nColumns_MRows+(r/64)] += auxMask;
    }
  //gaussian ellimination
  uint32_t r2;
  for (uint32_t r1 = 0; r1 < nRows; r1++)
    {
      if (r1%64 == 0)
	auxMask = 1;
      else
	auxMask = auxMask*2;
      printf("\n Inverting row %u out of %u", r1, nRows);
      if ((Aux.Matrix_Rows[r1*nColumns_MRows+(r1/64)]&auxMask) == 0)
	{
	  r2 = r1+1;
	  while (((Aux.Matrix_Rows[r2*nColumns_MRows+(r1/64)]&auxMask) == 0) && (r2 < nRows))
	    r2++;
	  if (r2 == nRows)
	    throw 98;//not invertsible matrix
	  Aux.SwapRows_MR(r1,r2);
	  SwapRows_MR(r1, r2);
	}
      for (r2 = 0; r2 < nRows; r2++)
	{
	  if ((r1 != r2) && ((Aux.Matrix_Rows[r2*nColumns_MRows+(r1/64)]&auxMask) != 0))
	    {
	      Aux.AddRows_MR(r2, r1);
	      AddRows_MR(r2,r1);
	    }
	}
    }
  return true;
}

int C_GF2_Matrix::Rank()
{
  C_GF2_Matrix Aux(*this);
  int rank = nRows;

  uint32_t r2;
  for (uint32_t r1 = 0; r1 < nRows; r1++)
    {
      printf("\n Inverting row %u out of %u", r1, nRows);
      if (Aux.Matrix[r1*nColumns+r1] == 0)
	{
	  r2 = r1+1;
	  while ((Aux.Matrix[r2*nColumns+r1] == 0) && (r2 < nRows))
	    r2++;
	  if (r2 == nRows)
            {
              rank --;
              r2 = r1;
            }
          else
            Aux.SwapRows(r1,r2);
	}
      for (r2 = 0; r2 < nRows; r2++)
	{
	  if ((r1 != r2) && (Aux.Matrix[r2*nColumns+r1] == 1))
	    {
	      Aux.AddRows(r2, r1);
	    }
	}
    }
  return rank;
}


void C_GF2_Matrix::Zero()
{
  for (uint32_t r = 0; r<nRows; r++)
    {
      for (uint32_t c = 0; c<nColumns; c++)
	{
	  Matrix[r*nColumns+c] = 0;
	}
    } 
}


bool C_GF2_Matrix::Systematize()
{
  C_GF2_Matrix Aux(*this);
  
  uint32_t r1=0;
  uint32_t r2;
  uint32_t cxswap;
  for (r1 = 0; r1<nRows; r1++)
    {
      printf("\n Gaussian elimination row %u out of %u", r1, nRows);
      if (Aux.Matrix[r1*nColumns+r1] == 0)
	{
	  r2 = r1+1;
	  while ((Aux.Matrix[r2*nColumns+r1] == 0) && (r2 < nRows))
	    r2++;
	  if (r2 == nRows)
            {             
              cxswap = r1+1;
              printf("\n r1 = %u, r2 = %u, cxswap = %u\n", r1, r2, cxswap);
              while ((Aux.Matrix[r1*nColumns+cxswap] == 0) && (cxswap < nColumns))
                {
                  cxswap++;
                }
              printf("\n r1 = %u, r2 = %u, cxswap = %u\n", r1, r2, cxswap);
              if (cxswap == nColumns)
                throw 98;//not systematizable matrix
              printf("\n Swapping columns %u with %u", r1, cxswap);
              Aux.SwapCols(r1, cxswap);
              SwapCols(r1,cxswap);
            }
          else
            Aux.SwapRows(r1,r2);
	  //SwapRows(r1, r2);
	}
      for (r2 = 0; r2 < nRows; r2++)
	{
	  if ((r1 != r2) && (Aux.Matrix[r2*nColumns+r1] == 1))
	    {
	      Aux.AddRows(r2, r1);
	      //AddRows(r2,r1);
	    }
	}
    }
  return true;
}

bool C_GF2_Matrix::Product(const C_GF2_Matrix &A, const C_GF2_Matrix &B)
{
  if ((A.nColumns != B.nRows) ||
      (A.nRows != nRows) ||
      (B.nColumns != nColumns))
    throw 1;
  for (uint32_t r = 0; r<nRows; r++)
    {
      for (uint32_t c = 0; c<nColumns; c++)
	{	  
	  Matrix[r*nColumns+c] = 0;
	  for (uint32_t aux = 0; aux < A.nColumns; aux++)
	    {
	      Matrix[r*B.nColumns+c] ^= (A.Matrix[r*A.nColumns+aux] & B.Matrix[aux*B.nColumns + c]);	      
	    }
	}
    }
  return true;
}


bool C_GF2_Matrix::Product_MC_M(const C_GF2_Matrix &A, const C_GF2_Matrix &B)
{
  if ((A.nColumns != B.nRows) ||
      (A.nRows != nRows) ||
      (B.nColumns != nColumns))
    throw 1;
  if ((A.Matrix_Cols==nullptr)||(!A.FreeOnDestroyer_MCols))
    throw 99;

  uint64_t aux;
  uint64_t auxMask;
  for (uint32_t r = 0; r<nRows; r+=64)
    {
      printf("\nMultiplying %6.2f%%", (float)r/(float)nRows*100.0f);
      for (uint32_t c = 0; c<nColumns; c++)
	{
	  aux = 0;
	  Matrix[r*nColumns+c] = 0;
	  for (uint32_t intern = 0; intern < A.nColumns; intern++)
	    {
	      aux ^= A.Matrix_Cols[(r/64)*A.nColumns+intern] * B.Matrix[intern*B.nColumns + c];
	      
	    }
	  auxMask = 1;
	  
	  for (uint32_t auxUnpack = 0; (auxUnpack <64)&&(auxUnpack+r < nRows); auxUnpack++)
	    {
	      Matrix[(r+auxUnpack)*B.nColumns+c] = ((aux&auxMask)!= 0)? 1: 0;
	      auxMask = auxMask*2;
	    }
	}
    }
  return true;
}


C_GF2_Matrix C_GF2_Matrix::operator * (const C_GF2_Matrix &B)
{
  if (nColumns != B.nRows)
    throw 5;
  C_GF2_Matrix temp(nRows, B.nColumns);
  for (uint32_t r = 0; r<nRows; r++)
    {
      printf("\nMultiplying %6.2f%%", (float)r/(float)nRows*100.0f);
      for (uint32_t c = 0; c<B.nColumns; c++)
	{
	  
	  temp.Matrix[r*B.nColumns+c] = 0;
	  for (uint32_t aux = 0; aux < nColumns; aux++)
	    {
	      temp.Matrix[r*B.nColumns+c] ^= (Matrix[r*nColumns+aux] & B.Matrix[aux*B.nColumns + c]);
	      
	    }
	}
    }
  return temp;
}


const C_GF2_Matrix &C_GF2_Matrix::operator = (const C_GF2_Matrix& M)
{
  FreeResources();
  nRows_MCols = M.nRows_MCols;
  nColumns_MRows = M.nColumns_MRows;
  nRows = M.nRows;
  nColumns = M.nColumns;
  
  Matrix = new uint32_t[(nRows) * (nColumns)];
  FreeOnDestroyer = true;
  for (uint32_t i = 0; i < nRows; i++)
    for (uint32_t j = 0; j < nColumns; j++)
      Matrix[i*nColumns+j] = M.Matrix[i*nColumns+j];

  FreeOnDestroyer_MRows = false;
  FreeOnDestroyer_MCols = false;
  
  return *this;
}

bool C_GF2_Matrix::CloneContents(const C_GF2_Matrix &Orig)
{
  if ((Orig.nRows != nRows) || (Orig.nColumns != nColumns))
    throw 19;
  else
    for (uint32_t r = 0; r < nRows ; r++)
      for (uint32_t c = 0; c < nColumns ; c++)
	Matrix[r*nColumns+c] = Orig.Matrix[r*nColumns+c];
  return true;
}



void C_GF2_Matrix::Display()
{
  printf("\n GF2 Matrix %u x %u", nRows, nColumns);
  printf("\n Matrix %ux%u. Located in %lu, freeOnDestroyer = %u", nRows, nColumns, (uint64_t)&Matrix[0], FreeOnDestroyer);
  printf("\n Matrix Rows %ux%u. Located in %lu, freeOnDestroyer = %u", nRows, nColumns_MRows, (uint64_t)&Matrix_Rows[0], FreeOnDestroyer_MRows);
  printf("\n Matrix Cols %ux%u. Located in %lu, freeOnDestroyer = %u", nRows_MCols, nColumns, (uint64_t)&Matrix_Cols[0], FreeOnDestroyer_MCols);



  
  for (uint32_t i = 0; i < nRows; i++)
    {
      printf("\n ");
      for (uint32_t j = 0; j < nColumns; j++)
        printf("%d ", element(i, j));
      //	printf("%d %d = %d     ", i, j, i*nCols+j);//Matrix[i*nCols+j]);
      }
}


void C_GF2_Matrix::Display(uint32_t Rstart, uint32_t Rend, uint32_t Cstart, uint32_t Cend)
  {
    printf("\n GF2 Matrix %u x %u", nRows, nColumns);
    printf("\n Matrix %ux%u. Located in %lu, freeOnDestroyer = %u", nRows, nColumns, (uint64_t)&Matrix[0], FreeOnDestroyer);
    printf("\n Matrix Rows %ux%u. Located in %lu, freeOnDestroyer = %u", nRows, nColumns_MRows, (uint64_t)&Matrix_Rows[0], FreeOnDestroyer_MRows);
    printf("\n Matrix Cols %ux%u. Located in %lu, freeOnDestroyer = %u", nRows_MCols, nColumns, (uint64_t)&Matrix_Cols[0], FreeOnDestroyer_MCols);
    printf("\n Displaying submatrix [%u:%u, %u:%u]", Rstart, Rend, Cstart, Cend);
    if ((Rstart < 1) || (Rstart > Rend) || (Rend > nRows) || (Cstart < 1) || (Cstart > Cend) || (Cend > nColumns))
      throw 6248;
  //printf("\n Matrix %dx%d. Located in %llu, freeOnDestroyer = %d", nRows, nColumns, &Matrix[0], FreeOnDestroyer);
  //printf("\n Matrix Rows %dx%d. Located in %llu, freeOnDestroyer = %d", nRows, nColumns_MRows, &Matrix_Rows[0], FreeOnDestroyer_MRows);
  //printf("\n Matrix Cols %dx%d. Located in %llu, freeOnDestroyer = %d", nRows_MCols, nColumns, &Matrix_Cols[0], FreeOnDestroyer_MCols);
  
      for (uint32_t i = Rstart-1; i < Rend; i++)
	{
	  printf("\n ");
	  for (uint32_t j = Cstart-1; j < Cend; j++)
	    printf("%d ", element(i, j));
	  //	printf("%d %d = %d     ", i, j, i*nCols+j);//Matrix[i*nCols+j]);
	}
  
}


void C_GF2_Matrix::Sync_Matrix2MatrixCols()
{
  if ((Matrix_Cols==nullptr)||(!FreeOnDestroyer_MCols))
    {
      nRows_MCols = ceil(float(nRows)/64.0f);
      Matrix_Cols = new uint64_t[nRows_MCols * nColumns];
      FreeOnDestroyer_MCols = true;
    }
  
  printf("\n Matrix -> MatrixCols");fflush(stdout);
  for (uint32_t r = 0; r<nRows_MCols; r++)
    {
      for (uint32_t c = 0; c<nColumns; c++)
	{
	  Matrix_Cols[r*nColumns+c] = 0;
	}
    }

  uint64_t auxMask;
  for (uint32_t r = 0; r<nRows; r++)
    {
      if (r%64 == 0)
	auxMask = 1;
      else
	auxMask = auxMask*2;
      for (uint32_t c = 0; c<nColumns; c++)
	{
	  Matrix_Cols[(r/64)*nColumns+c] += Matrix[r*nColumns+c] * auxMask;
	}
    }

}

void C_GF2_Matrix::Sync_MatrixCols2Matrix()
{
  if ((Matrix_Cols==nullptr)||(!FreeOnDestroyer_MCols))
    throw 99;
  printf("\n MatrixCols -> Matrix");fflush(stdout);
  uint64_t auxMask;
  for (uint32_t r = 0; r < nRows; r++)
    {
      if (r%64 == 0)
	auxMask = 1;
      else
	auxMask = auxMask*2;
      for (uint32_t c = 0; c < nColumns ; c++)
	{
	  Matrix[r*nColumns+c] = ((Matrix_Cols[(r/64)*nColumns+c]&auxMask)!=0)? 1: 0;
	}
    }
}



void C_GF2_Matrix::Sync_Matrix2MatrixRows()
{
  printf("\n Matrix -> MatrixRows");fflush(stdout);
  if (!FreeOnDestroyer)
    {
      printf("\n ERROR: Matrix not initialized");fflush(stdout);
      throw 411;
    }
  if ((Matrix_Rows==nullptr)||(!FreeOnDestroyer_MRows))
    {
      nColumns_MRows = ceil(float(nColumns)/64.0f);
      Matrix_Rows = new uint64_t[nColumns_MRows * nRows];
      FreeOnDestroyer_MRows = true;
    }
  
  for (uint32_t r = 0; r<nRows; r++)
    {
      for (uint32_t c = 0; c<nColumns_MRows; c++)
	{
	  Matrix_Rows[r*nColumns_MRows+c] = 0;
	}
    }

  uint64_t auxMask;
  for (uint32_t c = 0; c<nColumns; c++)
    {
      if (c%64 == 0)
	auxMask = 1;
      else
	auxMask = auxMask*2;
      for (uint32_t r = 0; r<nRows; r++)
	{
	  Matrix_Rows[r*nColumns_MRows+(c/64)] += Matrix[r*nColumns+c] * auxMask;
	}
    }

}

void C_GF2_Matrix::Sync_MatrixRows2Matrix()
{
  if ((Matrix_Rows==nullptr)||(!FreeOnDestroyer_MRows))
    throw 99;
  printf("\n MatrixRows -> Matrix");fflush(stdout);
  uint64_t auxMask;
  for (uint32_t c = 0; c < nColumns; c++)
    {
      if (c%64 == 0)
	auxMask = 1;
      else
	auxMask = auxMask*2;
      for (uint32_t r = 0; r < nRows ; r++)
	{
	  Matrix[r*nColumns+c] = ((Matrix_Rows[r*nColumns_MRows+(c/64)]&auxMask)!=0)? 1: 0;
	}
    }
}

#endif
