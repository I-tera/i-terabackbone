#include <stdio.h>
#include <string>
#include <cstring>
using namespace std;

#include "C_GF2_Matrix.h"
#include "C_GF2_Matrix.cpp"


int main (void)
{
  string MA_filename("MatrixA.txt");
  string MB_filename("MatrixB.txt");
  string MC_filename("MatrixC.txt");
  string ME_filename("MatrixE.txt");

  C_GF2_Matrix MatrixA, MatrixB, MatrixC, MatrixD, MatrixE;

  MatrixA.LoadFromFile(MA_filename);
  
  printf("\n MatrixA: ");
  MatrixA.Display();
  printf("\n");fflush(stdout);

  printf("\n MatrixD extracted from MatrixA[2:4,2:4]");
  MatrixD = MatrixA.ExtractSubmatrix(2,4,2,4);
  MatrixD.Display();
  fflush(stdout);

  MatrixB.LoadFromFile(MB_filename);
  printf("\n MatrixB: ");
  MatrixB.Display();
  printf("\n");
  fflush(stdout);
  
  MatrixC = MatrixA * MatrixB;
  printf("\n MatrixC: ");
  MatrixC.Display();
  fflush(stdout);

  printf("\n MatrixE: ");
  MatrixE.LoadFromFile(ME_filename);
  MatrixE.Display();
  MatrixE.Sync_Matrix2MatrixCols();
  MatrixE.Sync_MatrixCols2Matrix();
  MatrixE.Display();
  fflush(stdout);

  printf("\n MatrixE: M -> MR -> M");
  MatrixE.Sync_Matrix2MatrixRows();
  MatrixE.Sync_MatrixRows2Matrix();
  MatrixE.Display();
  fflush(stdout);
  
  MatrixC.SaveToFile(MC_filename);
  printf("\n");
}

  
  
