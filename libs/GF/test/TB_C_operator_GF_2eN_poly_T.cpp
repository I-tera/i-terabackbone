//#define D_DEBUGMODE

#include "../C_operator_GF_2eN_poly_T.hpp"
#include "../../json/json.hpp"
#include "../../VectorLogger/C_Vector_Logger_T.h"
#include "../../dynamic_memory/dynamic_memory.hpp"

#define T_P 8
#define T_BE uint32_t
#define T_E UME::SIMD::SIMDVec<T_BE, T_P>
#define D_NTESTS 200
#define D_MAX_ORDER 5

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <fstream>
#include <chrono>

using namespace std;

bool horizontal_and(UME::SIMD::SIMDVecMask<T_P> m0)
{
  bool res = true;
  T_E aux = 0;
  T_BE aux_for_unpack[T_P];
  aux[m0] = 1;
  aux.store(&aux_for_unpack[0]);
  for (int i = 0; i < T_P; i++)
    res = res && (aux_for_unpack[i] == 1);
  return res;
}

bool print_array(T_E a)
{
  T_BE aux_for_unpack[T_P];
  a.store(&aux_for_unpack[0]);
  for (int i= 0; i < T_P; i++)
	printf("%d ", aux_for_unpack[i]);
}

int main(void)
{
  
  //random seed initialization
  srand(time(NULL));

  //test parameters load
  uint32_t GF_Order = 10;
  uint32_t TestRepetitions = 1000;

  char *afa_a, *afa_b, *afa_c, *afa_r1, *afa_r2, *afa_raux, *afa_one, *afa_zero;
  
  T_E aux;
  T_E *a, *b, *c;
  T_E *one, *zero;
  T_BE *denom;
  T_E *r1, *r2, *raux;
  UME::SIMD::SIMDVecMask<T_P> m0;
  
  
  for (GF_Order = 2; GF_Order <= 24; GF_Order ++)
    {
      printf("\n\n TEST for GF2^%d", GF_Order);fflush(stdout);

      C_operator_GF_2eN_poly_T<T_E, T_BE, T_P> GF(GF_Order);
      T_BE GFMask = 1<<GF_Order;

      for (uint32_t i = 0; i < D_NTESTS; i++)
	{
          uint32_t a_size = 1+rand()%D_MAX_ORDER;
          uint32_t b_size = 1+rand()%D_MAX_ORDER;
          uint32_t c_size = 1+rand()%D_MAX_ORDER;
          uint32_t d_size = 1+rand()%D_MAX_ORDER;
          uint32_t one_size = 1;
          uint32_t zero_size = 1;
          uint32_t r1_size = a_size + b_size + c_size + d_size;
          uint32_t r2_size = a_size + b_size + c_size + d_size;
          uint32_t raux_size = a_size + b_size + c_size + d_size;

          afa_a = allocate_aligned_memory<T_E>(a_size, 128, a, "TB.a");
          afa_b = allocate_aligned_memory<T_E>(b_size, 128, b, "TB.b");
          afa_c = allocate_aligned_memory<T_E>(c_size, 128, c, "TB.c");
          afa_one = allocate_aligned_memory<T_E>(1, 128, one, "TB.one");
          afa_zero = allocate_aligned_memory<T_E>(1, 128, zero, "TB.zero");
          afa_r1 = allocate_aligned_memory<T_E>(r1_size, 128, r1, "TB.r1");
          afa_r2 = allocate_aligned_memory<T_E>(r2_size, 128, r2, "TB.r2");
          afa_raux = allocate_aligned_memory<T_E>(raux_size, 128, raux, "TB.raux");
          denom = new T_BE[d_size];
          
          one[0] = 1;
          zero[0] = 0;

          for (uint32_t i = 0; i < a_size; i++)
	      a[i] = 0;
	  for (uint32_t i = 0; i < b_size; i++)
	      b[i] = 0;
	  for (uint32_t i = 0; i < c_size; i++)
	      c[i] = 0;

          for (uint32_t i = 0; i < a_size; i++)
	    a[i] = rand()%GFMask;
          for (uint32_t i = 0; i < b_size; i++)
	    b[i] = rand()%GFMask;
          for (uint32_t i = 0; i < c_size; i++)
	    c[i] = rand()%GFMask;
          for (uint32_t i = 0; i < d_size; i++)
	    denom[i] = rand()%GFMask;
          if (denom[d_size-1] == 0)
            denom[d_size-1] = 1; //guaranteeing that highest order coefficient is not 0 


	  //1: additive closure (grado de suma <= maximo grado)
	  GF.ApB(a, a_size, b, b_size, r1, r1_size);
	  m0 = (GF.get_order(r1, r1_size) <= GF.get_order(a, a_size)) || (GF.get_order(r1, r1_size) <= GF.get_order(b, b_size));
	  if (!horizontal_and(m0))
	    {
		/*T_E aux_order;
		printf("\n A = ");
		GF.display(a, a_size, true);
		printf("\n A order = "); print_array(GF.get_order(a, a_size));
		printf("\n B = ");
		GF.display(b, b_size, true);
		printf("\n B order = "); print_array(GF.get_order(b, a_size));
		printf("\n R = ");
		GF.display(r1, r1_size, true);
		printf("\n R order = "); print_array(GF.get_order(r1, a_size));
		aux_order = 0;
		aux_order[m0] = 99;
		printf("\n bool = "); print_array(aux_order);	*/
		printf(" <---- additive closure failure "); return 1;}
	  
	
	  //2: additive identity
          GF.ApB(a, a_size, zero, zero_size, r1, r1_size);
          GF.ApB(zero, zero_size, a, a_size, r2, r2_size);
	  m0 = GF.comp_AB(a, a_size, r1, r1_size) && GF.comp_AB(r2, r2_size, a, a_size);
	  if (!horizontal_and(m0))
	    {
		/*printf("\n A = ");
		GF.display(a, a_size, true);
		printf("\n R1 = ");
		GF.display(r1, r1_size, true);
		printf("\n R2 = ");
		GF.display(r2, r2_size, true);*/
		printf(" <---- additive identity failure "); return 2;}
	 	  
	  //3: additive inverse
          GF.ApB(a, a_size, a, a_size, r1, r1_size);
	  m0 = GF.is_zero(r1, r1_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive inverse failure "); return 3;}
	 
	  //4: additive associativity
	  GF.ApB(a, a_size, b, b_size, r1, r1_size);
          GF.ApB(r1, r1_size, c, c_size, r1, r1_size);
          GF.ApB(b, b_size, c, c_size, r2, r2_size);
          GF.ApB(a, a_size, r2, r2_size, r2, r2_size);
	  m0 = GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive associativity failure "); return 4;}
	  
	  //5: additive commutativity
          GF.ApB(a, a_size, b, b_size, r1, r1_size);
          GF.ApB(b, b_size, a, a_size, r2, r2_size);
	  m0 = GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive commutativity failure "); return 5;}
          
	  //6: grado de multiplicacion = suma de grados
          GF.AtB(a, a_size, b, b_size, r1, r1_size);
	  m0 = (GF.get_order(r1, r1_size) == (GF.get_order(a, a_size) + GF.get_order(b, b_size))) || ((GF.get_order(r1, r1_size) == 0) && (GF.is_zero(a, a_size) || GF.is_zero(b, b_size)));
	  if (!horizontal_and(m0))
	    {
		printf(" <---- multiplication degree failure "); return 6;}
          
	  //7: grado de division = resta de grados
	  /*m0 = (((A/B).get_order() == (A.get_order() - B.get_order()))  ||
		(((A/B).get_order() == 0) && (A.get_order() < B.get_order())) ||
		B.is_zero());
	  if (!horizontal_and(m0))
          {printf(" <---- division degree failure "); return 7;}*/

          //
	  //8: grado del resto < grado del denominador
          GF.ArB(a, a_size, denom, d_size, r1, r1_size);
	  m0 = (GF.get_order(r1, r1_size) < GF.get_order(denom, d_size)) || (GF.get_order(denom, d_size) == 0);
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder degree failure "); return 8;}
	  
	  //9: A*1 = 1*A = A
          GF.AtB(a, a_size, one, one_size, r1, r1_size);
          GF.AtB(one, one_size, a, a_size, r2, r2_size);
	  m0 = GF.comp_AB(a, a_size, r1, r1_size) && GF.comp_AB(a, a_size, r2, r2_size);;
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative identity failure "); return 9;}
	  
	  //10: A*0 = 0*A = 0
          GF.AtB(a, a_size, zero, zero_size, r1, r1_size);
          GF.AtB(zero, zero_size, a, a_size, r2, r2_size);
	  m0 = GF.comp_AB(r1, r1_size, zero, zero_size) && GF.comp_AB(zero, zero_size, r2, r2_size);;
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative zero failure "); return 10;}
	  
	  //11: A*B = B*A
          GF.AtB(a, a_size, b, b_size, r1, r1_size);
          GF.AtB(b, b_size, a, a_size, r2, r2_size);
	  m0 = GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative commutativity failure "); return 11;}
	  /*
	  //12: A*(-B) = (-A)*B = -(A*B)
	  m0 = (A*(-B) == (-A)*B) && (A*(-B) == -(A*B));
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative * -1 failure "); return 12;}
          */
          
	  //13: (A*B)*C = A*(B*C)
          GF.AtB(a, a_size, b, b_size, raux, raux_size);
          GF.AtB(raux, raux_size, c, c_size, r1, r1_size);
          GF.AtB(b, b_size, c, c_size, raux, raux_size);
          GF.AtB(a, a_size, raux, raux_size, r2, r2_size);
	  m0 =  GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative associativity failure "); return 13;}
	  
	  //14: A*(B+C) = A*B + A*C
          GF.ApB(b, b_size, c, c_size, raux, raux_size);
          GF.AtB(a, a_size, raux, raux_size, r1, r1_size);
          GF.AtB(a, a_size, b, b_size, raux, raux_size);
          GF.AtB(a, a_size, c, c_size, r2, r2_size);
          GF.ApB(raux, raux_size, r2, r2_size, r2, r2_size);
	  m0 =  GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative distributivitivity over addition failure "); return 14;}
	  
	  //15: A*B % A = A*B%B = 0
          GF.AtB(denom, d_size, b, b_size, raux, raux_size);
          GF.ArB(raux, raux_size, denom, d_size, r1, r1_size);
          GF.AtB(a, a_size, denom, d_size, raux, raux_size);
          GF.ArB(raux, raux_size, denom, d_size, r2, r2_size);
	  m0 = GF.is_zero(r1, r1_size) && GF.is_zero(r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder of multiplication failure "); return 15;}
	  
	  //16: (A*B+C)%A = C%A
          GF.AtB(denom, d_size, b, b_size, raux, raux_size);
          GF.ApB(raux, raux_size, c, c_size, raux, raux_size);
          GF.ArB(raux, raux_size, denom, d_size, r1, r1_size);
          GF.ArB(c, c_size, denom, d_size, r2, r2_size);
          m0 =  GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder ((A*B+C)%%A != C%%A)"); return 16;}
	  
          /*
	  //17: A*B/B = A
	  m0 = ((A*B)/B == A) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division ((A*B)/B != A)"); return 17;}
          
	  //18: A*B/A = B
	  m0 = ((A*B)/A == B) || (A == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division ((A*B)/A != B)"); return 18;}
	  
	  //19: B/B = 1
	  m0 = (B/B == ONE) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (B/B != ONE)"); return 19;}
          */
	  //20: B%B = 0
          GF.ArB(denom, d_size, denom, d_size, r1, r1_size);
	  m0 = GF.is_zero(r1, r1_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (B%%B != ZERO)"); return 20;}
	  
	  //21: (A+B)%C = (A%C)+(B%C);
          GF.ApB(a, a_size, b, b_size, raux, raux_size);
          GF.ArB(raux, raux_size, denom, d_size, r1, r1_size);
          GF.ArB(a, a_size, denom, d_size, raux, raux_size);
          GF.ArB(b, b_size, denom, d_size, r2, r2_size);
          GF.ApB(r2, r2_size, raux, raux_size, r2, r2_size);
	  m0 = GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (A+B)%%C == (A%%C)+(B%%C)"); return 21;}
	  
	  //22: (A*B)%C = ((A%C)*(B%C))%C;
          GF.ArB(a, a_size, denom, d_size, raux, raux_size);
          GF.ArB(b, b_size, denom, d_size, r1, r1_size);
          GF.AtB(raux, raux_size, r1, r1_size, r2, r2_size);
          GF.ArB(r2, r2_size, denom, d_size, r1, r1_size);
          
          GF.AtB(a, a_size, b, b_size, raux, raux_size);
          GF.ArB(raux, raux_size, denom, d_size, r2, r2_size);
	  m0 = GF.comp_AB(r1, r1_size, r2, r2_size);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (A*B)%%C == (((A%%C)*(B%%C))%%C)"); return 22;}

	



          delete[] afa_a;
          delete[] afa_b;
          delete[] afa_c;
          delete[] denom;
          delete[] afa_one;
          delete[] afa_zero;
          delete[] afa_r1;
          delete[] afa_r2;
          delete[] afa_raux;
        }
    }
      
  
  /*
  for (GF_Order = 2; GF_Order <= 24; GF_Order ++)
    {
      printf("\n\n TEST for GF2^%d", GF_Order);fflush(stdout);
      a.change_GF_order(GF_Order);
      
      
      uint32_t GFMask = 1<<GF_Order;
      printf("(GF(2^%d) = 0x%X)", e.get_order(), e.get_poly());
      for (uint32_t i = 0; i < 200; i++)
	{
	  for (uint32_t i = 0; i <= D_OPERAND_POLY_ORDER_A; i++)
	      A[i] = 0;
	  for (uint32_t i = 0; i <= D_OPERAND_POLY_ORDER_B; i++)
	      B[i] = 0;
	  for (uint32_t i = 0; i <= D_OPERAND_POLY_ORDER_C; i++)
	      C[i] = 0;
	  order = rand()%(D_OPERAND_POLY_ORDER_A+1);
	  for (uint32_t i = 0; i <= order; i++)
	    A[i] = rand()%GFMask;
	  order = rand()%D_OPERAND_POLY_ORDER_B+1;
	  for (uint32_t i = 0; i <= order; i++)
	    B[i] = rand()%GFMask;
	  order = rand()%D_OPERAND_POLY_ORDER_C+1;
	  for (uint32_t i = 0; i <= order; i++)
	    C[i] = rand()%GFMask;
	  
	  a = rand() % GFMask;
	  b = rand() % GFMask;
	  c = rand() % GFMask;

	  //printf("\n A(x) = ");A.display();aux=A.get_order();printf("(order = ");Logger_1.Add_Log_Entry(&aux);printf(")");
	  //printf("\n B(x) = ");B.display();aux=B.get_order();printf("(order = ");Logger_1.Add_Log_Entry(&aux);printf(")");
	  //printf("\n C(x) = ");C.display();aux=C.get_order();printf("(order = ");Logger_1.Add_Log_Entry(&aux);printf(")");

	  //1: additive closure (grado de suma <= maximo grado)
	  m0 = ((A+B).get_order() <= A.get_order()) || ((A+B).get_order() <= B.get_order());
	  if (!horizontal_and(m0))
	    {printf(" <---- additive closure failure "); return 1;}
	  
	  //2: additive identity
	  m0 = ((A + ZERO) == (A)) && ((ZERO + A) == A);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive identity failure "); return 2;}
	  	  
	  //3: additive inverse
	  m0 = ((A - A) == (ZERO)) && (((-A) + A) == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive inverse failure "); return 3;}
	  
	  //4: additive associativity
	  m0 = (A+(B+C)) == ((A+B)+C);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive associativity failure "); return 4;}
	  
	  //5: additive commutativity
	  m0 = (A+B) == (B+A);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive commutativity failure "); return 5;}

	  //6: grado de multiplicacion = suma de grados
	  m0 = ((A*B).get_order() == (A.get_order() + B.get_order())) || (A == ZERO) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplication degree failure "); return 6;}

	  //7: grado de division = resta de grados
	  m0 = (((A/B).get_order() == (A.get_order() - B.get_order()))  ||
		(((A/B).get_order() == 0) && (A.get_order() < B.get_order())) ||
		B.is_zero());
	  if (!horizontal_and(m0))
	    {printf(" <---- division degree failure "); return 7;}
	  
	  //8: grado del resto < grado del denominador
	  m0 = (((A%B).get_order() < (B.get_order())) ||
		B.is_zero() || ((A%B).get_order() == 0));
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder degree failure "); return 8;}
	  
	  //9: A*1 = 1*A = A
	  m0 = (A*ONE == A) && (ONE*A == A);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative identity failure "); return 9;}
	  
	  //10: A*0 = 0*A = 0
	  m0 = (A*ZERO == ZERO) && (ZERO*A == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative zero failure "); return 10;}
	  
	  //11: A*B = B*A
	  m0 = (A*B == B*A);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative commutativity failure "); return 11;}
	  
	  //12: A*(-B) = (-A)*B = -(A*B)
	  m0 = (A*(-B) == (-A)*B) && (A*(-B) == -(A*B));
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative * -1 failure "); return 12;}
	  
	  //13: (A*B)*C = A*(B*C)
	  m0 = (A*B)*C == A*(B*C);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative associativity failure "); return 13;}
	  
	  //14: A*(B+C) = A*B + A*C
	  m0 = A*(B+C) == A*B + A*C;
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative distributivitivity over addition failure "); return 14;}
	  
	  //15: A*B % A = A*B%B = 0
	  m0 = ((A*B)%A == ZERO) && ((A*B)%B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder of multiplication failure "); return 15;}
	  
	  //16: (A*B+C)%A = C%A
	  m0 = ((A*B+C)%A == (C%A));
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder ((A*B+C)%%A != C%%A)"); return 16;}
	  

	  //17: A*B/B = A
	  m0 = ((A*B)/B == A) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division ((A*B)/B != A)"); return 17;}
	  
	  //18: A*B/A = B
	  m0 = ((A*B)/A == B) || (A == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division ((A*B)/A != B)"); return 18;}
	  
	  //19: B/B = 1
	  m0 = (B/B == ONE) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (B/B != ONE)"); return 19;}
	  
	  //20: B%B = 0
	  m0 = (B%B == ZERO) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (B%%B != ZERO)"); return 20;}
	  
	  //21: (A+B)%C = (A%C)+(B%C);
	  m0 = ((A+B)%C == (A%C)+(B%C)) || (C == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (A+B)%%C == (A%%C)+(B%%C)"); return 21;}
	  
	  //22: (A*B)%C = ((A%C)*(B%C))%C;
	  m0 = ((A*B)%C == (((A%C)*(B%C))%C)) || (C == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (A*B)%%C == (((A%%C)*(B%%C))%%C)"); return 22;}

  */
	  


	  /*
	  //addition
	  //F1: closure under addition
	  r1 = a + b;
	  m0 = r1.get_element() < GFMask;
	  if (!horizontal_and(m0))
	    {printf(" <---- closure under addition failure "); return 1;}

	  //F2: additive identity
	  m0 = (a + zero == a) && (zero + a == a);
	  if (!horizontal_and(m0)){printf(" <---- additive identity failure "); return 2;}

	  //F3: additive inverse
	  temp = -a;
	  m0 = (a + temp == zero) && (a-a == zero);
	  if (!horizontal_and(m0)){printf(" <---- additive inverse failure   "); return 3;}

	  //F4: associativity
	  m0 = ((a + b) + c) == (a + (b + c));
	  if (!horizontal_and(m0)){printf(" <---- additive associativity failure   "); return 4;}

	  //F5: commutativity
	  m0 = (a + b) == (b + a);
	  if (!horizontal_and(m0)){printf(" <---- additive commutativity failure   "); return 5;}

	  //F6: closure under multiplication
	  r1 = a * b;
	  m0 = r1.get_element() < GFMask;
	  if (!horizontal_and(m0)){printf(" <---- closure under multiplication failure "); return 6;}
      
	  //F7: multiplicative identity
	  m0 = (a * one == a) && (one * a == a);
	  if (!horizontal_and(m0)){printf(" <---- multiplicative identity failure "); return 7;}
      
	  //F8: multiplicative inverse
	  temp = a;
	  temp = temp.invert();
	  m0 = (a == 0) || ((a * temp == one) && (a/a == one));
	  if (!horizontal_and(m0)){printf(" <---- multiplicative inverse failure   ");return 8;}

	  //F9: multiplicative associativity
	  m0 = ((a * b) * c) == (a * (b * c));
	  if (!horizontal_and(m0)){printf(" <---- multiplicative associativity failure   "); return 9;}
      
	  //F10: multiplicative commutativity
	  m0 = (a * b) == (b * a);
	  if (!horizontal_and(m0)){printf(" <---- multiplicative commutativity failure   "); return 10;}

	  //F11: multiplicative distribution over addition
	  m0 = a * (b + c) == a * b + a * c;
	  if (!horizontal_and(m0)){printf(" <---- multiplicative distribution failure   "); return 11;}
	  */
  /*
	}
      printf(" passed");
    }
*/
  
  
  printf("\n");
  return 0;
}



