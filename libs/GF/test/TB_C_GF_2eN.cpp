//#define D_DEBUGMODE

#include "../C_GF_2eN_T.hpp"
#include "../../json/json.hpp"
#include "../../VectorLogger/C_Vector_Logger_T.h"

#define D_PARALLELISM 8
#define D_BASEELEMENTTYPE uint32_t
#define D_ELEMENTTYPE UME::SIMD::SIMDVec<D_BASEELEMENTTYPE, D_PARALLELISM>
#define D_GF_ORDER 8

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <fstream>
#include <chrono>

using namespace std;

bool horizontal_and(UME::SIMD::SIMDVecMask<D_PARALLELISM> m0)
{
  bool res = true;
  D_ELEMENTTYPE aux = 0;
  D_BASEELEMENTTYPE aux_for_unpack[D_PARALLELISM];
  aux[m0] = 1;
  aux.store(&aux_for_unpack[0]);
  for (int i = 0; i < D_PARALLELISM; i++)
    res = res && (aux_for_unpack[i] == 1);
  return res;
}

int main(void)
{
  
  C_Vector_Logger_T<D_ELEMENTTYPE> Logger_input;
  Logger_input.Initialize("screen",   nullptr, "input_stream" , 1, 20, 0, false, false, false);
  
  //random seed initialization
  srand(time(NULL));

  //test parameters load
  uint32_t GF_Order = D_GF_ORDER;
  uint32_t TestRepetitions = 1000;
  

  C_GF_2eN_T<D_ELEMENTTYPE,D_BASEELEMENTTYPE,D_PARALLELISM>   a(3);
  
  for (GF_Order = 3; GF_Order <= 24; GF_Order ++)
    {
      printf("\n\n TEST for GF2^%d", GF_Order);fflush(stdout);
      a.change_GF_order(GF_Order);
      C_GF_2eN_T<D_ELEMENTTYPE,D_BASEELEMENTTYPE,D_PARALLELISM>   b, c, d, e, f, temp, r1, r2, one, zero;
      UME::SIMD::SIMDVecMask<D_PARALLELISM> m0;
      D_ELEMENTTYPE aux_a, aux_b, aux_c, aux_d, aux_e, aux_f, aux_r1, aux_r2, aux_check;

      one = 1;
      zero = 0;
      
      uint32_t GFMask = 1<<GF_Order;
      printf("(irreducible polynomial for GF(2^%d) = 0x%X)", e.get_order(), e.get_poly());
      for (uint32_t i = 0; i < TestRepetitions; i++)
	{
	  a = rand() % GFMask;
	  b = rand() % GFMask;
	  c = rand() % GFMask;
	  d = rand() % GFMask;
	  e = rand() % GFMask;
	  f = rand() % GFMask;

	  //addition
	  //F1: closure under addition
	  r1 = a + b;
	  m0 = r1.get_element() < GFMask;
	  if (!horizontal_and(m0))
	    {printf(" <---- closure under addition failure "); return 1;}

	  //F2: additive identity
	  m0 = (a + zero == a) && (zero + a == a);
	  if (!horizontal_and(m0)){printf(" <---- additive identity failure "); return 2;}

	  //F3: additive inverse
	  temp = -a;
	  m0 = (a + temp == zero) && (a-a == zero);
	  if (!horizontal_and(m0)){printf(" <---- additive inverse failure   "); return 3;}

	  //F4: associativity
	  m0 = ((a + b) + c) == (a + (b + c));
	  if (!horizontal_and(m0)){printf(" <---- additive associativity failure   "); return 4;}

	  //F5: commutativity
	  m0 = (a + b) == (b + a);
	  if (!horizontal_and(m0)){printf(" <---- additive commutativity failure   "); return 5;}

	  //F6: closure under multiplication
	  r1 = a * b;
	  m0 = r1.get_element() < GFMask;
	  if (!horizontal_and(m0)){printf(" <---- closure under multiplication failure "); return 1;}
      
	  //F7: multiplicative identity
	  m0 = (a * one == a) && (one * a == a);
	  if (!horizontal_and(m0)){printf(" <---- multiplicative identity failure "); return 7;}
      
	  //F8: multiplicative inverse
	  temp = a;
	  temp = temp.invert();
	  m0 = (a == 0) || ((a * temp == one) && (a/a == one));
	  if (!horizontal_and(m0)){printf(" <---- multiplicative inverse failure   ");return 8;}

	  //F9: multiplicative associativity
	  m0 = ((a * b) * c) == (a * (b * c));
	  if (!horizontal_and(m0)){printf(" <---- multiplicative associativity failure   "); return 9;}
      
	  //F10: multiplicative commutativity
	  m0 = (a * b) == (b * a);
	  if (!horizontal_and(m0)){printf(" <---- multiplicative commutativity failure   "); return 10;}

	  //F11: multiplicative distribution over addition
	  m0 = a * (b + c) == a * b + a * c;
	  if (!horizontal_and(m0)){printf(" <---- multiplicative distribution failure   "); return 11;}
      
	}
      printf(" passed");
    }

  
  
  printf("\n");
  return 0;
}



