//#define D_DEBUGMODE

#include "../C_GF_2eN_T.hpp"
#include "../C_GF_2eN_poly_T.hpp"
#include "../../json/json.hpp"
#include "../../VectorLogger/C_Vector_Logger_T.h"

#define D_PARALLELISM 8
#define D_BASEELEMENTTYPE uint32_t
#define D_ELEMENTTYPE UME::SIMD::SIMDVec<D_BASEELEMENTTYPE, D_PARALLELISM>
#define D_GF_ORDER 8
#define D_OPERAND_POLY_ORDER_A 6
#define D_OPERAND_POLY_ORDER_B 9
#define D_OPERAND_POLY_ORDER_C 7
#define D_OPERAND_POLY_ORDER_Z 3
#define D_OPERAND_POLY_ORDER_O 1

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <fstream>
#include <chrono>

using namespace std;

bool horizontal_and(UME::SIMD::SIMDVecMask<D_PARALLELISM> m0)
{
  bool res = true;
  D_ELEMENTTYPE aux = 0;
  D_BASEELEMENTTYPE aux_for_unpack[D_PARALLELISM];
  aux[m0] = 1;
  aux.store(&aux_for_unpack[0]);
  for (int i = 0; i < D_PARALLELISM; i++)
    res = res && (aux_for_unpack[i] == 1);
  return res;
}

int main(void)
{
  
  //random seed initialization
  srand(time(NULL));

  //test parameters load
  uint32_t GF_Order = D_GF_ORDER;
  uint32_t TestRepetitions = 1000;
  
  D_ELEMENTTYPE aux;
  C_GF_2eN_T<D_ELEMENTTYPE,D_BASEELEMENTTYPE,D_PARALLELISM>   a(8);
  C_GF_2eN_T<D_ELEMENTTYPE,D_BASEELEMENTTYPE,D_PARALLELISM>   b, c, d, e, f, temp, r1, r2, one, zero;
  UME::SIMD::SIMDVecMask<D_PARALLELISM> m0;
  D_ELEMENTTYPE aux_a, aux_b, aux_c, aux_d, aux_e, aux_f, aux_r1, aux_r2, aux_check;
  C_GF_2eN_poly_T<D_ELEMENTTYPE,D_BASEELEMENTTYPE,D_PARALLELISM>   A(D_OPERAND_POLY_ORDER_A), B(D_OPERAND_POLY_ORDER_B), C(D_OPERAND_POLY_ORDER_C), ONE(D_OPERAND_POLY_ORDER_O), ZERO(D_OPERAND_POLY_ORDER_Z);
  uint32_t order;
  one = 1;
  zero = 0;
  ONE[0] = 1;
  
  
  for (GF_Order = 2; GF_Order <= 24; GF_Order ++)
    {
      printf("\n\n TEST for GF2^%d", GF_Order);fflush(stdout);
      a.change_GF_order(GF_Order);
      
      
      uint32_t GFMask = 1<<GF_Order;
      printf("(GF(2^%d) = 0x%X)", e.get_order(), e.get_poly());
      for (uint32_t i = 0; i < 200; i++)
	{
	  for (uint32_t i = 0; i <= D_OPERAND_POLY_ORDER_A; i++)
	      A[i] = 0;
	  for (uint32_t i = 0; i <= D_OPERAND_POLY_ORDER_B; i++)
	      B[i] = 0;
	  for (uint32_t i = 0; i <= D_OPERAND_POLY_ORDER_C; i++)
	      C[i] = 0;
	  order = rand()%(D_OPERAND_POLY_ORDER_A+1);
	  for (uint32_t i = 0; i <= order; i++)
	    A[i] = rand()%GFMask;
	  order = rand()%D_OPERAND_POLY_ORDER_B+1;
	  for (uint32_t i = 0; i <= order; i++)
	    B[i] = rand()%GFMask;
	  order = rand()%D_OPERAND_POLY_ORDER_C+1;
	  for (uint32_t i = 0; i <= order; i++)
	    C[i] = rand()%GFMask;
	  
	  a = rand() % GFMask;
	  b = rand() % GFMask;
	  c = rand() % GFMask;

	  //printf("\n A(x) = ");A.display();aux=A.get_order();printf("(order = ");Logger_1.Add_Log_Entry(&aux);printf(")");
	  //printf("\n B(x) = ");B.display();aux=B.get_order();printf("(order = ");Logger_1.Add_Log_Entry(&aux);printf(")");
	  //printf("\n C(x) = ");C.display();aux=C.get_order();printf("(order = ");Logger_1.Add_Log_Entry(&aux);printf(")");

	  //1: additive closure (grado de suma <= maximo grado)
	  m0 = ((A+B).get_order() <= A.get_order()) || ((A+B).get_order() <= B.get_order());
	  if (!horizontal_and(m0))
	    {printf(" <---- additive closure failure "); return 1;}
	  
	  //2: additive identity
	  m0 = ((A + ZERO) == (A)) && ((ZERO + A) == A);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive identity failure "); return 2;}
	  	  
	  //3: additive inverse
	  m0 = ((A - A) == (ZERO)) && (((-A) + A) == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive inverse failure "); return 3;}
	  
	  //4: additive associativity
	  m0 = (A+(B+C)) == ((A+B)+C);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive associativity failure "); return 4;}
	  
	  //5: additive commutativity
	  m0 = (A+B) == (B+A);
	  if (!horizontal_and(m0))
	    {printf(" <---- additive commutativity failure "); return 5;}

	  //6: grado de multiplicacion = suma de grados
	  m0 = ((A*B).get_order() == (A.get_order() + B.get_order())) || (A == ZERO) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplication degree failure "); return 6;}

	  //7: grado de division = resta de grados
	  m0 = (((A/B).get_order() == (A.get_order() - B.get_order()))  ||
		(((A/B).get_order() == 0) && (A.get_order() < B.get_order())) ||
		B.is_zero());
	  if (!horizontal_and(m0))
	    {printf(" <---- division degree failure "); return 7;}
	  
	  //8: grado del resto < grado del denominador
	  m0 = (((A%B).get_order() < (B.get_order())) ||
		B.is_zero() || ((A%B).get_order() == 0));
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder degree failure "); return 8;}
	  
	  //9: A*1 = 1*A = A
	  m0 = (A*ONE == A) && (ONE*A == A);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative identity failure "); return 9;}
	  
	  //10: A*0 = 0*A = 0
	  m0 = (A*ZERO == ZERO) && (ZERO*A == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative zero failure "); return 10;}
	  
	  //11: A*B = B*A
	  m0 = (A*B == B*A);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative commutativity failure "); return 11;}
	  
	  //12: A*(-B) = (-A)*B = -(A*B)
	  m0 = (A*(-B) == (-A)*B) && (A*(-B) == -(A*B));
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative * -1 failure "); return 12;}
	  
	  //13: (A*B)*C = A*(B*C)
	  m0 = (A*B)*C == A*(B*C);
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative associativity failure "); return 13;}
	  
	  //14: A*(B+C) = A*B + A*C
	  m0 = A*(B+C) == A*B + A*C;
	  if (!horizontal_and(m0))
	    {printf(" <---- multiplicative distributivitivity over addition failure "); return 14;}
	  
	  //15: A*B % A = A*B%B = 0
	  m0 = ((A*B)%A == ZERO) && ((A*B)%B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder of multiplication failure "); return 15;}
	  
	  //16: (A*B+C)%A = C%A
	  m0 = ((A*B+C)%A == (C%A));
	  if (!horizontal_and(m0))
	    {printf(" <---- remainder ((A*B+C)%%A != C%%A)"); return 16;}
	  

	  //17: A*B/B = A
	  m0 = ((A*B)/B == A) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division ((A*B)/B != A)"); return 17;}
	  
	  //18: A*B/A = B
	  m0 = ((A*B)/A == B) || (A == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division ((A*B)/A != B)"); return 18;}
	  
	  //19: B/B = 1
	  m0 = (B/B == ONE) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (B/B != ONE)"); return 19;}
	  
	  //20: B%B = 0
	  m0 = (B%B == ZERO) || (B == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (B%%B != ZERO)"); return 20;}
	  
	  //21: (A+B)%C = (A%C)+(B%C);
	  m0 = ((A+B)%C == (A%C)+(B%C)) || (C == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (A+B)%%C == (A%%C)+(B%%C)"); return 21;}
	  
	  //22: (A*B)%C = ((A%C)*(B%C))%C;
	  m0 = ((A*B)%C == (((A%C)*(B%C))%C)) || (C == ZERO);
	  if (!horizontal_and(m0))
	    {printf(" <---- division (A*B)%%C == (((A%%C)*(B%%C))%%C)"); return 22;}


	  


	  /*
	  //addition
	  //F1: closure under addition
	  r1 = a + b;
	  m0 = r1.get_element() < GFMask;
	  if (!horizontal_and(m0))
	    {printf(" <---- closure under addition failure "); return 1;}

	  //F2: additive identity
	  m0 = (a + zero == a) && (zero + a == a);
	  if (!horizontal_and(m0)){printf(" <---- additive identity failure "); return 2;}

	  //F3: additive inverse
	  temp = -a;
	  m0 = (a + temp == zero) && (a-a == zero);
	  if (!horizontal_and(m0)){printf(" <---- additive inverse failure   "); return 3;}

	  //F4: associativity
	  m0 = ((a + b) + c) == (a + (b + c));
	  if (!horizontal_and(m0)){printf(" <---- additive associativity failure   "); return 4;}

	  //F5: commutativity
	  m0 = (a + b) == (b + a);
	  if (!horizontal_and(m0)){printf(" <---- additive commutativity failure   "); return 5;}

	  //F6: closure under multiplication
	  r1 = a * b;
	  m0 = r1.get_element() < GFMask;
	  if (!horizontal_and(m0)){printf(" <---- closure under multiplication failure "); return 6;}
      
	  //F7: multiplicative identity
	  m0 = (a * one == a) && (one * a == a);
	  if (!horizontal_and(m0)){printf(" <---- multiplicative identity failure "); return 7;}
      
	  //F8: multiplicative inverse
	  temp = a;
	  temp = temp.invert();
	  m0 = (a == 0) || ((a * temp == one) && (a/a == one));
	  if (!horizontal_and(m0)){printf(" <---- multiplicative inverse failure   ");return 8;}

	  //F9: multiplicative associativity
	  m0 = ((a * b) * c) == (a * (b * c));
	  if (!horizontal_and(m0)){printf(" <---- multiplicative associativity failure   "); return 9;}
      
	  //F10: multiplicative commutativity
	  m0 = (a * b) == (b * a);
	  if (!horizontal_and(m0)){printf(" <---- multiplicative commutativity failure   "); return 10;}

	  //F11: multiplicative distribution over addition
	  m0 = a * (b + c) == a * b + a * c;
	  if (!horizontal_and(m0)){printf(" <---- multiplicative distribution failure   "); return 11;}
	  */
	}
      printf(" passed");
    }

  
  
  printf("\n");
  return 0;
}



