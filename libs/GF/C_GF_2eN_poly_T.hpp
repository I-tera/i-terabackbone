#ifndef C_GF_2eN_POLY_T_H
#define C_GF_2eN_POLY_T_H

#include "c_vector_logger_t.hpp"
#include "UMESimd.h"
#include "dynamic_memory.hpp"
#include <stdio.h>
#include <string>
#include <cstring>
#include <math.h>
#include "C_GF_2eN_T.hpp"



template <class T_E, class T_BE, uint32_t T_P>
class C_GF_2eN_poly_T
{
private:
  uint32_t reserved_order = 0;
  C_GF_2eN_T<T_E, T_BE, T_P>  *poly = nullptr;
  char *aux_for_alloc_0 = nullptr;

public:
  C_GF_2eN_poly_T(){poly = nullptr;}
  C_GF_2eN_poly_T(uint32_t i_reserved_order){Initialize(i_reserved_order);}
  void Initialize(uint32_t i_reserved_order){Destroy(); reserved_order = i_reserved_order;aux_for_alloc_0 = allocate_aligned_memory<C_GF_2eN_T<T_E, T_BE, T_P>>(reserved_order+1, 128, poly, "C_GF_2eN_poly_T.poly");}
  void Destroy(){if (poly != nullptr) {delete aux_for_alloc_0; poly = nullptr;}}
  inline C_GF_2eN_poly_T  operator +  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T  operator -  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T  operator -  ();
  inline C_GF_2eN_poly_T  operator *  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T  operator /  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T  operator %  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T& operator =  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T& operator =  (const T_E &b);
  inline UME::SIMD::SIMDVecMask<T_P> operator == (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b);
  inline C_GF_2eN_poly_T& load(const T_E *b, uint32_t n_elements);
  inline C_GF_2eN_poly_T& load(const T_E *b, uint32_t n_elements, uint32_t times_XeN);
  inline void download(T_E *b);
  inline UME::SIMD::SIMDVecMask<T_P> is_zero();
  inline void zero(){for (uint32_t i = 0; i<= reserved_order; i++){poly[i] = 0;}}
  uint32_t get_reserved_order() const {return reserved_order;}
  T_E get_order() const {T_E order = 0; for (int32_t i=0; i<=reserved_order; i++){order[!(poly[i] == (T_BE)0)] = i;} return order;}
  C_GF_2eN_T<T_E, T_BE, T_P>& operator[](uint32_t index) const {if (index <= reserved_order){return poly[index];}throw std::runtime_error("C_GF_2eN_poly_T error: [] operand used with argument higher than reserved order\n");}
  C_GF_2eN_T<T_E, T_BE, T_P> operator()(C_GF_2eN_T<T_E, T_BE, T_P> &indet);
  void display() const;
};

template <class T_E, class T_BE, uint32_t T_P>
C_GF_2eN_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator()(C_GF_2eN_T<T_E, T_BE, T_P> &indet)
{
  C_GF_2eN_T<T_E, T_BE, T_P> res, indet_ei;
  res = 0;
  indet_ei = 1;
  for (uint32_t i = 0; i <= reserved_order; i++)
    {
      res = res + poly[i]*indet_ei;
      indet_ei = indet_ei * indet;
    }
  return res;
}
	

  template <class T_E, class T_BE, uint32_t T_P>
  C_GF_2eN_poly_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator + (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
  {
    uint32_t matching_order = std::min(reserved_order, b.get_reserved_order());
    uint32_t res_reserved_order = std::max(reserved_order, b.get_reserved_order());
    C_GF_2eN_poly_T res(res_reserved_order);
    uint32_t i;
    for (i = 0; i <= matching_order; i++)
      res[i] = poly[i] + b[i];
    for (i=matching_order+1; i <= reserved_order; i++)
      res[i] = poly[i];
    for (i=matching_order+1; i <= b.get_reserved_order(); i++)
      res[i] = b[i];
    return res;
  }
  
  template <class T_E, class T_BE, uint32_t T_P>
  C_GF_2eN_poly_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator - (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
  {
    uint32_t res_reserved_order = std::max(reserved_order, b.get_reserved_order());
    C_GF_2eN_poly_T res(res_reserved_order);
    for (uint32_t i = 0; i <= res_reserved_order; i++)
      res[i] = poly[i] + b[i];
    return res;
  }


  template <class T_E, class T_BE, uint32_t T_P>
  C_GF_2eN_poly_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator - ()
  {
    uint32_t res_reserved_order = reserved_order;
    C_GF_2eN_poly_T res(res_reserved_order);
    for (uint32_t i = 0; i <= res_reserved_order; i++)
      res[i] = poly[i];
    return res;
  }

  template <class T_E, class T_BE, uint32_t T_P>
  C_GF_2eN_poly_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator * (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
  {
     uint32_t res_reserved_order = reserved_order + b.get_reserved_order();
    C_GF_2eN_poly_T res(res_reserved_order);
    for (uint32_t i = 0; i <= res_reserved_order; i++)
      res[i] = 0;
    for (uint32_t i = 0; i <= reserved_order; i++)
      {
	for (uint32_t j = 0; j <= b.get_reserved_order(); j++)
	  {
	    res[i+j] = res[i+j] + poly[i]*b[j];
	  }
      }  
    return res;
  }

  template <class T_E, class T_BE, uint32_t T_P>
  C_GF_2eN_poly_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator / (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
  {
    //fflush(stdout);
    //printf("\n\n Division start");
    UME::SIMD::SIMDVecMask<T_P> m0 = true;
    int32_t        b_reserved_order = b.get_reserved_order();
    int32_t        res_reserved_order = reserved_order;
    C_GF_2eN_poly_T aligned_denom(b_reserved_order);
    T_E             aux_shift_back = b_reserved_order;
    T_E             aux_remaining_el = get_reserved_order() - b.get_order() + 1;
    aux_remaining_el[b.get_order() > get_order()] = 0;
    T_E             temporal;

    
    //printf("\n ORIG DENOM:    ");
    //b.display();
    for (uint32_t i = 0; i <= b_reserved_order; i++) //lleno denominador pre alineado
      aligned_denom[i] = b[i];
    
    for (uint32_t i = 0; i <= b_reserved_order; i++) // alineamiento a MSB del denominador
      {
	m0 = (aligned_denom[b_reserved_order] == 0);
	aux_shift_back[m0] = aux_shift_back - 1;
	for (uint32_t j = b_reserved_order; j > 0; j--)
	  {
	    temporal = aligned_denom[j].get_element();
	    temporal[m0] = aligned_denom[j-1].get_element();
	    aligned_denom[j] = temporal;
	  }
	temporal = aligned_denom[0].get_element();
	temporal[m0] = 0;
	aligned_denom[0] = temporal;
      }
    
    //printf("\n ALIGNED DENOM: ");
    //aligned_denom.display();
    
    C_GF_2eN_poly_T quotient(res_reserved_order);
    C_GF_2eN_poly_T temp_remainder(reserved_order);
    for (uint32_t i = 0; i <= reserved_order; i++)
      temp_remainder[i] = poly[i];

    //printf("\n ORIGINAL NUM:  ");
    //display();
    //printf("\n---------");
    //printf("\n REMAINDER   :  ");
    //temp_remainder.display();
    //printf("\n QUOTIENT    :  ");
    //quotient.display();
    
    for (int32_t i = reserved_order; i >= 0; i--)
      {
	m0 = (aux_remaining_el > 0);
	aux_remaining_el[m0] = aux_remaining_el -1;
	temporal = 0;
	temporal[m0] = (temp_remainder[i] / aligned_denom[b_reserved_order]).get_element();
	quotient[i] = temporal;
	for (int32_t j = b_reserved_order; ((j >= 0) && (i+j-b_reserved_order >= 0)); j--)
	  {
	    temporal = temp_remainder[i+j-b_reserved_order].get_element();
	    temporal[m0] = (temp_remainder[i+j-b_reserved_order] - quotient[i] * aligned_denom[j]).get_element();
	    temp_remainder[i+j-b_reserved_order] = temporal;
	  }
	//printf("\n---------");
	//printf("\n REMAINDER   :  ");
	//temp_remainder.display();
	//printf("\n QUOTIENT    :  ");
	//quotient.display();
      }

    for (uint32_t i = 0; i <= b_reserved_order; i++) //alineamiento normal del cociente
      {
	m0 = aux_shift_back > 0;
	aux_shift_back[m0] = aux_shift_back - 1;
	for (uint32_t j = 0; j < res_reserved_order; j++)
	  {
	    temporal = quotient[j].get_element();
	    temporal[m0] = quotient[j+1].get_element();
	    quotient[j] = temporal;
	  }
	temporal = quotient[res_reserved_order].get_element();
	temporal[m0] = 0;
	quotient[res_reserved_order] = temporal;
	
	//printf("\n QUOTIENT  %d:  ", i);
	//quotient.display();
      }
    //printf("\n\n division completed");fflush(stdout);
    return quotient;
  }


  template <class T_E, class T_BE, uint32_t T_P>
  C_GF_2eN_poly_T<T_E, T_BE, T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator % (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
  {
    C_GF_2eN_poly_T<T_E, T_BE, T_P> remainder(reserved_order);
    remainder = (*this);
    C_GF_2eN_poly_T<T_E, T_BE, T_P> quotient(reserved_order);
    quotient = (*this) / b;
    remainder = remainder - (quotient * b);
    return remainder;
  }

  
  template <class T_E, class T_BE, uint32_t T_P>
  void C_GF_2eN_poly_T<T_E, T_BE, T_P>::display() const
  {
    c_vector_logger_t<T_E> Logger_input;
    T_E aux;
    Logger_input.Initialize("screen",   nullptr, "input_stream" , 1, 20, 0, false, false, false);

    aux = poly[0].get_element();
    Logger_input.Add_Log_Entry(&aux);
    //printf("\b");
    for (uint32_t i = 1; i <= reserved_order; i++)
      {
	aux = poly[i].get_element();
	//printf(" + ");
	Logger_input.Add_Log_Entry(&aux);
	//printf("*x^%d", i);
      }
  }

template <class T_E, class T_BE, uint32_t T_P>
C_GF_2eN_poly_T<T_E, T_BE, T_P>& C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator =  (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
{
  uint32_t i = 0;
  uint32_t min_order = std::min(reserved_order, b.reserved_order);
  for (; i <= min_order; i++)
    poly[i] = b.poly[i];
  for (; i <= reserved_order; i++)
    poly[i] = 0;
  return *this;
}

template <class T_E, class T_BE, uint32_t T_P>
C_GF_2eN_poly_T<T_E, T_BE, T_P>& C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator =  (const T_E &b)
{
  for (uint32_t i = 0; i <= reserved_order; i++)
    poly[i] = b[i];
  return *this;
}

template <class T_E, class T_BE, uint32_t T_P>
C_GF_2eN_poly_T<T_E, T_BE, T_P>& C_GF_2eN_poly_T<T_E, T_BE, T_P>::load(const T_E *b, const uint32_t n_elements)
{
  uint32_t i = 0;
  uint32_t min_order = std::min(reserved_order, n_elements);
  for (; i <= min_order; i++)
    poly[i] = b[i];
  for (; i <= reserved_order; i++)
    poly[i] = 0;
  return *this;
}

template <class T_E, class T_BE, uint32_t T_P>
C_GF_2eN_poly_T<T_E, T_BE, T_P>& C_GF_2eN_poly_T<T_E, T_BE, T_P>::load(const T_E *b, const uint32_t n_elements, const uint32_t times_XeN)
{
  uint32_t i = 0, j = 0;
  uint32_t min_order = std::min(reserved_order, n_elements+times_XeN);
  for (; i <times_XeN; i++)
    poly[i] = 0;
  for (; i <= min_order; i++, j++)
    poly[i] = b[j];
  for (; i <= reserved_order; i++)
    poly[i] = 0;
  return *this;
}


template <class T_E, class T_BE, uint32_t T_P>
void C_GF_2eN_poly_T<T_E, T_BE, T_P>::download(T_E *b)
{
  for (uint32_t i = 0; i <= reserved_order; i++)
    b[i] = poly[i].get_element();
}


template <class T_E, class T_BE, uint32_t T_P>
inline UME::SIMD::SIMDVecMask<T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::operator == (const C_GF_2eN_poly_T<T_E, T_BE, T_P> &b)
{
  UME::SIMD::SIMDVecMask<T_P> m0 = true;
  uint32_t min_order = std::min(reserved_order, b.reserved_order);
  uint32_t i = 0;
  for (; i <= min_order; i++)
    m0 = m0 && (poly[i] == b.poly[i]);
  for (i=min_order+1; i<= reserved_order; i++)
    m0 = m0 && (poly[i] == 0);
  for (i=min_order+1; i<= b.reserved_order; i++)
    m0 = m0 && (b.poly[i] == 0);
  return m0;
}
  
template <class T_E, class T_BE, uint32_t T_P>
inline UME::SIMD::SIMDVecMask<T_P> C_GF_2eN_poly_T<T_E, T_BE, T_P>::is_zero()
{  
  UME::SIMD::SIMDVecMask<T_P> m0 = true;
  for (uint32_t i = 0; i < reserved_order; i++)
    m0 = m0 && (poly[i] == 0);
  return m0;
}
  
#endif
