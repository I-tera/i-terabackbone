#include <math.h>
#include <matrix.h>
#include <mex.h>
#include "C_GF2_Matrix.h"
#include "C_GF2_Matrix.cpp"

#include <vector>
#include <map>
#include <algorithm>
#include <memory>
#include <string.h>
#include <sstream>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  const mwSize *dims;

  /////////////////////////////////////////////////////////////////////
  // Displaying call summary //////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////
  /*mexPrintf("\n======================================================");
  mexPrintf("\n== Function call summary==============================");
  mexPrintf("\n======================================================");
  mexPrintf("\n Called with %d arguments", nrhs);
  mexPrintf("\n Returning %d values", nlhs);
  for (int i = 0; i < nrhs; i++)
    {
      dims = mxGetDimensions(prhs[i]);
      mexPrintf("\n Argument[%d] has dimensions %d x %d", i, (int)dims[0], (int)dims[1]);
      }*/

  /////////////////////////////////////////////////////////////////////
  // command validation ///////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////
  dims = mxGetDimensions(prhs[0]);
  if ( mxIsChar(prhs[0]) != 1)
    mexErrMsgTxt("\n ERROR: Command must be a string");
  if (((int)dims[0] != 1))
    mexErrMsgTxt("\n ERROR: Command must be of size 1xN");
  char *i_Command = mxArrayToString(prhs[0]);

  if (strcmp(i_Command, "Invert") == 0)
    {
      mexPrintf("\n Comando = Invert");
      
      if (nrhs != 2)
        mexErrMsgTxt("\n ERROR: Invert function must receive 1 parameter\n");
      dims = mxGetDimensions(prhs[1]);
      if ((int)dims[0] != (int)dims[1])
        mexErrMsgTxt("\n ERROR: Invert munction must receive a square matrix\n");
      if (nlhs != 2)
	  mexErrMsgTxt("\n ERROR: Invert function must return 2 parameters\n");
      
      double *i_M;
      int Cols = (int)dims[1];
      int Rows = (int)dims[0];
      i_M = mxGetPr(prhs[1]);

      C_GF2_Matrix myMatrix;
      myMatrix.load_matrix_from_matlab_array(Rows, Cols, i_M);

      plhs[0] = mxCreateDoubleMatrix(Rows, Cols, mxREAL);
      plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
      double *o_M;
      o_M = mxGetPr(plhs[0]);
      double *o_Invertible;
      o_Invertible = mxGetPr(plhs[1]);
      
      //myMatrix.Display();

      try
        {
          myMatrix.Invert();
          myMatrix.store_matrix_in_matlab_array(o_M);
          *o_Invertible = 1;
        }
      catch (int e)
        {
          *o_Invertible = 0;
        }      
    }
  else if (strcmp(i_Command, "Systematize") == 0)
    {
      mexPrintf("\n Comando = Systematize");
      
      if (nrhs != 2)
        mexErrMsgTxt("\n ERROR: Invert function must receive 1 parameter\n");
      dims = mxGetDimensions(prhs[1]);
      if (nlhs != 2)
	  mexErrMsgTxt("\n ERROR: Invert function must return 2 parameters\n");
      
      double *i_M;
      int Cols = (int)dims[1];
      int Rows = (int)dims[0];
      i_M = mxGetPr(prhs[1]);

      C_GF2_Matrix myMatrix;
      myMatrix.load_matrix_from_matlab_array(Rows, Cols, i_M);

      plhs[0] = mxCreateDoubleMatrix(Rows, Cols, mxREAL);
      plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
      double *o_M;
      o_M = mxGetPr(plhs[0]);
      double *o_Systematizable;
      o_Systematizable = mxGetPr(plhs[1]);
      
      //myMatrix.Display();

      try
        {
          myMatrix.Systematize();
          myMatrix.store_matrix_in_matlab_array(o_M);
          *o_Systematizable = 1;
        }
      catch (int e)
        {
          *o_Systematizable = 0;
        }      
    }
  else
      mexErrMsgTxt("\n ERROR: Comando no reconocido");
    //mexPrintf("\n");
    
  }

