tic

QAM8Rot_PCSLambda = 0; %8.35 for 50%OH
QAM16_PCSLambda = 0; %5.45 for 50%OH
QAM32_PCSLambda = 0; %7.62 for 50%OH
QAM64_PCSLambda = 0; %8.5 for 50%OH
QAM256_PCSLambda = 0; %8.5 for 50%OH

SoftDemapper_RegisterBank = [];


myConst = C_ConstellationHandler('QPSK', 'SNRs', 20, 64, 2, 'rubbish');
myConst.PlotConstellation();
myConst.SetPCSLambda(0);
SD = myConst.CalculateSoftDemapper(7, 10000, 0.02);
myConst.CreateSoftDemapperReport(SD, 'Results/SoftDemapper_QPSK.txt');
SoftDemapper_RegisterBank.QAM8Rot = myConst.CreateSoftDemapperRegisterBank(SD);


myConst = C_ConstellationHandler('QAM8Rot', 'SNRs', 20, 64, 2, 'rubbish');
myConst.PlotConstellation();
myConst.SetPCSLambda(QAM8Rot_PCSLambda);
SD = myConst.CalculateSoftDemapper(7, 10000, 0.02);
myConst.CreateSoftDemapperReport(SD, 'Results/SoftDemapper_QAM8rot.txt');
SoftDemapper_RegisterBank.QAM8Rot = myConst.CreateSoftDemapperRegisterBank(SD);


myConst = C_ConstellationHandler('QAM16', 'SNRs', 20, 64, 2, 'rubbish');
myConst.PlotConstellation();
myConst.SetPCSLambda(QAM16_PCSLambda);
SD = myConst.CalculateSoftDemapper(7, 10000, 0.02);
myConst.CreateSoftDemapperReport(SD, 'Results/SoftDemapper_QAM16_PCS.txt')
SoftDemapper_RegisterBank.QAM16 = myConst.CreateSoftDemapperRegisterBank(SD);


myConst = C_ConstellationHandler('QAM32', 'SNRs', 20, 64, 2, 'rubbish');
myConst.PlotConstellation();
myConst.SetPCSLambda(QAM32_PCSLambda);
SD = myConst.CalculateSoftDemapper(7, 10000, 0.02);
myConst.CreateSoftDemapperReport(SD, 'Results/SoftDemapper_QAM32_PCS.txt')
SoftDemapper_RegisterBank.QAM32 = myConst.CreateSoftDemapperRegisterBank(SD);


myConst = C_ConstellationHandler('QAM64', 'SNRs', 20, 64, 2, 'rubbish');
myConst.PlotConstellation();
myConst.SetPCSLambda(QAM64_PCSLambda);
SD = myConst.CalculateSoftDemapper(7, 10000, 0.02);
myConst.CreateSoftDemapperReport(SD, 'Results/SoftDemapper_QAM64_PCS.txt')
SoftDemapper_RegisterBank.QAM64 = myConst.CreateSoftDemapperRegisterBank(SD);

%25-7 funciona para 1 2 y 3
myConst = C_ConstellationHandler('QAM256', 'SNRs', 26, 64, 2, 'rubbish');
myConst.PlotConstellation();
myConst.SetPCSLambda(QAM256_PCSLambda);
SD = myConst.CalculateSoftDemapper(7, 10000, 0.02);
myConst.CreateSoftDemapperReport(SD, 'Results/SoftDemapper_QAM256_PCS.txt')
SoftDemapper_RegisterBank.QAM256 = myConst.CreateSoftDemapperRegisterBank(SD);
