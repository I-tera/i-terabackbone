classdef C_ConstellationHandler < handle
    properties (SetAccess = public)
        ResultsFolder
        Modulation
        BaudRate_GBps
        Constellation
        CorrectionFactor;
        SymbolProbabilities
        BitRate_Gbps
        SNRb_dB
        SNRs_dB
        OSNR_dB
        NPolarizations
        BitsPerSymbol
        SignBitsPerSymbol
        SignalPower
        Sigma
        Entropy
        OH
    end
    methods
        function obj = C_ConstellationHandler(Modulation, SNRType, SNR_dB, BaudRate_GBps, NPolarizations, ResultsFolder)
            obj.ResultsFolder =ResultsFolder;
            obj.Modulation = Modulation;
            obj.BaudRate_GBps = BaudRate_GBps;
            obj.Constellation = [];
            if ((NPolarizations ~= 1) && (NPolarizations ~= 2))
                error ('Invalid NPolarizations');
            end
            obj.NPolarizations = NPolarizations;
            auxC = C_ConstellationMaker(Modulation, ResultsFolder);
            obj.Constellation = auxC.Alphabet;
            obj.BitsPerSymbol = auxC.BitsPerSymbol;
            obj.SignBitsPerSymbol = auxC.SignBitsPerSymbol;
            obj.SignalPower = auxC.SignalPower;
            obj.CorrectionFactor = auxC.CorrectionFactor;
            obj.BitRate_Gbps = BaudRate_GBps*obj.NPolarizations*obj.BitsPerSymbol;
            obj.SetSNR(SNRType, SNR_dB);
            obj.SymbolProbabilities = ones(size(obj.Constellation))/length(obj.Constellation);
        end
        function [OH, AvgPwr, AvgPwrUniform, BpSymbol] = SetPCSLambda(obj, lambda)
            obj.SymbolProbabilities = exp(-lambda*(abs(obj.Constellation).^2));
            obj.SymbolProbabilities = obj.SymbolProbabilities / sum(obj.SymbolProbabilities);
            [OH, AvgPwr, AvgPwrUniform, BpSymbol] = obj.CalculateConstellationProperties();
        end
        function [OH, AvgPwr, AvgPwrUniform, BpSymbol] = CalculateConstellationProperties(obj)
            obj.Entropy = -sum(obj.SymbolProbabilities(obj.SymbolProbabilities>0) .* log2(obj.SymbolProbabilities(obj.SymbolProbabilities>0)));
            obj.OH = 100*(obj.BitsPerSymbol / obj.Entropy -1);
            AvgPwrUniform = 1/(length(obj.Constellation)) * obj.Constellation * obj.Constellation';
            AvgPwr = (obj.SymbolProbabilities.*obj.Constellation) * obj.Constellation';
            OH = obj.OH;
            BpSymbol = obj.Entropy;
            obj.SignalPower = AvgPwr;
        end
            
        function [] = PlotConstelationProbabilities(obj)
            finite_resolution_const = round(obj.Constellation * 100000)/100000;
            gridR = unique(real(finite_resolution_const));
            gridI = unique(imag(finite_resolution_const));
            expandedconst = gridR + 1i*gridI.';
            expandedprob  = 0*expandedconst + 400;
            for i=1:length(obj.Constellation)
                expandedprob(expandedconst == finite_resolution_const(i)) = obj.SymbolProbabilities(i);
            end
            h = bar3(expandedprob);
            for i = 1:numel(h)
              index = logical(kron(expandedprob(:, i) == 400, ones(6, 1)));
              zData = get(h(i), 'ZData');
              zData(index, :) = nan;
              set(h(i), 'ZData', zData);
            end
            set(gca, 'XTickLabel', gridR)
            set(gca, 'YTickLabel', gridI)
            xlabel('I');
            ylabel('Q');
            zlabel('PDF');
        end
        function [] = SetSNR(obj, SNRType, SNR_dB)
            switch (SNRType)
                case "SNRb"
                    obj.SNRb_dB = SNR_dB;
                    obj.OSNR_dB = obj.SNRb_dB - 10*log10(12.5*obj.NPolarizations/obj.BitRate_Gbps);
                    obj.SNRs_dB = obj.OSNR_dB + 10*log10(12.5*obj.NPolarizations/obj.BitRate_Gbps) + 10*log10(obj.BitsPerSymbol);
                case "SNRs"
                    obj.SNRs_dB = SNR_dB;
                    obj.OSNR_dB = obj.SNRs_dB - 10*log10(12.5*obj.NPolarizations/obj.BitRate_Gbps) - 10*log10(obj.BitsPerSymbol);
                    obj.SNRb_dB = obj.OSNR_dB + 10*log10(12.5*obj.NPolarizations/obj.BitRate_Gbps);
                case "OSNR"
                    obj.OSNR_dB = SNR_dB;
                    obj.SNRs_dB = obj.OSNR_dB + 10*log10(12.5*obj.NPolarizations/obj.BitRate_Gbps) + 10*log10(obj.BitsPerSymbol);
                    obj.SNRb_dB = obj.OSNR_dB + 10*log10(12.5*obj.NPolarizations/obj.BitRate_Gbps);
                otherwise
                    error(['Not supported SNR Type = ', SNRType, ' - only SNRb, SNRs and OSNR are allowed']);
            end
            obj.Sigma = sqrt((obj.SignalPower/2) ./ (10.^(obj.SNRs_dB/10)));
        end
        function [LLRs, elapsedTime] = DemapMessage_Theoretical(obj, r)
            tstart = toc;
            if (size(r,1) == 1)
                LLRs = zeros(1,length(r)*obj.BitsPerSymbol);
            elseif (size(r,2) == 1)
                LLRs = zeros(length(r)*obj.BitsPerSymbol,1);
            else
                error('Incompatible received vector dymensions');
            end
            
            AuxIndexes = [0:length(obj.Constellation)-1];
            AuxSymbols = obj.Constellation(AuxIndexes+1);
            AuxBits    = de2bi(AuxIndexes);
            AuxProbabilities = zeros(length(r), length(obj.Constellation));
            for i=1: length(obj.Constellation)
                AuxProbabilities(:,i) = 1 / (2*pi*(obj.Sigma)^2) * exp(-1/(2*obj.Sigma^2)*(abs(r-AuxSymbols(i))).^2);
            end
            for i=1: obj.BitsPerSymbol
                LLRs(i:obj.BitsPerSymbol:end) = (-1)*(log(sum(AuxProbabilities(:,AuxBits(:,i)==1).*obj.SymbolProbabilities(AuxBits(:,i)==1),2) ./ ...
                    sum(AuxProbabilities(:,AuxBits(:,i)==0).*obj.SymbolProbabilities(AuxBits(:,i)==0),2)));
            end
            elapsedTime = toc - tstart;
        end
        
        function [SNR_at_NGMI] = get_SNR_at_NGMI(obj, n_symbols, SNR_range, target_NGMI, allowed_delta)
            accum_symbol_prob = 0 * obj.SymbolProbabilities;
            accum_symbol_prob(1) = obj.SymbolProbabilities(1);
            for i=2:length(obj.SymbolProbabilities)
                accum_symbol_prob(i) = accum_symbol_prob(i-1) + obj.SymbolProbabilities(i);
            end
            tx_message = zeros(n_symbols, 1);
            tx_symbol = zeros(n_symbols, 1);
            aux_idx = [1:length(obj.SymbolProbabilities)];
            for i = 1: n_symbols
                tx_message(i) = min(aux_idx(rand(1) < accum_symbol_prob));
                tx_symbol(i) = obj.Constellation(tx_message(i));
            end
            
            go_on = false;
            while (~go_on)
                go_on = true;
                %figure
                SNRs = sort(SNR_range);
                [~, NGMIs] = obj.GetGMI_light(n_symbols, SNRs, tx_symbol, tx_message);
                NGMIs = real(NGMIs);
                while ((min(abs(NGMIs - target_NGMI)) > allowed_delta) && (go_on))
                    [SNRs, idx] = sort(SNRs);
                    NGMIs = NGMIs(idx);
                    [NGMIs, idx] = unique(NGMIs);
                    SNRs = SNRs(idx);
                    new_SNRs = interp1(NGMIs, SNRs, target_NGMI, 'linear', 'extrap');
                    SNRs = [SNRs, new_SNRs];
                    [~, new_NGMIs] = obj.GetGMI_light(n_symbols, new_SNRs, tx_symbol, tx_message);
                    NGMIs = real([NGMIs, new_NGMIs]);
                    %plot(SNRs, NGMIs, 'bo-')
                    %hold on
                    if (min(min(abs(SNRs - SNRs.' + eye(length(SNRs))))) < 0.001)
                        go_on = false;
                    end
                end
                [~, idx] = min(abs(NGMIs - target_NGMI));
                SNR_at_NGMI = SNRs(idx);
            end
        end
        
        function [GMI, NGMI] = GetGMI_light(obj, n_symbols, SNRs, tx_symbol, tx_message)
            noise_power = obj.SignalPower ./ (10 .^ (SNRs/10));
            if (max(abs(imag(obj.Constellation))) > 0)
                noise = (randn(n_symbols, 1) + 1i*randn(n_symbols, 1))*sqrt(noise_power / 2);
            else
                noise = (randn(n_symbols, 1) )*sqrt(noise_power);
            end
            rx_symbol = tx_symbol + noise;
            %plot(real(rx_symbol), imag(rx_symbol), 'b.');
            sim_SNRs = 10*log10(mean(abs(tx_symbol).^2) ./ mean(abs(noise).^2));
            aux_const(1,1,:) = obj.Constellation;
            aux_symbol_probabilities(1,1,:) = obj.SymbolProbabilities;
            d_matrix = abs(rx_symbol - aux_const);
           % q_y_x_px = normpdf(d_matrix, 0, sqrt(noise_power)) .* obj.SymbolProbabilities;
            q_y_x_px = exp(-1*d_matrix.*d_matrix./(noise_power)) .* aux_symbol_probabilities;
            
            den = sum(q_y_x_px,3);    
            
            GMI = ones(1,length(SNRs)) * (-1 * sum(obj.SymbolProbabilities(obj.SymbolProbabilities>0) .* log2(obj.SymbolProbabilities(obj.SymbolProbabilities>0))));
            for bit = 1: log2(length(obj.SymbolProbabilities))
                tx_bit = 1*(mod(tx_message-1, 2^bit) >= 2^(bit-1));
                aux_bit = 1*(mod([0:length(obj.SymbolProbabilities)-1], 2^bit) >= 2^(bit-1));
                mask(:,1,:) = (1 - abs(aux_bit - tx_bit));
                num = sum((q_y_x_px .* mask),3);
                GMI = GMI + mean(log2(num ./ den));
            end
            
            NGMI = 1 - (obj.Entropy - GMI) / obj.BitsPerSymbol;
        end
            
        function [GMI, NGMI] = GetGMI(obj, n_symbols, SNRs)
            accum_symbol_prob = 0 * obj.SymbolProbabilities;
            accum_symbol_prob(1) = obj.SymbolProbabilities(1);
            for i=2:length(obj.SymbolProbabilities)
                accum_symbol_prob(i) = accum_symbol_prob(i-1) + obj.SymbolProbabilities(i);
            end
            tx_message = zeros(n_symbols, 1);
            tx_symbol = zeros(n_symbols, 1);
            aux_idx = [1:length(obj.SymbolProbabilities)];
            for i = 1: n_symbols
                tx_message(i) = min(aux_idx(rand(1) < accum_symbol_prob));
                tx_symbol(i) = obj.Constellation(tx_message(i));
            end
            [GMI, NGMI] = obj.GetGMI_light(n_symbols, SNRs, tx_symbol, tx_message);
        end
            
        function [UncodedBER] = GetTheoreticalUncodedBER(obj, symbol_nb)
            %constellation to bits
            const_bin = zeros(length(obj.Constellation), obj.BitsPerSymbol);
            for const_idx=1: length(obj.Constellation)
                const_bin(const_idx,:) = de2bi(const_idx - 1, obj.BitsPerSymbol);
            end
            %grid construction
            aux_grid = linspace(-2,2,2^(symbol_nb+1)+1);
            aux_grid = aux_grid(1:end-1);
            aux_grid = aux_grid + (aux_grid(2)-aux_grid(1))/2;
            my_grid = aux_grid + 1i*aux_grid.';
            %hard decision
            slicer = 0*my_grid;
            for I_idx = 1: size(my_grid,1)
                for Q_idx = 1: size(my_grid,2)
                    [~, slicer(I_idx, Q_idx)] = min(abs(my_grid(I_idx, Q_idx) - obj.Constellation));
                end
            end
            %numerical uncoded ber calculation
            UncodedBER = 0*obj.SNRs_dB;
            errorp = zeros(length(obj.Constellation), length(obj.SNRs_dB));
            for const_idx = 1: length(obj.Constellation)
                bit_error_multiplier = 0*my_grid;
                aux_current_symbol = const_bin(const_idx,:);
                for test_idx = 1: length(obj.Constellation)
                    aux_test_symbol = const_bin(test_idx,:);
                    nerrors = sum(mod(aux_current_symbol + aux_test_symbol, 2));
                    bit_error_multiplier(slicer == test_idx) = nerrors;
                end
                for snr_idx = 1: length(obj.SNRs_dB)
                    probabilities = exp (-0.5*(abs(my_grid-obj.Constellation(const_idx))/obj.Sigma(snr_idx)).^2);
                    probabilities = probabilities / (sum(sum(probabilities)));
                    %surf(real(my_grid), imag(my_grid), probabilities.* bit_error_multiplier);
                    %view(0, 90);
                    errorp(const_idx, snr_idx) = sum(sum(probabilities .* bit_error_multiplier));
                end
            end
            for snr_idx = 1: length(obj.SNRs_dB)
                UncodedBER(snr_idx) = sum((errorp(:,snr_idx).' .* obj.SymbolProbabilities))/obj.BitsPerSymbol;
            end
        end
        function [SDem] = CalculateSoftDemapper(obj, NBits, Saturation, MaxErrorRel, simplify_mirrors)
            
            mygrid = linspace(-1, 1, 2^NBits+1);
            mygrid = [2*mygrid(1) - mygrid(2), mygrid];
            mesh_R_ext = ones(length(mygrid),1)*mygrid;
            mesh_I_ext = (ones(length(mygrid),1)*mygrid).';
            mesh_R = mesh_R_ext(2:end-1,2:end-1);
            mesh_I = mesh_I_ext(2:end-1,2:end-1);
            PointArray = reshape( mesh_R_ext + 1i*mesh_I_ext, [],1);
            
            LLRs = obj.DemapMessage_Theoretical(PointArray);
            LLRs = min(LLRs, Saturation);
            LLRs = max(LLRs, -1*Saturation);
            LLRs = LLRs* obj.Sigma * obj.Sigma * obj.CorrectionFactor * obj.CorrectionFactor;
            MaxErrorAbs = max(max(abs(LLRs)))*MaxErrorRel;
            
            SDem = [];
            for llrindex=1:obj.BitsPerSymbol
                %selection of the current LLR
                currentLLR = LLRs(llrindex:obj.BitsPerSymbol:end);
                currentLLR = reshape(currentLLR, size(mesh_R_ext));
                aux_currentLLR = currentLLR(2:end-1,2:end-1);
                SDem(llrindex).Approximation = 0*aux_currentLLR;
                SDem(llrindex).Regions       = 0*aux_currentLLR;
                SDem(llrindex).Borders = [];
%                 
%                                  figure
%                                  surf(mesh_R_ext, mesh_I_ext, currentLLR)
%                                  title('LLR[' + string(llrindex) + ']')
%                                  xlabel('real')
%                                  ylabel('imag');
%                                  zlabel('LLR');
%                 
                %second derivative for finding edges between flat regions
                d2currentLLR_dx2  = currentLLR(2:end-1,3:end) - 2*currentLLR(2:end-1,2:end-1) + currentLLR(2:end-1,1:end-2);
                d2currentLLR_dy2  = currentLLR(3:end,2:end-1) - 2*currentLLR(2:end-1,2:end-1) + currentLLR(1:end-2,2:end-1);
                d2currentLLR_dxdy = ((currentLLR(1:end-2,3:end) - currentLLR(1:end-2,1:end-2)) - (currentLLR(3:end,3:end) - currentLLR(3:end,1:end-2)))/4;
                d2currentLLR  = sqrt(d2currentLLR_dx2.^2 + d2currentLLR_dy2.^2 + d2currentLLR_dxdy.^2);
                %
%                                   figure
%                                   surf(mesh_R, mesh_I, d2currentLLR)
%                                   title('d^2LLR[' + string(llrindex) + ']/(dxdy)')
%                                   xlabel('real')
%                                   ylabel('imag');
%                                   zlabel('LLR');
                
                %convert d2 into binary edge or no edge
                d2currentLLR = 1.0*((d2currentLLR > 0.02*max(max(d2currentLLR))) & (d2currentLLR > 0.000001*max(max(currentLLR))));
                figure
                surf(mesh_R, mesh_I, d2currentLLR)
                title('d^2LLR[' + string(llrindex) + ']/(dxdy)')
                xlabel('real')
                ylabel('imag');
                zlabel('LLR');
                %                 hold on;
                PointsLeft = 100;
                planeindex = 0;
                while(PointsLeft > 8) %do for all planes
                    
                    
                    planeindex = planeindex+1;
                    %expand edges until center of largest plane is found
                    distance2edge = 0;
                    aux_d2currentLLR = d2currentLLR;
                    while(min(min(aux_d2currentLLR)) == 0)
                        distance2edge = distance2edge + 1;
                        aux_d2currentLLR = [ones(1,size(aux_d2currentLLR,2)+2); [ones(size(aux_d2currentLLR,1),1), aux_d2currentLLR, ones(size(aux_d2currentLLR,1),1)]; ones(1,size(aux_d2currentLLR,2)+2)];
                        aux_d2currentLLR = min(aux_d2currentLLR, 5);
                        aux_d2currentLLR = aux_d2currentLLR(2:end-1,2:end-1) + ...
                            aux_d2currentLLR(1:end-2,2:end-1) + ...
                            aux_d2currentLLR(3:end  ,2:end-1) + ...
                            aux_d2currentLLR(2:end-1,1:end-2) + ...
                            aux_d2currentLLR(2:end-1,3:end  );
                        %                         hold on
                        %                         surf(mesh_R, mesh_I, aux_d2currentLLR);
                    end
                    [val, coor_xs] = min(aux_d2currentLLR);
                    [~, coor_y] = min(val);
                    coor_x = coor_xs(coor_y);
                    coor_x = coor_x +1;
                    coor_y = coor_y +1;
                    distance2edge = distance2edge - 1;
                    if (distance2edge > 0)
                        plane_a = (currentLLR(coor_x, coor_y-distance2edge) - currentLLR(coor_x, coor_y+distance2edge)) / ...
                            (mesh_R_ext(coor_x, coor_y-distance2edge) - mesh_R_ext(coor_x, coor_y+distance2edge));
                        plane_b = (currentLLR(coor_x-distance2edge, coor_y) - currentLLR(coor_x+distance2edge, coor_y)) / ...
                            (mesh_I_ext(coor_x-distance2edge, coor_y) - mesh_I_ext(coor_x+distance2edge, coor_y));
                        plane_c = currentLLR(coor_x, coor_y) - plane_a*mesh_R_ext(coor_x, coor_y) - plane_b*mesh_I_ext(coor_x, coor_y);
                    
                    
                        myplane = plane_a * mesh_R_ext + plane_b * mesh_I_ext + plane_c;

                        %%%%%%%%%%%%%DEBUG START
% 
%                         figure('Name', "LLR " + string(llrindex) + " plane " + string(planeindex));
%                         subplot(2,2,1)
%                         surf(mesh_R_ext, mesh_I_ext, currentLLR)
%                         title('LLR[' + string(llrindex) + ']')
%                         xlabel('real')
%                         ylabel('imag');
%                         zlabel('LLR');
%                         hold on
% 
%                         subplot(2,2,2) 
%                         surf(mesh_R, mesh_I, d2currentLLR)
%                         title('d^2LLR[' + string(llrindex) + ']/(dxdy)')
%                         xlabel('real')
%                         ylabel('imag');
%                         zlabel('LLR');
% 
%                         critical_points = 0*mesh_R_ext;
%                         critical_points(coor_x,coor_y) = 3;
%                         critical_points(coor_x,coor_y-distance2edge) = 3;
%                         critical_points(coor_x,coor_y+distance2edge) = 3;
%                         critical_points(coor_x-distance2edge,coor_y) = 3;
%                         critical_points(coor_x+distance2edge,coor_y) = 3;
%                         hold on
%                         surf (mesh_R_ext, mesh_I_ext, critical_points);
% 
%                         within = 0*mesh_R_ext;
%                         for i=1:size(mesh_R_ext,1)
%                             for j=1:size(mesh_R_ext,2)
%                                 if ((mesh_R_ext(i,j)-mesh_R_ext(coor_x, coor_y))^2+(mesh_I_ext(i,j)-mesh_I_ext(coor_x,coor_y))^2 <= (mesh_I_ext(1,1)-mesh_I_ext(1+distance2edge,1))^2)
%                                     within(i,j) = 0.5;
%                                 end
%                             end
%                         end
%                         surf(mesh_R_ext, mesh_I_ext, within);
%                         view(0,90);
% 
%                         points_to_match = 0*currentLLR;
%                         for i=1:size(mesh_R_ext,1)
%                             for j=1:size(mesh_R_ext,2)
%                                 if ((mesh_R_ext(i,j)-mesh_R_ext(coor_x, coor_y))^2+(mesh_I_ext(i,j)-mesh_I_ext(coor_x,coor_y))^2 <= (mesh_I_ext(1,1)-mesh_I_ext(1+distance2edge,1))^2)
%                                     points_to_match(i,j) = currentLLR(i,j);
%                                 end
%                             end
%                         end                    
%                         subplot(2,2,3)
%                         surf(mesh_R_ext, mesh_I_ext, points_to_match);
%                         view(0,90)
% 
%                         subplot(2,2,4)
%                         surf(mesh_R_ext, mesh_I_ext, 0.7*(abs(currentLLR - myplane) > MaxErrorAbs));
%                         hold on
%                         surf(mesh_R, mesh_I, d2currentLLR)
%                         view(0,90)
                        %%%%%%%%%%%%%% DEBUG STOP

                        SDem(llrindex).Regions(coor_x-1, coor_y-1) = planeindex;
                        d2currentLLR(coor_x-1, coor_y-1) = 1;
                        LastExpansion = 1;
                        while (LastExpansion > 0)

                            %surf(mesh_R, mesh_I, d2currentLLR);
                            LastExpansion = 0;
                            for r = 2:size(currentLLR,1)-1
                                for c=2:size(currentLLR,2)-1
                                    isflat = (d2currentLLR(r-1,c-1) == 0);
                                    isempty = (SDem(llrindex).Regions(r-1,c-1) == 0);
                                    if (isflat && isempty)
                                        neighbors = [0, 0, 0, 0];
                                        if (r>=3)
                                            neighbors(1) = SDem(llrindex).Regions(r-2, c-1);
                                        end
                                        if (c>= 3)
                                            neighbors(2) = SDem(llrindex).Regions(r-1, c-2);
                                        end
                                        if (r <= size(SDem(llrindex).Regions,1))
                                            neighbors(3) = SDem(llrindex).Regions(r, c-1);
                                        end
                                        if (c <= size(SDem(llrindex).Regions,2))
                                            neighbors(4) = SDem(llrindex).Regions(r-1, c);
                                        end
                                        %                                     neighbors = SDem(llrindex).Regions(max(1,r-2):min(size(SDem(llrindex).Regions,1),r),...
                                        %                                                                        max(1,c-2):min(size(SDem(llrindex).Regions,2),c));
                                        if (min(min(abs(neighbors - planeindex))) == 0)
                                            SDem(llrindex).Regions(r-1,c-1) = planeindex;
                                            LastExpansion = LastExpansion+1;
                                            d2currentLLR(r-1,c-1) = 1;
                                        end
                                    end
                                end
                            end
                        end


                        %                     surf(mesh_R_ext, mesh_I_ext, myplane)

                        error = abs(currentLLR - myplane);
                        error = 1*(error >= MaxErrorAbs);
                        error = error(2:end-1,2:end-1);
                        %                      subplot(2,1,2)
                        %                      surf(mesh_R, mesh_I, error)
                        %                      zlabel('error')

                        %                 subplot(2,2,3)
                        %                 surf(mesh_R, mesh_I, SDem(llrindex).Regions)
                        %                 title('LLR[' + string(llrindex) + '] regions')
                        %                 xlabel('real')
                        %                 ylabel('imag');
                        %                 zlabel('LLR regions');

                        SDem(llrindex).Planes(planeindex,1) = plane_a;
                        SDem(llrindex).Planes(planeindex,2) = plane_b;
                        SDem(llrindex).Planes(planeindex,3) = plane_c;

                        %d2currentLLR = max(d2currentLLR, 1-error);

                        PointsLeft = 0;
                        for r = 2:size(currentLLR,1)-1
                            for c=2:size(currentLLR,2)-1
                                isflat = (d2currentLLR(r-1,c-1) == 0);
                                isempty = (SDem(llrindex).Regions(r-1,c-1) == 0);
                                if (isflat && isempty)
                                    PointsLeft = PointsLeft+1;
                                end
                            end
                        end
                    else
                        planeindex = planeindex-1;
                        PointsLeft = 0;
                    end
                end
                
                
                %%%%%%%%%%%%%DEBUG START
%                 figure('Name', "LLR " + string(llrindex) + " best_fit_pre_simplification ");
%                 best_fit_error = abs(currentLLR);
%                 for i = 1: planeindex
%                     each_plane = SDem(llrindex).Planes(i,1) * mesh_R_ext + SDem(llrindex).Planes(i,2) * mesh_I_ext + SDem(llrindex).Planes(i,3);
%                     best_fit_error = min(best_fit_error, abs(currentLLR - each_plane));
%                 end
%                 surf(mesh_R_ext, mesh_I_ext, best_fit_error) 
%                 xlabel('real')
%                 ylabel('imag');
%                 zlabel('fit error abs');
%                 title('LLR[' + string(llrindex) + ']')
                %%%%%%%%%%%%%% DEBUG STOP

                
                
                
                
                %eliminating invalid ones
                auxPlanes = [];
                RegionCounter = 0;
                for i=1:size(SDem(llrindex).Planes,1)
                    if (isnan(SDem(llrindex).Planes(i,1)) || ...
                            isnan(SDem(llrindex).Planes(i,2)) || ...
                            isnan(SDem(llrindex).Planes(i,3)))
                        SDem(llrindex).Regions(SDem(llrindex).Regions == i) = 0;
                    else
                        RegionCounter =RegionCounter + 1;
                        auxPlanes(RegionCounter,:) = SDem(llrindex).Planes(i,:);
                        SDem(llrindex).Regions(SDem(llrindex).Regions == i) = RegionCounter;
                    end
                end
                SDem(llrindex).Planes = auxPlanes;
                
                
                %%%%%%%%%%%%%DEBUG START
%                 figure('Name', "LLR " + string(llrindex) + " best_fit_after_eliminating_invalids ");
%                 best_fit_error = abs(currentLLR);
%                 for i = 1: size(SDem(llrindex).Planes,1)
%                     each_plane = SDem(llrindex).Planes(i,1) * mesh_R_ext + SDem(llrindex).Planes(i,2) * mesh_I_ext + SDem(llrindex).Planes(i,3);
%                     best_fit_error = min(best_fit_error, abs(currentLLR - each_plane));
%                 end
%                 surf(mesh_R_ext, mesh_I_ext, best_fit_error) 
%                 xlabel('real')
%                 ylabel('imag');
%                 zlabel('fit error abs');
%                 title('LLR[' + string(llrindex) + ']')
                %%%%%%%%%%%%%% DEBUG STOP
                
                
                
                %Simplification of duplicated planes
                auxPlanes = SDem(llrindex).Planes(1,:);
                RegionCounter = 1;
                for idxcheck=2:size(SDem(llrindex).Planes,1)
                    survivingregion = true;
                    for idxref=1:RegionCounter
                        if ((abs(SDem(llrindex).Planes(idxcheck,3) - auxPlanes(idxref,3)) < MaxErrorAbs) && ... %same offset
                                ((SDem(llrindex).Planes(idxcheck,1) == auxPlanes(idxref,1) )) && ...  %equal slope on R
                                ((SDem(llrindex).Planes(idxcheck,2) == auxPlanes(idxref,2)))) %equal slope on I
                            SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = idxref;
                            survivingregion = false;
                        end
                    end
                    if (survivingregion)
                        RegionCounter = RegionCounter + 1;
                        auxPlanes(RegionCounter,:) = SDem(llrindex).Planes(idxcheck,:);
                        SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = RegionCounter;
                    end
                end
                SDem(llrindex).Planes = auxPlanes;
                
                
                %%%%%%%%%%%%%DEBUG START
%                 figure('Name', "LLR " + string(llrindex) + " best_fit_after_eliminating_duplicated ");
%                 best_fit_error = abs(currentLLR);
%                 for i = 1: size(SDem(llrindex).Planes,1)
%                     each_plane = SDem(llrindex).Planes(i,1) * mesh_R_ext + SDem(llrindex).Planes(i,2) * mesh_I_ext + SDem(llrindex).Planes(i,3);
%                     best_fit_error = min(best_fit_error, abs(currentLLR - each_plane));
%                 end
%                 surf(mesh_R_ext, mesh_I_ext, best_fit_error) 
%                 xlabel('real')
%                 ylabel('imag');
%                 zlabel('fit error abs');
%                 title('LLR[' + string(llrindex) + ']')
                %%%%%%%%%%%%%% DEBUG STOP
                
                auxPlanes = [SDem(llrindex).Planes(:,1), 0*SDem(llrindex).Planes(:,1), SDem(llrindex).Planes(:,2), 0*SDem(llrindex).Planes(:,1), SDem(llrindex).Planes(:,3)];
                SDem(llrindex).Planes = auxPlanes;
                
                if (simplify_mirrors)
                    
                    SDem(llrindex).Planes(:,1) = round(SDem(llrindex).Planes(:,1)*128)/128;
                    SDem(llrindex).Planes(:,3) = round(SDem(llrindex).Planes(:,3)*128)/128;
                    SDem(llrindex).Planes(:,5) = round(SDem(llrindex).Planes(:,5)*128)/128;
                    
                    %%Simplificating mirror planes
                    auxPlanes = SDem(llrindex).Planes(1,:);
                    RegionCounter = 1;
                    for idxcheck=2:size(SDem(llrindex).Planes,1)
                        survivingregion = true;
                        for idxref=1:RegionCounter
                            if (abs(SDem(llrindex).Planes(idxcheck,5) - auxPlanes(idxref,5)) < MaxErrorAbs) %same offset
                                if ((SDem(llrindex).Planes(idxcheck,3) ~= 0) && ... &non zero slope
                                        (SDem(llrindex).Planes(idxcheck,1) + auxPlanes(idxref,1) == 0) && ... %mirror on R
                                        (SDem(llrindex).Planes(idxcheck,3) + auxPlanes(idxref,5) == 0) && ... %mirror on R
                                        (sign(min(mesh_R(SDem(llrindex).Regions == idxcheck))) == sign(max(mesh_R(SDem(llrindex).Regions == idxcheck)))) && ...
                                        (sign(min(mesh_R(SDem(llrindex).Regions == idxref)))   == sign(max(mesh_R(SDem(llrindex).Regions == idxref)))) && ...
                                        (sign(min(mesh_I(SDem(llrindex).Regions == idxcheck))) == sign(max(mesh_I(SDem(llrindex).Regions == idxcheck)))) && ...
                                        (sign(min(mesh_I(SDem(llrindex).Regions == idxref)))   == sign(max(mesh_I(SDem(llrindex).Regions == idxref)))))
                                    auxPlanes(idxref,2) = auxPlanes(idxref,1) * sign(sum(sum(mesh_R(SDem(llrindex).Regions == idxref))));
                                    auxPlanes(idxref,1) = 0;
                                    auxPlanes(idxref,4) = auxPlanes(idxref,3) * sign(sum(sum(mesh_I(SDem(llrindex).Regions == idxref))));
                                    auxPlanes(idxref,3) = 0;
                                    SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = idxref;
                                    survivingregion = false;
                                elseif ((SDem(llrindex).Planes(idxcheck,1) ~= 0) && ... %non zero slope
                                        (SDem(llrindex).Planes(idxcheck,1) + auxPlanes(idxref,1) == 0) && ... %mirror on R
                                        (SDem(llrindex).Planes(idxcheck,3) == auxPlanes(idxref,3)) && ...
                                        (sign(min(mesh_R(SDem(llrindex).Regions == idxcheck))) == sign(max(mesh_R(SDem(llrindex).Regions == idxcheck)))) && ...
                                        (sign(min(mesh_R(SDem(llrindex).Regions == idxref)))   == sign(max(mesh_R(SDem(llrindex).Regions == idxref))))) % equal on I
                                    auxPlanes(idxref,2) = auxPlanes(idxref,1) * sign(sum(sum(mesh_R(SDem(llrindex).Regions == idxref))));
                                    auxPlanes(idxref,1) = 0;
                                    SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = idxref;
                                    survivingregion = false;
                                elseif ((SDem(llrindex).Planes(idxcheck,3) ~= 0) && ... &non zero slope
                                        (SDem(llrindex).Planes(idxcheck,1) == auxPlanes(idxref,1)) && ... %equal on R
                                        (SDem(llrindex).Planes(idxcheck,3) + auxPlanes(idxref,3) == 0) && ...
                                        (sign(min(mesh_I(SDem(llrindex).Regions == idxcheck))) == sign(max(mesh_I(SDem(llrindex).Regions == idxcheck)))) && ...
                                        (sign(min(mesh_I(SDem(llrindex).Regions == idxref)))   == sign(max(mesh_I(SDem(llrindex).Regions == idxref))))) %mirror on I
                                    auxPlanes(idxref,4) = auxPlanes(idxref,3) * sign(sum(sum(mesh_I(SDem(llrindex).Regions == idxref))));
                                    auxPlanes(idxref,3) = 0;
                                    SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = idxref;
                                    survivingregion = false;
                                end
                            end
                        end
                        if (survivingregion)
                            RegionCounter = RegionCounter + 1;
                            auxPlanes(RegionCounter,:) = SDem(llrindex).Planes(idxcheck,:); %[SDem(llrindex).Planes(idxcheck,1), 0, SDem(llrindex).Planes(idxcheck,3), 0, SDem(llrindex).Planes(idxcheck,3)];
                            SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = RegionCounter;
                        end
                    end
                    SDem(llrindex).Planes = auxPlanes;


                    %%%%%%%%%%%%%DEBUG START
    %                 figure('Name', "LLR " + string(llrindex) + " best_fit_after_eliminating_mirrors ");
    %                 best_fit_error = abs(currentLLR);
    %                 for i = 1: size(SDem(llrindex).Planes,1)
    %                     each_plane = SDem(llrindex).Planes(i,1) *     mesh_R_ext + ...
    %                                  SDem(llrindex).Planes(i,2) * abs(mesh_R_ext) + ...
    %                                  SDem(llrindex).Planes(i,3) *     mesh_I_ext + ...
    %                                  SDem(llrindex).Planes(i,4) * abs(mesh_I_ext) + ...
    %                                  SDem(llrindex).Planes(i,5);
    %                     best_fit_error = min(best_fit_error, abs(currentLLR - each_plane));
    %                 end
    %                 surf(mesh_R_ext, mesh_I_ext, best_fit_error) 
    %                 xlabel('real')
    %                 ylabel('imag');
    %                 zlabel('fit error abs');
    %                 title('LLR[' + string(llrindex) + ']')
                    %%%%%%%%%%%%%% DEBUG STOP




                    %Simplificating mirror of mirrors
                    auxPlanes = SDem(llrindex).Planes(1,:);
                    RegionCounter = 1;
                    for idxcheck=2:size(SDem(llrindex).Planes,1)
                        survivingregion = true;
                        for idxref=1:RegionCounter
                            if (abs(SDem(llrindex).Planes(idxcheck,5) - auxPlanes(idxref,5)) < MaxErrorAbs) %same offset
                                if ((SDem(llrindex).Planes(idxcheck,1) ~= 0) && ... %non zero slope
                                        (SDem(llrindex).Planes(idxcheck,1) + auxPlanes(idxref,1) == 0) && ... %mirror on R
                                        (SDem(llrindex).Planes(idxcheck,3) == auxPlanes(idxref,3)) && ...
                                        (sign(min(mesh_R(SDem(llrindex).Regions == idxcheck))) == sign(max(mesh_R(SDem(llrindex).Regions == idxcheck)))) && ...
                                        (sign(min(mesh_R(SDem(llrindex).Regions == idxref))) == sign(max(mesh_R(SDem(llrindex).Regions == idxref))))) % equal on I
                                    auxPlanes(idxref,2) = auxPlanes(idxref,1) * sign(sum(sum(mesh_R(SDem(llrindex).Regions == idxref))));
                                    auxPlanes(idxref,1) = 0;
                                    SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = idxref;
                                    survivingregion = false;
                                elseif ((SDem(llrindex).Planes(idxcheck,3) ~= 0) && ... &non zero slope
                                        (SDem(llrindex).Planes(idxcheck,1) == auxPlanes(idxref,1)) && ... %equal on R
                                        (SDem(llrindex).Planes(idxcheck,3) + auxPlanes(idxref,3) == 0) && ...
                                        (sign(min(mesh_I(SDem(llrindex).Regions == idxcheck))) == sign(max(mesh_I(SDem(llrindex).Regions == idxcheck)))) && ...
                                        (sign(min(mesh_I(SDem(llrindex).Regions == idxref  ))) == sign(max(mesh_I(SDem(llrindex).Regions == idxref  ))))) %mirror on I
                                    auxPlanes(idxref,4) = auxPlanes(idxref,3) * sign(sum(sum(mesh_I(SDem(llrindex).Regions == idxref))));
                                    auxPlanes(idxref,3) = 0;
                                    SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = idxref;
                                    survivingregion = false;
                                end
                            end
                        end
                        if (survivingregion)
                            RegionCounter = RegionCounter + 1;
                            auxPlanes(RegionCounter,:) = SDem(llrindex).Planes(idxcheck,:);
                            SDem(llrindex).Regions(SDem(llrindex).Regions == idxcheck) = RegionCounter;
                        end
                    end
                    SDem(llrindex).Planes = auxPlanes;


                    %%%%%%%%%%%%%DEBUG START
    %                 figure('Name', "LLR " + string(llrindex) + " best_fit_after_eliminating_mirrors_of_mirrors ");
    %                 best_fit_error = abs(currentLLR);
    %                 for i = 1: size(SDem(llrindex).Planes,1)
    %                     each_plane = SDem(llrindex).Planes(i,1) *     mesh_R_ext + ...
    %                                  SDem(llrindex).Planes(i,2) * abs(mesh_R_ext) + ...
    %                                  SDem(llrindex).Planes(i,3) *     mesh_I_ext + ...
    %                                  SDem(llrindex).Planes(i,4) * abs(mesh_I_ext) + ...
    %                                  SDem(llrindex).Planes(i,5);
    %                     best_fit_error = min(best_fit_error, abs(currentLLR - each_plane));
    %                 end
    %                 surf(mesh_R_ext, mesh_I_ext, best_fit_error) 
    %                 xlabel('real')
    %                 ylabel('imag');
    %                 zlabel('fit error abs');
    %                 title('LLR[' + string(llrindex) + ']')
                    %%%%%%%%%%%%%% DEBUG STOP
                end
                
                aux_currentLLR = currentLLR(2:end-1,2:end-1);
                for r=1:size(aux_currentLLR,1)
                    for c=1:size(aux_currentLLR,2)
                        point = [mesh_R(1,c); abs(mesh_R(1,c)); mesh_I(r,1); abs(mesh_I(r,1)); 1];
                        fitalternatives = SDem(llrindex).Planes * point;
                        if (SDem(llrindex).Regions(r,c) == 0)
                            [err, reg] = min(abs(aux_currentLLR(r,c) - fitalternatives));
                            SDem(llrindex).Regions(r,c) = reg;
                        end
                        SDem(llrindex).Approximation(r,c) = fitalternatives(SDem(llrindex).Regions(r,c));
                    end
                end
                MaxError = max(max(abs(aux_currentLLR - SDem(llrindex).Approximation)));
                fprintf("\n LLR[%d] plane[%d], MaxError = %f", llrindex, planeindex, MaxError);
                
                
                figure
                subplot(2,2,1)
                surf(mesh_R, mesh_I, currentLLR(2:end-1,2:end-1))
                xlabel('real')
                ylabel('imag');
                zlabel('LLR');
                
                subplot(2,2,2)
                surf(mesh_R, mesh_I, SDem(llrindex).Approximation)
                xlabel('real')
                ylabel('imag');
                zlabel('LLR approximation');
                
                
                subplot(2,2,3)
                surf(mesh_R, mesh_I, SDem(llrindex).Regions)
                title('LLR[' + string(llrindex) + '] regions')
                xlabel('real')
                ylabel('imag');
                zlabel('LLR regions');
                view(0,90);
                
                
                subplot(2,2,4)
                surf(mesh_R, mesh_I, currentLLR(2:end-1,2:end-1) - SDem(llrindex).Approximation)
                title('LLR[' + string(llrindex) + ']')
                xlabel('real')
                ylabel('imag');
                zlabel('LLR Error');
                
                borderindex = 1;
                allborders = 0*SDem(llrindex).Regions(1:end-1,1:end-1);
                for ip1 = 1:size(SDem(llrindex).Planes,1)-1
                    for ip2 = ip1+1:size(SDem(llrindex).Planes,1)
                        allborders = allborders + ...
                                     1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip1) + abs(SDem(llrindex).Regions(1:end-1,2:end)-ip2)) + ...
                                     1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip2) + abs(SDem(llrindex).Regions(1:end-1,2:end)-ip1)) + ...
                                     1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip1) + abs(SDem(llrindex).Regions(2:end,1:end-1)-ip2)) + ...
                                     1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip2) + abs(SDem(llrindex).Regions(2:end,1:end-1)-ip1));
                        border_R = sum(sum(1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip1) + abs(SDem(llrindex).Regions(1:end-1,2:end)-ip2)) + ...
                                   1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip2) + abs(SDem(llrindex).Regions(1:end-1,2:end)-ip1))));
                        border_I = sum(sum(1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip1) + abs(SDem(llrindex).Regions(2:end,1:end-1)-ip2)) + ...
                                   1*(0==abs(SDem(llrindex).Regions(1:end-1,1:end-1)-ip2) + abs(SDem(llrindex).Regions(2:end,1:end-1)-ip1))));
                        border_R = max(0, border_R-2);
                        border_I = max(0, border_I-2);
%                         if ((llrindex == 3) && (ip1 == 1) && (ip2 == 2))
%                             pepe = 2;
%                         end
                        if (border_R + border_I > 1)
                            SDem(llrindex).Borders(borderindex).R1 = ip1;
                            SDem(llrindex).Borders(borderindex).R2 = ip2;
                            syms I Q;
                            if (border_R >= border_I)
                                brdr = solve(SDem(llrindex).Planes(ip1,1)*I + ...
                                             SDem(llrindex).Planes(ip1,2)*sqrt(I^2) + ...
                                             SDem(llrindex).Planes(ip1,3)*Q + ...
                                             SDem(llrindex).Planes(ip1,4)*sqrt(Q^2) + ...
                                             SDem(llrindex).Planes(ip1,5) == ...
                                             SDem(llrindex).Planes(ip2,1)*I + ...
                                             SDem(llrindex).Planes(ip2,2)*sqrt(I^2) + ...
                                             SDem(llrindex).Planes(ip2,3)*Q + ...
                                             SDem(llrindex).Planes(ip2,4)*sqrt(Q^2) + ...
                                             SDem(llrindex).Planes(ip2,5), I, 'Real', true); 
                                if (size(brdr,1) == 0) 
                                    brdr = solve(SDem(llrindex).Planes(ip1,1)*I + ...
                                                 SDem(llrindex).Planes(ip1,2)*sqrt(I^2) + ...
                                                 SDem(llrindex).Planes(ip1,3)*Q + ...
                                                 SDem(llrindex).Planes(ip1,4)*sqrt(Q^2) + ...
                                                 SDem(llrindex).Planes(ip1,5) == ...
                                                 SDem(llrindex).Planes(ip2,1)*I + ...
                                                 SDem(llrindex).Planes(ip2,2)*sqrt(I^2) + ...
                                                 SDem(llrindex).Planes(ip2,3)*Q + ...
                                                 SDem(llrindex).Planes(ip2,4)*sqrt(Q^2) + ...
                                                 SDem(llrindex).Planes(ip2,5), Q, 'Real', true); 

                                    if (size(brdr,1)>1)
                                        if (subs(brdr(1),0.489) > 0)
                                            SDem(llrindex).Borders(borderindex).Eq = sprintf("|Q| = %s;", brdr(1));
                                        else
                                            SDem(llrindex).Borders(borderindex).Eq = sprintf("|Q| = %s;", brdr(2));
                                        end
                                    else
                                        SDem(llrindex).Borders(borderindex).Eq = sprintf("Q = %s;", brdr);
                                    end
                                    SDem(llrindex).Borders(borderindex).Eq = strrep(SDem(llrindex).Borders(borderindex).Eq, "(I^2)^(1/2)", "|I|");
                                elseif (size(brdr,1)>1)
                                    if (subs(brdr(1),0.489) > 0)
                                        SDem(llrindex).Borders(borderindex).Eq = sprintf("|I| = %s;", brdr(1));
                                    else
                                        SDem(llrindex).Borders(borderindex).Eq = sprintf("|I| = %s;", brdr(2));
                                    end
                                else
                                    SDem(llrindex).Borders(borderindex).Eq = sprintf("I = %s;", brdr);
                                end
                                SDem(llrindex).Borders(borderindex).Eq = strrep(SDem(llrindex).Borders(borderindex).Eq, "(Q^2)^(1/2)", "|Q|");
                            else %(border_R == 0)
                                brdr = solve(SDem(llrindex).Planes(ip1,1)*I + ...
                                             SDem(llrindex).Planes(ip1,2)*sqrt(I^2) + ...
                                             SDem(llrindex).Planes(ip1,3)*Q + ...
                                             SDem(llrindex).Planes(ip1,4)*sqrt(Q^2) + ...
                                             SDem(llrindex).Planes(ip1,5) == ...
                                             SDem(llrindex).Planes(ip2,1)*I + ...
                                             SDem(llrindex).Planes(ip2,2)*sqrt(I^2) + ...
                                             SDem(llrindex).Planes(ip2,3)*Q + ...
                                             SDem(llrindex).Planes(ip2,4)*sqrt(Q^2) + ...
                                             SDem(llrindex).Planes(ip2,5), Q, 'Real', true); 
                                if (size(brdr,1) == 0) 
                                    brdr = solve(SDem(llrindex).Planes(ip1,1)*I + ...
                                             SDem(llrindex).Planes(ip1,2)*sqrt(I^2) + ...
                                             SDem(llrindex).Planes(ip1,3)*Q + ...
                                             SDem(llrindex).Planes(ip1,4)*sqrt(Q^2) + ...
                                             SDem(llrindex).Planes(ip1,5) == ...
                                             SDem(llrindex).Planes(ip2,1)*I + ...
                                             SDem(llrindex).Planes(ip2,2)*sqrt(I^2) + ...
                                             SDem(llrindex).Planes(ip2,3)*Q + ...
                                             SDem(llrindex).Planes(ip2,4)*sqrt(Q^2) + ...
                                             SDem(llrindex).Planes(ip2,5), I, 'Real', true); 
                                    if (size(brdr,1)>1)
                                        if (subs(brdr(1),0.489) > 0)
                                            SDem(llrindex).Borders(borderindex).Eq = sprintf("|I| = %s;", brdr(1));
                                        else
                                            SDem(llrindex).Borders(borderindex).Eq = sprintf("|I| = %s;", brdr(2));
                                        end
                                    else
                                        SDem(llrindex).Borders(borderindex).Eq = sprintf("I = %s;", brdr);
                                    end                                    
                                elseif (size(brdr,1)>1)
                                    if (subs(brdr(1),0.489) > 0)
                                        SDem(llrindex).Borders(borderindex).Eq = sprintf("|Q| = %s;", brdr(1));
                                    else
                                        SDem(llrindex).Borders(borderindex).Eq = sprintf("|Q| = %s;", brdr(2));
                                    end
                                else
                                    SDem(llrindex).Borders(borderindex).Eq = sprintf("Q = %s;", brdr);
                                end
                                SDem(llrindex).Borders(borderindex).Eq = strrep(SDem(llrindex).Borders(borderindex).Eq, "(I^2)^(1/2)", "|I|");
%                             else
%                                 optiona = solve(SDem(llrindex).Planes(ip1,1)*I + ...
%                                              SDem(llrindex).Planes(ip1,2)*sqrt(I^2) + ...
%                                              SDem(llrindex).Planes(ip1,3)*Q + ...
%                                              SDem(llrindex).Planes(ip1,4)*sqrt(Q^2) + ...
%                                              SDem(llrindex).Planes(ip1,5) == ...
%                                              SDem(llrindex).Planes(ip2,1)*I + ...
%                                              SDem(llrindex).Planes(ip2,2)*sqrt(I^2) + ...
%                                              SDem(llrindex).Planes(ip2,3)*Q + ...
%                                              SDem(llrindex).Planes(ip2,4)*sqrt(Q^2) + ...
%                                              SDem(llrindex).Planes(ip2,5), I, 'Real', true); 
%                                  optionb =  solve(SDem(llrindex).Planes(ip1,1)*I + ...
%                                              SDem(llrindex).Planes(ip1,2)*sqrt(I^2) + ...
%                                              SDem(llrindex).Planes(ip1,3)*Q + ...
%                                              SDem(llrindex).Planes(ip1,4)*sqrt(Q^2) + ...
%                                              SDem(llrindex).Planes(ip1,5) == ...
%                                              SDem(llrindex).Planes(ip2,1)*I + ...
%                                              SDem(llrindex).Planes(ip2,2)*sqrt(I^2) + ...
%                                              SDem(llrindex).Planes(ip2,3)*Q + ...
%                                              SDem(llrindex).Planes(ip2,4)*sqrt(Q^2) + ...
%                                              SDem(llrindex).Planes(ip2,5), Q, 'Real', true); 
%                                  if ((length(optiona) <= length(optionb)) || (length(optionb) == 0))
%                                     SDem(llrindex).Borders(borderindex).Eq = sprintf("I = %s;", brdr);
%                                     SDem(llrindex).Borders(borderindex).Eq = strrep(SDem(llrindex).Borders(borderindex).Eq, "(Q^2)^(1/2)", "|Q|");
%                                  else
%                                     SDem(llrindex).Borders(borderindex).Eq = sprintf("Q = %s;", brdr);
%                                     SDem(llrindex).Borders(borderindex).Eq = strrep(SDem(llrindex).Borders(borderindex).Eq, "(I^2)^(1/2)", "|I|");
%                                  end
                            end         
                            borderindex = borderindex+1;
                        end
                    end
                end
                %figure
                %spy(allborders)
                SDem(llrindex).Planes = SDem(llrindex).Planes;
            end
        end
        function CreateSoftDemapperReport(obj, SD, filename)
            
            FileHandler = fopen(filename,'w');
            for llrindex=1:length(SD)
                fprintf(FileHandler, "\n\nLLR %d", llrindex-1);
                for planeindex=1:size(SD(llrindex).Planes,1)
                    fprintf(FileHandler, "\n----Region %d: LLR(I,Q) = ", planeindex-1);
                    if (SD(llrindex).Planes(planeindex,1) ~= 0)
                        fprintf(FileHandler, " %+f * I ", SD(llrindex).Planes(planeindex,1));
                    end
                    if (SD(llrindex).Planes(planeindex,2) ~= 0)
                        fprintf(FileHandler, " %+f * |I| ", SD(llrindex).Planes(planeindex,2));
                    end
                    if (SD(llrindex).Planes(planeindex,3) ~= 0)
                        fprintf(FileHandler, " %+f * Q ", SD(llrindex).Planes(planeindex,3));
                    end
                    if (SD(llrindex).Planes(planeindex,4) ~= 0)
                        fprintf(FileHandler, " %+f * |Q| ", SD(llrindex).Planes(planeindex,4));
                    end
                    if (SD(llrindex).Planes(planeindex,5) ~= 0)
                        fprintf(FileHandler, " %+f", SD(llrindex).Planes(planeindex,5));
                    end
                end
                for borderindex=1: length(SD(llrindex).Borders)
                    aux = strsplit(SD(llrindex).Borders(borderindex).Eq, ';');
                    for eqidx=1:length(aux)
                        if (length(char(aux(eqidx)))>1)
                            fprintf(FileHandler, "\n----Boundary [%d - %d]: %s", SD(llrindex).Borders(borderindex).R1-1, SD(llrindex).Borders(borderindex).R2-1, aux(eqidx));
                        end
                    end
                end
            end
            fclose(FileHandler);
        end
        function [SDRegBank] = CreateSoftDemapperRegisterBank(obj, SD)
            SDRegBank = [];
            for idx_llr = 1 : length(SD)
                for idx_plane = 1: size(SD(idx_llr).Planes,1)
                    fieldname = char(sprintf("LLR_%d_region_%d_PlaneOffset", idx_llr-1, idx_plane-1));
                    fieldnameEq = char(sprintf("LLR_%d_region_%d", idx_llr-1, idx_plane-1));
                    aux_LLR_eq = sprintf("LLR_%d_r%d(I,Q) = ", idx_llr, idx_plane-1);
                    if (SD(idx_llr).Planes(idx_plane,1) ~= 0)
                        aux_LLR_eq = sprintf("%s %+f * I ", aux_LLR_eq, SD(idx_llr).Planes(idx_plane,1));
                    end
                    if (SD(idx_llr).Planes(idx_plane,2) ~= 0)
                        aux_LLR_eq = sprintf("%s %+f * |I| ", aux_LLR_eq, SD(idx_llr).Planes(idx_plane,2));
                    end
                    if (SD(idx_llr).Planes(idx_plane,3) ~= 0)
                        aux_LLR_eq = sprintf("%s %+f * Q ", aux_LLR_eq, SD(idx_llr).Planes(idx_plane,3));
                    end
                    if (SD(idx_llr).Planes(idx_plane,4) ~= 0)
                         aux_LLR_eq = sprintf("%s %+f * |Q| ", aux_LLR_eq, SD(idx_llr).Planes(idx_plane,4));
                    end
                    if (SD(idx_llr).Planes(idx_plane,5) ~= 0)
                        aux_LLR_eq = sprintf("%s %+f", aux_LLR_eq, SD(idx_llr).Planes(idx_plane,5));
                    end
                    SDRegBank.(fieldnameEq) = char(aux_LLR_eq);
                    SDRegBank.(fieldname) = SD(idx_llr).Planes(idx_plane,end);
                end
                for idx_border = 1: length(SD(idx_llr).Borders)
                    fieldnameEq = char(sprintf("LLR_%d_Border_%d_%d", idx_llr-1, SD(idx_llr).Borders(idx_border).R1-1, SD(idx_llr).Borders(idx_border).R2-1));
                    fieldname = char(sprintf("LLR_%d_Border_%d_%d_Offset", idx_llr-1, SD(idx_llr).Borders(idx_border).R1-1, SD(idx_llr).Borders(idx_border).R2-1));
                    SDRegBank.(fieldnameEq) = char(SD(idx_llr).Borders(idx_border).Eq);
                    aux = strsplit(SD(idx_llr).Borders(idx_border).Eq, "=");
                    aux = strsplit(aux(end), ";");
                    aux = aux(1);
                    aux = strrep(aux, "|I|", "(I^2)^(1/2)");
                    aux = strrep(aux, "|Q|", "(Q^2)^(1/2)");
                    offset = eval(subs( evalin(symengine,aux), 0));
                    SDRegBank.(fieldname) = offset;
                end
            end
            
        end
        function PlotConstellation(obj)
            delta = 0.1;
            bit = 1;
            figure
            plot(real(obj.Constellation), imag(obj.Constellation), 'ko')
            grid on
            for i=1:length(obj.Constellation)
                index=fi(i-1,0,log2(length(obj.Constellation)),0);
                text(real(obj.Constellation(i))-delta,imag(obj.Constellation(i))-delta,dec2bin(i-1, obj.BitsPerSymbol))
            end
            axis([-1 1 -1 1]);
            xlabel('I');
            ylabel('Q');
            title('Constellation points');
        end
        function PlotConstellationPerBit(obj, BitIndex, NPoints)
            if (BitIndex > obj.BitsPerSymbol)
                error('Bit index too big for the constellation')
            end
            [r,i] = meshgrid(linspace(-1, 1,NPoints), ...
                linspace(-1, 1,NPoints));
            Grid = reshape(r + 1i*i,[],1);
            
            [LLRs_Theo, elapsedTime] = obj.DemapMessage_Theoretical(Grid);
            
            LLRs_Theo = (reshape(LLRs_Theo, obj.BitsPerSymbol, [])).';
            
            
            
            figure
            subplot(4,4,[1:3,5:7,9:11])
            plot(real(Grid(LLRs_Theo(:,BitIndex)>= 0)), imag(Grid(LLRs_Theo(:,BitIndex)>= 0)), 'ro')
            hold on
            plot(real(Grid(LLRs_Theo(:,BitIndex)< 0)), imag(Grid(LLRs_Theo(:,BitIndex)< 0)), 'bx')
            xlabel('I')
            ylabel('Q')
            grid
            subplot(4,4,[13:15])
            plot(real(Grid), LLRs_Theo(:,BitIndex), 'b.')
            xlabel('I')
            ylabel('LLR')
            grid
            subplot(4,4,[4:4:12])
            plot(LLRs_Theo(:,BitIndex), imag(Grid), 'b.')
            xlabel('LLR THEO')
            ylabel('Q')
            grid
            
            fprintf('\nTheoretical: LLR E [%f, %f]', min(LLRs_Theo(:,BitIndex)), max(LLRs_Theo(:,BitIndex)))
            fprintf('\n');
        end
    end
    
end