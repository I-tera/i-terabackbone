# Install script for directory: /home/drosso/Documents/wrappers/umevectorNT/umesimd

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/umesimd" TYPE DIRECTORY FILES "/home/drosso/Documents/wrappers/umevectorNT/umesimd/doc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/umesimd" TYPE DIRECTORY FILES "/home/drosso/Documents/wrappers/umevectorNT/umesimd/examples")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/umesimd" TYPE DIRECTORY FILES "/home/drosso/Documents/wrappers/umevectorNT/umesimd/plugins")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/umesimd" TYPE DIRECTORY FILES "/home/drosso/Documents/wrappers/umevectorNT/umesimd/utilities")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/umesimd" TYPE FILE FILES
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMEBasicTypes.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMEInline.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMEMemory.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimd.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimdInterface.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimdInterfaceFunctions.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimdScalarEmulation.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimdScalarOperators.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimdTraits.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/UMESimdVectorEmulation.h"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/README.md"
    "/home/drosso/Documents/wrappers/umevectorNT/umesimd/LICENSE"
    )
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/drosso/Documents/wrappers/umevectorNT/umesimd/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
