#include <cmath> 
#include "UMESimd.h"

#ifndef C_FXP_C_H
#define C_FXP_C_H

/*
  #define D_SatS_Coef_p(nb, nbf) ((float)((pow(2,nb-1)-1)/pow(2,nbf)))
  #define D_SatS_Coef_n(nb, nbf) ((float)(-(pow(2,nb-1)/pow(2,nbf))))
  #define D_SatS(a, nb, nbf)     ((a>D_SatS_Coef_p(nb, nbf)) ? D_SatS_Coef_p(nb,nbf) : ((a<D_SatS_Coef_n(nb,nbf)) ? D_SatS_Coef_n(nb,nbf) : a))

  #define D_SatU_Coef(nb, nbf)   ((float)((pow(2,nb)-1)/pow(2,nbf)))
  #define D_SatU(a, nb, nbf)     ((a>D_SatU_Coef(nb,nbf)) ? D_SatU_Coef(nb,nbf) : a)

  #define D_Round_Coef(nbf)      (float(3* pow(2,22-nbf)))
  #define D_Round_Corr(nb,nbf)   0.0f//(float(1.0f * pow(2,-nbf-(24-nb))))
  #define D_Trunc_Coef(nb,nbf)   (float(pow(2,-nbf-1))-D_Round_Corr(nb,nbf))
  #define D_Round(a, nb, nbf)    ((a<0) ? (((a)+D_Round_Coef(nbf))-D_Round_Coef(nbf)) : -(((-a)-D_Round_Coef(nbf))+D_Round_Coef(nbf)))
  #define D_Trunc(a, nb, nbf)    (((a-D_Trunc_Coef(nb,nbf))+D_Round_Coef(nbf))-D_Round_Coef(nbf))

  //#define D_SatS_Round(a, nb, nbf) (D_Round(D_SatS(a,nb,nbf),nb,nbf))
  //#define D_SatU_Round(a, nb, nbf) (D_Round(D_SatU(a,nb,nbf),nb,nbf))


  //#define D_SatS_Trunc(a, nb, nbf) (D_Trunc(D_SatS(a,nb,nbf),nb,nbf))
  //#define D_SatU_Trunc(a, nb, nbf) (D_Trunc(D_SatU(a,nb,nbf),nb,nbf))

  #define D_LSB_Weight(nbf)     (float(pow(2, -nbf)))
  #define D_LSB_WeightInv(nbf)  (float(pow(2, nbf)))


  #define D_SatS_Trunc(a, nb, nbf) (D_SatS((floor(a*D_LSB_WeightInv(nbf))*D_LSB_Weight(nbf)),nb,nbf))
  #define D_SatU_Trunc(a, nb, nbf) (D_SatU(floor(a*D_LSB_WeightInv(nbf))*D_LSB_Weight(nbf)),nb,nbf))

  #define D_SatS_Round(a, nb, nbf) (D_SatS((floor((a+D_LSB_Weight(nbf)/2)*D_LSB_WeightInv(nbf))*D_LSB_Weight(nbf)),nb,nbf))
  #define D_SatU_Round(a, nb, nbf) (D_SatU((floor((a+D_LSB_Weight(nbf)/2)*D_LSB_WeightInv(nbf))*D_LSB_Weight(nbf)),nb,nbf))// */



template <int T_P, class T_E>
class c_fxp_t
{
private:
  T_E aux_sat_high;
  T_E aux_sat_low;
  T_E aux_lsb_weight;
  T_E aux_round_coef;
  T_E aux_lsb_weight_inv;
  int nb;
  int nbf;
  bool is_signed;
  bool round;
public:
  c_fxp_t();
  c_fxp_t(int i_nb, int i_nbf, bool i_is_signed, bool i_round) {initialize(i_nb, i_nbf, i_is_signed, i_round);}
  void initialize(int i_nb, int i_nbf, bool i_is_signed, bool i_round);
  void apply_fxp(T_E& variable);
  UME::SIMD::SIMDVec<int32_t,T_P> float_2_int_eq(T_E variable);
  T_E int_eq_2_float(UME::SIMD::SIMDVec<int32_t,T_P> variable);
  T_E int_eq_2_float(int32_t variable);
  T_E apply_sat(T_E& variable);
  int get_nb(){return nb;}
  int get_nbf(){return nbf;}
  int get_is_signed(){return is_signed;}
  int get_round(){return round;}
  T_E get_lsb_weight(){return aux_lsb_weight;}
  };

  
template <int T_P, class T_E>
  void c_fxp_t<T_P, T_E>::initialize(int i_nb, int i_nbf, bool i_is_signed, bool i_round)
{
  nb = i_nb;
  nbf = i_nbf;
  is_signed = i_is_signed;
  round = i_round;
  if (is_signed)
    {
      aux_sat_high = ((float)((pow(2,nb-1)-1)/pow(2,nbf)));
      aux_sat_low  = ((float)(-(pow(2,nb-1)/pow(2,nbf))));
    }
  else
    {
      aux_sat_high = ((float)((pow(2,nb)-1)/pow(2,nbf)));
      aux_sat_low  = 0.0f;
    }
  aux_lsb_weight = (float(pow(2, -nbf)));
  if (round)
    aux_round_coef = aux_lsb_weight / 2;
  else
    aux_round_coef = 0;
    
  aux_lsb_weight_inv = (float(pow(2, nbf)));
}

  
template <>
void c_fxp_t<8, UME::SIMD::SIMDVec<uint32_t,8>>::apply_fxp(UME::SIMD::SIMDVec<uint32_t,8>& variable)
{
  UME::SIMD::SIMDVecMask<8> m1;
  m1 = variable > aux_sat_high;
  variable[m1] = aux_sat_high; //saturation positive
  m1 = variable < aux_sat_low;
  variable[m1] = aux_sat_low; //saturation negative
}

template <int T_P, class T_E>
void c_fxp_t<T_P, T_E>::apply_fxp(T_E& variable)
{
  UME::SIMD::SIMDVecMask<T_P> m1;
  variable = (variable + aux_round_coef) * aux_lsb_weight_inv;
  variable = variable.floor() * aux_lsb_weight;
  m1 = variable > aux_sat_high;
  variable[m1] = aux_sat_high; //saturation positive
  m1 = variable < aux_sat_low;
  variable[m1] = aux_sat_low; //saturation negative
}


template <int T_P, class T_E>
UME::SIMD::SIMDVec<int32_t,T_P> c_fxp_t<T_P, T_E>::float_2_int_eq(T_E variable)
{
  T_E local_var = variable;
  apply_fxp(local_var);
  return (UME::SIMD::SIMDVec<int32_t,T_P>)(local_var * aux_lsb_weight_inv);
}

template <int T_P, class T_E>
T_E c_fxp_t<T_P, T_E>::int_eq_2_float(UME::SIMD::SIMDVec<int32_t,T_P> variable)
{
  T_E ret = (T_E)(variable) * aux_lsb_weight;
  apply_fxp(ret);
  return ret;
}


template <int T_P, class T_E>
T_E c_fxp_t<T_P, T_E>::int_eq_2_float(int32_t variable)
{
  T_E ret = (T_E)(variable) * aux_lsb_weight;
  apply_fxp(ret);
  return ret;
}

  
template <>
void c_fxp_t<1, float>::apply_fxp(float& variable)
{
  variable = (floor((variable + aux_round_coef) * aux_lsb_weight_inv)) * aux_lsb_weight;
  variable = (variable > aux_sat_high) ? aux_sat_high : variable;
  variable = (variable < aux_sat_low)  ? aux_sat_low  : variable;
}



template <int T_P, class T_E>
  T_E c_fxp_t<T_P, T_E>::apply_sat(T_E& variable)
{
  T_E myvar = variable;
  UME::SIMD::SIMDVecMask<T_P> m1;
  m1 = myvar > aux_sat_high;
  myvar[m1] = aux_sat_high; //saturation positive
  m1 = myvar < aux_sat_low;
  myvar[m1] = aux_sat_low; //saturation negative
  return myvar;
}


template <>
float c_fxp_t<1, float>::apply_sat(float& variable)
{
  float myvar = variable;
  myvar = (myvar > aux_sat_high) ? aux_sat_high : myvar;
  myvar = (myvar < aux_sat_low)  ? aux_sat_low  : myvar;
  return myvar;
}
#endif
