#ifndef DYNAMIC_MEMORY_HPP
#define DYNAMIC_MEMORY_HPP
#include <sstream>
#include <sys/stat.h>
#include <memory>


inline void *std_align( std::size_t alignment,
                        std::size_t size,
                        void *&ptr,
                        std::size_t &space )
{
  std::uintptr_t pn = reinterpret_cast< std::uintptr_t >( ptr );
  std::uintptr_t aligned = ( pn + alignment - 1 ) & - alignment;
  std::size_t padding = aligned - pn;
  if ( space < size + padding ) return nullptr;
  space -= padding;
  return ptr = reinterpret_cast< void * >( aligned );
  }

template <class T>
inline char *allocate_aligned_memory( uint32_t nof_elements,
                                      uint32_t alignment,
                                      T *&ptr,
                                      const char* variable_name)
{
  try
    {
      void *aux_ptr;
      std::size_t required_size = sizeof(T)*nof_elements + alignment -1;
      aux_ptr = new char[required_size];
      if (aux_ptr == nullptr)
	{throw std::runtime_error("ERROR: dynamic_memory: allocate_aligned_memory");}
      void *aux_for_delete;
      aux_for_delete= aux_ptr;
      if (std_align(alignment, sizeof(T)*(nof_elements), aux_ptr, required_size))
	ptr = reinterpret_cast<T*>(aux_ptr);
      else
	{throw std::runtime_error("ERROR: dynamic_memory: allocate_aligned_memory");}
#ifdef D_PLOT_MEMORY
      printf("\n %12u [B] allocated to %s", uint32_t(sizeof(T))*nof_elements, variable_name);
#endif
      return reinterpret_cast<char*>(aux_for_delete);
    }
  catch (...)
    {
      throw std::runtime_error("ERROR: dynamic_memory: allocate_aligned_memory");
    }
}

template <class T>
inline T *allocate_memory(uint32_t nof_elements,
			  const char* variable_name)
{
  try
    {
      T *new_pointer;
      new_pointer = new T[nof_elements];
      if (new_pointer == nullptr)
	{throw std::runtime_error("ERROR: dynamic_memory: allocate_memory");}
#ifdef D_PLOT_MEMORY
      printf("\n %12u [B] allocated to %s", uint32_t(sizeof(T))*nof_elements, variable_name);
#endif
      return new_pointer;
    }
  catch (...)
    {
      throw std::runtime_error("ERROR: dynamic_memory: allocate_memory");
    }
}
  
                                      




#endif
