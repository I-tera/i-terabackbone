#include "../../AWGN/system/C_AWGN_T.h"
#include <stdio.h>
#include <yaml-cpp/yaml.h>
#include <stdio.h>
#include <chrono>

#include "../../AWGN/system/C_AWGN.h"
#include "../../AWGN/system/C_AWGN.cpp"
#include "c_vector_logger_t.h"

#include <stdio.h>
#include <random>
#include "../../AWGN/system/mt.cc"

#define D_STORE_ARRAYS
#define NElements  300
#define NMessages  10
#define DSIGMA  1.5f

#define D_P 8
#define D_BE float
#define D_E UME::SIMD::SIMDVec<D_BE, D_P>

using namespace std;


int main(void)
{
  //aux variables
  std::size_t RequiredSize;
  void *p;
  YAML::Node config = YAML::LoadFile("TB_Input.yaml");
  std::chrono::duration<double> SISD_duration(0.0);
  std::chrono::duration<double> SIMDT_duration_a(0.0);
  
  ///////////////////////////////////////////////////////////////
  //Sequential random number generator with C++ random library
  ///////////////////////////////////////////////////////////////
  float *GaussianSeqR = new float[NElements];
  float *GaussianSeqI = new float[NElements];
  std::default_random_engine generator;
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<float> distribution(0.0, DSIGMA);
  
#ifdef D_STORE_ARRAYS
  c_vector_logger_t<float> Logger_SISD_R("Results/AWGN_SISD_R.txt", &GaussianSeqR[0], "noheader", NElements, 5, 2, true, true);
  c_vector_logger_t<float> Logger_SISD_I("Results/AWGN_SISD_I.txt", &GaussianSeqI[0], "noheader", NElements, 5, 2, true, false);
  
  FILE *p_file1;
  FILE *p_file2;
  p_file1 = fopen("Results/SequentialRandR.txt", "w");
  p_file2 = fopen("Results/SequentialRandI.txt", "w");
#endif
  
  //////////////////////////////////////////////////////////////////////////
  // SIMD //////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  //Generation of the SIMD array
  C_AWGN_T<D_E, D_BE, D_P> myAWGN(config);
  D_E Sigma_a = (D_E)DSIGMA;
  D_E *Real_a;
  RequiredSize = sizeof(D_E)*(NElements) + 128 - 1;
  char* auxForAlloc1_a = new char[RequiredSize];
  p = (auxForAlloc1_a);
  if (std::align(128, sizeof(D_E)*NElements, p, RequiredSize))
    Real_a = reinterpret_cast<D_E*>(p);
  else
    throw 889;

  D_E *Imag_a;
  RequiredSize = sizeof(D_E)*(NElements) + 128 - 1;
  char* auxForAlloc2_a = new char[RequiredSize];
  p = (auxForAlloc2_a);
  if (std::align(128, sizeof(D_E)*NElements, p, RequiredSize))
    Imag_a = reinterpret_cast<D_E*>(p);
  else
    throw 889;


#ifdef D_STORE_ARRAYS
  c_vector_logger_t<UME::SIMD::SIMDVec<D_BE, D_P>> Logger_SIMD_R("Results/AWGN_SIMD_R.txt", &Real_a[0], "myheader", NElements, 5, 2, true, true);
  c_vector_logger_t<UME::SIMD::SIMDVec<D_BE, D_P>> Logger_SIMD_I("Results/AWGN_SIMD_I.txt", &Imag_a[0], "myheader", NElements, 5, 2, true, false);
  
  
  
  FILE *p_file_R;
  FILE *p_file_I;
  p_file_R = fopen("Results/AWGN_T_R.txt", "w");
  p_file_I = fopen("Results/AWGN_T_I.txt", "w");
#endif

  for (unsigned int MessageCounter = 0; MessageCounter < NMessages; MessageCounter++)
    {
      
      auto start = std::chrono::high_resolution_clock::now();  
      for (int i = 0; i<NElements; i++)
        {
          GaussianSeqR[i] = distribution(gen);
          GaussianSeqI[i] = distribution(gen);
          //printf("\n %f", GaussianSeq[i]);
        }
      auto finish = std::chrono::high_resolution_clock::now();
      SISD_duration += finish - start;
      
#ifdef D_STORE_ARRAYS
      Logger_SISD_R.add_log_entry();
      Logger_SISD_I.add_log_entry();
      
      for (int i = 0; i < NElements; i++)
        {
          fprintf(p_file1, "%f\n", GaussianSeqR[i]);
          fprintf(p_file2, "%f\n", GaussianSeqI[i]);
        }
#endif

      
      for (unsigned long i=0; i<NElements; i++)
        {
          Real_a[i] = (D_E)0.0f;
          Imag_a[i] = (D_E)0.0f;
        }

  
      
      auto start3 = std::chrono::high_resolution_clock::now();  
      myAWGN.Add_Noise_Complex(&Real_a[0], &Imag_a[0], Sigma_a, NElements);
      auto finish3 = std::chrono::high_resolution_clock::now();
      SIMDT_duration_a += finish3 - start3;

      
#ifdef D_STORE_ARRAYS
      Logger_SIMD_R.add_log_entry();
      Logger_SIMD_I.add_log_entry();
      
      D_BE auxXCompareR_a[D_P];
      D_BE auxXCompareI_a[D_P];
      for (unsigned long i=0; i<NElements; i++)
        {
          //printf("\n %6lu - ", i);
          Real_a[i].store(&auxXCompareR_a[0]);
          Imag_a[i].store(&auxXCompareI_a[0]);
          fprintf(p_file_R, "%f", auxXCompareR_a[0]);
          fprintf(p_file_I, "%f", auxXCompareI_a[0]);
          for (int j = 1; j < D_P; j++)
            {
              fprintf(p_file_R, ", %f", auxXCompareR_a[j]);
              fprintf(p_file_I, ", %f", auxXCompareI_a[j]);
              //printf(" | %4.1f %4.1f", auxXCompareR_a[j], auxXCompareI_a[j]);
              /*if ((RealSisd[i] != auxXCompareR_a[j]) ||
                (ImagSisd[i] != auxXCompareI_a[j]))
                {
                nErrors_a ++;
                }*/
            }
          fprintf(p_file_R, "\b\b\n");
          fprintf(p_file_I, "\b\b\n");
        }
#endif
    }

  
  float SISD_random_speed = float(NElements*NMessages)/SISD_duration.count();
  printf("\n\n SISD_random Speed: %5.1f [MBaud]", SISD_random_speed/1e6);
  
  float SIMDT_Speed_a = float(NElements*D_P*NMessages)/SIMDT_duration_a.count();
  printf("\n\n SIMDT_a <%2d x %2lub> Speed: %5.1f [MBaud]", D_P, 8*sizeof(D_BE), SIMDT_Speed_a/1e6);
#ifdef D_STORE_ARRAYS
  fclose(p_file1);
  fclose(p_file2);
  fclose(p_file_R);
  fclose(p_file_I);
#endif
  
  delete[] auxForAlloc1_a;
  delete[] auxForAlloc2_a;
  printf("\n");
  return 0;
}
  
