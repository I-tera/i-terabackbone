#include <stdio.h>
#include <cmath>
#include "UMESimd.h"
#include <cstring>
#include <string>
#include "c_fxp_t.hpp"

#ifndef C_ELEMENT_TO_LOG_H
#define C_ELEMENT_TO_LOG_H
using namespace std;

template <class T_E>
class c_vector_logger_t
{
 private:
  string  file_name;
  bool    insert_newline = true;
  FILE   *p_file = nullptr;
  T_E    *element_to_log;
  int     n_elements;
  int     nb;
  int     nbf;
  bool    is_signed;
  bool    int_eq;
  bool    first_entry;

 public:
  c_vector_logger_t(){p_file=nullptr; element_to_log=nullptr; n_elements=0;};
  c_vector_logger_t(string i_file_name,
                    T_E *i_element_to_log,
                    string i_Header,
                    int i_n_elements,
                    int i_nb, int i_nbf,
                    bool i_signed,
                    bool i_int_eq,
                    bool i_print_header = false)
    {initialize(i_file_name, *i_element_to_log, i_Header, i_n_elements, i_nb, i_nbf, i_signed, i_int_eq, i_print_header);}
  ~c_vector_logger_t(){if (p_file != nullptr) {fflush(p_file); if (p_file != stdout) {fclose(p_file);}}}

  void initialize(string i_file_name,
                  T_E   *i_element_to_log,
                  string i_Header,
                  int    i_n_elements,
                  int    i_nb,
                  int    i_nbf,
                  bool   i_signed,
                  bool   i_int_eq,
                  bool   i_print_header = false);

  void add_zero_entries(uint n_entries);
  void add_log_entry() {add_log_entry(element_to_log);};
  void add_log_entry(T_E *i_element_to_log);
  void add_log_entry_single_element(T_E *i_element_to_log);
  void add_newline() {if (!first_entry){ fprintf(p_file, "\n");}  first_entry = false;}
};


template <class T_E>
void c_vector_logger_t<T_E>::initialize(string i_file_name,
					T_E   *i_element_to_log,
					string i_Header,
					int    i_n_elements,
					int    i_nb,
					int    i_nbf,
					bool   i_signed,
					bool   i_int_eq,
					bool   i_print_header)
{
  file_name = i_file_name;
  if (i_file_name.compare("screen") == 0)
    {
      p_file = stdout;
      insert_newline = false;
    }
  else
    {
      insert_newline = true;
      p_file = fopen(file_name.c_str(), "w");
    }
  first_entry = true;
  element_to_log = i_element_to_log;
  n_elements = i_n_elements;
  nb = i_nb;
  nbf = i_nbf;
  is_signed = i_signed;
  int_eq = i_int_eq;
  if (i_print_header)
    {
      fprintf(p_file, "%s | [%d:0]", i_Header.c_str(), n_elements-1);
      if (is_signed)
        fprintf(p_file, " S");
      else
        fprintf(p_file, " U");
      fprintf(p_file, "(%d,%d)", nb, nbf);
      if (int_eq)
        fprintf(p_file, ", equivalent integer format");
      else
        fprintf(p_file, ", floating point format");
    }
  fflush(p_file);  
}

template <class T_E>
void c_vector_logger_t<T_E>::add_zero_entries(uint n_entries)
{
  for (int i = 0; i < n_entries; i++)
    {
      if (!first_entry && insert_newline)
	fprintf(p_file, "\n");
      first_entry = false;
      
      for (int i = 0; i < n_elements; i++)
	{
	  fprintf(p_file, "%d ", 0);
	}
    }
}


template <>
void c_vector_logger_t<float>::add_log_entry(float *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float scale_factor = 1 / myFXP.get_lsb_weight();
  //printf("\n\n NB = %d; NBF = %d; scale factor =%f", nb, nbf, scale_factor);
  if (!int_eq)
    scale_factor = 1;
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  if (int_eq)
    {
      for (int i = 0; i < n_elements; i++)
        {
          num2log = i_element_to_log[i];
          myFXP.apply_fxp(num2log);              
          fprintf(p_file, "%d ", (int)(num2log*scale_factor));
        }
    }
  else
    {
      for (int i = 0; i < n_elements; i++)
        {
          num2log = i_element_to_log[i];
          myFXP.apply_fxp(num2log);
          fprintf(p_file, "%f ", num2log);
        }
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<uint32_t>::add_log_entry(uint32_t *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  
  for (int i = 0; i < n_elements; i++)
    {
      fprintf(p_file, "%u ", i_element_to_log[i]);
    }
  fflush(p_file); 
  
}


template <>
void c_vector_logger_t<int32_t>::add_log_entry(int32_t *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  
  for (int i = 0; i < n_elements; i++)
    {
      fprintf(p_file, "%d ", i_element_to_log[i]);
    }
  fflush(p_file); 
  
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 1>>::add_log_entry(UME::SIMD::SIMDVec<float, 1> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[1];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  if (int_eq)
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%d ", (int)(num2log*scale_factor));
        }
    }
  else
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%f ", num2log);
        }
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 2>>::add_log_entry(UME::SIMD::SIMDVec<float, 2> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[2];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  if (int_eq)
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%d ", (int)(num2log*scale_factor));
        }
    }
  else
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%f ", num2log);
        }
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 4>>::add_log_entry(UME::SIMD::SIMDVec<float, 4> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[4];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  if (int_eq)
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%d ", (int)(num2log*scale_factor));
        }
    }
  else
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%f ", num2log);
        }
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 8>>::add_log_entry(UME::SIMD::SIMDVec<float, 8> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[8];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  if (int_eq)
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%d ", (int)(num2log*scale_factor));
        }
    }
  else
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%f ", num2log);
        }
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 16>>::add_log_entry(UME::SIMD::SIMDVec<float, 16> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[16];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  if (int_eq)
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%d ", (int)(num2log*scale_factor));
        }
    }
  else
    {
      for (int i = 0; i < n_elements; i++)
        {
          i_element_to_log[i].store(&aux_x_unpack[0]);
          num2log = aux_x_unpack[0];
          myFXP.apply_fxp(num2log);   
          fprintf(p_file, "%f ", num2log);
        }
    }
  fflush(p_file); 
}




template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 1>>::add_log_entry_single_element(UME::SIMD::SIMDVec<float, 1> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[1];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");

  if (int_eq)
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%d ", (int)(num2log*scale_factor));
    }
  else
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%f ", (num2log));
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 2>>::add_log_entry_single_element(UME::SIMD::SIMDVec<float, 2> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[2];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");

  if (int_eq)
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%d ", (int)(num2log*scale_factor));
    }
  else
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%f ", (num2log));
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 4>>::add_log_entry_single_element(UME::SIMD::SIMDVec<float, 4> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[4];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");

  if (int_eq)
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%d ", (int)(num2log*scale_factor));
    }
  else
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%f ", (num2log));
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 8>>::add_log_entry_single_element(UME::SIMD::SIMDVec<float, 8> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[8];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");

  if (int_eq)
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%d ", (int)(num2log*scale_factor));
    }
  else
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%f ", (num2log));
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<float, 16>>::add_log_entry_single_element(UME::SIMD::SIMDVec<float, 16> *i_element_to_log)
{
  c_fxp_t<1,float> myFXP(nb,nbf,is_signed,false);
  float num2log;
  float aux_x_unpack[16];
  float scale_factor = 1 / myFXP.get_lsb_weight();
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");

  if (int_eq)
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%d ", (int)(num2log*scale_factor));
    }
  else
    {
      i_element_to_log[0].store(&aux_x_unpack[0]);
      num2log = aux_x_unpack[0];
      myFXP.apply_fxp(num2log);   
      fprintf(p_file, "%f ", (num2log));
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<uint32_t>::add_log_entry_single_element(uint32_t *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  
  fprintf(p_file, "%u ", i_element_to_log[0]);
  
  fflush(p_file); 
}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<uint32_t, 1>>::add_log_entry(UME::SIMD::SIMDVec<uint32_t, 1> *i_element_to_log)
{
  uint32_t aux_x_unpack[1];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%u ", (aux_x_unpack[0]));
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<uint32_t, 2>>::add_log_entry(UME::SIMD::SIMDVec<uint32_t, 2> *i_element_to_log)
{
  uint32_t aux_x_unpack[2];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%u ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<uint32_t, 4>>::add_log_entry(UME::SIMD::SIMDVec<uint32_t, 4> *i_element_to_log)
{
  uint32_t aux_x_unpack[4];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%u ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}
template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<uint32_t, 8>>::add_log_entry(UME::SIMD::SIMDVec<uint32_t, 8> *i_element_to_log)
{
  uint32_t aux_x_unpack[8];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%u ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<uint32_t, 16>>::add_log_entry(UME::SIMD::SIMDVec<uint32_t, 16> *i_element_to_log)
{
  uint32_t aux_x_unpack[16];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%u ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}



template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<int32_t, 1>>::add_log_entry(UME::SIMD::SIMDVec<int32_t, 1> *i_element_to_log)
{
  int32_t aux_x_unpack[1];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", (aux_x_unpack[0]));
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<int32_t, 2>>::add_log_entry(UME::SIMD::SIMDVec<int32_t, 2> *i_element_to_log)
{
  int32_t aux_x_unpack[2];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<int32_t, 4>>::add_log_entry(UME::SIMD::SIMDVec<int32_t, 4> *i_element_to_log)
{
  int32_t aux_x_unpack[4];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}
template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<int32_t, 8>>::add_log_entry(UME::SIMD::SIMDVec<int32_t, 8> *i_element_to_log)
{
  int32_t aux_x_unpack[8];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVec<int32_t, 16>>::add_log_entry(UME::SIMD::SIMDVec<int32_t, 16> *i_element_to_log)
{
  int32_t aux_x_unpack[16];
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;

  for (int i = 0; i < n_elements; i++)
    {
      i_element_to_log[i].store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", (aux_x_unpack[0]));
    }
  fflush(p_file); 

}





template <>
void c_vector_logger_t<UME::SIMD::SIMDVecMask<1>>::add_log_entry(UME::SIMD::SIMDVecMask<1> *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  
  
  UME::SIMD::SIMDVec<uint32_t, 1> auxMask = 1;
  uint32_t aux_x_unpack[1];
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;
  for (int i = 0; i < n_elements; i++)
    {
      UME::SIMD::SIMDVec<uint32_t, 1> auxXConversion = 0;
      auxXConversion[i_element_to_log[i]] = auxMask;
      auxXConversion.store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", aux_x_unpack[0]);
    }
  fflush(p_file); 
}



template <>
void c_vector_logger_t<UME::SIMD::SIMDVecMask<2>>::add_log_entry(UME::SIMD::SIMDVecMask<2> *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  
  
  UME::SIMD::SIMDVec<uint32_t, 2> auxMask = 1;
  uint32_t aux_x_unpack[2];
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;
  for (int i = 0; i < n_elements; i++)
    {
      UME::SIMD::SIMDVec<uint32_t, 2> auxXConversion = 0;
      auxXConversion[i_element_to_log[i]] = auxMask;
      auxXConversion.store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", aux_x_unpack[0]);
    }
  fflush(p_file); 
}



template <>
void c_vector_logger_t<UME::SIMD::SIMDVecMask<4>>::add_log_entry(UME::SIMD::SIMDVecMask<4> *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  
  
  UME::SIMD::SIMDVec<uint32_t, 4> auxMask = 1;
  uint32_t aux_x_unpack[4];
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;
  for (int i = 0; i < n_elements; i++)
    {
      UME::SIMD::SIMDVec<uint32_t, 4> auxXConversion = 0;
      auxXConversion[i_element_to_log[i]] = auxMask;
      auxXConversion.store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", aux_x_unpack[0]);
    }
  fflush(p_file); 
}

template <>
void c_vector_logger_t<UME::SIMD::SIMDVecMask<8>>::add_log_entry(UME::SIMD::SIMDVecMask<8> *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  
  
  UME::SIMD::SIMDVec<uint32_t, 8> auxMask = 1;
  uint32_t aux_x_unpack[8];
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;
  for (int i = 0; i < n_elements; i++)
    {
      UME::SIMD::SIMDVec<uint32_t, 8> auxXConversion = 0;
      auxXConversion[i_element_to_log[i]] = auxMask;
      auxXConversion.store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", aux_x_unpack[0]);
    }
  fflush(p_file); 
}


template <>
void c_vector_logger_t<UME::SIMD::SIMDVecMask<16>>::add_log_entry(UME::SIMD::SIMDVecMask<16> *i_element_to_log)
{
  if (i_element_to_log == nullptr)
    throw std::runtime_error("c_vector_logger_t ERROR: invalid element to log\n");
  
  
  UME::SIMD::SIMDVec<uint32_t, 16> auxMask = 1;
  uint32_t aux_x_unpack[16];
  if (!first_entry && insert_newline)
    fprintf(p_file, "\n");
  first_entry = false;
  for (int i = 0; i < n_elements; i++)
    {
      UME::SIMD::SIMDVec<uint32_t, 16> auxXConversion = 0;
      auxXConversion[i_element_to_log[i]] = auxMask;
      auxXConversion.store(&aux_x_unpack[0]);
      fprintf(p_file, "%d ", aux_x_unpack[0]);
    }
  fflush(p_file); 
}

#endif
