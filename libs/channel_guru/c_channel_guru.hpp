#ifndef C_CHANNEL_GURU_H
#define C_CHANNEL_GURU_H

#include <cmath>

enum e_channel_modulation{
  OOK,
  BPSK,
  QPSK,
  PSK8,
  PSK16,
  PAM2,
  PAM4,
  PAM8,
  QAM4,
  QAM8,
  QAM8STAR,
  QAM16,
  QAM32,
  QAM64,
  QAM128,
  QAM256};

enum e_channel_snr_type{
  SNRb,
  SNRs,
  OSNR};

class c_channel_guru
{
 private:
  e_channel_modulation modulation;
  unsigned int         n_bits_per_symbol;
  float                baudrate_Gbps;
  float                SNR_b_dB;
  float                SNR_s_dB;
  float                OSNR_dB;
  unsigned int         n_polarizations;
  float                signal_power;
  float                sigma;
  bool                 valid;
 public:
  c_channel_guru()
  {
    modulation = OOK;
    n_bits_per_symbol = 0;
    baudrate_Gbps = 0;
    SNR_b_dB = 0;
    SNR_s_dB = 0;
    OSNR_dB = 0;
    n_polarizations = 0;
    signal_power = 0;
    sigma = 0;
    valid = false;
  }
  bool initialize(e_channel_modulation i_mod,
		  float i_baudrate_Gbps,
		  unsigned int i_n_polarizations,
		  e_channel_snr_type i_SNR_type = SNRs,
		  float i_SNR = 10.0f)
  {
    if ((n_polarizations != 1) && (n_polarizations != 0))
      return false;
    baudrate_Gbps = i_baudrate_Gbps;
    modulation = i_mod;
    n_polarizations = i_n_polarizations;

    switch (modulation)
      {
      case OOK:
	n_bits_per_symbol = 1;
	signal_power = 0.5f/2.0f;
	break;
	//PSK modulation types
      case BPSK:
	n_bits_per_symbol = 1;
	signal_power = 1.0f/4.0f;
	break;     
      case QPSK:
	n_bits_per_symbol = 2; 
	signal_power = 2.0f/4.0f; 
	break;      
      case PSK8:
	n_bits_per_symbol = 3;
	signal_power = 4.0f/8.0f;
	break;
      case PSK16:
	n_bits_per_symbol = 4;
	signal_power = 8.0f/16.0f;
	break;
	//PAM modulation types
      case PAM2:
	n_bits_per_symbol = 1;
	signal_power = 0.5f/2.0f;
	break;
      case PAM4:
	n_bits_per_symbol = 2;
	signal_power = 1.25f/4.0f;
	break;
      case PAM8:
	n_bits_per_symbol = 3;
	signal_power = 2.625f/8.0f;
	break;
	//QAM modulation types
      case QAM4:
	n_bits_per_symbol = 2;
	signal_power = 2.0f/4.0f;
	break;
      case QAM8:
	n_bits_per_symbol = 3;
	signal_power = 5.0f/8.0f; //was 10/16
	break;
      case QAM8STAR:
	n_bits_per_symbol = 3;
	signal_power = 0.5505f; //(1;0) and (1.366;1.366) normalized to (1.366+sqrt(2))
	break;
      case QAM16:
	n_bits_per_symbol = 4;
	signal_power = 10.0f/16.0f;
	break;
      case QAM32:
	n_bits_per_symbol = 5;
	signal_power = 17.77777777f/32.0f;
	break;
      case QAM64:
	n_bits_per_symbol = 6;
	signal_power = 42.0f/64.0f;
	break;
      case QAM256:
	n_bits_per_symbol = 8;
	signal_power = 170.0f/256.0f;
	break;
      case QAM128:
        throw std::runtime_error("\nc_channel_guru ERROR: QAM128 not supported yet");
	break;
      }


    set_SNR(i_SNR_type, i_SNR);
    valid = true;
    return valid;
  }
  bool set_SNR(	  e_channel_snr_type i_SNR_type,
		  float i_SNR)
  {
    switch (i_SNR_type)
      {
      case SNRb:
	SNR_b_dB = i_SNR;
	OSNR_dB = SNR_b_dB - 10 * log10 (12.5 * n_polarizations / baudrate_Gbps);
	SNR_s_dB = SNR_b_dB + 10 * log10(n_bits_per_symbol);
	break;
      case SNRs:
	SNR_s_dB = i_SNR;
	SNR_b_dB = SNR_s_dB - 10 * log10(n_bits_per_symbol);
	OSNR_dB = SNR_b_dB - 10 * log10 (12.5 * n_polarizations / baudrate_Gbps);
	break;
      case OSNR:
	OSNR_dB = i_SNR;
	SNR_b_dB = OSNR_dB + 10 * log10 (12.5 * n_polarizations / baudrate_Gbps);
	SNR_s_dB = SNR_b_dB + 10 * log10(n_bits_per_symbol);
      }
    sigma = sqrt((signal_power/2) / (pow(10, SNR_s_dB/10)));
    return true;
  }

  e_channel_modulation get_modulation()           {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return modulation;}
  unsigned int        get_n_bits_per_symbol()     {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return n_bits_per_symbol;}
  float               get_baudrate_Gbps()         {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return baudrate_Gbps;}
  float               get_SNR_b_dB()              {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return SNR_b_dB;}
  float               get_SNR_s_dB()              {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return SNR_s_dB;}
  float               get_OSNR_dB()               {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return OSNR_dB;}
  unsigned int        get_n_polarizations()       {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return n_polarizations;}
  float               get_signal_power()          {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return signal_power;}
  float               get_sigma()                 {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return sigma;}
  bool                get_valid()                 {if(!valid){throw std::runtime_error("c_channel_guru ERROR: invalid initialization\n");}return valid;}
  
  
};


#endif
